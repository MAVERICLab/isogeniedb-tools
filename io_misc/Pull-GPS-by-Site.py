#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import pandas as pd
from datetime import datetime
#import simplekml
#import osr
#from osgeo import ogr
from py2neo import Graph
from py2neo.packages.httpstream import http
from pprint import pprint
http.socket_timeout = 9999

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import *

"""
This script gets all the core GPSs and outputs them to a CSV.
It just uses whatever value for GPS__ is already in the Core node, and doesn't actually modify the DB.
"""

# GDAL_DATA=//anaconda/envs/maps/share/gdal


loc = sys.argv[1]
#loc = 'test'

if not any(loc in args for args in ['local', 'test', 'db']):
    error('Please use one of the keywords to define WHERE the script is being run')

# Connect to DB, get info
graph_db, root_dir = load_config(config_file_1, loc)  # config_file (path) defined in local_file_paths

# Spatial Reference System
inputEPSG = 4326
outputEPSG = 900913


def parse_DMS(input_ref, output_ref, dms):
    """
    Convert degree-minute-seconds to signed decimal degrees.
    
    https://github.com/mlaloux/My-Python-GIS_StackExchange-answers/blob/master/OGR%20Projection%20transformation%20error.md
    """
    # Many of the special cases should no longer be necessary, as the GPS is better standardized while importing.
    # Add options for S and W where N and E are used.

    try:
        lat_txt, lon_txt = dms.split(',')
    except ValueError: 
        error('Could not parse GPS into lat/lon: {}'.format(dms))

    if len(lat_txt.split()) == 4:  # It's still DMS

        if lat_txt[-1] in ['N', 'S']:
            lat_deg, lat_min, lat_sec, lat_dir = lat_txt.split()
        else:
            lat_dir, lat_deg, lat_min, lat_sec = lat_txt.split()

        if lon_txt[-1] in ['E', 'W']:
            lon_deg, lon_min, lon_sec, lon_dir = lon_txt.split()
        else:
            lon_dir, lon_deg, lon_min, lon_sec = lon_txt.split()

        lat_dd = int(lat_deg) + (float(lat_min) / 60) + (float(lat_sec) / 3600)
        lon_dd = int(lon_deg) + (float(lon_min) / 60) + (float(lon_sec) / 3600)

    if len(lat_txt.split()) == 3:

        if lat_txt[-1] in ['N', 'S']:
            lat_deg, lat_min, lat_dir = lat_txt.split()
        else:
            lat_dir, lat_deg, lat_min = lat_txt.split()

        if lon_txt[-1] in ['E', 'W']:
            lon_deg, lon_min, lon_dir = lon_txt.split()
        else:
            lon_dir, lon_deg, lon_min = lon_txt.split()

        lat_dd = int(lat_deg) + (float(lat_min) / 60)
        lon_dd = int(lon_deg) + (float(lon_min) / 60)

    if lat_dir == ('S' or 'W'):
        lat_dd = lat_dd * -1

    if lon_dir == ('S' or 'W'):
        lon_dd = lon_dd * -1

    return lat_dd, lon_dd

# Get all cores with GPS and put into dataframe
query = "MATCH (n:Core) WHERE EXISTS(n.`GPS__`) RETURN n"
df = gather_df(query, graph_db)

# Remove junk columns
for rm_col in ['CreationDate__', 'image_href__']:
    if rm_col in df.columns:
        df.drop(rm_col, axis='columns', inplace=True)
df.dropna(axis='columns', how='all', inplace=True)  # Try to reduce further

df.drop(['CoreID', 'SiteID'], axis='columns', inplace=True)

df['Year'] = df['CoreDated__'].str.split('|').str[1].str[0:4]

df.sort_values(['Site__', 'Year'], inplace=True)

df.to_csv(os.path.join(output_save_dir, 'Persson-GPS_Sites.csv'), sep=',', quotechar='"', index=False)  # output_save_dir from local_file_paths

#exit()
#
#for siteName, siteGroup_df in df.groupby('Site__'):  # By Sites
#
#    for year, yearGroup_df in siteGroup_df.groupby('Year'):
#
#        gps_list = yearGroup_df['GPS__'].tolist()
#
#        elements = ogr.Geometry(ogr.wkbLinearRing)
#
#        for coord in gps_list:
#
#            lat, lon = parse_DMS(inputEPSG, outputEPSG, coord)
#
#            elements.AddPoint(lat, lon)
#
#        poly = ogr.Geometry(ogr.wkbPolygon)
#
#        poly.AddGeometry(elements)
#
#        centroid = poly.Centroid()
#
#        print(centroid)