#!/usr/bin/env python
"""
This script is outdated and seems to be no longer needed, but keeping for now until sure.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
from py2neo import Graph
import numpy as np
import pandas as pd
import seaborn as sns
import sys

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import *


# Connect to DB, get info
graph_db, root_dir = load_config(config_file_2, 'db')  # config_file (path) defined in local_file_paths


normal_wtd_query = "MATCH (n) WHERE EXISTS(n.`Water Table Depth (WTD, cm, neg = below peat surface)`) RETURN n"
normal_wtd_df = gather_df(normal_wtd_query, graph_db)
normal_wtd_df['WTD'] = normal_wtd_df['Water Table Depth (WTD, cm, neg = below peat surface)']

reverse_wtd_query = "MATCH (n) WHERE EXISTS(n.`Water Table Depth (WTD, cm, neg = water above ground)`) RETURN n"
reverse_wtd_df = gather_df(reverse_wtd_query, graph_db)

for index, series in reverse_wtd_df.iterrows():
    wtd = series['Water Table Depth (WTD, cm, neg = water above ground)']
    wtd = wtd.strip('?')

    try:
        wtd = int(wtd)
        reverse_wtd_df.loc[index, 'WTD'] = -1 * int(wtd)
    except ValueError:
        reverse_wtd_df.loc[index, 'WTD'] = np.nan

wtd_df = pd.concat([normal_wtd_df, reverse_wtd_df])

note_columns = [column for column in wtd_df.columns if 'Note' in str(column)]

note_columns.extend(['Coring Day Weather Conditions', 'Core Length (cm)', 'Conductivity (uS)', 'Coring End Time',
                     'Coring Start Time', 'Data Transcriber', 'Distance to autochamber (and which autochamber)',
                     'East', 'Field Team', 'GPS Logger', 'North', 'O2 (%)', 'Samples Taken', 'DataID', 'Core',
                     'Redox Potential (ORP) (uncalibrated; relative values only)', 'Temp Meas. Date', 'Air Temp (C)',
                     'Time (coring begun)', 'Time (porewater begun)', 'Water Table Depth Meas. Date', 'Pictures',
                     'Core Depth (cm, below peat/soil surface)', 'Date for Porewater, ALD', 'Soil Temp (C)', 'pH',
                     'Depth of Porewater Sample (cm below peat/soil surface, +/- 1.5 cm)', 'Soil Sampling Date',
                     'Coring Time', 'Depth__', 'Porewater pH (n/a = sample was pore gas)', 'peat pH', 'DepthID',
                     'Temp 13 cm (C, below surface)', 'Temp 3 cm (C, below surface)', 'Depth', 'Core__'])

# Drop
wtd_df.drop(note_columns, axis='columns', inplace=True)  # [some] junk columns
wtd_df = wtd_df[pd.notnull(wtd_df.WTD)]  # WTD that aren't valid
wtd_df.drop_duplicates(subset='CoreID', keep='first', inplace=True)  # Duplicate rows (i.e. depths)
wtd_df = wtd_df[wtd_df['Habitat__'].isin(['Bog', 'Palsa', 'Fen'])]  # WTD that aren't valid

# Convert WTD column to numbers
wtd_df['WTD'] = pd.to_numeric(wtd_df['WTD'], errors='coerce')
wtd_df['DateTime'] = pd.to_datetime(wtd_df['Date__'], format="%Y%m%d")

# Sort by date
wtd_df.sort_values('DateTime', inplace=True)
wtd_df.reset_index(drop=True)

# Filter to only include July/Aug Years
wtd_df['Month'] = wtd_df['DateTime'].apply(lambda x: x.strftime('%B'))
wtd_df['Year'] = wtd_df['DateTime'].apply(lambda x: x.strftime('%Y'))
wtd_df['Day'] = wtd_df['DateTime'].apply(lambda x: x.strftime('%d'))

# wtd_df = wtd_df[wtd_df['Year'].isin(['2014'])]

wtd_df.loc[wtd_df['Habitat__'] == 'Palsa', 'Average'] = wtd_df[wtd_df['Habitat__'] == 'Palsa']['WTD'].mean()
wtd_df.loc[wtd_df['Habitat__'] == 'Bog', 'Average'] = wtd_df[wtd_df['Habitat__'] == 'Bog']['WTD'].mean()
wtd_df.loc[wtd_df['Habitat__'] == 'Fen', 'Average'] = wtd_df[wtd_df['Habitat__'] == 'Fen']['WTD'].mean()

import matplotlib.pyplot as plt

flatui = ["#703C1B", "#058000", "#0001FF"]

ax = sns.swarmplot(x='DateTime', y='WTD', hue='Habitat__', hue_order=['Palsa', 'Bog', 'Fen'], data=wtd_df,
                 palette=sns.color_palette(flatui), linewidth=0.5)

new_labels = []
for label in ax.get_xticklabels():
    label.set_rotation(45)
    # new_labels.append(label.get_text().split(' ')[0])
    new_labels.append(label.get_text().split('T00')[0])

ax.set_xticklabels(new_labels)

ax.set_title('')
# fig.suptitle('WTD Bog', fontsize=12, fontweight='bold')
ax.set_ylabel('WTD')
ax.set_xlabel('Date')

plt.tight_layout()

plt.savefig(os.path.join(output_save_dir, 'Kelsey_All-20xx-WTD.png'), dpi=1200, figsize=(7, 6)) # output_save_dir from local_file_paths

wtd_df.to_csv(os.path.join(output_save_dir, 'Kelsey_All-20xx-WTD.csv')) # output_save_dir from local_file_paths

# Bog, Fen - Week of, Month of - Overall trend

