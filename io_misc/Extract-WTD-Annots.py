#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
from datetime import datetime
from py2neo import Graph
import numpy as np
import pandas as pd
from pprint import pprint

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import *


# Connect to DB, get info
graph_db, root_dir = load_config(config_file_1, 'db') # config_file (path) defined in local_file_paths


biogeochem_query = "MATCH (n)-[:HAS_BIOGEOCHEMISTRY*]->(x) RETURN x"
biogeochem_df = gather_df(biogeochem_query, graph_db)

# Drops
note_columns = [column for column in biogeochem_df.columns if 'notes' in str(column)]
biogeochem_df.drop(note_columns, axis='columns', inplace=True)  # [some] junk columns
biogeochem_df.dropna(axis='columns', how='all', inplace=True)  # Try to reduce further

for coreID in biogeochem_df['CoreID'].unique():
    siteID_query = "MATCH (n:Core {{CoreID:'{}'}})-[*]-(s:Site) RETURN s".format(coreID)

    site_df = gather_df(siteID_query, graph_db)
    siteID = site_df['SiteID'].values[0]
    Site__ = site_df['Site__'].values[0]

    sys.stdout.write("\rProcessing {}: {}".format(coreID, siteID))

    biogeochem_df.loc[biogeochem_df['CoreID'] == coreID, 'SiteID'] = siteID
    biogeochem_df.loc[biogeochem_df['CoreID'] == coreID, 'Site__'] = Site__

biogeochem_df = biogeochem_df[biogeochem_df['Site'].isin(['P', 'S', 'E'])]

ftir_columns = [column for column in biogeochem_df.columns if 'FTIR' in str(column)]
eem_columns = [column for column in biogeochem_df.columns if 'eem' in str(column)]
fricrms_columns = [column for column in biogeochem_df.columns if 'fticrms' in str(column)]

err_columns = ['spectral_slope_ratio', 'samples_taken_from_sampling_spreadsheet', 'Date_T', 'Date_WTD', 'Date_core',
                'Date_pg', 'Date_pw', 'Depth.cm', 'Depth_Code', 'GPS', 'Name_s', 'Name_unique']
err2_columns = ['{}_err'.format(column) for column in err_columns]

final_columns = ['DataID', 'DepthID']

drop_columns = ftir_columns + eem_columns + fricrms_columns + err_columns + err2_columns + final_columns

biogeochem_df.drop(drop_columns, axis='columns', inplace=True)  # [some] junk columns

ans_data = []
for date in biogeochem_df['Date__'].unique().astype('str'):
    date_fmt = datetime.strptime(date, '%Y%m%d').strftime('%Y-%m-%d')
    date_ans_query = "MATCH (n:`ANS-Hour` {{Date__:'{}'}}) RETURN n".format(date_fmt)
    date_ans_query_df = gather_df(date_ans_query, graph_db)

    ans_data.append(date_ans_query_df)

ans_df = pd.concat(ans_data)

biogeochem_df.to_csv(os.path.join(output_save_dir, 'Wehr-biogeochem.csv')) # output_save_dir from local_file_paths
ans_df.to_csv(os.path.join(output_save_dir, 'Wehr-ans.csv')) # output_save_dir from local_file_paths
