#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
import sys
import pandas as pd
from py2neo import Graph
from py2neo.packages.httpstream import http
from pprint import pprint
http.socket_timeout = 9999

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import *

# GDAL_DATA=//anaconda/envs/maps/share/gdal

# Connect to DB, get info
graph_db, root_dir = load_config(config_file_1, 'db') # config_file (path) defined in local_file_paths

node_labels = graph_db.node_labels


def IntTest(str):
    try:
        int(str)
        return True
    except ValueError:
        return False

labels_to_remove = [node for node in node_labels if IntTest(node)]

for labels in labels_to_remove:
    label_nodes = graph_db.find(labels)

    for label_node in label_nodes:
        label_node.remove_label(labels)
        label_node.push()

exit()
sites_to_remove = ['AJ-Erio', 'AJ-Sphag', 'Inc-S']  # 'Inc-E',

nodes = set()
relationships = set()

# query = "MATCH (n:Site {Site__: '{0}'})-[r*]->() DETACH DELETE n".format(sites)
query = "MATCH (n {SiteID: 'b96a0d7e-3e75-4db5-bd4b-d36ac35882c1'}) RETURN n"  #.format(sites)

result = graph_db.run(query).data()

for result_dict in result:

    for node_n, node in result_dict.items():

        # print(node.relationships())
        for rel in node.match():
            relationships.update([rel])

        nodes.update([node])

[graph_db.separate(rel) for rel in relationships]
[graph_db.delete(node) for node in nodes]