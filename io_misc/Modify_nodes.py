#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
#import simplekml
#import osr
import pandas as pd
from datetime import datetime
import neobolt
from pprint import pprint

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import *
from tools.imports import nodes_new as nodes
from tools.utilities.validate_filepaths import validate_filepaths
# TODO: Make the ENTIRE thing use the neo4j driver (run_correct_gps_from_chem still uses py2neo, although it doesn't
#  use anything from nodes_old).

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

# Putting the DB or session as an argument for each function so that they can be run from other scripts.

def run_add_missing_sample_metadata_from_soildepths(session, keys):
    # Gets pre-defined properties ('keys' argument) from each Soil-Depth node, and applies these properties to all
    #  other nodes with the same DepthID that don't have those properties.
    # This is only necessary for updating nodes that were created prior to the addition of new properties to Soil-Depth
    #  nodes, as nodes.Vertex.get_upstreams() should take care of these properties for any newer nodes.
    # TODO: Add an optional (default FALSE) argument to *overwrite* properties that already exist.
    # TODO: Make this more generic, for updating sample metadata from any other node type (Core, etc.)
    #  (see set_property_by_id() in common_vars, which already partially accomplishes this)

    def get_depthIDs(tx):
        # Gets list of depthIDs for all Soil-Depth nodes in the DB.
        result = tx.run("MATCH (d:`Soil-Depth`) RETURN d.DepthID AS DepthID")
        return [record['DepthID'] for record in result]

    def get_soildepth_properties(tx, depthID, keys):
        # Gets pre-defined properties for the Soil-Depth node with a given DepthID.
        result = tx.run("MATCH (d:`Soil-Depth` {DepthID: $depthID}) RETURN d", depthID=depthID)

        nodes = [record['d'] for record in result]
        if len(nodes) > 1:
            error('More than one node with DepthID {}'.format(depthID))
        elif len(nodes) == 0:
            error('No nodes found with DepthID {}, even though this DepthID was previously found.'.format(depthID))
        else:
            result_props = nodes[0]._properties
            props = {key: result_props[key] for key in keys if key in result_props.keys()}
            return props

    def set_property(tx, depthID, key, value):
        # Sets a key:value property for all nodes with a given DepthID that don't have that property.
        updatedate = datetime.now().strftime('%Y%m%d')

        result = tx.run("MATCH (n {DepthID: $depthID}) WHERE NOT(EXISTS(n.`%s`)) SET n.`%s` = $value, n.UpdateDate__ = $updatedate RETURN n" % (key, key),
                        depthID=depthID, value=value, updatedate=updatedate)
        count = 0
        for record in result:
            count += 1

        if count > 0:
            print('Updated {} = {} on {} nodes with DepthID = {}'.format(key, value, count, depthID))

    depthIDs = session.read_transaction(get_depthIDs)
    print('{} total DepthIDs found'.format(len(depthIDs)))

    for depthID in depthIDs:
        properties = session.read_transaction(get_soildepth_properties, depthID, keys)

        for key, value in properties.items():
            session.write_transaction(set_property, depthID, key, value)


def run_update_metadata(session, file_list_arg):
    # 7/15/22: Added the ability to pass an existing dataframe for the file list instead of a CSV filepath.
    #  This is useful for subsetting a file list CSV into files with metadata only, vs. those with full import scripts,
    #  and then running this function on the metadata-only files from build_db.py or other scripts.
    #  Which method is used depends on the type for file_list_arg (string or dataframe).

    if type(file_list_arg) == str:
        print('Updating Metadata nodes from list: {}'.format(file_list_arg))
        file_list = pd.read_csv(file_list_arg, sep=',', quotechar='"', na_filter=False)

    elif type(file_list_arg) == pd.core.frame.DataFrame:
        print('Updating Metadata nodes from dataframe.')
        file_list = file_list_arg

    else:
        error('Invalid file_list_arg: {}'.format(file_list_arg))
    
    for index, row in file_list.iterrows():

        print('Updating Metadata: {}'.format(row['DatasetID']))

        # determine whether this is supposed to be a "metadata only" node, i.e. not fully imported, for suppressing warning about update_existing_data
        full_data_imported = row['_imported']
        if type(full_data_imported) != bool:  # just in case
            if full_data_imported == "False":  # assume it's a string
                full_data_imported = False
            else:
                full_data_imported = True  # if it's anything else, assume True just in case
        metadata_only = not(full_data_imported)
        
        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(row)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }
        
        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=False, assume_metadata_only=metadata_only)
        
        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        additional_labels = [label for label in additional_labels if label not in ['', 'NaN']]  # remove empty strings created when one of the columns above is empty

        try:
            metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                           labels=additional_labels)
        except neobolt.exceptions.ClientError:
            print('ERROR: Invalid additional_labels:')
            print(additional_labels)
            exit()

def run_correct_gps_from_chem(db):
    # 1/12/2019: This seemed to be taking WAY too long to run, so further constrained the 3 queries to look only for
    # nodes in their expected locations, and added print statements to see where it's hanging up.
    
    print('\n\nUpdating unknown and incorrect GPS values based on Biogeochemistry nodes:')
    
    # Corrects core GPS using chemistry GPS, for known core GPS mistakes, based on a set of criteria.
    
    # Use solid geochemistry if it exists for the core, otherwise use any geochemistry. Thus if one
    # "core" has two GPSs (one for the actual core and one for porewater), the actual core is used.
    
    # Generate queries
    query_cores_with_chem = 'MATCH (c:Core)-[*2]->(b:Biogeochemistry) RETURN DISTINCT c'
    query_chem_solid = 'MATCH (d:`Soil-Depth`)-[:HAS_SOLID_BIOGEOCHEMISTRY]->(b:Biogeochemistry) RETURN DISTINCT b'
    query_chem_any = 'MATCH (d:`Soil-Depth`)-->(b:Biogeochemistry) RETURN DISTINCT b'
    
    # Run queries
    print("Querying for cores with biogeochemistry...")
    cores_df = gather_df(query_cores_with_chem, db)
    print("Querying for solid biochemistry data...")
    chem_solid_df = gather_df(query_chem_solid, db)
    print("Querying for ANY biogoechemistry data...")
    chem_any_df = gather_df(query_chem_any, db)

    print("Crunching dataframes...")

    # Keep only the columns with relevant metadata
    metadata_cols = ['Area__', 'Site__', 'SiteID', 'Core__', 'CoreID', 'FieldSampling__', 'CoreDated__']
    cores_df = cores_df[metadata_cols + ['GPS__']]
    chem_solid_df = chem_solid_df[metadata_cols + ['GPS__', 'DepthID', 'DataID']]
    chem_any_df = chem_any_df[metadata_cols + ['GPS__', 'DepthID', 'DataID']]
    
    # Add a 'ChemType' column
    chem_solid_df['ChemType'] = 'solid'
    chem_any_df['ChemType'] = 'any'
    
    # ----------------------------------------------------------------------------------------------
    
    # Now we want to merge ONLY the rows from chem_any_df with sets of metadata_cols that don't exist in chem_solid_df.
    
    chem_df = pd.merge(left=chem_solid_df, right=chem_any_df, on=metadata_cols, how='outer', indicator=True)
    
    # chem_df is actually more complicated than what we want, so we need to clean it up
    
    # Columns with _x are from chem_solid_df, columns with _y are from chem_any_df
    differing_cols = [col for col in chem_df if col.endswith('_x')]
    
    # First fill in the empty _x columns (empty if the row is from the right_only) with the values from _y columns
    for index, row in chem_df.iterrows():
        if row['_merge'] == 'right_only':
            for col in differing_cols:
                chem_df.loc[index, col] = row[col.replace('_x','_y')]
                
    # Next delete the '_y' columns and rename the '_x' columns to get rid of the '_x'
    keep_cols = [col for col in chem_df.columns if ('_y' not in col) and col != '_merge']
    chem_df = chem_df[keep_cols]
    for col in keep_cols:
        if col.endswith('_x'):
            chem_df.rename(columns={ col : col[0:-2] }, inplace=True)
            
    # Finally, delete the many duplicate rows that were generated during the merging process
    chem_df.drop_duplicates(keep='first', inplace=True)
    # and reset the indices to count up from 0
    chem_df.reset_index(drop=True, inplace=True)
                
#    chem_df.to_csv(os.path.join(output_save_dir, ('chem_' + str(datetime.datetime.now()) + '.csv')))
#    cores_df.to_csv(os.path.join(output_save_dir, ('cores_' + str(datetime.datetime.now()) + '.csv')))

    print("Checking integrity of cleaned dataframes...")
    
    # At this point, the unique CoreIDs in chem_df should be the same as those in cores_df.
    # Verify that this is indeed the case:
    if set(chem_df['CoreID']) != set(cores_df['CoreID']):
        error('Unique CoreIDs from Biogeochemistry nodes do not match CoreIDs from Core nodes.')

    # Also verify that each unique CoreID in chem_df has only one GPS:
    for coreID, unique_core_df in chem_df.groupby(by='CoreID'):
        if len(unique_core_df['GPS__'].unique()) != 1:
            error('More than one unique GPS__ identified for the same Core {} in chemistry data:\n'
                  '{}'.format(coreID, unique_core_df['GPS__'].unique()))
            
    # And do the same for cores_df, which also should not have duplicate CoreIDs, so check for this:
    if len(cores_df['CoreID']) != len(set(cores_df['CoreID'])):
        error('More than one unique CoreID in the Core nodes.')
        
    # ----------------------------------------------------------------------------------------------
    
    # If everything checks out OK, then we can start comparing the GPS__ of each core.
    
    # This modifies only the Core and *-Depth nodes, not the Data nodes with relationship HAS_DEPTH_INFO,
    #  because we want to retain a copy of the original coring sheet GPS__ in the DEPTH_INFO.
    def fix_core_gps(db, coreID, gps):
        
        # CORES -------------
        coreNodes = check_names(graph=db, label='Core', key='CoreID', id=coreID)
        
        if not coreNodes:
            # if there are no core nodes with that ID, something is wrong
            error('Could not find any Core nodes with CoreID={}'.format(coreID))
            
        count = 0
        for core_node in coreNodes:
            # should only execute once; use a counter because can't use len(coreNodes)
            count += 1
            if count > 1:
                error('More than one Core node with the same CoreID: {}'.format(coreID))
                
            core_node['GPS__'] = gps
            core_node['UpdateDate__'] = datetime.now().strftime('%Y%m%d')
            core_node.push()
            
        
        # DEPTHS -------------
        depthNodes = check_names(graph=db, label='Soil-Depth', key='CoreID', id=coreID)
        
        if not depthNodes:
            # Before giving up, try label = 'Core-Depth'
            depthNodes = check_names(graph=db, label='Core-Depth', key='CoreID', id=coreID)
            
        if not depthNodes:
            # if there are STILL not any depth nodes, something is wrong
            error('Could not find any Soil-Depth or Core-Depth nodes with CoreID={}'.format(coreID))
            
        # Unlike cores, there can be multiple depth nodes with the same CoreID
        for depth_node in depthNodes:                
            depth_node['GPS__'] = gps
            depth_node['UpdateDate__'] = datetime.now().strftime('%Y%m%d')
            depth_node.push()


    print("Making GPS comparison dataframe...")
 
    # make a combined dataframe to compare coring and chem GPS in each core
    comparison_df = chem_df[metadata_cols + ['GPS__']]
    comparison_df.rename(columns={'GPS__':'GPS_chemistry'}, inplace=True) # rename GPS__ to GPS_chemistry
    
    # Drop duplicates and reset indices to count up from 0.
    # Already checked that there's only one GPS__ from each source for each CoreID, so only need to use the first row
    comparison_df.drop_duplicates(keep='first', inplace=True)
    comparison_df.reset_index(drop=True, inplace=True)
    
    if len(comparison_df['CoreID']) != len(cores_df['CoreID']):
        error('Unequal numbers of rows in comparison_df ({}) vs. cores_df ({}).'.format(len(comparison_df['CoreID']), len(cores_df['CoreID'])))

    # make a new column for coring sheet GPS
    gps_coring_dict = cores_df.set_index('CoreID')['GPS__'].to_dict()
    comparison_df['GPS_coring'] = comparison_df['CoreID'].map(gps_coring_dict)

    print("Performing comparisons...")
    
    # compare each set of GPS's, checking for inconsistencies and then correcting them in the DB
    for index, row in comparison_df.iterrows():
        if row['GPS_coring'] != row['GPS_chemistry']:
            print('\n')
            print('Site__:             {}'.format(row['Site__']))
            print('CoreDated__:        {}'.format(row['CoreDated__']))
            print('CoreID:             {}'.format(row['CoreID']))
            print('Coring sheet GPS__: {}'.format(row['GPS_coring']))
            print('Chemistry GPS__:    {}'.format(row['GPS_chemistry']))
            
            if 'nan' in row['GPS_coring'].lower():
                fix_core_gps(db=db, coreID=row['CoreID'], gps=row['GPS_chemistry'])
                print("Updated previously-unknown coring GPS based on biogeochemistry data.")
                
            # if it's not just replacing an NaN, replace it only if it's a known discrepancy
            elif row['Site__'] == 'Fen2' and row['CoreDated__'] == '1|2011-06':  
                fix_core_gps(db=db, coreID=row['CoreID'], gps=row['GPS_chemistry'])
                print("Fixed the incorrect coring GPS from Fen2 2011-06 based on biogeochemistry data.")
                
            elif 'nan' in row['GPS_chemistry'].lower():
                warning("Missing GPS for chemistry, but not coring data. Usually this is automatically "
                        "filled in during import, but it has since been changed back to NaN. "
                        "Leaving as-is as this may have been intentional.")
            
            else:
                error("Could not reconcile inconsistent GPS__ between coring and chemistry data.")
    
    
## OUTDATED FUNCTIONS   
#
#def run_modify_gps(db):
#    # Converts degree-minute-second to degree-decimalminutes.
#    # Should no longer be necessary (at least for ANS) since the format was changed prior to re-importing.
#
#    dataNodes = [data for data in db.find('ANS')]
#
#    for node in dataNodes:
#
#        # N 68 21 13.3482, E 18 48 55.785
#        # N 68 21.137, E 19 02.814
#
#        if node['GPS']:
#
#            dms = node['GPS']
#
#            dms_lat, dms_lon = dms.split(',')
#
#            dir_lat, deg_lat, min_lat, sec_lat = dms_lat.split()
#            dir_lon, deg_lon, min_lon, sec_lon = dms_lon.split()
#
#            dd_lat = float(deg_lat) + float(min_lat)/60 + float(sec_lat)/(60*60)
#            dd_lon = float(deg_lon) + float(min_lon)/60 + float(sec_lon)/(60*60)
#
#            dd_min_lat = float(min_lat) + (float(sec_lat) / 60)
#            dd_min_lon = float(min_lon) + (float(sec_lon) / 60)
#
#            fmt_gps = '{0} {1} {2:.6f}, {3} {4} {5:.6f}'.format(dir_lat, deg_lat, dd_min_lat, dir_lon, deg_lon, dd_min_lon)
#
#            node['_GPS'] = fmt_gps
#
#            node.push()
#
#
#def run_modify_anvi(db):
#    # Also no longer necessary, as the Anvi misspelling has been corrected using Standardize-Coring-Sheets.
#
#    dataNodes = [node for node in db.find('Site')]
#
#    for node in dataNodes:
#
#        if 'Anvi' in node['Site__']:
#
#            node['Site__'] = node['Site__'].replace('Anvi', 'Avni')
#
#            node.push()

# For running just one function directly from this script
if __name__ == '__main__':
    # GDAL_DATA=//anaconda/envs/maps/share/gdal
    
    # Connect to DB, get info
    loc = sys.argv[1]
    function = sys.argv[2]  # name of function to run
    
    if not any(loc in args for args in ['local', 'test', 'db']):
        error('Please use one of the keywords to define WHERE the script is being run')
    
    # Connect to DB, get info
    driver, root_dir = load_config_driver(config_file_1, loc)
    
    if function == 'run_correct_gps_from_chem':
        # This function uses outdated py2neo code, so those imports are put in here to isolate them.
        from py2neo import Graph
        from py2neo.packages.httpstream import http
        http.socket_timeout = 9999

        graph_db, root_dir = load_config(config_file_1, loc)  # config_file (path) defined in local_file_paths

        run_correct_gps_from_chem(db=graph_db)

    elif function == 'run_add_missing_sample_metadata_from_soildepths':

        # EDIT THE LINE BELOW TO CHANGE WHICH KEYS WILL BE UPDATED.
        # TODO: Set keys when running instead. Need to make sure this can easily be done with command-line syntax.
        keys = ['DepthAvg__']

        with driver.session() as session:
            run_add_missing_sample_metadata_from_soildepths(session=session, keys=keys)
    
    elif function == 'run_update_metadata':
        
        # Get correct file list filename
        if loc in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
            
        file_list_filepath = os.path.join(root_dir, file_list_filename)

        # Validate filepaths in list
        print('Validating file paths:')
        validate_filepaths(file_list=file_list_filepath, db=loc)

        with driver.session() as session:
            run_update_metadata(session=session, file_list_arg=file_list_filepath)
        
    else:
        error('Please specify a valid function name to be run from this script. Valid options:\n'
              'run_correct_gps_from_chem\n'
              'run_update_metadata')
