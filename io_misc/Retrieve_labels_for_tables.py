#!/usr/bin/env python
"""
Purpose: Generate cached query results for "Queries" pages of web portals.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import sys
import os
import json
import re
#from py2neo.packages.httpstream import http
#http.socket_timeout = 9999
import pandas as pd
import numpy as np
import argparse
from collections import OrderedDict

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import load_config_driver, error

parser = argparse.ArgumentParser(description="Generate cached queries")
inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database from which to retrieve cached queries.")

inputs.add_argument('-u', '--updatestart', dest='update_start_date', default=False,
                    help="Earliest CreationDate__ or UpdateDate__ (format: YYYYMMDD) of nodes that should be queried. "
                         "If specified, then only those nodes with a CreationDate__ or UpdateDate__ on or later than "
                         "that date will be queried. This option provides a straightforward way of updating only those "
                         "cached queries which actually include new (meta)data.")

inputs.add_argument('-l', '--labels', dest='specific_labels', default=False, nargs='+',
                    help="Specific node labels to query. "
                         "If specified, then only the nodes with ALL those labels will be queried, plus any labels "
                         "that overlap with that combination of labels (i.e., labels that share at least some of the "
                         "same nodes as the specified labels). 'Metadata' nodes are always queried regardless."
                         "If unspecified, then ALL labels will be queried.")

args = parser.parse_args()

# # Connect to DB, get info
# loc = sys.argv[1]
#
# if not any(loc in args for args in ['local', 'test', 'db']):
#     error('Please use one of the keywords to define WHERE the script is being run')

access_labels = ['Public', 'IsoGenie', 'A2A']

# Functions for Neo4j Python Driver

def get_simple_list(tx, query):
    # General transaction function for any query whose output is a simple list of strings.
    # Result is a datastream, which is not subsettable. To get just the strings as a list, need to use list
    #  comprehension to put the first (and only) item in each record into a list (which will always be length 1), and
    #  then get the first (and only) item in *that* list.
    result = tx.run(query)
    return [record[0] for record in result][0]

def get_node_datastream(tx, query):
    # Transaction function for any query whose result is several nodes.
    # Output of the query is a datastream; this function returns the datastream as such, so that it can be iterated upon
    #  in any custom way.
    return tx.run(query)

def get_metadata_info(session, label, access_label, metadata_properties, label_fn):
    # For getting info from Metadata nodes directly connected to nodes with the given label

    # Update 6/18/20: Omit anything with the "Sequence" label, as there's a TON of them, and the long sequences
    # aren't conducive to tables.
    metadata_query = 'MATCH (d:`{}`:`{}`)-[:HAS_METADATA]->(m:Metadata) WHERE NOT(d:Sequence) RETURN DISTINCT(m) AS n'.format(label, access_label)
    datastream = session.read_transaction(get_node_datastream, metadata_query)

    # CSV

    label_fn = label_fn + '.csv'

    metadata_properties = ['node labels'] + metadata_properties
    df = pd.DataFrame(columns=metadata_properties)

    row_num = 0

    # Fill rows of the df one by one from the datastream
    for record in datastream:

        prop_dict = record['n']._properties

        # Put the properties in the same order as the metadata_properties list, and fill any NaNs
        prop_dict = {key: prop_dict[key] if (key in prop_dict.keys()) else np.nan for key in metadata_properties}

        # Add the labels
        prop_dict.update({"node labels": ', '.join(record['n']._labels)})

        df.loc[row_num] = prop_dict

        row_num += 1

    # Stop if there's nothing in the result
    if df.empty:
        return

    # Delete empty columns
    df.dropna(axis='columns', how='all', inplace=True)

    # Make new columns for actual file URLs instead of the server-specific hrefs
    href_cols = [col for col in df.columns if ('href__' in col or col == 'upload_path')]

    for href_col in href_cols:
        url_col = href_col.replace('href__', 'url__')
        url_col = url_col.replace('_path', '_url__')  # Should probably get rid of this "exception" from Sequencing imports
        for index, row in df.iterrows():
            if pd.isnull(row[href_col]):  # added this to avoid erroring out
                continue
            for href, url in href_to_url.items():  # Loop through all, but only one thing should result in a replacement
                if href in row[href_col]:
                    df.loc[index, url_col] = row[href_col].replace(href, url)
                    break

    # Write to CSV
    df.to_csv(label_fn)  # output_save_dir from local_file_paths


def get_big_data(session, label, access_label, properties, label_fn, file_type, n_records='NaN'):
    # Created this function to handle labels with large amounts of data that ate up all the memory when queried using
    #   the previous version of this script, specifically the Data label with IsoGenie access_label (the final json was
    #   8.9 GB).
    # This method improves memory use by only storing the entire result in ONE variable, final_dict, rather than
    #  converting the result to a dict, then a df, then another dict for export (thus storing the same data in
    #  3 variables). "result" returned by graph_db.run(query) is a Cursor type, which only loads each record one by one
    #  instead of all simultaneously.
    # EDIT ON 5-1-19: Added an option to export as CSV. Unfortunately, because the output of graph_db.run() is gone
    #  after being looped through once, this function must either be run twice in order to export both file types (at the
    #  cost of slow run time), or be rewritten to write the json dict and the csv df in one loop (at the cost of running
    #  out of memory for big datasets).
    # EDIT ON 7-16-20: Changed to use Neo4j Driver session instead of py2neo's graph_db, but the basic principles above
    #  are still the same.
    # EDIT ON 7-17-20: Add n_records argument, which is used to pre-allocate the dataframe if file_type="csv".
    #  If file_type=="json", it instead outputs this number, for use when the function is later called with
    #  file_type=="csv" for the same set of nodes. This is because json is much faster than csv without preallocation.

    # RE-ORDER THE PROPERTIES

    # First sort alphabetically (as they likely are already), then put times at the end, as there are a lot of them
    time_regex = re.compile('[0-9]:[0-9][0-9]')
    non_times = sorted([x for x in properties if not time_regex.search(x)])
    times = sorted([x for x in properties if time_regex.search(x)])
    properties = non_times + times

    # Now move important metadata to the beginning
    metadata_props = [x for x in properties if ('__' in x) and (x != 'CreationDate__') and (x != 'UpdateDate__')]
    if 'CreationDate__' in properties:
        metadata_props = metadata_props + ['CreationDate__']
    if 'UpdateDate__' in properties:
        metadata_props = metadata_props + ['UpdateDate__']
    non_metadata_props = [x for x in properties if '__' not in x]
    properties = metadata_props + non_metadata_props

    # Make url__ properties
    href_props = [prop for prop in properties if ('href__' in prop or prop == 'upload_path')]
    url_props_dict = {}
    for href_prop in href_props:
        if href_prop == 'upload_path':
            url_props_dict.update({href_prop: href_prop.replace('_path', '_url__')})  # Should probably get rid of this "exception" from Sequencing imports
        else:
            url_props_dict.update({href_prop: href_prop.replace('href__', 'url__')})
    properties = properties + list(url_props_dict.values())

    # Lastly, add "node labels" as a "property"
    properties = ["node labels"] + properties

    # Update 6/18/20: Omit anything with the "Sequence" label, as there's a TON of them, and the long sequences
    # aren't conducive to tables.
    query = 'MATCH (n:`{}`:`{}`) WHERE NOT(n:Sequence) RETURN n'.format(label, access_label)

    datastream = session.read_transaction(get_node_datastream, query)

    if file_type == 'json':

        json_table_headers = [[prop] for prop in properties]

        label_fn = label_fn + '.json'

        final_dict = {
            'columns': json_table_headers,
            'data': []
        }

        record_count = 0

        for record in datastream:

            prop_dict = record['n']._properties

            # Put the properties in the same order as the properties list, and fill any NaNs
            # NOTE: For jsons (only), use "NaN" instead of np.nan
            prop_dict = {key:prop_dict[key] if (key in prop_dict.keys()) else "NaN" for key in properties}

            # Add the labels
            labels_sorted = list(record['n']._labels)
            labels_sorted.sort()
            prop_dict.update({"node labels": ', '.join(labels_sorted)})

            # Add the url__ properties, if applicable
            if len(url_props_dict) > 0:

                # Loop through url_props_dict, updating prop_dict if there's something there
                for href_key, url_key in url_props_dict.items():
                    if pd.isnull(prop_dict[href_key]) or prop_dict[href_key] == 'NaN':
                        continue  # added this to avoid erroring out
                    for href, url in href_to_url.items():  # Loop through all, but only one thing should result in a replacement
                        if href in str(prop_dict[href_key]):
                            if type(prop_dict[href_key]) == str:
                                prop_dict.update({url_key: prop_dict[href_key].replace(href, url)})
                            elif type(prop_dict[href_key]) == list:
                                prop_dict.update({url_key: [item.replace(href, url) for item in prop_dict[href_key]]})
                            else:
                                error('Unrecognized prop_dict[href_key] type: {}\n{}'.format(type(prop_dict[href_key]),
                                                                                             prop_dict[href_key]))
                            break

            final_dict['data'].append(list(prop_dict.values()))

            record_count += 1

        with open(label_fn, 'w') as label_fh:
            json.dump(final_dict, label_fh, sort_keys=True, indent=4)
            return record_count


    elif file_type == 'csv':

        label_fn = label_fn + '.csv'

        if pd.isnull(n_records) or n_records == 'NaN':
            df = pd.DataFrame(columns=properties)
        else:
            df = pd.DataFrame(columns=properties, index=list(range(0, n_records)))

        row_num = 0

        # Fill rows of the df one by one from the datastream
        for record in datastream:

            prop_dict = record['n']._properties

            # Put the properties in the same order as the properties list, and fill any NaNs
            prop_dict = {key: prop_dict[key] if (key in prop_dict.keys()) else np.nan for key in properties}

            # Add the labels
            labels_sorted = list(record['n']._labels)
            labels_sorted.sort()
            prop_dict.update({"node labels": ', '.join(labels_sorted)})

            # Add the url__ properties, if applicable
            if len(url_props_dict) > 0:

                # Loop through url_props_dict, updating prop_dict if there's something there
                for href_key, url_key in url_props_dict.items():
                    if pd.isnull(prop_dict[href_key]) or prop_dict[href_key] == 'NaN':
                        continue  # added this to avoid erroring out
                    for href, url in href_to_url.items():  # Loop through all, but only one thing should result in a replacement
                        if href in str(prop_dict[href_key]):
                            if type(prop_dict[href_key]) == str:
                                prop_dict.update({url_key: prop_dict[href_key].replace(href, url)})
                            elif type(prop_dict[href_key]) == list:
                                prop_dict.update({url_key: [item.replace(href, url) for item in prop_dict[href_key]]})
                            else:
                                error('Unrecognized prop_dict[href_key] type: {}\n{}'.format(type(prop_dict[href_key]),
                                                                                             prop_dict[href_key]))
                            break

            df.loc[row_num] = prop_dict

            row_num += 1

        df.to_csv(label_fn)  # output_save_dir from local_file_paths

    else:
        error('Must specify either "json" or "csv" for the output file_type.')


driver, root_dir = load_config_driver(config_file_1, args.database)
with driver.session() as session:

    for access_label in access_labels:
        print('RETRIEVING {} DATA'.format(access_label))

        # Get list of labels for all nodes that have a given access label
        if not (args.specific_labels):
            query = 'MATCH (n:`{}`)'.format(access_label)
        else:
            query = 'MATCH (n:`{}`:`{}`)'.format(access_label, '`:`'.join(args.specific_labels))

        if args.update_start_date:
            update_start_date = int(args.update_start_date)
            query = query + ' WHERE (toFloat(n.UpdateDate__) >= {} OR toFloat(n.CreationDate__) >= {})'.format(update_start_date, update_start_date)

        query = query + ' unwind(labels(n)) as labels RETURN collect(distinct(labels)) as labels'

        print(query)

        result = session.read_transaction(get_simple_list, query)
        if 'Metadata' not in result:  # the Metadata label should always be queried, so add it just in case it's not included in args.specific_labels
            result += ['Metadata']
        node_labels = sorted([x for x in result if x not in access_labels])

        print(node_labels)

        label_to_property_map = {}

        # To replace "href" filepath strings with their corresponding URL string, this dict is looped through,
        #  stopping when there's a match. For this to work with a *regular* dict, the Python version must be >=3.5,
        #  ideally >=3.7 (see https://gandenberger.org/2018/03/10/ordered-dicts-vs-ordereddict/).
        # UPDATE 7/19/22: The "/images/raw/" key was removed as it is no longer needed, and the regular dict changed
        #  to an OrderedDict to avoid potential issues with older Python versions.
        href_to_url = OrderedDict({
            "/var/www/isogenie/public/public_data/": "https://isogenie-db.asc.ohio-state.edu/public_data/",
            "/var/www/isogenie/data/": "https://isogenie-db.asc.ohio-state.edu/data/",
            "/var/www/isogenie/public/images/": "https://isogenie-db.asc.ohio-state.edu/images/",
            "/srv/big_isogenie/public/": "https://isogenie-db.asc.ohio-state.edu/public/",
            "/srv/big_isogenie/private/": "https://isogenie-db.asc.ohio-state.edu/private/",
            "/var/www/a2a/public/public_data/": "https://a2a-db.asc.ohio-state.edu/public_data/",
            "/var/www/a2a/data/": "https://a2a-db.asc.ohio-state.edu/data/",
            "/var/www/a2a/public/images/": "https://a2a-db.asc.ohio-state.edu/images/",
            "/srv/big_a2a/": "https://a2a-db.asc.ohio-state.edu/big_a2a/"  # is this correct? Not consistent with big_isogenie URLs...
            #"/images/raw/": "https://isogenie-db.asc.ohio-state.edu/images/raw/"
        })

        for major_label in node_labels:

            # Skip general Data and Sequence labels (until we can get it not to take forever...)
            if major_label == 'Data' or major_label == 'Sequence':
                continue

            # Retrieve lists of property keys (just the keys; no data yet) that exist for each label.
            # Update 6/18/20: Omit anything with the "Sequence" label, as there's a TON of them, and the long sequences
            # aren't conducive to tables.
            query = 'MATCH (n:`{}`) WHERE n:`{}` AND NOT(n:Sequence) unwind(keys(n)) as keys RETURN collect(distinct keys) as keys'.format(major_label, access_label)

            result = session.read_transaction(get_simple_list, query)
            filtered_result = sorted(result)

            label_to_property_map[major_label] = filtered_result

        for label, properties in label_to_property_map.items():  # properties should be sorted!

            label_fn = os.path.join(output_save_dir, 'cached_query_jsons_{}', '{}').format(access_label, label)  # output_save_dir from local_file_paths
            label_fn = label_fn.replace(' ', '')  # Remove spaces

            metadata_label_fn = os.path.join(output_save_dir, 'cached_query_jsons_{}', 'Metadata_{}').format(access_label, label)  # output_save_dir from local_file_paths
            metadata_label_fn = metadata_label_fn.replace(' ', '')  # Remove spaces

            # Get Metadata CSVs

            # Comment this out if wanting to update existing results without deleting them
            # Uncomment if having to re-start this script after it unexpectedly stops
            # if os.path.exists(metadata_label_fn + '.csv'):
            #     continue

            print('\rProcessing source file Metadata for {}\n'.format(label))

            get_metadata_info(session, label, access_label, label_to_property_map['Metadata'], metadata_label_fn)

            # Get actual data

            # Comment this out if wanting to update existing results without deleting them
            # Uncomment if having to re-start this script after it unexpectedly stops
            # if os.path.exists(label_fn + '.csv'):
            #     continue

            print('\rProcessing {}\n'.format(label))

            n_records = get_big_data(session, label, access_label, properties, label_fn, 'json')
            get_big_data(session, label, access_label, properties, label_fn, 'csv', n_records)
