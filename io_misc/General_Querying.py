#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 14:34:59 2018

@author: suzanne


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
from py2neo import Graph
import numpy as np
import pandas as pd
import datetime

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append one '..' to the current directory of this file, because current directory is one level down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from local_file_paths import *
from tools.data.common_vars import *


# error() copied from imports.py
def error(msg):
    sys.stderr.write(msg + os.linesep)
    sys.stderr.flush()
    sys.exit(1)


# Connect to DB, get info
loc = sys.argv[1]

if not any(loc in args for args in ['local', 'test', 'db']):
    error('Please use one of the keywords to define WHERE the script is being run')

# added to take care of custom query in command line
if len(sys.argv) != 3:
    error('Please use three arguments after "python": (0) name of this script, (1) keyword for which database, (2) Neo4j query (in single quotes; strings within query in double quotes).\nNumber of arguments used: ' + str(len(sys.argv)))

graph_db, root_dir = load_config(config_file_1, loc)  # config_file (path) defined in local_file_paths


# Make query and convert results to dataframe

query = sys.argv[2]
# Put the Cypher query in SINGLE quotes as the last argument when running, and any strings inside the query in DOUBLE quotes.
# The reason for this convention is that the shell will interpret anything in backquotes (`) (such as node labels with special
# characters) as commands, and throw an error, if these are inside double quotes.

#query.replace('`', '\\\`')  # escape all backquotes - this does nothing if the query is in double quotes, but is unnecessary if it's in single quotes
print("Running query:")
print(query)

output_df = gather_df(query, graph_db)


# CLEANUP (written for biogeochem data, but other data types shouldn't break it) ------------------

all_columns = list(output_df.columns)

# Important columns to move to beginning
name_columns = [column for column in all_columns if 'Name_' in column]
depth_columns = [column for column in all_columns if 'Depth' in column and 'T_soil' not in column]
date_columns = [column for column in all_columns if 'Date' in column]
id_columns = [column for column in all_columns if '__' in column]

# "Junk" columns to delete
err_columns = [column for column in all_columns if '_err' in column]
eem_columns = [column for column in all_columns if 'eem_' in column]
fticrms_columns = [column for column in all_columns if 'fticrms_' in column]
ftir_columns = [column for column in all_columns if 'FTIR_' in column]
inc_columns = [column for column in all_columns if 'inc_' in column]
notes_columns = [column for column in all_columns if 'notes_' in column or 'samples_taken_' in column]

# Choose columns to move to beginning, or to delete
# CHANGE THIS DEPENDING ON NEEDS
beginning_columns = name_columns + id_columns + depth_columns + date_columns
junk_columns = err_columns + ftir_columns + eem_columns + fticrms_columns + inc_columns + notes_columns

# Append beginning_columns to beginning of all_columns
all_columns_with_dups = beginning_columns + all_columns

# Remove junk_columns
good_columns_with_dups = [column for column in all_columns_with_dups if column not in junk_columns]

# Remove duplicates created above
ordered_columns = []
for column in good_columns_with_dups:
    if column not in ordered_columns:
        ordered_columns.append(column)

# Re-select columns in output_df based on above
output_df = output_df[ordered_columns]

# Export dataframe as CSV ----------------
output_df.to_csv(os.path.join(output_save_dir, ('output_' + str(datetime.datetime.now()).replace(':','.') + '.csv')))  # output_save_dir from local_file_paths
