#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  3 21:31:38 2018

@author: suzanne

Relative file paths for config and data files.

Absolute machine-specific file paths still exist in IsoGenieDB.config, but this does not matter, as this file is only
stored locally anyway (since it includes passwords).


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os

# SELF-CONTAINED DIRECTORY PATHS

# Source data directories -------------------
source_chem_dir = os.path.join('test_data', 'IsoGenie_datasets', 'geochemistry', '')
source_cores_dir = os.path.join('test_data', 'IsoGenie_datasets', 'coring_sheets', '')
source_GPMP_dir = os.path.join('test_data', 'A2A_datasets', 'private_data', 'GPMP', '')

# Config file -------------------
# This is located in the root directory of isogenie_tools, but is ignored by git for security reasons.
config_file_1 = 'IsoGenieDB.config'
config_file_2 = config_file_1

# Directory for saving query results -------------------
# Used for output csv from General_Querying.py. This is also ignored by git.
output_save_dir = 'querying_output'

# Directory for temporary saving of map json file
isogenie_web_publicimages2_dir = os.path.join(output_save_dir, 'map_jsons')

# Core images
isogenie_web_sources_dir = os.path.join('test_data', 'IsoGenie_datasets', 'misc')
