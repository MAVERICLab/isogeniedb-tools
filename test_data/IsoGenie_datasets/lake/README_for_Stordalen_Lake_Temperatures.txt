﻿README file for Stordalen Lake Temperatures

Temperature measurements are made in three small subarctic lakes in the Stordalen Mire, 9 km east of Abisko, south of Lake Torneträsk, in northern Sweden.

Lake names and locations:
Inre Harrsjön 68°21'30.84"N, 19° 2'43.36"E
Mellersta Harrsjön 68°21'30.38"N, 19° 2'30.13"E
Villasjön 68°21'16.63 "N, 19° 3'7.84"E

Measurement time period:
2009-06-11 to 2015-06-20

Depths of measurements:
0.1, 0.3, 0.5, 1.0, 3.0, 5.0, and 6.7-7.0 m

Measurement interval:
5 to 60 minutes

Data coverage (depth, time, sampling intervals) vary over time and between the lakes. 
Intercalibrated HOBO U22 dataloggers are used.
Relative precision estimated to be 0.01-0.03oC after correction
Accuracy is estimated to be 0.2oC

Data publisher:
Bolin Centre for Climate Research, Stockholm University, SE-106 91 Stockholm, Sweden

Contact Person:
Patrick Crill
Professor of Biogeochemistry

Department of Geological Sciences
Stockholm University
Svante Arrhenius väg 8C
SE-106 91 Stockholm
Sweden

tfn: +46 (0)8 16 4740
email: patrick.crill@geo.su.se

File names:
Strdln_Twater_090611-090828_corrd.csv
Strdln_Twater_090829-091012_corrd.csv
Strdln_Twater_091015-100602_corrd.csv
Strdln_Twater_100609-100720_corrd.csv
Strdln_Twater_100721-100828_corrd.csv
Strdln_Twater_100829-100927_corrd.csv
Strdln_Twater_100928-110610_corrd.csv
Strdln_Twater_110611-111020_corrd.csv
Strdln_Twater_111020-120529_corrd.csv
Strdln_Twater_120530-120928_corrd.csv
Strdln_Twater_120928-130627_corrd.csv
Strdln_Twater_130620-130923_corrd.csv

Data points are separated by semicolons. See below for information about the file content and other details.

Raw data files are available from Patrick Crill.


Relative Offsets for HOBO dataloggers
	avg offset of the average of the twenty loggers (n=2991)

T-loggers in a stirred water bath for two weeks
before May 2 - 5, 2008, 2 minute samples used	

New set of dataloggers intercalibrated in a stirred water bath as the previous intercalibration with some old loggers 5-16 October 2015

OFFSET determined by comparison of individual logger temperature logged every two minutes to the midpoint average of a 10 minute running average of ALL 20 loggers

The average relative offset of all loggers is 0.007 oC

		OFFSET
ser num		Mean2min
1285438		-0.03736
1285449		0.01335
1285450		-0.02240
1285451		0.00708
1298954		-0.07561
1298955		-0.00493
1298956		-0.02065
1298957		0.04157
1298958		0.00873
1298959		0.01368
1298960		0.03610
1298961		-0.01701
1298962		-0.02512
1298963		0.01078
1298964		0.00045
1298965		0.04134
1298966		0.00219
1298967		0.02936
1298968		-0.00739
1298969		0.00584
10062764	0.04897
10062765	0.00018
10062766	0.06682
10062767	0.05665
10062768	0.10906
10062769	0.01582
10062770	-0.01355
10062774	0.02436
10349154	-0.04469
10349155	0.04150
10349155	0.04092
10460722	-0.02178
10460723	-0.01098
10460724	-0.00354
10460725	-0.01365
10460726	-0.00327
10460727	-0.02399
10460728	-0.00805
10460729	-0.01085
10460730	-0.00457
10460731	-0.02200
10504888	0.02535
10504889	0.03681
10504890	0.02206
10504891	0.01428
10504892	0.00007
10504893	-0.01160
10504894	0.00831
10504895	0.01256


T-loggers	11 Jun 10 - 20 Jul 10 deployment
Locations on strings not altered for subsequent deployments

InreHarrsjön		MellanHarrsjön 		Villasjön 

Depth(m) ID		Depth(m) ID		Depth(m) ID	
0.1	1298961		0.1	1298959		0.1	1298957	
0.3	1285451		0.3	1298963		0.3	1298968	
0.5	1285450		0.5	1298955		0.5	1298962	
1	1298969		1	1298960		1	1285438	
3	1298965		3	1285449				
5	1298964		5	1298954				
			7	1298966				


WARNING: Watch out for unusual temperatures during the download period
	 the strings are being pulled from the water 

Details
T strings deployed in the deepest parts in three lakes around the palsa mire complex; Stordalen Mire about 10 km East of Abisko, Sweden
IH = Inre Harrsjön, 68o 21Ž 32.37"N; 19o 02Ž41.75"E
MH = Mellan Harrsjön, 68o 21Ž30.37"N; 19o 02Ž27.63"E
VS = Villasjön, 68o 21Ž16.81"N; 19o 03'04.20"E
locations from Google Earth
Further descriptions of locations and lakes can be found in
Wik, M., P.M. Crill, R.K. Varner and D. Bastviken (2013). Multiyear measurements of ebullitive methane flux from three subarctic lakes. J. Geophys. Res.Biogeosci, 118, 1307-1321, doi:10.1002/jgrg.20103.

Logging history
there is one header line, logging does begin and end coincidently
Strdln_Twater_090611-090828_corrd.csv (11188 lines)
090611-090828:  MH and IH only, 0.1m to 1.0m logged every 10 minutes, 3, 5 an 7m logged every 60 minutes
Strdln_Twater_090829-091012_corrd.csv (6358 lines)
090829-091012:  MH and IH only, 0.1m to 1.0m logged every 10 minutes, 3, 5 an 7m logged every 60 minutes
Strdln_Twater_091015-100602_corrd.csv (22086 lines)
091015-100602:  MH and IH only, 0.1m and 0.3m loggers removed for winter; 0.5m to bottom logged every 15 minutes
Strdln_Twater_100609-100720_corrd.csv (11716 lines)
100609-100720:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_100721-100828_corrd.csv (10917 lines)
100721-100828:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_100829-100927_corrd.csv (8415 lines)
100829-100927:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_100928-110610_corrd.csv (24477 lines)
100928-110610:  MH,IH and VS all depth logged every 15 minutes
Strdln_Twater_110611-111020_corrd.csv (37700 lines)
110611-111020:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_111020-120529_corrd.csv (21319 lines)
110611-111020:  MH,IH and VS all depth logged every 15 minutes
Strdln_Twater_120530-120928_corrd.csv (34718 lines)
120530-120928:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_120928-130627_corrd.csv (26073 lines)
120928-130627:  MH,IH and VS all depth logged every 15 minutes
Strdln_Twater_130620-130923_corrd.csv (9130 lines)
130620-130923:  MH,IH and VS all depth logged every 15 minutes

Data available in Corrected *.csv files									
	 				Inre Harrsjön					Mellersta Harrsjön				Villasjön					Gaps, time	
File	 				start	 		stop			start			stop			start			stop			IH	MH	VS
Strdln_Twater_090611-090828_corrd	2009-06-11 22:00	2009-08-28 13:40	2009-06-11 22:00	2009-08-28 14:20	NoData	NoData			
Strdln_Twater_090829-091012_corrd	2009-08-29 13:00	2009-10-12 15:30	2009-08-29 13:00	2009-10-12 16:20	NoData	NoData	0.972	0.944	#VALUE!
Strdln_Twater_091015-100602_corrd	2009-10-15 17:00	2010-06-02 18:00	2009-10-15 17:00	2010-06-02 18:00	NoData	NoData	3.063	3.028	#VALUE!
Strdln_Twater_100609-100720_corrd	2010-06-09 21:00	2010-07-20 13:10	2010-06-09 21:00	2010-07-20 12:45	2010-06-09 21:00	2010-07-20 12:10	7.125	7.125	#VALUE!
Strdln_Twater_100721-100828_corrd	2010-07-21 17:00	2010-08-28 13:35	2010-07-21 17:00	2010-08-28 13:45	2010-07-21 17:00	2010-08-28 14:35	1.160	1.177	1.201
Strdln_Twater_100829-100927_corrd	2010-08-29 11:00	2010-09-27 16:05	2010-08-29 11:00	2010-09-27 14:35	2010-08-29 11:00	2010-09-27 13:30	0.892	0.885	0.851
Strdln_Twater_100928-110610_corrd	2010-09-28 15:00	2011-06-10 13:45	2010-09-28 15:00	2011-06-10 13:30	2010-09-28 15:00	2011-06-09 12:45	0.955	1.017	1.063
Strdln_Twater_110611-111020_corrd	2011-06-11 16:00	2011-10-20 13:05	2011-06-11 16:00	2011-10-20 13:05	2011-06-11 16:00	2011-10-20 13:30	1.094	1.104	2.135
Strdln_Twater_111020-120529_corrd	2011-10-20 16:00	2012-05-29 17:15	2011-10-20 16:00	2012-05-29 17:00	2011-10-20 16:00	2012-05-29 14:15	0.122	0.122	0.104
Strdln_Twater_120530-120928_corrd	2012-05-30 20:00	2012-09-28 09:00	2012-05-30 20:00	2012-09-28 09:00	2012-05-30 20:00	2012-09-28 09:00	1.115	1.125	1.240
Strdln_Twater_120928-130627_corrd	2012-09-28 15:00	2013-06-20 11:15	2012-09-28 15:00	2013-06-20 10:45	2012-09-28 15:15	2013-06-27 04:45	0.250	0.250	0.260
Strdln_Twater_130620-130923_corrd	2013-06-20 11:30	2013-09-23 11:15	2013-06-20 11:15	2013-09-23 10:45	2013-06-27 12:00	2013-09-23 13:15	0.010	0.021	0.302
Strdln_Twater_130923_140609_corrd	2013-09-23 11:30	2014-06-09 16:00	2013-09-23 11:00	2014-06-09 15:30	2013-09-23 16:00	2014-06-09 11:30	0.010	0.010	0.115
Strdln_Twater_140609_140829_corrd	2014-06-09 16:30	2014-08-29 16:20	2014-06-09 16:00	2014-08-29 15:55	2014-06-09 14:00	2014-08-29 17:00	0.021	0.021	0.104
Strdln_Twater_140829_141003_corrd	2014-08-29 16:30	2014-10-03 14:20	2014-08-29 16:05	2014-10-03 13:20	NoData	NoData	0.007	0.007	
Strdln_Twater_141003_150520_corrd	2014-12-06 00:00	2015-06-20 14:37	2014-10-03 17:00	2015-06-20 16:25	2014-11-19 16:00	2015-06-20 11:00	63.403	0.153	81.958
									


Instrument Information, From manufacturer specifications

HOBO® Pro v2 Water Temperature (400 ft.) Data Logger								
Operation range: -40° to 70°C (-40° to 158°F) in air; maximum 								
sustained temperature of 50°C (122°F) in water								
Accuracy: 0.2°C over 0° to 50°C (0.36°F over 32° to 122°F)							
Resolution: 0.02°C at 25°C (0.04°F at 77°F)							
Response time: (90%) 5 minutes in water; 12 minutes in air moving 2 m/sec (typical)								
Stability (drift): 0.1°C (0.18°F) per year								
								
Logger								
Real-time clock: ± 1 minute per month 0° to 50°C (32° to 122°F)								
Battery: 2/3 AA, 3.6 Volt Lithium, factory-replaceable ONLY								
Battery life (typical use): 6 years with 1 minute or greater logging interval								
Memory (non-volatile): 64K bytes memory (approx. 42,000 12-bit temperature measurements)								
Weight: 42 g (1.5 oz)								
Dimensions: 3.0 cm (1.19 in.) maximum diameter, 11.4 cm (4.5 in.) length; mounting hole 6.3 mm (0.25 inches) diameter								
Wetted materials: Polypropylene case, EPDM® o-rings, stainless steel retaining ring								
Buoyancy (fresh water): +13 g (0.5 oz.) in fresh water at 25°C (77°F); +17 g (0.6 oz.) with optional boot								
Waterproof: To 120 m (400 ft.)								
Shock/drop: 1.5 m (5 ft.) drop at 0°C to 70°C (32°F to 150°F)								
Logging interval: Fixed-rate or multiple logging intervals, with up to 8 user-defined logging intervals and durations; logging intervals from 1 second to 18 hours. Refer to HOBOware software manual.								
Launch modes: Immediate start and delayed start								
Offload modes: Offload while logging; stop and offload								
Battery indication: Battery voltage can be viewed in status screen and optionally logged in datafile. Low battery indication in datafile.								
