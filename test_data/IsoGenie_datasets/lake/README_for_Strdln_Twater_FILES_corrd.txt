README file for
Strdln_Twater_090611-090828_corrd.csv
Strdln_Twater_090829-091012_corrd.csv
Strdln_Twater_091015-100602_corrd.csv
Strdln_Twater_100609-100720_corrd.csv
Strdln_Twater_100721-100828_corrd.csv
Strdln_Twater_100829-100927_corrd.csv
Strdln_Twater_100928-110610_corrd.csv
Strdln_Twater_110611-111020_corrd.csv
Strdln_Twater_111020-120529_corrd.csv
Strdln_Twater_120530-120928_corrd.csv
Strdln_Twater_120928-130627_corrd.csv
Strdln_Twater_130620-130923_corrd.csv

Raw data files are available from pmc


Relative Offsets for HOBO dataloggers 								
	avg offset of the average of the twenty loggers (n=2991)							
	
T-loogers in a stirred water bath for two weeks 
before May 2 - 5, 2008 2 minute samplesused						

OFFSET determined by comparison of individual logger temperature logged every two minutes
to the midpoint average of a 10 minute running average of ALL 20 loggers

		OFFSET						
	ser num	Mean2min absolute numbers						
file_1	1285438	-0.03736						
file_2	1285449	0.01335						
file_3	1285450	-0.02240						
file_4	1285451	0.00708						
file_5	1298954	-0.07561						
file_6	1298955	-0.00493						
file_7	1298956	-0.02065						
file_8	1298957	0.04157						
file_9	1298958	0.00873						
file_10	1298959	0.01368						
file_11	1298960	0.03610						
file_12	1298961	-0.01701						
file_13	1298962	-0.02512						
file_14	1298963	0.01078						
file_15	1298964	0.00045						
file_16	1298965	0.04134						
file_17	1298966	0.00219						
file_18	1298967	0.02936						
file_19	1298968	-0.00739						
file_20	1298969	0.00584						
								
								
T-loggers	11 Jun 10 - 20 Jul 10 deployment Locations on strings not altered for subsequent deployments							
								
InreHarrsj�n		MellanHarrsj�n 		Villasj�n 					
								
Depth(m) ID		Depth(m) ID		Depth(m) ID	
0.1	1298961		0.1	1298959		0.1	1298957	
0.3	1285451		0.3	1298963		0.3	1298968	
0.5	1285450		0.5	1298955		0.5	1298962	
1	1298969		1	1298960		1	1285438	
3	1298965		3	1285449				
5	1298964		5	1298954				
			7	1298966				
								

WARNING: Watch out for unusual temperatures during the download period								
	the strings are being pulled from the water 							
								
Details
T strings deployed in the deepest parts in three lakes around the palsa mire complex; Stordalen Mire about 10 km East of Abisko, Sweden
IH = Inre Harrsj�n, 68o 21� 32.37"N; 19o 02�41.75"E
MH = Mellan Harrsj�n, 68o 21�30.37"N; 19o 02�27.63"E
VS = Villasj�n, 68o 21�16.81"N; 19o 03'04.20"E
locations from Google Earth
Further descriptions of locations and lakes can be found in 
Wik, M., P.M. Crill, R.K. Varner and D. Bastviken (2013).  Multiyear measurements of ebullitive methane flux from three subarctic lakes. J. Geophys. Res.Biogeosci, 118, 1�14, doi:10.1002/jgrg.20103, 2013.

Logging history
there is one header line, logging does begin and end coincidently
Strdln_Twater_090611-090828_corrd.csv (11188 lines)
090611-090828:  MH and IH only, 0.1m to 1.0m logged every 10 minutes, 3, 5 an 7m logged every 60 minutes
Strdln_Twater_090829-091012_corrd.csv (6358 lines)
090829-091012:  MH and IH only, 0.1m to 1.0m logged every 10 minutes, 3, 5 an 7m logged every 60 minutes
Strdln_Twater_091015-100602_corrd.csv (22086 lines)
091015-100602:  MH and IH only, 0.1m and 0.3m loggers removed for winter; 0.5m to bottom logged every 15 minutes
Strdln_Twater_100609-100720_corrd.csv (11716 lines)
100609-100720:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_100721-100828_corrd.csv (10917 lines)
100721-100828:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_100829-100927_corrd.csv (8415 lines)
100829-100927:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_100928-110610_corrd.csv (24477 lines)
100928-110610:  MH,IH and VS all depth logged every 15 minutes
Strdln_Twater_110611-111020_corrd.csv (37700 lines)
110611-111020:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_111020-120529_corrd.csv (21319 lines)
110611-111020:  MH,IH and VS all depth logged every 15 minutes
Strdln_Twater_120530-120928_corrd.csv (34718 lines)
120530-120928:  MH,IH and VS all depth logged every 5 minutes
Strdln_Twater_120928-130627_corrd.csv (26073 lines)
120928-130627:  MH,IH and VS all depth logged every 15 minutes
Strdln_Twater_130620-130923_corrd.csv (9130 lines)
130620-130923:  MH,IH and VS all depth logged every 15 minutes

Instrument Information
HOBO� Pro v2 Water Temperature (400 ft.) Data Logger								
Operation range�: -40� to 70�C (-40� to 158�F) in air; maximum 								
sustained temperature of 50�C (122�F) in water								
Accuracy: 0.2�C over 0� to 50�C (0.36�F over 32� to 122�F), see Plot A								
Resolution: 0.02�C at 25�C (0.04�F at 77�F), see Plot A								
Response time: (90%) 5 minutes in water; 12 minutes in air moving 2 m/sec (typical)								
Stability (drift): 0.1�C (0.18�F) per year								
								
Logger								
Real-time clock: � 1 minute per month 0� to 50�C (32� to 122�F)								
Battery: 2/3 AA, 3.6 Volt Lithium, factory-replaceable ONLY								
Battery life (typical use): 6 years with 1 minute or greater logging interval								
Memory (non-volatile): 64K bytes memory (approx. 42,000 12-bit temperature measurements)								
Weight: 42 g (1.5 oz)								
Dimensions: 3.0 cm (1.19 in.) maximum diameter, 11.4 cm (4.5 in.) length; mounting hole 6.3 mm (0.25 inches) diameter								
Wetted materials: Polypropylene case, EPDM� o-rings, stainless steel retaining ring								
Buoyancy (fresh water): +13 g (0.5 oz.) in fresh water at 25�C (77�F); +17 g (0.6 oz.) with optional boot								
Waterproof: To 120 m (400 ft.)								
Shock/drop: 1.5 m (5 ft.) drop at 0�C to 70�C (32�F to 150�F)								
Logging interval: Fixed-rate or multiple logging intervals, with up to 8 user-defined logging intervals and durations; logging intervals from 1 second to 18 hours. Refer to HOBOware software manual.								
Launch modes: Immediate start and delayed start								
Offload modes: Offload while logging; stop and offload								
Battery indication: Battery voltage can be viewed in status screen and optionally logged in datafile. Low battery indication in datafile.								
Contact Person:
Patrick Crill
Professor of Biogeochemistry

Department of Geological Sciences
Stockholm University
Svante Arrhenius v�g 8C
106 91 Stockholm
Sweden

tfn: +46 (0)8 16 4740