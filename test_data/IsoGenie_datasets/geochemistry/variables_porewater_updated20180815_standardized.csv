ColumnGroup__,Column__,ColumnDescription__
,Name_unique,"Unique names for each date, site, and depth combination."
,Name_f,(pore water and pore gas) Names that I had written on the vials.
,Name_s,(peat) Names that I had written on the peat bags.
,Type,Habitat type.
,Year,Sampling year.
,Month,"Sampling month (for the 2010 samples, I just put ""August"" for easier sorting even though some samples were gathered on Sept. 1)."
,Site,"Site name. For the autochamber sites, I omitted the profile/core number, which is instead under ""Rep."""
,Depth_Code,"A code that I made up for different depth ranges, for easier automated matching of slightly mismatched peat and porewater depths. I did this somewhat arbitrarily, so you can ignore this."
,Rep,"For the autochamber sites, this denotes profile/core 1, 2, or 3. For PHS and Fen1 sites sampled in August 2012, this denotes porewater gathered from piezometer wells on two different dates (just in case different weather, etc. might affect the results)."
,Depth.cm,(pore water and pore gas) Average sample depth below peat surface (cm).
,Depth_Min.cm,(peat) Depth of top of core section (cm).
,Depth_Max.cm,(peat) Depth of bottom of core section (cm).
,GPS,"GPS coordinates, if obtained."
,Date_pw,Exact date of pore water sampling.
,Date_pg,Exact date of pore gas sampling.
,Date_core,Exact date of peat sampling.
,Date_WTD,Exact date of water table depth measurement.
,Date_T,Exact date of temperature measurement.
,Time_pw,Time of pore water sampling.
,Time_pg,Time of pore gas sampling.
,Time_core,Time of peat sampling.
,WTD.cm_neg,"Water table depth (cm). The ""_neg"" indicates that the signs are opposite of the sample depths and ALD, i.e., negative values indicate depth below the peat surface while positive indicates height above the surface."
,ALD.cm,Active layer depth (cm).
,T_air.degC,Air temperature (deg C).
,T_soil.degC,"Soil temperature (deg C). For samples gathered in 2010-2011, I copied the temperature at 3 or 13cm (as measured below the water OR peat surface, whichever was higher) so that these values could be compared with the larger-depth ranges measured in 2012. So far I've only done this conversion for the pore water samples. Due to differences in how these 3 and 13cm depths were measured, these don't match up too well with the sample depths for the 2010-2011 samples, so I would use the T_3cm and T_13cm columns for these years unless comparing with 2012."
,Depth_T_soil.cm,"Depth of soil temperature measurement (cm), since this often differed slightly from the closest sample depth. For samples gathered in 2010-2011, I converted the 3cm or 13cm depth (which was measured below either the peat OR water table surface, whichever was higher) into depth below the peat surface. So far I've only done this conversion for the pore water samples. These don't match up too well with the sample depths for the 2010-2011 samples, so I would use the T_3cm and T_13cm columns for these years unless comparing with 2012."
,T_3cm.degC,"(2010-2011 only) Temperature (deg C) at 3cm below the peat or water table surface, whichever was higher."
,T_13cm.degC,"(2010-2011 only) Temperature (deg C) at 13cm below the peat or water table surface, whichever was higher."
,notes_Suzanne,Notes I added regarding any special considerations.
,notes_sampling_spreadsheet,Notes directly copied and pasted from the sampling spreadsheets (the sheets that were shared with the whole Isogenie group right after sampling).
,samples_taken_from_sampling_spreadsheet,"Types of samples taken, directly copied and pasted from the sampling spreadsheets (the sheets that were shared with the whole Isogenie group right after sampling)."
,Cond.uS,Conductivity (microsiemens).
,ORP,"Oxidation-reduction potential. This was measured on an uncalibrated instrument, and I'm not sure of the units (it was a last-minute addition), so I would interpret these values tentatively as relative potentials across samples. More negative indicates more reducing conditions."
,pH_pw,pH of porewater.
,CH4.mM_oldmethod,"Dissolved CH4 concentration (mM). Calculated using experimentally determined ""extraction efficiency"" for pore water vials."
,DIC.mM_oldmethod,"Dissolved inorganic carbon (DIC, or ""CO2"") concentration (mM). Calculated using experimentally determined ""extraction efficiency"" for pore water vials."
,CH4.mM_HenryLaw,Dissolved CH4 concentration (mM). Calculated using Henry's Law constant of 0.0015 mol/(L*atm).
,DIC.mM_HenryLaw,"Dissolved inorganic carbon (DIC, or ""CO2"") concentration (mM). Calculated using Henry's Law constant of 0.037 mol/(L*atm)."
,d13C_CH4,d13C value of dissolved CH4.
,d13C_DIC,"d13C value of DIC (""CO2"")."
,dD_CH4,dD value of dissolved CH4.
,dD_H2O,dD value of water.
,d18O_H2O,d18O value of water.
,DOC.mM,"DOC concentration (mM). For the samples up through August 2011, these values may be slightly overestimated relative to values measured in Oct. 2011 and later, due to differences in sample prep (the later dates were acidified first whereas the earlier dates were not). The acidified samples (Oct. 2011 and later) probably have more accurate values, but the non-acidified samples are still accurate within reason."
,TN.mM,"Total nitrogen concentration (mM), including both organic and inorganic N. Since this was measured at the same time as the DOC and the DOM has very high C/N ratios, many of the TN values were very close to the detection limit, so I would use them with caution."
,acetate.uM,Acetate concentration (µM).
,phosphate.uM,Phosphate concentration (µM).
,sulfate.uM,Sulfate concentration (µM).
,nitrate.uM,Nitrate concentration (µM).
,ammonia.uM,Ammonia concentration (µM).
,D14C_DIC,"Radiocarbon of DIC, expressed as D14C."
,Ca.uM,Calcium concentration (µM).
,Mg.uM,Magnesium concentration (µM).
UVVIS,A254,UV absorbance at 254 nm (m-1).
UVVIS,A340,UV absorbance at 340 nm (m-1).
UVVIS,SUVA_254.L.mg.m,"(UV absorbance at 254 nm [m-1])/(DOC concentration [mg/L]). This correlates positively with DOM aromaticity; see Weishaar et al. 2003, doi:10.1021/es030360x"
UVVIS,spectral_slope_275_295,"Log-transformed slope of the UV/Vis spectra from 275-295 nm (Helms et al. 2008, doi:10.4319/lo.2008.53.3.0955)."
UVVIS,spectral_slope_350_400,"Log-transformed slope of the UV/Vis spectra from 350-400 nm (Helms et al. 2008, doi:10.4319/lo.2008.53.3.0955)."
UVVIS,spectral_slope_ratio,"Ratio of spectral_slope_275_295/spectral_slope_350_400. This correlates negatively with DOM molecular weight; see Helms et al. 2008, doi:10.4319/lo.2008.53.3.0955"
EEMS,eem_C.intensity,"Maximum peak C intensity (QSE). Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS and UVVIS,eem_C.intensity.a340,"Ratio of eem_C.intensity/a340, where a340 = A340*2.303. This is inversely correlated with molecular weight; see Stewart and Wetzel 1981, doi:10.4319/lo.1981.26.3.0590; also Baker et al. 2008, doi:10.1016/j.chemosphere.2008.09.018. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_C.em.max,"Emission wavelength (nm) of the maximum fluorescence of peak C. This increases with aromaticity; see Senesi 1990, doi:10.1016/S0003-2670(00)81226-X; also Tfaily et al. 2013, doi:10.1016/j.gca.2013.03.002. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_BIX,"Biological index, defined as the ratio of fluorescence intensities at emission wavelengths (380 nm)/(430 nm) at an excitation wavelength of 310 nm. This increases with the amount of microbially-derived organic matter and decreases with aromaticity; see Huguet et al. 2009, doi:10.1016/j.orggeochem.2009.03.002. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_FI,"Fluorescence index, defined as the ratio of fluorescence intensities at emission wavelengths (470 nm)/(520 nm) at an excitation wavelength of 370 nm. Although this doesn't seem to show as many trends in the Stordalen samples as BIX, this should increase with the amount of microbially-derived organic matter and decrease with aromaticity; see McKnight et al. 2001, doi:10.4319/lo.2001.46.1.0038. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_HIX,"Humification index, defined as the ratio of H/L, where H and L are the integrated emission intensities from 434–480 nm and 300–344 nm (respectively) measured at an excitation wavelength of 255 nm. THIS IS NOT THE SAME AS THE HUMIFICATION INDEX DEFINED BASED ON SOLID-PHASE FTIR SPECTRA (abbreviated HI). Essentially, HIX indicates the ratio of humic-like to tryptophan- and tyrosine-like fluorophores (whereas the wavelengths used to define BIX and FI all fall within the humic-like fluorescence region). HIX can show a variety of trends; it can increase with decomposition as organic matter becomes more humified, but it can also decrease as microbially-derived organic matter increases. See Zsolnay et al. 1999, doi:10.1016/S0045-6535(98)00166-0. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_A.vol,"Volume of peak A (Coble 1996, doi:10.1016/0304-4203(95)00062-3), NOT normalized to DOC concentration. Normally we don't use this value because it's too closely related to DOC concentration, and normalizing it to DOC doesn'ts seem to reveal any trends, but we make note of it just in case. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_C.vol,"Volume of peak C (Coble 1996, doi:10.1016/0304-4203(95)00062-3), NOT normalized to DOC concentration. Normally we don't use this value because it's too closely related to DOC concentration, and normalizing it to DOC doesn'ts seem to reveal any trends, but we make note of it just in case. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_M.vol,"Volume of peak M (Coble 1996, doi:10.1016/0304-4203(95)00062-3), NOT normalized to DOC concentration. Normally we don't use this value because it's too closely related to DOC concentration, and normalizing it to DOC doesn'ts seem to reveal any trends, but we make note of it just in case. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_T.vol,"Volume of peak T (Coble 1996, doi:10.1016/0304-4203(95)00062-3), NOT normalized to DOC concentration. Normally we don't use this value because it's too closely related to DOC concentration, and normalizing it to DOC doesn'ts seem to reveal any trends, but we make note of it just in case. Used 15-75 2nd order Rayleigh scattering corrections and the correct calibration file ""Miller Perkin-Elmer LS50B (27-Feb-2007)."""
EEMS,eem_C.intensity_old,"eem_C.intensity, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_C.intensity.a340_old,"eem_C.intensity.a340, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_C.em.max_old,"eem_C.em.max, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_BIX_old,"eem_BIX, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_FI_old,"eem_FI, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_HIX_old,"eem_HIX, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_A.vol_old,"eem_A.vol, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_C.vol_old,"eem_C.vol, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_M.vol_old,"eem_M.vol, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_T.vol_old,"eem_T.vol, calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA)."
EEMS,eem_C1_old,"Relative intensity of PARAFAC component C1 as derived by Tfaily et al. 2015, doi:10.1111/php.12448. Calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA), which was also used to process the spectra used by Tfaily et al. 2015 to build the PARAFAC model; hence there is no version using the correct calibration file."
EEMS,eem_C2_old,"Relative intensity of PARAFAC component C2 as derived by Tfaily et al. 2015, doi:10.1111/php.12448. Calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA), which was also used to process the spectra used by Tfaily et al. 2015 to build the PARAFAC model; hence there is no version using the correct calibration file."
EEMS,eem_C3_old,"Relative intensity of PARAFAC component C3 as derived by Tfaily et al. 2015, doi:10.1111/php.12448. Calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA), which was also used to process the spectra used by Tfaily et al. 2015 to build the PARAFAC model; hence there is no version using the correct calibration file."
EEMS,eem_C4_old,"Relative intensity of PARAFAC component C4 as derived by Tfaily et al. 2015, doi:10.1111/php.12448. Calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA), which was also used to process the spectra used by Tfaily et al. 2015 to build the PARAFAC model; hence there is no version using the correct calibration file."
EEMS,eem_C5_old,"Relative intensity of PARAFAC component C5 as derived by Tfaily et al. 2015, doi:10.1111/php.12448. Calculated using EEMS spectrum processed using a default (now known as incorrect) calibration file ""Michael_Fluoromax4_April08 (07-Apr-2008)"" (version used in Hodgkins et al. 2016 GCA), which was also used to process the spectra used by Tfaily et al. 2015 to build the PARAFAC model; hence there is no version using the correct calibration file."
FTICRMS,fticrms_CHO,"Proportion of compounds with C, H, and O only."
FTICRMS,fticrms_CHON,"Proportion of compounds with C, H, O, and N only."
FTICRMS,fticrms_CHOS,"Proportion of compounds with C, H, O, and S only."
FTICRMS,fticrms_CHONS,"Proportion of compounds with C, H, O, N, and S."
FTICRMS,fticrms_lipid,"Proportion of ""lipid-like"" compounds (O/C = 0–0.29 and H/C = 1.6–2)."
FTICRMS,fticrms_protein,"Proportion of ""protein-like"" compounds (O/C = 0.29–0.6 and H/C = 1.5–2)."
FTICRMS,fticrms_AS.carb,"Proportion of ""aminosugar- and carbohydrate-like"" compounds (O/C = 0.6–1 and H/C = 1.5–2)."
FTICRMS,fticrms_UH,"Proportion of ""unsaturated hydrocarbon"" compounds (O/C = 0–0.29 and H/C = 1–1.6)."
FTICRMS,fticrms_CA,"Proportion of ""condensed aromatics"" (O/C = 0–0.4 and H/C = 0–0.7)."
FTICRMS,fticrms_lignin,"Proportion of ""lignin-like"" compounds (O/C = 0.29–0.65 and H/C = 0.7–1.5)."
FTICRMS,fticrms_tannin,"Proportion of ""tannin-like"" compounds (O/C = 0.65–1 and H/C = 0.5–1.5)."
FTICRMS,fticrms_MW.avg,Weighted average molecular weight (weighted by relative abundance of assigned compounds).
FTICRMS,fticrms_OtoC.total,"Total O/C ratio = sum(rel. abund. * #O)/sum(rel. abund. * #C), summed across all compounds in sample. This can be thought of as a weighted average O/C ratio."
FTICRMS,fticrms_HtoC.total,"Total H/C ratio = sum(rel. abund. * #H)/sum(rel. abund. * #C), summed across all compounds in sample. This can be thought of as a weighted average H/C ratio."
FTICRMS,fticrms_NtoC.total,"Total N/C ratio = sum(rel. abund. * #N)/sum(rel. abund. * #C), summed across all compounds in sample. This can be thought of as a weighted average N/C ratio."
FTICRMS,fticrms_StoC.total,"Total S/C ratio = sum(rel. abund. * #S)/sum(rel. abund. * #C), summed across all compounds in sample. This can be thought of as a weighted average S/C ratio."
FTICRMS,fticrms_AI.avg,"Weighted average aromaticity index (weighted by relative abundance of assigned compounds). See Koch and Dittmar 2006, doi:10.1002/rcm.2386 for a definition of aromaticity index. "
FTICRMS,fticrms_DBEtoC.avg,"Weighted average DBE/C ratio (weighted by relative abundance of assigned compounds), where DBE = double bond equivalence = number of rings plus double bonds. See Stenson et al. 2003, doi:10.1021/ac026106p for a definition of DBE."
FTICRMS,fticrms_DBEOtoC.avg,"Weighted average (DBE-O)/C ratio (weighted by relative abundance of assigned compounds), where DBE = double bond equivalence = number of rings plus double bonds. See Stenson et al. 2003, doi:10.1021/ac026106p for a definition of DBE. DBE-O is sometimes used instead of just DBE to approximate the number of carbon-carbon double bonds, since most oxygen is assumed to be bonded to carbon by a double bond."
,pool.DOM,"Whether or not the DOM sample (which includes the DOC.mM, TN.mM, sulfate.uM, nitrate.uM, ammonia.uM, Ca.uM, Mg.uM, SUVA_254.L.mg.m, all eem, and all fticrms variables) was pooled across all porewater profiles. This is already indicated by blue boxes around these values, but I also included it as a separate variable for importing into programs that don't preserve formatting. "
,pool.dD_CH4,"Whether or not the dD_CH4 sample was pooled across all porewater profiles. This is already indicated by blue boxes around these values, but I also included it as a separate variable for importing into programs that don't preserve formatting."
