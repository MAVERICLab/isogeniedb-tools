#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 23:40:46 2018

@author: suzanne

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import pandas as pd
import geojson as g

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append two '..' to the current directory of this file, because current directory is two levels down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from local_file_paths import *
from tools.data.common_vars import *

map_csv = sys.argv[1]

map_df = pd.read_csv(map_csv, sep=',', quotechar='"')
    
# Drop empty columns
map_df.dropna(axis='columns', how='all', inplace=True)

ultimate_list = []

# The code below generates a json for each unique 'Site Type', and each json in turn becomes a grouping
#  of markers that can be toggled on and off.
# So to have the same marker exist in multiple categories, would need to re-design how the jsons are built, or
#  perhaps have redundancy between the jsons.

# For the boilerplate A2A map interface, try making redundant JSONs each with a different data type,
#  but still color the points by habitat. That way, can have a way to visually identify both habitats AND data types.
# Use the Labels column to generate a list of all the datatype labels, then group rows in the df based on whether each
#  unique label is present for that row.

datatypes = []
for index, row in map_df.iterrows():
    for label in row['Labels'].split(', '):
        if label not in datatypes:
            datatypes.append(label)
            
habitat_styles = {
    'Permafrost Forest': '/images/pin_permafrost_forest.png',
    'Palsa': '/images/pin_palsa.png',
    'Collapsed Palsa': '/images/pin_collapsed_palsa.png',
    'Bog': '/images/pin_bog.png',
    'Poor Fen': '/images/pin_poor_fen.png',
    'Fen': '/images/pin_fen.png'
}

            
for typeName in datatypes:
    typeGroup_df = map_df.loc[map_df['Labels'].str.contains(typeName)]

    print('Processing {}'.format(typeName))

    feature_list = []  # For feature collection

    for index, row in typeGroup_df.iterrows():
    
        # Ignore rows with no Latitude, Longitude (columns used for separate decimal degrees)
        # TODO Add the ability to use parse_DMS() from Create_Map_Geojson_by_Site to parse non-decimal GPS__
        if pd.isnull(row['Latitude']) or row['Latitude']=='' or pd.isnull(row['Longitude']) or row['Longitude']=='':
            continue
        else:
            lat = row['Latitude']
            lon = row['Longitude']
            
        name = row['Name']
        style = habitat_styles[row['Habitat__']]
        
        description = '<p><b>{0}</b></p>'.format(name)
        description += ' <p><b>Data types:</b></p> {0}'.format(row['Datasets'])
        
        feature = g.Feature(geometry=g.Point((lon, lat)), properties={
            'name': name,
            'description': str(description),
            'marker': style
        })

        feature_list.append(feature)
        ultimate_list.append(feature)

    feature_collection = g.FeatureCollection(feature_list)
    
    bad_fns = [',', '(', ')']

    fn = typeName
    for bad_fn in bad_fns:
        fn = fn.replace(bad_fn, '')
    
    with open(os.path.join(isogenie_web_publicimages2_dir, "{}_tmp.json").format(fn), 'w') as out_fh:
        # isogenie_web_publicimages2_dir from local_file_paths.py
        g.dump(feature_collection, out_fh)

ultimate_collection = g.FeatureCollection(ultimate_list)

with open(os.path.join(isogenie_web_publicimages2_dir, "All_tmp.json").format(fn), 'w') as out_fh:
    # isogenie_web_publicimages2_dir from local_file_paths.py
    g.dump(ultimate_collection, out_fh)