#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import glob
import subprocess
from pprint import pprint

root_dir = os.path.normpath('/Users/bolduc.10/Box Sync/SWES-MEL/IsoGenieDB-development/PDFs/')
thumbs_dir = os.path.join(root_dir, 'thumbnails/')

pdfs = glob.glob(os.path.join(root_dir, 'raw/*.pdf'), recursive=True)


def run(cmd):
    sys.stdout.write('Executing: {}\n'.format(cmd))
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE).stdout

    return result


thumb_template = 'convert -resize 816x1056 -define png:compression-level=9 -alpha remove -flatten "{}"[0] "{}"'
for pdf in pdfs:
    thumb_fn = os.path.basename(pdf)

    if not any(thumb_fn in fn for fn in os.listdir(thumbs_dir)):
        thumb_fp = os.path.join(thumbs_dir, thumb_fn.rsplit('.', 1)[0] + '.jpg')

        if not os.path.exists(thumb_fp):

            convert_cmd = thumb_template.format(pdf, thumb_fp)
            out = run(convert_cmd)
