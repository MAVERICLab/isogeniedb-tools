#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import numpy as np
import pandas as pd
from datetime import datetime
#import simplekml
#import osr
from py2neo import Graph, walk
from py2neo.packages.httpstream import http
from pprint import pprint
import geojson as g
http.socket_timeout = 9999

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
# append two '..' to the current directory of this file, because current directory is two levels down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from local_file_paths import *
from tools.data.common_vars import *

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

loc = sys.argv[1]

if not any(loc in args for args in ['local', 'test', 'db']):
    error('Please use one of the keywords to define WHERE the script is being run')

# Connect to DB, get info
graph_db, root_dir = load_config(config_file_2, loc) # config_file (path) defined in local_file_paths

core_query = "MATCH (n) WHERE EXISTS(n.`GPS__`) AND ((n:Core) OR (n:Site)) RETURN n"

core_df = gather_df2(core_query, graph_db)  # Will retrieve all cores with GPS

# Choose nodes to use, based on labels (may need to add more later).
#core_df = core_df[core_df['Labels'].str.contains('Site|Core')]  # rows where Labels contains "Site" or "Core" (regular expression)
core_df = core_df[~core_df['GPS__'].isin(['No Core', 'Not Supplied', 'nan, nan', 'NaN', 'No Sample'])]  # Remove bad GPS

# Remove junk columns
for rm_col in ['CreationDate__', 'Name', 'contact', 'dataProvider']:
    if rm_col in core_df.columns:
        core_df.drop(rm_col, axis='columns', inplace=True)

core_df.dropna(how='all', axis='columns', inplace=True)  # Final drops

# Spatial Reference System
# COMMENT: Doesn't appear to be used.
inputEPSG = 4326
outputEPSG = 900913

# Improved version from Pull-GPS-by-Site.py
def parse_DMS(dms):  #, input_ref=4326, output_ref=900913):
    """
    Convert degree-minute-seconds to signed decimal degrees.
    
    https://github.com/mlaloux/My-Python-GIS_StackExchange-answers/blob/master/OGR%20Projection%20transformation%20error.md
    """
    # Many of the special cases should no longer be necessary, as the GPS is better standardized while importing.
    # Add options for S and W where N and E are used.

    try:
        lat_txt, lon_txt = dms.split(',')
    except ValueError: 
        error('Could not parse GPS into lat/lon: {}'.format(dms))

    if len(lat_txt.split()) == 4:  # It's still DMS

        try:
            if lat_txt[-1] in ['N', 'S']:
                lat_deg, lat_min, lat_sec, lat_dir = lat_txt.split()
            else:
                lat_dir, lat_deg, lat_min, lat_sec = lat_txt.split()

            if lon_txt[-1] in ['E', 'W']:
                lon_deg, lon_min, lon_sec, lon_dir = lon_txt.split()
            else:
                lon_dir, lon_deg, lon_min, lon_sec = lon_txt.split()
        except ValueError:
            print(lat_txt)
            print(lon_txt)
            exit()

        lat_dd = int(lat_deg) + (float(lat_min) / 60) + (float(lat_sec) / 3600)
        lon_dd = int(lon_deg) + (float(lon_min) / 60) + (float(lon_sec) / 3600)

    if len(lat_txt.split()) == 3:

        if lat_txt[-1] in ['N', 'S']:
            lat_deg, lat_min, lat_dir = lat_txt.split()
        else:
            lat_dir, lat_deg, lat_min = lat_txt.split()

        if lon_txt[-1] in ['E', 'W']:
            lon_deg, lon_min, lon_dir = lon_txt.split()
        else:
            lon_dir, lon_deg, lon_min = lon_txt.split()

        lat_dd = int(lat_deg) + (float(lat_min) / 60)
        lon_dd = int(lon_deg) + (float(lon_min) / 60)

    if lat_dir == ('S' or 'W'):
        lat_dd = lat_dd * -1

    if lon_dir == ('S' or 'W'):
        lon_dd = lon_dd * -1

    return lat_dd, lon_dd


# Get data relationships for Cores
for coreID in core_df['CoreID'].unique():

    if not pd.isnull(coreID):
        
        site_name = core_df.loc[core_df['CoreID'] == coreID, 'Site__']
        core_name = core_df.loc[core_df['CoreID'] == coreID, 'CoreDated__']

        # these are actually length-one series with unknown index
        if type(site_name) == pd.core.series.Series:
            site_name = site_name.reset_index(drop=True)[0]
        if type(core_name) == pd.core.series.Series:
            core_name = core_name.reset_index(drop=True)[0]

        print("Processing {}: {}".format(site_name, core_name))

        # Find out which Data nodes are associated with this core
        # NEW: To improve speed, specify a maximum path length of 3 relationships (Reads-Raw and MAG nodes).
        #  "Sequence" nodes are intentionally left out of this, as they are numerous and cause the script to run out of
        #  memory.
        #  May need to adjust this upward if paths longer than this are added.
        path_query = "MATCH p=(n:Core {{CoreID:'{}'}})-[r*1..3]->(t:Data) WHERE NOT(t:Sequence) RETURN p".format(coreID)

        retrieve_paths = graph_db.run(path_query).data()

        paths = set()

        for n_node in retrieve_paths:

            # path is type of walkable, which is a subgraph with path info - can be treated as a set
            relationships = [paths.update(path.types()) for cypher_key, path in n_node.items()]

        paths = list(paths)
        paths.sort()
        core_df.loc[core_df['CoreID'] == coreID, 'Relationships'] = ','.join(paths)
    
# Do the same thing with non-cores (these should all be Sites)
non_core_df = core_df[core_df['CoreID'].isnull()]
for siteID in non_core_df['SiteID'].unique():
    
    if not pd.isnull(siteID):
        
        site_name = non_core_df.loc[non_core_df['SiteID'] == siteID, 'Site__']
        if type(site_name) == pd.core.series.Series:
            site_name = site_name.reset_index(drop=True)[0]

        print("Processing {}".format(site_name))
        
        # Find out which Data nodes are associated with this site
        # NEW: To improve speed, specify a maximum path length of 3 relationships (may need to adjust this if longer
        #  paths to Data nodes are added).
        path_query = "MATCH p=(n:Site {{SiteID:'{}'}})-[r*1..3]->(t:Data) RETURN p".format(siteID)

        retrieve_paths = graph_db.run(path_query).data()

        paths = set()

        for n_node in retrieve_paths:

            # path is type of walkable, which is a subgraph with path info - can be treated as a set
            relationships = [paths.update(path.types()) for cypher_key, path in n_node.items()]

        # update core_df (not non_core_df, which was just for conveniently separating non-cores)
        paths = list(paths)
        paths.sort()
        core_df.loc[core_df['SiteID'] == siteID, 'Relationships'] = ','.join(paths)

# Convert GPS to map-readable units
core_df['Map Coords'] = core_df['GPS__'].apply(parse_DMS,  # input_ref=inputEPSG, output_ref=outputEPSG,
                                               convert_dtype=False)

# Set up styles

core_df['Site Type'] = core_df['Site__'].map(map_categories)

habitat_styles = {
    'ANS': '/images/ANSLOGOblue.png',
    'Palsa (autochamber site)': '/images/pin-palsa.png',
    'Palsa (other)': '/images/pin-palsa_nonauto.png',
    'Collapsed Palsa': '/images/pin-collapsed_palsa.png',
    'Bog (autochamber site)': '/images/pin-bog.png',
    'Bog (other)': '/images/pin-bog_nonauto.png',
    'Poor Fen': '/images/pin-poor_fen.png',
    'Fen (autochamber site)': '/images/pin-fen.png',
    'Fen (other)': '/images/pin-fen_nonauto.png',
    'Lake': '/images/pin-lake.png',
}

# Export to CSV as an intermediate error-checking step
# Apparently core_df is HUGE and contains a lot of extra columns from the Data nodes...
#core_df.to_csv(os.path.join(output_save_dir, 'core_df_forJSON.csv'), sep=',', quotechar='"', index=False) # output_save_dir from local_file_paths
#exit()

ultimate_list = []

# The code below generates a json for each unique 'Site Type', and each json in turn becomes a grouping
#  of markers that can be toggled on and off.
# So to have the same marker exist in multiple categories, would need to re-design how the jsons are built, or
#  perhaps have redundancy between the jsons.
for typeName, typeGroup_df in core_df.groupby('Site Type'):  # By Sites = Site__

    print('Processing {}'.format(typeName))

    feature_list = []  # For feature collection

    for index, series in typeGroup_df.iterrows():

        lat, lon = series['Map Coords']
        name = False

        major_images = set()
        quadrat_images = set()

        if 'ANS' == typeName:
            name = 'Abisko Scientific Research Station'
            style = '/images/ANSLOGOblue.png'
            major_images.update(['/images/5_ANS_WebRS_Nils_Ake_Andersson.jpg'])

        # Site__ and labels are different, so they are switched between depending on the type

        elif typeName == 'Lake':

            #continue  # Until lakes are settled
            
            siteName = series['Site__']
            name = '{0} Lake'.format(siteName)
            #style = lakes[no_site[0]]['style']  # Need lakes style
            style = habitat_styles[series['Site Type']]

            try:
                img_str = series['image_href__']
                major_images.update([img.replace('/var/www/isogenie/public', '') for img in img_str.split(', ')])  # /images/raw/cores/...
            except TypeError:  # If node doesn't have it
                pass
            except KeyError:  # If node doesn't have it
                pass
            except AttributeError:  # If node doesn't have it
                pass

        elif typeName in habitat_styles.keys():

            style = habitat_styles[series['Site Type']]

            # Grab core info
            core_str = series['CoreDated__']
            core_num, core_date = core_str.split('|')

            siteName = series['Site__']

            name = '{0}: <br>Core {1} ({2})'.format(siteName, core_num, core_date)

            try:
                img_str = series['image_href__']
                major_images.update([img.replace('/var/www/isogenie/public', '') for img in img_str.split(', ')])  # /images/raw/...
            except TypeError:  # If node doesn't have it
                pass
            except KeyError:  # If node doesn't have it
                pass
            except AttributeError:  # If node doesn't have it
                pass

            # Do the same with vegetation quadrat images
            try:
                quad_img_str = series['quadrat_image_href__']
                quadrat_images.update([img.replace('/var/www/isogenie/public', '') for img in quad_img_str.split(', ')])
            except TypeError:  # If node doesn't have it
                pass
            except KeyError:  # If node doesn't have it
                pass
            except AttributeError:  # If node doesn't have it
                pass

        else:
            print('Unable to identify habitat type in dataset')
            print(series)
            exit(1)

        # With that done,
        if not name or name == 'name':

            print('Unable to identify data in dataset')
            print(series)
            exit(1)

        # Set up descriptions and images using data embedded in node info

        description = '<p><b>{0}</b></p>'.format(name)  # DUPLICATES NAME

        try:
            relationships = series['Relationships'].replace(',', '<br>')
        except AttributeError:  # numpy nan = float
            relationships = ''

        description += ' <p><b>Properties:</b><br>{0}</p>'.format(relationships)

        # Core images
        images = ''
        for image in major_images:
            images += '<img class="core-img" src="{0}"/>'.format(image.strip())
        if images != '':
            description += ' <p><b>Pictures:</b><div class="grid">{0}</div></p>'.format(images)

        # Quadrat images
        q_images = ''
        for quadrat_image in quadrat_images:
            q_images += '<img class="core-img" src="{0}"/>'.format(quadrat_image.strip())  # just re-use class "core-img" for easier styling in map webpage
        if q_images != '':
            description += ' <p><b>Quadrat pictures:</b><div class="grid">{0}</div></p>'.format(q_images)

        feature = g.Feature(geometry=g.Point((lon, lat)), properties={
            'name': name,
            'description': str(description),
            'marker': style
        })

        feature_list.append(feature)
        ultimate_list.append(feature)

    feature_collection = g.FeatureCollection(feature_list)

    # omit characters that can't go in json filenames
    bad_fns = [',']

    fn = typeName
    for bad_fn in bad_fns:
        fn = fn.replace(bad_fn, '')

    with open(os.path.join(isogenie_web_publicimages2_dir, "{}.json").format(fn), 'w') as out_fh:
        # isogenie_web_publicimages2_dir from local_file_paths.py
        g.dump(feature_collection, out_fh)

ultimate_collection = g.FeatureCollection(ultimate_list)

with open(os.path.join(isogenie_web_publicimages2_dir, "All.json").format(fn), 'w') as out_fh:
    # isogenie_web_publicimages2_dir from local_file_paths.py
    g.dump(ultimate_collection, out_fh)