#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import json
import networkx as nx
from networkx.readwrite import json_graph
from pprint import pprint

# import local_file_paths, which is in the root isogenie-tools directory
# append two '..' to the current directory of this file, because current directory is two levels down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from local_file_paths import *


edges = [
    ('IsoGenie', 'The Ohio State University'),
    ('IsoGenie', 'University of Arizona'),
    ('IsoGenie', 'University of Queensland'),
    ('IsoGenie', 'Florida State University'),
    ('IsoGenie', 'Pacific Northwest National Laboratory'),
    ('IsoGenie', 'Lawrence Berkeley National Laboratory'),
    ('IsoGenie', 'University of Stockholm'),
    ('IsoGenie', 'University of New Hampshire'),
    ('The Ohio State University', 'Virginia Rich'),
    ('The Ohio State University', 'Matthew Sullivan'),
    ('University of Arizona', 'Scott Saleska'),
    ('University of Arizona', 'Bonnie Hurwitz'),
    ('University of Queensland', 'Gene Tyson'),
    ('Florida State University', 'Jeffrey Chanton'),
    ('Florida State University', 'Bill Cooper'),
    ('Pacific Northwest National Laboratory', 'Malak Tfaily'),
    ('Lawrence Berkeley National Laboratory', 'Eoin Brodie'),
    ('Lawrence Berkeley National Laboratory', 'William Riley'),
    ('University of Stockholm', 'Patrick Crill'),
    ('University of New Hampshire', 'Steve Frolking'),
    ('University of New Hampshire', 'Ruth Varner'),
    ('University of New Hampshire', 'Changsheng Li')
]

properties = {
    'IsoGenie': {
        'name': 'IsoGenie',
        'link': 'http://isogenie.osu.edu/',
        'img': '/images/IsoGenie_logo.png',
        'size': 1000,
        'imgsize': 125,
        'focus': ''
    },
    'The Ohio State University': {
        'name': 'Ohio State University',
        'link': '',
        'img': '/images/OSU_logo.jpg',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'University of Arizona': {
        'name': 'University of Arizona',
        'link': '',
        'img': '/images/UA_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'University of Queensland': {
        'name': 'University of Queensland',
        'link': '',
        'img': '/images/UQ_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'Florida State University': {
        'name': 'Florida State University',
        'link': '',
        'img': '/images/FSU_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'Pacific Northwest National Laboratory': {
        'name': 'Pacific Northwest National Laboratory',
        'link': '',
        'img': '/images/PNNL_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'Lawrence Berkeley National Laboratory': {
        'name': 'Lawrence Berkeley National Laboratory',
        'link': '',
        'img': '/images/LBNL_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'University of Stockholm': {
        'name': 'University of Stockholm',
        'link': '',
        'img': '/images/Stockholm_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'University of New Hampshire': {
        'name': 'University of New Hampshire',
        'link': '',
        'img': '/images/UNH_logo.png',
        'size': 1000,
        'imgsize': 100,
        'focus': ''
    },
    'Virginia Rich': {
        'name': 'Virginia Rich',
        'link': 'https://microbiology.osu.edu/people/rich.270',
        'img': '/images/people/Rich_face.jpg',
        'size': 750,
        'imgsize': 50,
        'focus': 'My lab studies how microbes respond to, and in turn help shape, environmental change. We are '
                 'particularly interested in global change interactions with biogeochemical cycling. Our lens into '
                 'microbial community composition and function is through molecular "meta-omics" tools, which we bring'
                 ' to robust interdisciplinary collaborations with biogoechemists and modelers to generate a '
                 'systems-level understanding of ecosystems undergoing change.',
        'location': 'Ohio State University'
    },
    'Matthew Sullivan': {
        'name': 'Matthew Sullivan',
        'link': 'https://microbiology.osu.edu/people/sullivan.948',
        'img': '/images/people/Sullivan_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Dr. Matthew B. Sullivan\'s research focus is on the co-evolution of microbe and virus (phage) in'
                 ' environmental populations, as well as the impact of marine phages on microbe-mediated global '
                 'biogeochemistry. Genomics and model-systems-based experimentation revealed that cyanobacterial '
                 'phages often contain host photosynthesis genes, which are expressed during infection and act as a '
                 'diversity generator for their numerically-dominant, globally-distributed phtosynthetic hosts. Using'
                 ' a genomic and metagenomic toolkit, we query \'wild\' viral populations to identify important '
                 'hypotheses that can be evaluated using model-system approaches with appropriate phage isolates.',
        'location': 'Ohio State University'
    },
    'Scott Saleska': {
        'name': 'Scott Saleska',
        'link': 'http://eeb.arizona.edu/people/dr-scott-saleska',
        'img': '/images/people/Saleska_face.jpg',
        'size': 750,
        'imgsize': 50,
        'focus': 'My research focuses on what might be called "biogeochemical ecology," asking questions about how '
                 'climate interacts with plant physiology, demography, and ecological processes to influence or control'
                 ' biogeochemical cycling from local to global scales. Just one example of the need for more complete '
                 'understanding in this area is the lack of species interactions in modern global climate models, even '
                 'though such interactions can be critically important in controlling ecosystem carbon cycling and '
                 'hence, feedbacks to climate. Progress has been limited by the difficulty of bridging the gap between'
                 ' local-scale ecological interactions and broader biogeochemical processes. I use multidisciplinary '
                 'approaches that combine classical techniques of field ecology and forestry with advanced '
                 'technological methods (e.g., the micrometeorological eddy covariance method, isotopic techniques) and'
                 ' modeling to integrate biogeochemical processes to ecosystem scales.',
        'location': 'University of Arizona'
    },
    'Bonnie Hurwitz': {
        'name': 'Bonnie Hurwitz',
        'link': 'http://www.hurwitzlab.org/',
        'img': '/images/people/Hurwitz_face.jpeg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Dr. Bonnie Hurwitz is an Assistant Professor of Biosystems Engineering at the University of Arizona'
                 ' and Bio5 Research Institute Fellow. She has worked as a computational biologist for nearly two '
                 'decades on interdisciplinary projects in both industry and academia. Her research on the '
                 'human/earth microbiome incorporates large-scale -omics datasets, high-throughput computing, '
                 'and big data analytics towards research questions in "One Health". In particular, Dr. Hurwitz is '
                 'interested in the relationship between the environment, microbial communities, and their hosts. '
                 'Dr. Hurwitz is well-cited for her work in computational biology in diverse areas from plant genomics'
                 ' to viral metagenomics with over 1700 citations.',
        'location': 'University of Arizona'
    },
    'Gene Tyson': {
        'name': 'Gene Tyson',
        'link': 'http://ecogenomic.org/users/gene-tyson',
        'img': '/images/people/Tyson_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Professor Gene Tyson is a microbial ecologist whose research applies culture-independent molecular '
                 'approaches to understand the structure and function of microbial communities in the environment. '
                 'Professor Tyson\'s group at the University of Queensland is using metagenomic and metatranscriptomic'
                 ' approaches he, as a graduate student and post-doctoral researcher, helped pioneer to investigate '
                 'microbial communities in a wide range of different communities in both engineered systems and '
                 'natural environments.',
        'location': 'University of Queensland'
    },
    'Jeffrey Chanton': {
        'name': 'Jeffrey Chanton',
        'link': 'http://eoas.fsu.edu/people/faculty/dr-jeff-chanton',
        'img': '/images/people/Chanton_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'My research focus is fairly broad but follows the focus of either methane and carbon dioxide '
                 'production, emission and cycling, and/or stable isotope analysis. At this time I am working in six '
                 'different areas: (1) Wetlands, looking at permafrost decomposition in the northern boreal zone; (2)'
                 ' Food webs, learning about trophic relationships in estuaries; (3) Reducing methane emissions to the'
                 ' atmosphere, designing landfill cover soils which promote the growth of methane-consuming bacteria; '
                 '(4) Methane gas hydrates, which some estimate may be a large reservoir of fossil fuel to be mined; '
                 '(5) Pine forests, which can be large sinks for excess CO2; and (6) Groundwater discharge, an '
                 'overlooked process which is important to the nutrient budgets of coastal waters.',
        'location': 'Florida State University'
    },
    'Bill Cooper': {
        'name': 'Bill Cooper',
        'link': 'https://www.chem.fsu.edu/~cooper/',
        'img': '/images/people/Cooper_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Research in our group is focused on the organic geochemistry of natural waters and watersheds. '
                 'This includes studies of the biogeochemical reactivity of naturally occurring organic compounds '
                 '(i.e. "humic substances"), as well as the development of more powerful spectral identification '
                 'methods for the complex mixtures commonly found in nature. Water bodies are dynamic, and the '
                 'ecological impacts and ultimate fate of anthropogenic chemical contaminants introduced into them are'
                 ' largely dependent on their reactions with the "natural" constituents of the watershed. '
                 'Unfortunately, the underlying chemical principles governing these reactions are largely unknown, '
                 'primarily because of a lack of sufficiently powerful analytical techniques. Our goal is a detailed '
                 'understanding of the environmental geochemistry of surface and ground waters, with a major emphasis '
                 'on development and application of new analytical techniques.',
        'location': 'Florida State University'
    },
    'Malak Tfaily': {
        'name': 'Malak Tfaily',
        'link': 'https://www.emsl.pnl.gov/emslweb/people/malak-tfaily',
        'img': '/images/people/Tfaily_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Dr. Malak Tfaily is a staff scientist in the mass spectrometry group in EMSL. She has been assisting'
                 ' EMSL users in implementing the next-generation state-of-the-art mass spectrometry, and infrared and'
                 ' fluorescence spectroscopy techniques to understand carbon (C) cycling in the environment. Dr. Tfaily'
                 ' has extensive experience especially in applying Ion cyclotron resonance Mass Spectrometry to '
                 'characterizing organic matter composition from different terrestrial and aquatic systems, and helping'
                 ' users interpret these data to enrich the understanding of biotic and abiotic processes that impact '
                 'carbon dynamics such as microbial respiration, photo degradation, and anthropogenic activities. She '
                 'is also interested in investigating the effect of climate change on C dynamics in the terrestrial '
                 'ecosystem. Dr. Tfaily has recently developed extraction protocols for the characterization of carbon'
                 ' compounds in soil organic matter (SOM), thereby providing the chemical and structural detail needed'
                 ' to develop mechanistic descriptions of soil carbon flow processes',
        'location': 'Pacific Northwest National Laboratory'
    },
    'Eoin Brodie': {
        'name': 'Eoin Brodie',
        'link': 'http://eesa.lbl.gov/profiles/eoin-brodie/',
        'img': '/images/people/Brodie_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Eoin Brodie is a Senior Scientist in the Ecology Department of Berkeley Lab\'s Earth and '
                 'Environmental Sciences Area (EESA). Dr. Brodie serves as the Deputy Director of the Climate and '
                 'Ecosystem Sciences Division, Program Domain Lead for Environmental and Biological Systems Sciences'
                 ' and co-lead of the labwide Microbes-to-Biomes initiative. At the University of California, Berkeley,'
                 ' Dr. Brodie is an Adjunct Assistant Professor in the Department of Environmental Science, Policy and'
                 ' Management. He obtained his Ph.D. from University College Dublin in Ireland and joined LBNL '
                 'following postdoctoral research at UC Berkeley.',
        'location': 'Lawrence Berkeley National Laboratory'
    },
    'William Riley': {
        'name': 'William Riley',
        'link': 'http://eesa.lbl.gov/profiles/william-j-riley/',
        'img': '/images/people/Riley_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Bill focuses on modeling terrestrial ecosystems and their interactions with climate and climate '
                 'change. He has a varied educational background, including degrees in mechanical and aerospace '
                 'engineering, physics, and civil and environmental engineering. His published work includes '
                 'development, testing, and application of numerical models that represent soil microbial dynamics, '
                 'effects of abiotic processes such as mineral surface interactions, nutrient competition between '
                 'microbes and plants, watershed-scale hydrological and biogeochemical processes, and climate-scale '
                 'carbon and nutrient cycle processes.',
        'location': 'Lawrence Berkeley National Laboratory'
    },
    'Patrick Crill': {
        'name': 'Patrick Crill',
        'link': 'http://www.geo.su.se/index.php?option=com_jbpeople&view=profile&uid=51',
        'img': '/images/people/Crill_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Patrick Crill is a Professor of Biogeochemistry and Director of the Geochemistry program at Stockholm'
                 ' University. He has broad academic and research interests mostly concerning biological mediation of '
                 'element cycles in an effort to understand local, or even molecular, scale processes and their impact '
                 'on planetary systems. He has focused on trace gases and atmosphere/biosphere exchange particularly of'
                 ' CH4, CO2 and N2O and impacts of climatic change and landscape scale processes (such as thawing '
                 'permafrost or forestry) on the biogeochemistry sequestration, formation and exchange of carbon with '
                 'soils, peat, lacustrine and marine environments. Patrick also has research interests in innovative '
                 'use of novel technologies in the development of field measurement methodologies, urban metabolism and'
                 ' the biogeochemistry of CH3Br, CH3Cl and other methyl halogens. He has had the immense pleasure that,'
                 ' over the years, he has met many very smart people who have showed him incredibly amazing things. He'
                 ' collaborates with a broad range of experts from different disciplines from many different '
                 'countries.',
        'location': 'University of Stockholm'
    },
    'Steve Frolking': {
        'name': 'Steve Frolking',
        'link': 'https://ceps.unh.edu/faculty/frolking',
        'img': '/images/people/Frolking_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Steve Frolking is Research Professor of Earth System Science in the Earth Systems Research Center at '
                 'the University of New Hampshire. Frolking earned a B.S./M.S. in Physics, and a Ph.D. in Earth '
                 'Sciences/Biogeochemistry, all from UNH, and was a post-doc in NOAA\'s Program in Climate and Global '
                 'Change. Frolking\'s research emphasis is on the interaction between natural and/or managed '
                 'terrestrial ecosystems and the climate system (e.g., through greenhouse gas emissions), in the '
                 'context of climate and global change. His current research is focused in several areas within the '
                 'broader field of Earth System Science: (1) peatland carbon balance and long-term carbon accumulation,'
                 ' including consequences of land use and permafrost thaw; (2) water resources and sustainability in '
                 'the 21st century; (3) global land use, including carbon and nitrogen cycling in agroecosystems at '
                 'local to regional to global scales; (4) remote sensing analyses of urban heat islands; and (5) remote'
                 ' sensing analyses of drought impacts on tropical forests. Research tools include ecosystem and '
                 'hydrological modeling, remote sensing, and data analysis, and generally involve interdisciplinary '
                 'collaborations.',
        'location': 'University of New Hampshire'
    },
    'Ruth Varner': {
        'name': 'Ruth Varner',
        'link': 'http://www.eos.sr.unh.edu/Faculty/Varner',
        'img': '/images/people/Varner_face.jpg',
        'size': 500,
        'imgsize': 50,
        'focus': 'Ruth Varner is an Associate Professor in the Earth Systems Research Center of the Institute for the'
                 ' Study of Earth, Oceans, and Space, and in the Department of Earth Sciences. She is also the '
                 'Director of the Joan and James Leitzel Center for Mathematics, Science, and Engineering Education at'
                 ' the University of New Hampshire. Her research experience at UNH began with developing a gas '
                 'chromatography/cryo-focusing technique to quantify methyl bromide in ambient air samples. This work '
                 'led to the discovery of soil as a significant biological sink of atmospheric methyl bromide. This '
                 'research also led to the discovery of freshwater wetlands as a source of methyl bromide and methyl '
                 'chloride to the atmosphere. Her work at UNH also includes carbon dioxide, methane and nitrous oxide'
                 ' exchange using autochamber technology in terrestrial ecosystems: boreal (BOREAS, NASA), temperate '
                 'and tropical forests (LBA, NASA). Currently, her research focus is on the measurement of trace gas '
                 'emissions from agricultural and wetland ecosystems with funded projects from the USDA, USGS and NSF.',
        'location': 'University of New Hampshire'
    },
    'Changsheng Li': {
        'name': 'Changsheng Li',
        'link': '',
        'img': '',
        'size': 500,
        'imgsize': 50,
        'focus': 'Changsheng Li of the Institute for the Study of Earth, Oceans, and Space (EOS) traveled the globe '
                 'for more than two decades, gathering data from scientists, farmers, and field workers to build the'
                 ' DeNitrification-DeComposition (DNDC) ecosystem model. This model simulates greenhouse gas emissions'
                 ' across a variety of terrestrial ecosystems and climatic conditions, helpng in efforts to curb global'
                 ' warming. Professor Li collaborated with researchers from numerous countries and there are now '
                 'versions of the DNDC model tailored to accurately mirror large, regional ecosystems. Professor Li '
                 'worked at UNH from 1992 to 2015 and returned to China periodically to collaboratively improve '
                 'agricultural practices for better environmental outcomes based on his research.',
        'location': 'University of New Hampshire'
    }
}

graph = nx.DiGraph()

graph.add_node('IsoGenie', {
    'name': 'IsoGenie',
    'size': 10000,
    'img': '/images/IsoGenie_logo.png',
    'imgsize': 150,
    'link': 'https://isogenie.osu.edu/'
})

for (edge1, edge2) in edges:
    graph.add_node(edge1, properties[edge1])
    graph.add_node(edge2, properties[edge2])
    graph.add_edge(edge1, edge2)

j = json_graph.tree_data(graph, root='IsoGenie')

json_fn = os.path.join(isogenie_web_publicimages_dir, 'IsogenieDB-People-Network.json') # isogenie_web_publicimages_dir from local_file_paths.py
with open(json_fn, 'w') as json_fh:
    json.dump(j, json_fh, indent=4)
