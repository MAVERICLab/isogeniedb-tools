# Photo Tagging Overview

Note: Most metadata handling in these scripts is implemented with the [ExifTool](https://exiftool.org/) command-line application (requires separate installation).

## Adding new photos

### Initial photo organization

1. After downloading and extracting the images from their source (Google Drive, etc.) into a centralized location, run ```list-raw-photos.py``` using that location as the ```--photo-root``` argument.

2. In the outputted CSV (Photos-Raw.csv), manually fill in the Photo Credit and Year columns, and re-save as "Photos-Raw-annotated.csv".

3. Run the script ```organize-photos.py``` to organize the photos into new directories by Photo Credit and Year (this also converts any non-JPEGs to JPEGs, replaces any pre-existing IPTC and XMP tags, and renames the files & folders to remove spaces). IMPORTANT: Before running with ```--copy-files```, first run it without this argument to ensure that the new file paths look as expected.

### Tagging

The recommended method of tagging photos is to use a GUI (such as XnViewMP; see below) to add tags to the embedded IPTC and XMP metadata, and then export those embedded tags to CSV for further use. The use of embedded metadata ensures that the tags "travel" with the images as they are shared. Photos can also be tagged using a CSV (as has been done in the past), but this means that the user must manually match opened image filenames to the appropriate line in the CSV (with resulting increased potential for human error), and those CSV tags should also still be embedded in the images themselves. To do this, the function ```update_embedded_photo_tags.py``` has been provided for embedding tags in individual image files, and can be called from a script that reads and gets tags from a CSV listing multiple images (TODO for tags on photos from years prior to 2017, listed in Photos-All_retagged.csv).

#### Tagging with XnViewMP

[XnViewMP](https://www.xnview.com/en/xnviewmp/) is recommended for this photo tagging pipeline due to its flexiblity, multi-platform compatibility (including Windows, MacOS, and Linux), and free licensing for private, non-profit, and educational use.

For the process below to work, the XnViewMP settings need to be tweaked as follows (this should only need to be done once):

* Settings -> Metadata -> Categories & keywords: Check all boxes except "Write categories into images as hierarchical keywords".

* [right-click a test image] -> Edit IPTC -> Options: Set "Mode" to "IPTC-IMM, update or create XMP". Then click "Write" to apply changes for all future tag edits.

* Settings -> Thumbnail -> Labels: Select the following properties (can also include other info if useful): "Filename", "IPTC: Byline", "IPTC: Headline", "IPTC: Keywords".


_To start tagging:_

In the "Categories" pane (bottom right), first delete any categories that were not created as part of this workflow (this is optional, but keeps the workspace clean). Then import the list of tags (tools/data/photo_tags_list.txt) using:

* [small down arrow at top right of Categories pane] -> Categories Management -> Import Categories File

You can also optionally organize these categories into "Category Sets" (e.g. different sets of tags for years, months, habitats, etc.). Currently, the only known way to do this is to create a new empty set (Category Sets -> Manager...) and then add tags to it one by one by typing in the box at the top. When done creating your sets, you can then drag them around the "Category Sets" tab to change the order, and then save the layout by clicking the "+" icon at the top right. You will then be able to access this layout in the future from your XnViewMP installation (though it's unknown how/whether it's possible to share the layout with others). This will not change the way in which the tags are implemented under the hood, but will make it visually easier to find the tags you need as you're tagging photos.

Then, update the Categories list with any IPTC and XMP tags that were previously applied (e.g. from running organize-photos.py). To do this, first go one directory above the one in which you're tagging photos, then select your tagging directory, and then select:

* View -> Update catalog from files

Now, when you go into the directory and select an image, all previously applied tags should be visible as checked boxes in the Categories list, as well as in the IPTC and XMP metadata.

_To add more tags:_

Simply select the relevant checkboxes from the Categories list, and they will automatically be applied to the IPTC and XMP metadata. You can also tag multiple files at once by selecting multiple files (using the CTRL and/or SHIFT keys on Windows or Linux) and then checking the relevant boxes; this will not affect any other tags on those images.

In addition to "category"-type tags, photos of cores and vegetation quadrats should also be labeled with the core name. This should be done using the "Headline" IPTC & XMP field, accessed by right-clicking the image, selecting "Edit IPTC...", and then entering the name in the "Headline" field (under the "Caption" tab). Core names should conform to a standard format, which includes the short site name (see tools/data/site_tag.json) and core number, separated by an underscore (to conform to the current standardized EMERGE Sample IDs; though previously a hyphen was used).

WARNING: Updating IPTC and XMP metadata overwrites the entire image file. Therefore, if you're tagging images from inside a directory with automatic cloud backup, it will use a LOT of data as it constantly tries to keep the files in sync. So you may want to pause your backups until you're done tagging for the day, so that only one file update needs to be pushed for each image. There may be workarounds for this in the XnViewMP settings, but these haven't been tested.

When done, export the tags to CSV:

1. Browse to one directory above the one containing the tagged images, and then select the directory.

2. From the top menu bar, go to Create --> File listing. 

3. In the dialog that appears, select Format "CSV", check "Include subfolders", uncheck "Only image files" and "Append to file", and create four fields at the top with the following values:

    * {Directory}{Filename With Ext}
   
    * {IPTC:Byline}
   
    * {IPTC:Headline}
   
    * {IPTC:Keywords}

4. Save it with a filename that includes the date range of the photos, i.e. "Photos-All_YYYY-YYYY.txt" (note: XnViewMP's "CSV" export actually delimits fields with ":" instead of ",", hence the ".txt" extension when saving).

### Merging newly-tagged photos (from XnViewMP) with the existing photo archive

After tagging photos and exporting the tags to a TXT file using XnViewMP (see previous step), run the script ```consolidate-tagged-photos.py``` to consolidate these newly-tagged photos with the existing photo archive. This script merges the exported tags list into the existing standardized tags list, moves those new images into the web directory, and creates thumbnails for these new images. See the documentation on that script for details on its use and implementation.

To update the Pictures webpage (until a new system is set up that automates this from the embedded IPTC metadata), first make sure the script ```Build-Pictures-HTML.py``` (in the "old" subdirectory) reflects the most current tagging info (including directory structure, tag names, and standardized tags list filename), and then run that script to generate HTML for the pictures list.

### Updating core and vegetation quadrat images in the graph DB

The script ```io_misc/Update_Core_Images.py``` (located outside this photo_tagging directory) reads the standardized tags list CSV (updated using the steps above), and uses it to update the Core nodes in the graph DB with core and vegetation quadrat image filepaths. See that script for details on how to use it.
