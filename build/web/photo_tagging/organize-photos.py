#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: suzanne

Description:
    This script organizes all the image files in a given directory into new folders by photo credit and year, using the
    file Photos-Raw-annotated.csv (manually edited from the Photos-Raw.csv file outputted by list-raw-photos.py) for
    getting this info. Additionally, non-JPEG images are converted to JPEG (preserving as much of the original file
    metadata as possible), and the initial tags from Photos-Raw-annotated.csv are embedded into the IPTC and XMP
    metadata for all files. This script also outputs a CSV (Photos-Organizing-Results.csv) showing the resulting new
    directory structures, as well as whether each file was copied (or intended to be copied).

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import argparse
import os
import sys
import subprocess
import pandas as pd
import numpy as np
from PIL import Image
import pillow_heif

# Most metadata handling is implemented with the ExifTool command-line application (requires separate installation).
# ExifTool documentation: https://exiftool.org/exiftool_pod.html

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..')))
from tools.utilities.utils import error, warning
from build.web.photo_tagging.update_embedded_photo_tags import update_embedded_photo_tags

parser = argparse.ArgumentParser(description="Reorganizes photos into directory for web.")
inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-s', '--source-dir', dest='source_dir',
                    default='/home/suzanne/IsoGenieDB/Photo-Tagging/New-Photos-Pre-Organization/',
                    help='Full path of source directory containing raw photos. This directory should also contain the '
                         'file Photos-Raw-annotated.csv in its root.')

inputs.add_argument('-d', '--dest-dir', dest='dest_dir',
                    default='/home/suzanne/IsoGenieDB/Photo-Tagging/New-Photos-Post-Organization/',
                    help='Full path of destination directory for putting organized and converted photos. '
                         'This directory can already have files in it; the new paths will simply be integrated into '
                         'the existing directory structure. By default, no existing files are replaced; this behavior '
                         'can be overridden with --allow-replace.')

inputs.add_argument('-c', '--copy-files', dest='copy_files', action='store_true',
                    help='Option for actually copying the files instead of just generating a CSV of the new directory '
                         'structures. To avoid accidentally copying the files before the new filepaths (shown in '
                         'Photos-Organizing-Results.csv) have been manually inspected, this option is NOT enabled by '
                         'default, but should be enabled only when ready.')

inputs.add_argument('-r', '--allow-replace', dest='allow_replace', action='store_true',
                    help="Allows for copying files that would replace existing files in the destination directory. "
                         "If not set, any new files that would replace existing files are ignored (with a warning). "
                         "To allow for inspecting the potential results of copying files before they're actually "
                         "copied, --allow-replace is honored when generating Photos-Organizing-Results.csv even if "
                         "--copy-files is not set.")



if __name__ == "__main__":

    args = parser.parse_args()
    pillow_heif.register_heif_opener()  # for opening .HEIC files painlessly

    source_list_fp = os.path.join(args.source_dir, 'Photos-Raw-annotated.csv')
    photos_df = pd.read_csv(source_list_fp, sep=',', quotechar='"')

    # Work from preset lists of allowed image extensions (assume there are none with a mix of caps and lowercase)
    # JPEG (these are copied as is)
    jpeg_image_extensions = ['.JPG', '.JPEG']
    jpeg_image_extensions += [ext.lower() for ext in jpeg_image_extensions]
    # non-JPEG (these are converted to JPG)
    non_jpeg_image_extensions = ['.HEIC', '.PNG']   # start with these for now; may need more later
    non_jpeg_image_extensions += [ext.lower() for ext in non_jpeg_image_extensions]

    #tag_cols = [col for col in photos_df.columns.tolist() if col != 'File Location']  # for later

    # preallocate new columns for output
    # also add "header" rows with arguments used?
    output_csv_cols = [
        'Source',
        'Destination',
        'Converted From',  # original file format if it's a non-JPEG image (.HEIC, .PNG), "no conversion" if already a JPEG (including its alternate spellings), or "NOT AN IMAGE" if it's not an image (in which case no conversion is performed)
        'Replaces Existing',  # True/False - whether or not copying the file would replace a file in the destination
        'To Be Copied'  # True/False. This is filled in the same way regardless of whether args.copy_files is set, and is determined by the value of 'Replaces Existing' in addition to args.allow_replace.
    ]
    for col in output_csv_cols:
        photos_df[col] = ''

    for year, year_df in photos_df.groupby(by='Year'):

        # Get the root directory for each year, for simplifying new filepaths
        src_year_root = os.path.commonpath(year_df['File Location'].tolist())
        print(year)
        print(src_year_root + '\n')

        for index, row in year_df.iterrows():

            # Record the full path of the source file location
            # Although looping through year_df, the index for photos_df should be the same
            # "File Location" = path relative to args.source_dir
            # "Source" = full file path
            old_fp = os.path.join(args.source_dir, row['File Location'])
            photos_df.loc[index, 'Source'] = old_fp

            # Generate new filepath from Photo Credit, Year
            unknown_author_str = 'Misc-and-Unknown'  # string used in existing web directory
            if pd.isnull(row['Photo Credit']):
                author = unknown_author_str
            elif row['Photo Credit'] in ['', 'NaN', np.nan]:  # probably overkill
                author = unknown_author_str
            else:
                author = row['Photo Credit']

            # Get last part of filepath ('File Location' minus src_year_root), the structure of which will stay the same
            new_fp_end = row['File Location'].replace(src_year_root + '/', '')  # ensure that new_fp_end has no leading '/'

            # Get file type, and if a non-JPEG image, replace current extension with '.JPG' (actual file formats will
            #  be converted later if args.copy_files is enabled)
            new_fp_end_components = os.path.splitext(new_fp_end)
            extension = new_fp_end_components[1]

            if extension in jpeg_image_extensions:
                photos_df.loc[index, 'Converted From'] = 'no conversion'
            elif extension in non_jpeg_image_extensions:
                new_fp_end = new_fp_end_components[0] + '.JPG'  # re-assign new_fp_end, replacing original extension
                photos_df.loc[index, 'Converted From'] = extension
            else:
                # new_fp_end stays the same, so no need to change
                photos_df.loc[index, 'Converted From'] = 'NOT AN IMAGE'

            # Put together the new filepath from the various parts
            try:
                new_fp = os.path.join(args.dest_dir, author, str(year), new_fp_end)
            except TypeError:  # invalid component?
                print("ERROR: One or more of the file path components below was found to be invalid:")
                print(args.dest_dir)
                print(author)
                print(str(year))
                print(new_fp_end)
                exit()

            # Replace spaces with underscores
            new_fp = new_fp.replace(' ', '_')

            # Record the new filepath
            photos_df.loc[index, 'Destination'] = new_fp

            # Check to see if the new filepath already exists
            replaces_existing = os.path.exists(new_fp)
            photos_df.loc[index, 'Replaces Existing'] = replaces_existing

            if not(replaces_existing) or args.allow_replace:
                to_copy = True
            else:
                to_copy = False
            photos_df.loc[index, 'To Be Copied'] = to_copy

            # Copy the files!
            # Do this only if args.copy_files is set, AND if to_copy==True.
            # This should include creating any directories that don't yet exist, converting file format if needed,
            #  and adding initial tags.
            if to_copy and args.copy_files:

                # First make the subdirectory if it doesn't already exist
                if not(os.path.exists(os.path.dirname(new_fp))):
                    os.makedirs(os.path.dirname(new_fp), mode=0o775)

                if extension not in non_jpeg_image_extensions:
                    # either already JPEG, or NOT AN IMAGE
                    # just copy the file as-is
                    cp_cmd = "cp '" + old_fp + "' " + new_fp
                    subprocess.check_call(cp_cmd, shell=True)
                else:
                    # image needing format conversion
                    photo = Image.open(old_fp)

                    # if png, cannot just use save; must first convert
                    if not photo.mode == 'RGB':
                        photo = photo.convert('RGB')

                    photo.save(new_fp)
                    photo.close()

                    # Copy original file's metadata to the converted file using ExifTool
                    # This helpfully writes exiftool output to this script's output.
                    # In this case, -overwrite_original refers to new_fp; it's OK to overwrite since it was only just
                    #  now created with no metadata.
                    exiftool_copy_cmd = "exiftool -tagsfromfile '" + old_fp + "' " + new_fp + " -overwrite_original"
                    subprocess.check_call(exiftool_copy_cmd, shell=True)

                # Add Photo Credit and Year tags to IPTC and XMP metadata (if it's an image).
                if extension in jpeg_image_extensions + non_jpeg_image_extensions:

                    if author == unknown_author_str:
                        author_tag = None
                    else:
                        author_tag = author

                    year_tag = str(year)

                    update_embedded_photo_tags(image_fp=new_fp, byline=author_tag, keywords=[year_tag],
                                               keyword_mode="replace", overwrite_original=True)

    # Make sure there are no duplicates in the new file paths (and give a warning if so; at this point the files have
    #  already been copied so this will just be denoted in the output CSV). This is done in addition to the behavior of
    #  args.allow_replace, as duplicate filepaths that aren't actually created (due to args.copy_files=False) won't be
    #  flagged by the 'Replaces Existing' column. Duplicate filepaths are unlikely, but possible from replacing spaces
    #  (e.g., two files in the same folder originally named 'IMG_1.jpg' and 'IMG 1.jpg').
    duplicate_fps = photos_df.duplicated(subset='Destination')
    if any(duplicate_fps):
        warning("Duplicate filepaths found; see column 'DUPLICATE' in Photos-Organizing-Results.csv.")
        photos_df['DUPLICATE'] = duplicate_fps

    # Write to CSV
    photos_list_new_fp = os.path.join(args.dest_dir, 'Photos-Organizing-Results.csv')
    photos_df.to_csv(photos_list_new_fp, sep=',', index=False)


