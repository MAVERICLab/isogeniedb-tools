#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: suzanne

Description:
    This script generates a list of all photos in a given directory to be tagged. It does not assume any specific
    directory structure or file format, but just outputs a CSV (Photos-Raw.csv) with the list of files and empty
    columns for initial tags (Photo Credit, Year). Once these initial tags are manually filled in, the script
    organize-photos.py then organizes the photos into new directories by contributor and year (which mirrors the web
    photo directory structure) for further tagging.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import argparse
import os
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser(description="Generates a list of all photos in a given directory for later organization and tagging.")
inputs = parser.add_argument_group('Inputs')
inputs.add_argument('--photo-root', dest='photo_root',
                    default='/home/suzanne/IsoGenieDB/Photo-Tagging/New-Photos-Pre-Organization/',
                    help='Full path of root directory containing photos.')

if __name__ == "__main__":

    args = parser.parse_args()

    photo_root = args.photo_root
    #tagging_root = os.path.join(photo_root, 'Tagging/')

    # Collect photos
    photos = []
    exclusions = ['.DS_', 'Thumbs.db', '_DS_Store']  # mac files
    for root, dirs, files in os.walk(photo_root):
        for file in files:
            if not any(exclusion in file for exclusion in exclusions):
                photos.append(os.path.join(root, file))

    photos_df = pd.DataFrame(data=photos, columns=['File Location'])
    photos_df['Photo Credit'] = np.nan
    photos_df['Year'] = np.nan

    # Sort
    photos_df.sort_values('File Location', inplace=True)

    # Get rid of long path name that won't be similar
    photos_df['File Location'] = photos_df['File Location'].str.replace(photo_root, '')

    # Write to CSV
    photos_list_fp = os.path.join(photo_root, 'Photos-Raw.csv')
    photos_df.to_csv(photos_list_fp, sep=',', index=False)
