#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: suzanne

Description:
    This script merges an exported list of embedded tags on new photos (from XnViewMP; see Photo-Tagging-Overview.md)
    into the existing standardized tags list, moves new images into the web directory, and creates thumbnails for these
    new images. Non-images and data sheet images (with the "Data Sheet" keyword) are ignored, as they are either in an
    incorrect format for the Pictures page, or are better suited for other sharing methods.
    TODO: Add functionality to be able to:
     - update tags list for images already in the web directory
     - better handle/organize non-images and Data Sheet images by putting them into new folders (instead of just
       leaving them in-place)?

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import argparse
import os
import sys
import subprocess
import pandas as pd
import numpy as np
from PIL import Image

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..')))
from tools.utilities.utils import error, warning
from tools.data.photo_tagging_vars import *

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

parser = argparse.ArgumentParser(description="Reorganizes photos into directory for web.")
inputs = parser.add_argument_group('Inputs')

inputs.add_argument('--new-tags-file', dest='new_tags_file',
                    help='File path (can be either absolute, or relative to this repository) of a colon (:)-delimited '
                         'TXT file containing the full file paths of newly-tagged photos, along with the tags on those '
                         'photos, exported from XnViewMP using the settings specified in Photo-Tagging-Overview.md.')

inputs.add_argument('--current-tags-csv', dest='current_tags_csv',
                    help='File path (can be either absolute, or relative to this repository) of the standardized photo '
                         'tags CSV, to be updated using this script. The filename should start with "Photos-All" and '
                         'end with "_standardized.csv", and be the currently-most-up-to-date version.')

inputs.add_argument('--new-tags-root', dest='new_tags_root', default='default',
                    help='Path to root directory for newly-tagged photos (including trailing /). This directory should '
                         'contain subdirectories organized by photo credit and year, analogous to the web photo '
                         'directory (web_dir).\n'
                         'If the newly-tagged photos listed within new_tags_file are already within web_dir, then '
                         'anything entered here will be ignored, and the root directory will be set to the web '
                         'directory (and no raw image files will be moved). This is done to protect the web directory '
                         'structure and the images contained within.\n'
                         'If the newly-tagged photos exist elsewhere, and --new-tags-root is not specified, then '
                         'os.path.commonpath([first column in new_tags_file]) (with a trailing /) is used as the root '
                         'directory. Note, it is possible for this commonpath to actually be a subdirectory of the '
                         '"true" new photos root directory (e.g., if all new photos are from the same creator and '
                         'year); hence this --new-tags-root argument exists to provide the ability to instead choose '
                         'a higher directory.')

inputs.add_argument('--web-dir', dest='web_dir',
                    default='/home/suzanne/IsoGenieDB/Bitbucket/isogenie-web/public/images/raw/',
                    help='Full path of the destination web photo root directory. This path must exist, and end with '
                         '"/raw/". It can either be empty, or already have files in it; any newly copied files will '
                         'be integrated into the existing directory structure. By default, no existing files are '
                         'replaced; this behavior can be overridden with --allow-replace.')

# inputs.add_argument('--extra-files-dir', dest='extra_files_dir',
#                     help='Full path of directory for putting non-images and Data Sheet images for later handling.')

inputs.add_argument('--move-files', dest='move_files', action='store_true',
                    help='Option for actually moving the files instead of just generating a CSV of the consolidated '
                         'photos. To avoid accidentally moving any files before the new filepaths (shown in '
                         'the output CSV) have been manually inspected, this option is NOT enabled by '
                         'default, but should be enabled only when ready.')

# inputs.add_argument('--allow-replace', dest='allow_replace', action='store_true',
#                     help="Allows for moving files that would replace existing files in the web directory. "
#                          "If not set, and new files are found that would replace existing files, the script exits "
#                          "with an error.")

inputs.add_argument('--output-csv-name', dest='output_csv_name',
                    help='Filename of the finished CSV to output. This filename should ideally be in a similar format '
                         'as current_tags_csv; but it CANNOT be identical. This file will be written into the same '
                         'folder as current_tags_csv.')


if __name__ == "__main__":

    args = parser.parse_args()

    # use keep_default_na=False to avoid errors from unexpected NaN formats
    current_tags_df = pd.read_csv(args.current_tags_csv, sep=',', quotechar='"', dtype=object, keep_default_na=False)
    new_tags_df = pd.read_csv(args.new_tags_file, sep=':', quotechar='"', dtype=object, keep_default_na=False)

    # simplify column names (choosing names that match current_tags_df, if applicable)
    rename_cols_new_tags = {
        'DirectoryFilename With Ext': 'filepath',
        'IPTC:Byline': 'Photo Credit',
        'IPTC:Headline': 'Core Code',
        'IPTC:Keywords': 'keywords'
    }
    new_tags_df.rename(columns=rename_cols_new_tags, inplace=True)

    # First verify the directories: Verify that web_dir looks OK, identify the "true" new tags root directory, and see
    # if the new tags are updating existing files already in the web directory (or if they're entirely new photos).

    if not(os.path.exists(args.web_dir)) or not(args.web_dir.endswith('/raw/')):
        error('Invalid --web-dir. The directory must exist, and must end with "/raw/".')

    updating_existing_images = False  # this will change to True if the newly-tagged images are found to be in web_dir

    # Get the commonpath (and join with '' in order to get trailing /)
    new_tags_commonpath = os.path.join(os.path.commonpath(new_tags_df['filepath'].tolist()), '')

    if new_tags_commonpath.startswith(args.web_dir):  #args.web_dir in new_tags_commonpath:
        new_tags_dir = args.web_dir
        updating_existing_images = True

        if args.new_tags_root != 'default':
            warning("Ignoring user-specified --new-tags-root, as the web directory (--web-dir) is already a parent "
                    "of the lowest common path in --new-tags-file. Using --web-dir as the root.")

    elif args.new_tags_root == 'default':
        new_tags_dir = new_tags_commonpath

    elif new_tags_commonpath.startswith(args.new_tags_root):  #args.new_tags_root in new_tags_commonpath:
        new_tags_dir = args.new_tags_root

    else:
        new_tags_dir = 'NaN'
        error("Bad --new-tags-root argument; this should be a parent directory of all the paths "
              "listed in the new tags file.")

    new_tags_df['File Location'] = [path.replace(new_tags_dir, '') for path in new_tags_df['filepath'].tolist()]

    # Verify that the output CSV filename is OK
    if args.output_csv_name == os.path.basename(args.current_tags_csv):
        error("The output CSV filename cannot be the same as the current_tags_csv filename.")

    # Define thumbnails directory
    thumbs_dir = args.web_dir.replace("/raw/", "/thumbnails/")

    # Now we can safely go through the newly-tagged images.
    # Consolidate the tags CSV before moving any files, in case of errors with the tags or filepaths.
    if updating_existing_images:
        # TODO: If updating tags of photos already in web_dir, then this workflow is different from adding completely
        #  new photos, and involves just updating the corresponding existing row in the current tags CSV.
        error("Attempting to update existing images in the web directory; this functionality is not yet set up.")

    else:  # Completely new photos to add

        # First, make sure the parts of the filepaths after new_tags_dir look like "CREATOR/YEAR/path-to-file".

        files_in_dirs = [path for path in new_tags_df['File Location'].tolist() if '/' in path]  # excludes top-level files
        if len(files_in_dirs) == 0:
            error("Bad new photo root directory; it must contain subdirectories, organized by "
                  "name/year/[remaining path to file].")

        bad_path_msg = "The new photo root directory contains at least one incorrectly-named subdirectory. " \
                       "The second component of the path must be a year between 2000-2100. Bad path:\n{}"

        for path in files_in_dirs:
            year_component = path.split('/')[1]
            try:
                year = int(year_component)
                if 2000 <= year < 2100:  # make sure it's a 21st-century year
                    continue
                else:
                    error(bad_path_msg.format(path))
            except ValueError:
                error(bad_path_msg.format(path))

        # Next, make sure there are no new filepaths that would replace existing images (may later allow this with args.allow_replace).
        duplicate_fps = [fp for fp in new_tags_df['File Location'].tolist() if fp in current_tags_df['File Location'].tolist()]

        if len(duplicate_fps) > 0:
            print(duplicate_fps)
            error("New images would replace existing ones already in web_dir (see list above).")

        # Now we can update the tags. First do this within new_tags_df.
        ok_to_move_files = args.move_files  # if True, this will change to False if an error is found

        for index, row in new_tags_df.iterrows():
            if (row['keywords']=='') or ('Data Sheet' in row['keywords']):
                # Only process images with keywords, and only if those keywords don't include "Data Sheet" (data sheets
                # will be dealt with separately, by packaging with their corresponding coring sheet datasets).
                # TODO: Need to move these to a dedicated folder, organized based on whether they're Data Sheets, or
                #  misc. non-image files.
                new_tags_df.loc[index, '_to_process'] = False
                continue

            new_tags_df.loc[index, '_to_process'] = True  # if False, wouldn't get to this point

            # Make sure the thumbnail wouldn't replace any existing thumbnails
            # TODO: THIS DOES NOT CHECK WHETHER DUPLICATE THUMBNAIL FILENAMES EXIST *WITHIN* THE NEW IMAGE LIST; SOME
            #  DUPLICATES DO EXIST IN 2017-2021 IMAGES!!! THIS WAS DISCOVERED TOO LATE AFTER HAVING ALREADY MOVED FILES
            #  AND CREATED THUMBNAILS; NEED TO FIGURE OUT A WAY TO FIX.
            if os.path.exists(os.path.join(thumbs_dir, os.path.basename(row['File Location']))):
                warning("The following image already has a thumbnail, which would be replaced: {}".format(row['File Location']))
                ok_to_move_files = False

            keywords = row['keywords'].split(',')
            for keyword in keywords:
                if keyword in keywords_tags_all['_general']:
                    new_tags_df.loc[index, keyword] = 'X'
                elif keyword in keywords_tags_all['Month']:
                    new_tags_df.loc[index, 'Month'] = keyword
                elif keyword in keywords_tags_all['Year']:
                    new_tags_df.loc[index, 'Year'] = keyword
                elif keyword in keywords_tags_all['Season']:
                    new_tags_df.loc[index, 'Season'] = keyword
                elif keyword in keywords_tags_all['Habitat']:
                    new_tags_df.loc[index, 'Habitat'] = keyword
                else:
                    print(row['File Location'])
                    warning('Unrecognized keyword: "{}"'.format(keyword))
                    ok_to_move_files = False

        # Get subset of files that will be processed
        images_to_move_df = new_tags_df.loc[new_tags_df['_to_process']]
        print('{} new images out of {} files will be combined with the existing list.'.format(len(images_to_move_df),
                                                                                              len(new_tags_df)))

        # Before moving files (even if --move-files wasn't specified), check to make sure that no filepaths contain
        #  single quotes
        if any(["'" in path for path in images_to_move_df['filepath'].tolist()]):
            warning("One or more new images contains an illegal single quote (') character. Check the output CSV, rename "
                    "any bad files, and rerun.")
            ok_to_move_files = False

        # Now move the files and create thumbnails! (if OK)
        if ok_to_move_files:
            for index, row in images_to_move_df.iterrows():
                source_fp = row['filepath']
                dest_fp = os.path.join(args.web_dir, row['File Location'])

                # First make the subdirectory if it doesn't already exist
                if not (os.path.exists(os.path.dirname(dest_fp))):
                    os.makedirs(os.path.dirname(dest_fp), mode=0o775)

                # Move the file (filepaths enclosed in single quotes, thus ensuring that anything that worked with
                #  update_embedded_photo_tags() will also work here; paths were also checked previously)
                mv_cmd = "mv '" + source_fp + "' '" + dest_fp + "'"
                subprocess.check_call(mv_cmd, shell=True)
                images_to_move_df.loc[index, '_file_exists'] = True

                # Create thumbnail
                thumb_fp = os.path.join(thumbs_dir, os.path.basename(dest_fp))
                image = Image.open(dest_fp)
                image.thumbnail((300, 1000))  # use disproportionately large height to keep all widths at 300px
                image.save(thumb_fp)
                images_to_move_df.loc[index, '_thumbnail_exists'] = True

        elif not(args.move_files):
            print("No files were moved due to --move-files not set.")

        elif args.move_files and not(ok_to_move_files):
            warning("Even though --move-files was specified, no files were moved due to issue(s) found. Check the "
                    "preceding output.")
        else:
            print(args.move_files)
            print(ok_to_move_files)
            error("Unrecognized ok_to_move_files logic.")


        # Now combine the new tags with the existing tags
        combined_df = pd.concat([current_tags_df, images_to_move_df], ignore_index=True)

        # remove unnecessary columns
        cols_to_remove = ['_to_process', 'keywords', 'filepath']
        combined_df.drop(columns=cols_to_remove, inplace=True)

        # Write combined tags list to CSV
        output_csv_fullpath = os.path.join(os.path.split(args.current_tags_csv)[0], args.output_csv_name)
        combined_df.to_csv(output_csv_fullpath, sep=',', index=False)


