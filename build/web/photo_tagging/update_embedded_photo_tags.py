#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: suzanne

Description:
    This is a reusable function for adding IPTC and XMP tags to images using the ExifTool command-line tool. Tags are
    written to the corresponding IPTC and XMP fields (similar to the behavior of the XnViewMP GUI when editing IPTC-IMM
    with Mode: "IPTC-IMM, update or create XMP"). This function includes the option of either replacing or appending to
    the existing keyword lists, with similar flexibility also planned for the other fields.
    Argument names in this function match the corresponding IPTC tags.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import subprocess


def update_embedded_photo_tags(image_fp, keywords=None, byline=None, headline=None, keyword_mode="append",
                               overwrite_original=False, dry_mode=False):
    # image_fp:     Filepath of the image to update (assumes it's a JPEG; filepaths with spaces are OK here although
    #               this is generally bad practice)
    # keywords:     A list of strings.
    # byline:       String.
    # headline:     String.
    # keyword_mode: "append" (default; this appends the new keywords to the existing ones, avoiding duplicates) or
    #               "replace" (replaces all keywords with the new ones).
    # overwrite_original:  If True, appends -overwrite_original to the end of the command (thus avoiding the creation of duplicate files).
    # dry_mode:     Runs the function in "dry mode", printing the resulting exiftool command instead of running it.
    # Note on defaults: keywords, byline, and headline are None by default (this does not affect existing fields).
    #  To delete these fields, pass an empty list to keywords (and use keyword_mode="replace"),
    #  or an empty string to byline or headline.

    if ((keywords is None) or (keywords == [] and keyword_mode != "replace")) and (byline is None) and (headline is None):
        # no changes specified
        return  # do nothing

    cmd = "exiftool "

    # Byline (called "by-line" with hyphen in IPTC, "creator" in xmp)
    if byline is None:
        pass
    elif not(type(byline) == str):
        print(image_fp)
        print("Invalid byline (must be a string):")
        print(byline)
        exit()
    elif byline == "":
        cmd += " -by-line= -xmp:creator="  # deletes these
    else:
        cmd += " -by-line='" + byline + "' -xmp:creator='" + byline + "'"

    # Headline (same name in IPTC and XMP)
    if headline is None:
        pass
    elif not(type(headline) == str):
        print(image_fp)
        print("Invalid headline (must be a string):")
        print(headline)
        exit()
    elif headline == "":
        cmd += " -headline="  # deletes
    else:  # One "headline" tag updates both IPTC and XMP
        cmd += " -headline='" + headline + "'"

    # Keywords
    # if single string, convert to list
    if type(keywords) == str:
        keywords = [keywords]

    if keywords is None:
        pass
    elif not(type(keywords) == list):
        print(image_fp)
        print("Invalid keywords (must be a list of strings):")
        print(keywords)
        exit()
    elif keywords == [] and keyword_mode == "replace":  # required options for user-specified deletion of keywords
        cmd += " -keywords= -xmp:subject= -xmp:hierarchicalSubject="  # deletes
    else:
        for keyword in keywords:
            if type(keyword) == str and keyword != "":
                if keyword_mode == "replace":
                    cmd += " -keywords='" + keyword + "' -xmp:subject='" + keyword + "' -xmp:hierarchicalSubject='" + keyword + "'"
                elif keyword_mode == "append":
                    # If appending keywords, this must be done in two parts (see https://exiftool.org/faq.html#Q17):
                    # (1) delete them if they already exist using -= (this prevents duplicates)
                    cmd += " -keywords-='" + keyword + "' -xmp:subject-='" + keyword + "' -xmp:hierarchicalSubject-='" + keyword + "'"
                    # (2) append the keywords using +=
                    cmd += " -keywords+='" + keyword + "' -xmp:subject+='" + keyword + "' -xmp:hierarchicalSubject+='" + keyword + "'"
                else:
                    print("Invalid keyword_mode:")
                    print(keyword_mode)
                    exit()
            else:
                print("Invalid keyword (must be a non-empty string):")
                print(keyword)
                exit()

    # specify the file to edit
    cmd += " '" + image_fp + "'"

    # overwrite original (if specified)
    if overwrite_original:
        cmd += " -overwrite_original"

    # run the command (or print it if dry_mode)
    if dry_mode:
        print(cmd)
    else:
        subprocess.check_call(cmd, shell=True)
