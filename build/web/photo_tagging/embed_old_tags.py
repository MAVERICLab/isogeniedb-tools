"""
@author: suzanne

Description:
    This script takes the most recent version of the photo tags list for 2010-2016 photos (Photos-All_retagged.csv)
    and uses it to embed those tags into the IPTC and XMP metadata. This includes re-standardizing core names to match
    the standardized SampleID__ nomenclature before embedding them in the IPTC & XMP "headline" attributes. Some tags
    (i.e., those not previously used on the website) are not embedded, either due to privacy considerations (e.g. names
    of people in photos), or due to lack of adequate standardization in original tags (e.g. site names).
    In addition, any non-JPEG images are converted to JPEG (retaining original files until they can be safely deleted).
    In addition to embedding metadata, the script also outputs a "standardized" version of the tags CSV that includes
    the revised core names, new filepaths of any converted files, and re-ordered tag columns based on which ones were
    actually embedded in the images.
    By default, the image files are NOT actually modified unless --edit-files is enabled, but the standardized tags CSV
    is generated (and JPEG conversions checked for creation of duplicates) for inspection of the tags before they're
    actually embedded.
    This script should only need to be run once successfully; all new photos will be processed via a new pipeline
    (see Photo-Tagging-Overview.md) that updates the embedded tags FIRST before exporting them to CSV.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import argparse
import os
import sys
import subprocess
import pandas as pd
import numpy as np
from PIL import Image
import pillow_heif

# Most metadata handling is implemented with the ExifTool command-line application (requires separate installation).
# ExifTool documentation: https://exiftool.org/exiftool_pod.html

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..')))
from tools.utilities.utils import error, warning
from build.web.photo_tagging.update_embedded_photo_tags import update_embedded_photo_tags
from tools.data.photo_tagging_vars import *
from tools.data.common_vars import *

parser = argparse.ArgumentParser(description="Embeds the tags from Photos-All_retagged.csv into the photos themselves.")
inputs = parser.add_argument_group('Inputs')

inputs.add_argument('--photo-root', dest='photo_root',
                    default='/home/suzanne/IsoGenieDB/Bitbucket/isogenie-web/public/images/raw/',
                    help='Full path of web directory (should end in "/raw/") containing photos to be processed.')

inputs.add_argument('--tags-csv', dest='tags_csv',
                    default='test_data/IsoGenie_datasets/private_data/misc/Photos-All_retagged.csv',
                    help='Path of the CSV containing photo tags (can be either absolute, or relative to this repository).')

inputs.add_argument('--allow-replace', dest='allow_replace', action='store_true',
                    help="When renaming files or creating JPEG versions of non-JPEG images, this allows for replacing "
                         "existing files with the same names (if any). This does not apply to renaming/editing "
                         "thumbnails, which have all been manually verified to be OK to rename. "
                         "If not set, then before attempting to update any tags, the script first checks to see if "
                         "creating new JPEGs would replace any existing files (and if so, the script returns an error "
                         "with a list of the problematic filenames).")

inputs.add_argument('--edit-files', dest='edit_files', action='store_true',
                    help='Option for actually embedding tags in the image files instead of just generating the '
                         'standardized CSV. To avoid accidentally modifying the image files before the standardized '
                         'tags have been manually inspected, this option is NOT enabled by default, but should be '
                         'enabled only when ready.')

inputs.add_argument('--edit-thumbs', dest='edit_thumbs', action='store_true',
                    help='Option for editing the corresponding thumbnail files for images that require reformatting or '
                         'renaming. To avoid accidentally modifying the image files before the standardized '
                         'tags have been manually inspected, this option is NOT enabled by default, but should be '
                         'enabled only when ready.')



if __name__ == "__main__":

    args = parser.parse_args()
    pillow_heif.register_heif_opener()  # for opening .HEIC files painlessly

    # Read tags CSV (keep_default_na=False ensures that empty cells are read as "", not NaN)
    photos_df = pd.read_csv(args.tags_csv, sep=',', quotechar='"', dtype=object, keep_default_na=False)

    # Preallocate new columns for output - Add to these lists as more are found.
    # NOTE: To keep the output clean, some columns should be removed before final output.

    # renamed old columns (to replace with new columns of the same name for standardized output)
    cols_to_redo = [
        'File Location',
        'Month',
        #'Year',
        'Season',
        'Habitat',
        'Core Code'
    ]
    for col in cols_to_redo:
        photos_df.rename(columns={col: (col + '_OLD')}, inplace=True)
        photos_df[col] = ''

    # completely rename "Sampling Grid" tag column to "Quadrat"
    photos_df.rename(columns={'Sampling Grid': 'Quadrat'}, inplace=True)

    # completely new columns
    new_output_cols = [
        'full_fp_orig',
        'full_fp_new',
        'Converted From'  # original file format if it's a non-JPEG image (HEIC, PNG), "no conversion" if already a JPEG (including its alternate spellings), or "NOT AN IMAGE" if it's not an image (in which case no conversion is performed)
    ]
    for col in new_output_cols:
        photos_df[col] = ''

    # Work from preset lists of allowed image extensions (assume there are none with a mix of caps and lowercase)
    # JPEG (these are modified as is)
    jpeg_image_extensions = ['.JPG', '.JPEG']
    jpeg_image_extensions += [ext.lower() for ext in jpeg_image_extensions]
    # non-JPEG (these are converted to JPG)
    non_jpeg_image_extensions = ['.HEIC', '.PNG']  # start with these for now; may need more later
    non_jpeg_image_extensions += [ext.lower() for ext in non_jpeg_image_extensions]


    # Variables for standardizing columns

    # Month abbreviations dict
    month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    month_abbrevs = {month[0:3].upper(): month for month in month_names}

    # Changed core abbreviations:
    # Use one dict for both site abbrevs (made to conform to site_tag.json) and core names (of which there's just one: TimeSeries)
    core_code_replacements = {
        'AJ-Palsa': 'AJP',
        'AJ-Sphag': 'AJS',
        'AJ-Erio': 'AJE',
        'CPN1-H': 'CPN1H',
        'CPN1-R': 'CPN1R',
        'Inc-P': 'IncP',
        'Inc-S': 'IncS',
        'Inc-E': 'IncE',
        'PHB-H': 'PHBH',
        'PHB-R': 'PHBR',
        'PHS': 'PHSH',
        'PW1': 'AvAPW1',
        'PW2': 'AvAPW2',
        'PW3': 'AvBPW3',
        'PW4': 'AvBPW4',
        'PW5': 'AvBPW5',
        'PW6': 'AvBPW6',
        'Time-Series': 'TimeSeries1005'  # All of these specific photos have been determined to be the 10:05 core based on EXIF Date Taken.
    }

    valid_site_abbrevs = [site_attrs['Short'] for site_attrs in site_tag.values()]

    # Thumbnails directory: Assume it's the same as args.photo_root, replacing /raw/ with /thumbnails/
    thumbs_dir = args.photo_root.replace('/raw/', '/thumbnails/')

    # First generate the new filepaths, checking to see if converting non-JPEGs would replace any existing files
    # empty list for converted filenames that would replace existing ones
    converted_files_already_exist = []

    for index, row in photos_df.iterrows():

        fp_old = row['File Location_OLD']
        fp_orig = fp_old.replace('Tagging/', args.photo_root)

        # First make sure that the current filepath actually exists
        if not(os.path.exists(fp_orig)):
            error("This filepath is listed in the CSV, but doesn't actually exist:\n{}".format(fp_orig))

        # Get file type, and if a non-JPEG image, replace current extension with '.JPG' (actual file formats will be
        #   converted later if args.copy_files is enabled)
        fp_components = os.path.splitext(fp_orig)
        extension = fp_components[1]

        if row['_thumbnail_exists'] == 'NOT AN IMAGE':
            # NOTE: There is one file with a .JPG extension (Moira_Hough/2015/Quadrats/DSC02437.JPG) that is actually
            #  not an image, so this check is performed first.
            fp_new = fp_orig
            photos_df.loc[index, 'Converted From'] = 'NOT AN IMAGE'

        elif extension in jpeg_image_extensions:
            fp_new = fp_orig
            photos_df.loc[index, 'Converted From'] = 'no conversion'

        elif extension in non_jpeg_image_extensions:
            fp_new = fp_components[0] + '.JPG'  # re-assign new_fp_end, replacing original extension
            photos_df.loc[index, 'Converted From'] = extension

            # See if fp_new would replace anything that already exists
            if os.path.exists(fp_new):
                converted_files_already_exist += [fp_new]

        else:
            # unrecognized non-image
            error("This file is not in a recognized image format, but is not labeled as 'NOT AN IMAGE':\n{}".format(fp_orig))

        # Remove any single quote characters from fp_new
        if "'" in fp_new:
            fp_new = fp_new.replace("'", "")

            # See if fp_new would replace anything that already exists
            if os.path.exists(fp_new):
                converted_files_already_exist += [fp_new]

        # Fill in the new File Location value with the converted filepath (minus the root dir).
        photos_df.loc[index, 'File Location'] = fp_new.replace(args.photo_root, "")
        # Fill in the full filepath columns for later use when editing images.
        photos_df.loc[index, 'full_fp_orig'] = fp_orig
        photos_df.loc[index, 'full_fp_new'] = fp_new

        # Standardize "group"-type tags (Month, Habitat, etc.).
        # Note: Build-Pictures-HTML.py replaces multi-word values (e.g. "Aug; Sep") with single strings by replacing
        #  ';' with '', and ' ' with '-'. So, "Aug; Sep" becomes "Aug-Sep", and "Poor Fen" becomes "Poor-Fen".
        #  This is NOT good to keep doing (as multi-tag values are indistinguishable from multi-word tags), but is OK
        #  for this particular CSV. The code below has been written to be consistent with this previous behavior,
        #  except replacing month abbreviations with full words, removing hyphens from tags where they don't naturally
        #  belong, and other changes to be consistent with tags on newly-added photos.
        # TODO: Make sure the pictures page HTML is updated with any renamed tags.

        # Month
        month_tag = row['Month_OLD'].replace('; ', '-').upper()
        for key in month_abbrevs.keys():
            month_tag = month_tag.replace(key, month_abbrevs[key])
        photos_df.loc[index, 'Month'] = month_tag

        # Year is ok; no need to change. Season and Habitat are relatively simple.
        photos_df.loc[index, 'Season'] = row['Season_OLD'].replace('; ', '-')
        photos_df.loc[index, 'Habitat'] = row['Habitat_OLD'].replace('Palsa; Fen', 'Palsa-Fen transition')

        # Standardize core names
        core_code = row['Core Code_OLD']
        for key in core_code_replacements.keys():
            core_code = core_code.replace(key, core_code_replacements[key])
        core_code = core_code.replace('-', '_')

        # Make sure there is only one underscore, and that the first part (before the underscore) is a valid site abbrev
        core_code_components = core_code.split('_')
        if core_code != "" and (len(core_code_components) != 2 or core_code_components[0] not in valid_site_abbrevs):
            error('"{}" is not a valid core code.'.format(core_code))
        else:
            photos_df.loc[index, 'Core Code'] = core_code



    # Output an error for any converted filenames that already exist (if args.allow_replace is not set)
    if not(args.allow_replace) and len(converted_files_already_exist)>0:
        print("ERROR: The following JPG-converted images would replace existing files. Please make sure this is OK "
              "before rerunning with --allow-replace.")
        print(converted_files_already_exist)
        exit()
    else:
        print("Success! All image filepaths are valid.")


    # Embed photo tags
    for index, row in photos_df.iterrows():

        if row['Converted From'] == 'NOT AN IMAGE':
            continue  # Nothing needed if not an image, so continue to the next one.

        elif row['Converted From'] in non_jpeg_image_extensions:
            # image needing format conversion

            if args.edit_files:
                photo = Image.open(row['full_fp_orig'])

                # if png, cannot just use save; must first convert
                if not photo.mode == 'RGB':
                    photo = photo.convert('RGB')

                photo.save(row['full_fp_new'])
                photo.close()
                # In this CSV, all non-JPEG images are pngs, so no need to attempt to copy exif metadata.

            if args.edit_thumbs:
                # assume thumbnail already exists for the old file; it just needs to be converted
                thumb_orig = os.path.join(thumbs_dir, os.path.basename(row['full_fp_orig']))
                thumb_new = os.path.join(thumbs_dir, os.path.basename(row['full_fp_new']))

                thumb = Image.open(thumb_orig)

                # if png, cannot just use save; must first convert
                if not thumb.mode == 'RGB':
                    thumb = thumb.convert('RGB')

                thumb.save(thumb_new)
                thumb.close()

        elif row['Converted From'] == 'no conversion' and row['full_fp_orig'] != row['full_fp_new']:
            # JPG with changed filename (from removing single quote characters, etc.)
            # The commands below assumes no double quotes exist in the filename, but accounts for single quotes and other unusual characters

            if args.edit_files:
                cp_cmd = 'cp "' + row['full_fp_orig'] + '" "' + row['full_fp_new'] + '"'
                subprocess.check_call(cp_cmd, shell=True)

            if args.edit_thumbs:
                # assume thumbnail already exists for the old file; it just needs to be converted
                thumb_orig = os.path.join(thumbs_dir, os.path.basename(row['full_fp_orig']))
                thumb_new = os.path.join(thumbs_dir, os.path.basename(row['full_fp_new']))

                cp_cmd = 'cp "' + thumb_orig + '" "' + thumb_new + '"'
                subprocess.check_call(cp_cmd, shell=True)

        elif row['Converted From'] == 'no conversion':
            pass

        else:
            error('Unrecognized image format specified in "Converted From": "{}"'.format(row['Converted From']))

        # Define tags to embed
        if row['Photo Credit'] in ['', 'Misc-and-Unknown']:
            author_tag = None
        else:
            author_tag = row['Photo Credit']

        if row['Core Code'] == '':
            title_tag = None
        else:
            title_tag = row['Core Code']

        # Keywords are divided into presence/absence and group-type keywords; need to combine these.
        keyword_tags = []
        keyword_tags += [tag for tag in keywords_presabs_tags if row[tag].upper() == 'X']
        keyword_tags += [row[tag] for tag in keyword_groups_tags if row[tag] != '']

        # Embed tags
        if args.edit_files:
            update_embedded_photo_tags(image_fp=row['full_fp_new'], byline=author_tag, headline=title_tag,
                                       keywords=keyword_tags, keyword_mode="replace", overwrite_original=True)
        # else:
        #     photos_df.loc[index, '_tags_concatenated'] = '; '.join(keyword_tags)



    # Clean up and output the final results to CSV.
    cols_to_remove = [col for col in photos_df.columns if (col.endswith('_OLD') and col != "File Location_OLD")]
    cols_to_remove += ['Lowest Directory', 'full_fp_orig', 'full_fp_new']
    photos_df.drop(columns=cols_to_remove, inplace=True)

    # Reorder existing columns
    final_column_order = internal_tags + byline_tags + headline_tags + keywords_presabs_tags + keyword_groups_tags
    final_column_order = [col for col in final_column_order if col in photos_df.columns]
    additional_cols = [col for col in photos_df.columns if col not in final_column_order]
    final_column_order += additional_cols
    photos_df = photos_df[final_column_order]

    output_csv = args.tags_csv.replace('.csv', '_2010-2016_standardized.csv')
    photos_df.to_csv(output_csv, sep=',', index=False)