#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Description:
    This script takes a DB-outputted CSV of Core info (core_list_from_cached_query.csv), and another CSV with photo
    tags (Photos-All_retagged.csv), and outputs a CSV (Core-Images.csv) with image file locations for each core.
    Core-Images.csv is then used as input for the script build/web/io_misc/Update_Core_Images.py (which imports these
    image filepaths into the DB).
    Note that some of the file paths used in this script, and in Update_Core_Images.py, need to be updated.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import pandas as pd
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.data.common_vars import *

tags_fp = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'test_data', 'misc', 'Photos-All_retagged.csv'))
tags_df = pd.read_csv(tags_fp, sep=',', index_col=None, header=0, dtype=object)

# Core list (used CSV exported from cached query, as this was recently updated and should be current; this has unneeded columns removed)
corelist_fp = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'test_data', 'misc', 'core_list_from_cached_query.csv'))
cores_df = pd.read_csv(corelist_fp, sep=',', index_col=None, header=0, dtype=object)

# Remove __ in columns
cores_df.columns = cores_df.columns.str.replace('__','')

# Make a column in both cores_df and tags_df for matching cores

cores_df['core_identifier'] = ''
for index, row, in cores_df.iterrows():
    site = site_tag[row['Site']]['Short']
    cores_df.loc[index, 'core_identifier'] = site + '-' + row['CoreDated']

tags_df['core_identifier'] = ''
for index, row in tags_df.iterrows():
    if not (pd.isnull(row['Core Code']) or row['Core Code'] == ''):
        year = str(row['Year'])
        if year == '2010':
            short_date = '2010-08'  # Using this date in the DB to cover Aug-Sept overlap
        else:
            if not(pd.isnull(row['Month'])):
                try:
                    month = datetime.strptime(row['Month'], '%b').month
                except TypeError:
                    error('Got a weird Month value ({}) for row:\n{}'.format(row['Month'], row))
                    
                if month < 10:
                    month = '0' + str(month)
                else:
                    month = str(month)
                short_date = str(year) + '-' + month
            else:
                error('Found a core photo with an identified site and core # and unknown month:\n{}'.format(row))
                      
        tags_df.loc[index, 'core_identifier'] = row['Core Code'] + '|' + short_date
        
# Get list of images for each core
cores_df['image_href__'] = ''
for index, row in cores_df.iterrows():
    image_lst = list(tags_df[tags_df['core_identifier'] == row['core_identifier']]['File Location'])
    image_lst = [img.replace('Tagging/', '/images/raw/') for img in image_lst]
    image_lst_str = ', '.join(image_lst)
    cores_df.loc[index, 'image_href__'] = image_lst_str
    
# Write to csv
export_csv = corelist_fp.replace('core_list_from_cached_query.csv', 'Core-Images.csv')
cores_df.to_csv(export_csv, index=False, quotechar='"', sep=',')
