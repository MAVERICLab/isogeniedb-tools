#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 16:07:56 2019

@author: suzanne

Description:
    Merge old tagging CSVs into one big CSV and fix the file paths to match the new directories, which have been
    reorganized to give correct photo credit.
    Also finds new images and adds them, and creates thumbnails if they don't exist (or renames thumbnails if the
    extensions don't match case).
    After running this script, still need to manually tag new photos & fix existing tags, including adding a Habitat
    column to better categorize habitats.
    THEN AFTER RE-TAGGING, RE-SAVE AS A DIFFERENT FILENAME, in test_data/misc/Photos-All_retagged.csv

    TODO: Need to provide a means of appending brand new photos & tags to the existing manually-edited
      Photos-All_retagged.csv file.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import re
import pandas as pd
from PIL import Image

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.data.common_vars import *

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

tags_dir = '/home/suzanne/IsoGenieDB/Photo-Tagging/Photo-Tagging-Groups/'
images_dir = '/home/suzanne/IsoGenieDB/Bitbucket/isogenie-web/public/images/raw/'

tags_csvs = []
for root, dirs, files in os.walk(tags_dir):
    for fname in files:
        if 'Photos-Group-' in fname and '-Photos.csv' in fname:
            # Add a leading 0 to single digit groups to help with sorting later
            fname_renumbered = re.sub('Group-(?=\d_)', 'Group-0', fname)
            # Add to the list
            tags_csvs.append(os.path.join(tags_dir, fname_renumbered))
            
# Sort the files alphabetically
tags_csvs.sort()

# Remove the leading 0s to get back to original filename
for index in range(len(tags_csvs)):
    tags_csvs[index] = re.sub('Group-0(?=\d_)', 'Group-', tags_csvs[index])
    

# Read the CSVs and append to a big master CSV
for tags_csv in tags_csvs:
    tags_df = pd.read_csv(tags_csv, sep=',', quotechar='"')
    
    # Rename Grid column as it messes with the HTML
    tags_df.rename(columns={"Grid" : "Sampling Grid"}, inplace=True)
    
    # Make a column for tagging group number
    group_number = int(re.split('_', re.split('Photos-Group-',tags_csv)[1])[0]) # group number from filename
    tags_df['_tagging_group'] = group_number
    
    # if it's the first one, need to initialize the dataframe
    if 'Photos-Group-0_' in tags_csv:
        complete_tags_df = tags_df
    # otherwise just append it to the existing dataframe
    else:
        complete_tags_df = pd.concat([complete_tags_df, tags_df], ignore_index=True)
        
# Change file paths to reflect simple folder renaming        
for index, row in complete_tags_df.iterrows():
    
    # Replace spaces
    complete_tags_df.loc[index, 'File Location'] = complete_tags_df.loc[index, 'File Location'].replace(' ', '_')
    complete_tags_df.loc[index, 'Lowest Directory'] = complete_tags_df.loc[index, 'Lowest Directory'].replace(' ', '_')
    
    # Substitute formerly first names for full names
    complete_tags_df.loc[index, 'File Location'] = complete_tags_df.loc[index, 'File Location'].replace('Tagging/Moira/', 'Tagging/Moira_Hough/')
    complete_tags_df.loc[index, 'File Location'] = complete_tags_df.loc[index, 'File Location'].replace('Tagging/Nicole/', 'Tagging/Nicole_Raab/')
    complete_tags_df.loc[index, 'File Location'] = complete_tags_df.loc[index, 'File Location'].replace('Tagging/Amelia/', 'Tagging/Amelia_McClure/')
    
    complete_tags_df.loc[index, 'Lowest Directory'] = complete_tags_df.loc[index, 'Lowest Directory'].replace('Tagging/Moira/', 'Tagging/Moira_Hough/')
    complete_tags_df.loc[index, 'Lowest Directory'] = complete_tags_df.loc[index, 'Lowest Directory'].replace('Tagging/Nicole/', 'Tagging/Nicole_Raab/')
    complete_tags_df.loc[index, 'Lowest Directory'] = complete_tags_df.loc[index, 'Lowest Directory'].replace('Tagging/Amelia/', 'Tagging/Amelia_McClure/')
    
    if complete_tags_df.loc[index, 'Photo Credit'] == 'Nicole':
        complete_tags_df.loc[index, 'Photo Credit'] = 'Nicole Raab'
    
# Make a column for storing information on whether the file path is still the same
complete_tags_df['_file_exists'] = False

for index, row in complete_tags_df.iterrows():
    actual_path = row['File Location'].replace('Tagging/', images_dir)
    
    if os.path.exists(actual_path):
        complete_tags_df.loc[index, '_file_exists'] = True
        complete_tags_df.loc[index, 'Photo Credit'] = complete_tags_df.loc[index, 'Photo Credit'].replace('_', ' ')
        
        if complete_tags_df.loc[index, 'Photo Credit'] != row['File Location'].split('/')[1].replace('_', ' '):
            error("The file {} exists but its directory structure does not match the file credit given ({}).".format(row['File Location'], row['Photo Credit']))
            
    else:
        
        filename_only = actual_path.split('/')[-1]
        for root, dirs, files in os.walk(images_dir):
            for fname in files:
                if fname == filename_only:
                    complete_tags_df.loc[index, '_file_exists'] = True
                    complete_tags_df.loc[index, 'File Location'] = os.path.join(root, fname).replace(images_dir, 'Tagging/')
                    complete_tags_df.loc[index, 'Lowest Directory'] = root.replace(images_dir, 'Tagging/')
                    complete_tags_df.loc[index, 'Photo Credit'] = root.replace(images_dir, '').split('/')[0].replace('_', ' ')
                    
# Remove rows with nonexistent files (already checked these manually to make sure they're not otherwise accounted for)
complete_tags_df = complete_tags_df[complete_tags_df._file_exists]

# Get a list of files not accounted for, and append them to the end of the dataframe
all_images = []
for root, dirs, files in os.walk(images_dir):
    for fname in files:
        all_images.append(os.path.join(root.replace(images_dir, 'Tagging/'), fname))
        
images_to_add = []
for image in all_images:
    if image not in list(complete_tags_df['File Location']) and 'Tagging/2016/' not in image:  # omit the '2016' directory as it's not currently included in the html...
        images_to_add.append(image)

new_images_df = pd.DataFrame({'File Location' : images_to_add, 
                              'Lowest Directory' : [img.split('/' + img.split('/')[-1])[0] for img in images_to_add],
                              'Photo Credit' : [img.replace('Tagging/', '').split('/')[0].replace('_', ' ') for img in images_to_add]})
   
final_df = pd.concat([complete_tags_df, new_images_df], ignore_index=True)[complete_tags_df.columns]

# Thumbnails

thumbs_dir = images_dir.replace('/raw','/thumbnails')
                    
final_df['_thumbnail_exists'] = False
for index, row in final_df.iterrows():
    filename = row['File Location'].split('/')[-1]
    if os.path.exists(os.path.join(thumbs_dir, filename)):
        final_df.loc[index, '_thumbnail_exists'] = True
        
    elif os.path.exists(os.path.join(thumbs_dir, filename.replace('.JPG','.jpg'))):
        # Rename the thumbnail to match
        try:
            os.rename(os.path.join(thumbs_dir, filename.replace('.JPG','.jpg')), os.path.join(thumbs_dir, filename))
            final_df.loc[index, '_thumbnail_exists'] = True
        except FileNotFoundError:  # Already got renamed? Then it shouldn't get to this point, and yet it is anyway
            warning('Could not rename {} thumbnail even though it should exist.'.format(filename))
            final_df.loc[index, '_thumbnail_exists'] = 'with lowercase extension; UNABLE TO CHANGE TO UPPERCASE'
            
    elif os.path.exists(os.path.join(thumbs_dir, filename.replace('.jpg','.JPG'))):  # there shouldn't be any of these, but just in case
        # Rename the thumbnail to match
        try:
            os.rename(os.path.join(thumbs_dir, filename.replace('.jpg','.JPG')), os.path.join(thumbs_dir, filename))
            final_df.loc[index, '_thumbnail_exists'] = True
        except FileNotFoundError:  # Already got renamed? Then it shouldn't get to this point, and yet it is anyway
            warning('Could not rename {} thumbnail even though it should exist.'.format(filename))
            final_df.loc[index, '_thumbnail_exists'] = 'with lowercase extension; UNABLE TO CHANGE TO UPPERCASE'
            
    elif filename.endswith('pptx') or filename.endswith('MTS'): # non-image files
        final_df.loc[index, '_thumbnail_exists'] = 'NOT AN IMAGE'
    elif filename == 'DSC02437.JPG': # Random file that is named like an image, but isn't an image
        final_df.loc[index, '_thumbnail_exists'] = 'NOT AN IMAGE'
    else:
        # If all else fails, make a new thumbnail
        filepath = row['File Location'].replace('Tagging/', images_dir)
        image = Image.open(filepath)
        image.thumbnail((300,1000)) # use disproportionately large height to keep all widths at 300px
        image.save(os.path.join(thumbs_dir, filename))
        final_df.loc[index, '_thumbnail_exists'] = True
            
# Export to csv
final_csv = os.path.join(tags_dir, 'Photos-All.csv')
final_df.to_csv(final_csv, index=False, quotechar='"', sep=',')