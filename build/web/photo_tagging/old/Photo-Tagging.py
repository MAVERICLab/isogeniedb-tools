#!/usr/bin/env python
"""
Description:
    This script generates a list of all photos in a given directory to be tagged, and then exports template CSVs to use
    for tagging the photos. It assumes that the photo directory structure follows the format:
        Tagging/[contributor]/[year]/[optional subdirectories]/[photos]

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
from collections import Counter
import numpy as np
import pandas as pd

from pprint import pprint

photo_root = '/Users/bolduc.10/Box Sync/SWES-MEL/IsoGenieDB-development/Photo-Tagging/'
tagging_root = os.path.join(photo_root, 'Tagging/')

# Collect photos
photos = []
exclusions = ['.DS_', 'Thumbs.db', '_DS_Store']
for root, dirs, files in os.walk(tagging_root):
    for file in files:
        if not any(exclusion in file for exclusion in exclusions):  # grr Mac files
            photos.append(os.path.join(root, file))

photos_df = pd.DataFrame(data=photos, columns=['File Location'])
photos_df['Tags (; separated)'] = np.nan
photos_df['Notes/Qs'] = np.nan

# Sort
photos_df.sort_values('File Location', inplace=True)

dirs = photos_df['File Location'].tolist()

# Get all directories 1st
paths = {}
for dir_ in dirs:
    full_path = os.path.join(photo_root, dir_)
    dir_path = os.path.dirname(full_path)

    dir_contents = [d for d in os.listdir(dir_path) if not os.path.isdir(d)]

    # Don't use collections.Counter as it'll count for every file
    if dir_path not in paths:
        paths[dir_path] = len(dir_contents)

for index, series in photos_df.iterrows():
    lowest_dir = os.path.dirname(series['File Location'])
    if lowest_dir in paths:
        photos_df.loc[index, 'Lowest Directory'] = lowest_dir.replace(photo_root, '')

# Get rid of long path name that won't be similar
photos_df['File Location'] = photos_df['File Location'].str.replace(photo_root, '')

# Master list, and from that, produce list of files with tags grouped
balance_df = pd.DataFrame(columns=['Directory', 'Contributor', 'Files'])
balance_df['Directory'] = photos_df['Lowest Directory'].unique()

# Rebuilt paths
paths = {key.replace(photo_root, ''): value for key, value in paths.items()}
balance_df['Files'] = balance_df['Directory'].map(paths)
balance_df['Contributor'] = balance_df['Directory'].str.split('/').str[1]
balance_df['Year'] = balance_df['Directory'].str.split('/').str[2]

balance_df.sort_values('Directory', inplace=True)

group_counter = 0
for contrib, contrib_df in balance_df.groupby('Contributor'):

    contrib_total = contrib_df['Files'].sum()
    sub_counter = 0
    sub_indexes = []

    if contrib_total <= 50:  # Painless
        balance_df.loc[balance_df['Contributor'] == contrib, 'Group'] = group_counter
        group_counter += 1

    if contrib_total > 50:

        for index, series in contrib_df.iterrows():
            count = series['Files']

            if sub_counter <= 50:
                sub_indexes.append(index)  # Add to list
                sub_counter += count

            if sub_counter > 50:  # We'll go over!
                balance_df.loc[sub_indexes, 'Group'] = group_counter  # Gather indexes

                # Increment group, reset sub_counter
                group_counter += 1
                sub_counter = 0
                sub_indexes = []

    # Close out group
    if len(sub_indexes) > 0:
        balance_df.loc[sub_indexes, 'Group'] = group_counter
        group_counter += 1
        # No real need to reset sub_ as they'll do so with the loop

balance_fp = os.path.join(photo_root, 'Master-Sheet.csv')
balance_df.to_csv(balance_fp, sep=',', index=False)

# Write out fileslist
for group_name, group_df in balance_df.groupby('Group'):  # Iterate through each group
    # Get files (from photos) that have the same directory as the 'Group' directory (from balance)
    photos_group_df = photos_df.loc[photos_df['Lowest Directory'].isin(group_df['Directory'])]

    re_photos_df = photos_group_df[['File Location', 'Lowest Directory', 'Tags (; separated)', 'Notes/Qs']]
    re_photos_fp = os.path.join(photo_root, 'Photos-Group-{:g}_{}-Photos.csv'.format(group_name, len(re_photos_df)))

    re_photos_df.to_csv(re_photos_fp, sep=',', index=False)
