#!/usr/bin/env python
"""
Description:
    This script builds the HTML for the IsoGenieDB Pictures page from the photo tags file (Photos-All_retagged.csv).
    It assumes that a thumbnail file exists for each image; therefore THUMBNAILS WILL NEED TO BE CREATED FOR NEW IMAGES.
    There is no direct interaction with the graph DB.
    UPDATE 7/13/22: This method of building the Pictures HTML will soon be phased out, so this script was only updated
    minimally to work with the current filepaths, tag names, and tagging CSV; no additional improvements (such as input
    arguments) were made.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import glob
import pandas as pd
from string import Template
from itertools import chain
from pprint import pprint

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')))
from tools.data.photo_tagging_vars import *

#true_dir = '/Users/bolduc.10/Research/Bioinformatics/Repositories/IsoGenieDB/isogeniedb-web/public/'
true_dir = '/home/suzanne/IsoGenieDB/Bitbucket/isogenie-web/public/'

# # Remove Thumbs.db files
# thumbs = glob.glob(os.path.join(true_dir, 'images/raw/**/Thumbs.db'), recursive=True)
# for thumb in thumbs:
#     os.remove(thumb)

# Find bad filepaths (those with spaces) and throw an error for any that exist.
# The reason this has to be done is because one directory with all the raw images was analyzed by undergrads, tagged,
# and a CSV was generated. A separate, but identical copy of the raw images was at a different location. It's easy to
# replace spaces in a list, not so much for files.
files = glob.glob(os.path.join(true_dir, 'images/raw/**/*.*'), recursive=True)
bad_dirs = [fp for fp in files if ' ' in os.path.dirname(fp)]  # Get dirs that will mess with pathing
bad_fns = [fp for fp in files if ' ' in os.path.basename(fp)]  # Get files that will mess with everything

for bad_fp in bad_fns:
    print('ERROR: Found a filename with a space: {}'.format(bad_fp))
    exit()

# Do the same for thumbnail filepaths.
thumbs2 = glob.glob(os.path.join(true_dir, 'images/thumbnails/*.*'), recursive=False)
bad_thumbs = [fp for fp in thumbs2 if ' ' in os.path.basename(fp)]
if len(bad_thumbs) > 0:
    print('ERROR: Found a filename with a space: {}'.format(bad_fp))
    exit()

# Import photo tags CSV.
photos_dir = os.path.join('test_data', 'IsoGenie_datasets', 'private_data', 'misc')
photos_fp = os.path.join(photos_dir, 'Photos-All_2010-2021_standardized.csv')
joined_df = pd.read_csv(photos_fp, sep=',', index_col=None, header=0, dtype=object)

#joined_df = joined_df.drop_duplicates(subset=['File Location'], keep='first')  # UPDATE: Consolidated tags in duplicate rows; there should no longer be any.
#joined_df.replace({'x': 'X', 'JUN': 'Jun', 'JUL': 'Jul', 'AUG': 'Aug', 'OCT': 'Oct'}, inplace=True)

# # Get rid of semicolons, which mess up the HTML
# joined_df['Habitat'] = joined_df['Habitat'].str.replace(';', '')
# joined_df['Month'] = joined_df['Month'].str.replace(';', '')
# joined_df['Season'] = joined_df['Season'].str.replace(';', '')

# joined_df.to_csv(os.path.join(root_dir, 'summary_results.csv'))

# "internal tags" are columns that we do not want converted into tags for the website.
# internal_tag = [
#     'File Location', 'Notes/Qs', 'Photo Credit', 'Person Name (in photo)', 'Lowest Directory', 'Core Code', 'Date Code',
#     'Day', '_tagging_group', '_file_exists', '_thumbnail_exists',  'Site Name']  # Now using Habitat instead of Site Name
non_keyword_cols = internal_tags + byline_tags + headline_tags + ['File Location', 'File Location_OLD', 'Lowest Directory', 'Converted From', '_tagging_group', '_file_exists', '_thumbnail_exists']
tags = [column for column in joined_df.columns.tolist() if column not in non_keyword_cols]

# Identify which 'tags' are filter groups or presence/absence
presence_absence = []
groups = {}
for column in tags:
    count_s = joined_df[column].value_counts()

    if len(count_s) == 1:  # Only X, or presence/absence
        presence_absence.append(column)
    else:  # Multiple types
        groups[column] = ''

# Sampling site should be Sampling site, not X...
button_groups = set(presence_absence)  # We know that all of the items in presence_absence we want tagged
for tag in groups.keys():
    items = [item for item in joined_df[tag].unique() if (not pd.isnull(item)) and (item != 'X')]
    groups[tag] = items

# Update file locations to server locations
# NEW: 'File Location' no longer starts with 'Tagging/'
joined_df['File Location'] = '/images/raw/' + joined_df['File Location']

# Templates
button_template = Template('<button data-filter=".$filter_group" class="btn">$name</button>')
href_item_template = Template('<div class="element-item $element_items">\n<a href="$raw_image" target="_blank" title="$title">\n<img src="$thumb_image"/>\n</a>\n</div>')

button_groups.update(set(chain.from_iterable(groups.values())))  # Add to final button groups
with open(os.path.join(photos_dir, 'button_groups.html'), 'w') as button_groups_fh:
    for tag in button_groups:
        button_groups_fh.write(button_template.substitute(filter_group=tag.lower().replace(' ', '-'), name=tag) + '\n')

# But now need to go through and be able to select columns from presence and values from cell
with open(os.path.join(photos_dir, 'img_items.html'), 'w') as img_items_fh:
    for index, row_s in joined_df.iterrows():
        
        if row_s['_thumbnail_exists'] == 'NOT AN IMAGE':
            continue

        hrefs = [] # for storing tags for this image
        href_imgs = [] # for storing the image href, copied same number of times as len(hrefs)?
        for column in set(presence_absence):  # Presence/Absence
            value = row_s[column]
            if not pd.isnull(value):
                hrefs.append(column.lower().replace(' ', '-'))
                href_imgs.append(row_s['File Location'])

        for column, value_list in groups.items():
            value = row_s[column]

            if value in value_list:
                hrefs.append(value.lower().replace(' ', '-'))
                href_imgs.append(row_s['File Location'])

        href_img = list(set(href_imgs))[0] # href_imgs are all identical, so only need the first one

        href_thumb = os.path.join('/images/thumbnails/', os.path.basename(href_img))
        href_name = os.path.basename(href_img).rsplit('.', 1)[0]

        href_item = href_item_template.safe_substitute(element_items=' '.join(hrefs),
                                                       raw_image=href_img,
                                                       title=href_name,
                                                       thumb_image=href_thumb)

        img_items_fh.write(href_item + '\n')

# Works, but broken links due to spaces and special characters in file paths.
# Add photo credits

