#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import json
from pprint import pprint
import networkx as nx
from networkx.readwrite import json_graph

# import local_file_paths, which is in the root isogenie-tools directory
# append two '..' to the current directory of this file, because current directory is two levels down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from local_file_paths import *


graphml_graph_fn = os.path.join(output_save_dir, 'IsogenieDB-Graph.graphml') # output_save_dir from local_file_paths.py

diGraph = nx.read_graphml(graphml_graph_fn)

mapping = {}
for node, data in diGraph.nodes(data=True):
    mapping[node] = data['label']

diGraph = nx.relabel_nodes(diGraph, mapping, copy=False)

pprint(diGraph.nodes(data=True))

data = json_graph.node_link_data(diGraph)

graph_fn = os.path.join(isogenie_web_publicimages_dir, 'IsogenieDB-Graph.json') # isogenie_web_publicimages_dir from local_file_paths.py
with open(graph_fn, 'w') as graph_fh:
    json.dump(data, graph_fh, sort_keys=True, indent=4)

