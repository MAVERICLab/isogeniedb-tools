#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
import numpy as np
from dateutil.parser import parse
# from creates import *
# from graph import check_name
import argparse
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.imports import nodes_new as nodes
from tools.utilities.utils import error, warning, printv
from tools.data.common_vars import *

""" Created this by editing import_autochamber.py. Changed the language to be more general so it could be used for other 
data types, but then realized we'll still need to specify custom site info in this script. We should probably streamline 
this as the number of datasets increases, but can continue to do it this way for now."""

parser = argparse.ArgumentParser(description="Import multi-value timeseries (i.e., multiple variables measured at each "
                                             "time) with decimal time values (i.e. fractions of a day) into a neo4j "
                                             "database.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with timeseries data")

inputs.add_argument('--columns', dest='columns_fp', help="File with metadata describing columns in main input file.")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n"
                         "\nIn *this* script, 'none' argument is automatically changed to 'missing' to take care of times that break across CSVs.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'

update_existing = args.update_existing

# if update_existing == 'none':
#     update_existing = 'missing'

overall_info = {   # Not all of these items will go on the same node "level."
    'Area__': 'APEX',
    'Site__': 'APEX-Alpha',
    'Habitat__': 'Fen',
    'GPS__': standardize_GPS(lat=64.70138, lon=-148.31034),
    'Station__': 'Meteorological station'
}

def convert_date(time_row):
    # Convert year + DOY into YYYY-MM-DD (accounts for leap years).

    return datetime.strptime('{} {}'.format(time_row['year'], int(float(time_row['dec. DOY']))),
                             '%Y %j').date().isoformat()

def convert_time(time_row):
    # Convert fraction of a day to HH:MM (with leading zero).
    fractional_time = float(time_row['dec. DOY']) - int(float(time_row['dec. DOY']))
    hh = int(fractional_time*24)
    mm = int((fractional_time*24 - hh)*60)
    return parse(str(hh) + ':' + str(mm)).strftime('%H:%M')


if __name__ == "__main__":

    # Input file
    timeseries_fp = args.input_fp
    print('Importing timeseries information from {}'.format(timeseries_fp))

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:
    
        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # Add column info nodes.
        columns_fp = args.columns_fp
        columns_df = pd.read_csv(columns_fp, sep=',', dtype=object)
        del columns_df['Unnamed: 0']

        for index, column_info in columns_df.iterrows():

            # delete leading space from column name to store
            column_name = column_info['Column__']
            column_attrs = column_info.squeeze().fillna('NaN').to_dict()

            column_node = nodes.Column(session=session, name=column_name, metadata_node=metadata_node,
                                       data_dict=column_attrs)

            column_node.node = session.write_transaction(column_node.update_labels, id_node=column_node.node._id,
                                                         labels=additional_labels)

        # DATAFILE ----

        # Re-structured import process to include more verification and feedback at each level

        # Pre-process file
        timeseries_df = pd.read_csv(timeseries_fp, sep=',', dtype=object, skipinitialspace=True)
        # Keep as string, or else pd tries to be smart
        # skipInitialSpace removes initial space from non-quoted strings only

        timeseries_df['Date__'] = timeseries_df.apply(convert_date, axis=1)
        timeseries_df['Hour__'] = timeseries_df.apply(convert_time, axis=1)

        area = overall_info['Area__']
        site = overall_info['Site__']
        habitat = overall_info['Habitat__']
        station = overall_info['Station__']

        area_node = nodes.Area(session=session, name=area)
        area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id,
                                                       labels=additional_labels)

        site_node = nodes.Site(session=session, name=site, area_node=area_node, data_dict={'Habitat__': habitat})

        site_node.node = session.write_transaction(site_node.update_labels, id_node=site_node.node._id,
                                                   labels=[habitat] + additional_labels)

        station_node = nodes.Device(session=session, name=station, kind='Station', site_node=site_node)  # add any data_dict?
        station_node.node = session.write_transaction(station_node.update_labels, id_node=station_node.node._id,
                                                      labels=[habitat] + additional_labels)

        for date, date_df in timeseries_df.groupby(by='Date__'):

            # Tested some edits below (and in nodes_new) to try and speed things up, and it does simplify the code,
            #  but only slightly improves speed (26 seconds vs. ~37 seconds).

            date_node = nodes.Date(session=session, name=date, source_node=station_node, source_id_key='StationID',
                                   update_existing=update_existing, additional_labels=[habitat] + additional_labels)

            for index, time_s in date_df.iterrows():

                time_info = time_s.squeeze().fillna('NaN')
                time_hour = time_s['Hour__']

                time_node = nodes.Hour(session=session, name=time_hour, update_existing=update_existing,
                                       metadata_node=metadata_node, data_dict=time_info.to_dict(), date_node=date_node,
                                       additional_labels=[habitat, 'Data'] + additional_labels)
