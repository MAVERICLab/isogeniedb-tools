#!/usr/bin/env python
"""
This is a generalization of bin/isogenie/import_cores.py, meant to be used for any arbitrary soil coring data with the
following node structure:

  Area-->Site-->Core-->Depth-->Data
          |      ^              |
          v      |              v
        FieldSampling        Metadata

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
import numpy as np
import argparse

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.data.common_vars import *
from tools.data.common_vars_A2A import *
from tools.imports import nodes_new as nodes

import warnings
# warnings.filterwarnings("ignore", category=DeprecationWarning)
# warnings.filterwarnings("ignore", category=FutureWarning)

parser = argparse.ArgumentParser(description="Import cores")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with coring information")

inputs.add_argument('--columns', dest='columns_fp', default=None, help="File with metadata describing columns in main input file.")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

inputs.add_argument('--additional-labels', dest='additional_labels', default=None,
                    nargs='+',  # nargs='+' allows a list of arguments to be passed
                    help="Additional labels to add to all nodes created with this script.")

inputs.add_argument('--dataset-type', dest='data_type',
                    help="Dataset type label to be added to Data nodes, and relationships pointing to them, created "
                         "with this script. Valid values include "
                         "'Depth-Info', 'Site-Info', or anything ending in the word 'Biogeochemistry'. "
                         "These allowed dataset types are limited so as to avoid too many different labels in the DB. "
                         "If another dataset type needs to be added, please edit the script to add it.")

inputs.add_argument('--area', dest='area',
                    help="Which Area node the data is for (if not specified in an Area__ column).")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'
    
update_existing = args.update_existing

def find_habitat_level(df):
    # Finds the highest level (Area, Site, or Core) at which a variable (usually Habitat__) is consistently defined.
    # This function is here as a placeholder in case this is deemed necessary. For now, Habitat__ will henceforth be
    # assumed to be defined at Core level (previously it was at Site level, but we're changing it to Core level to
    # accommodate (1) Habitat change for a given Site with thaw over time, and (2) Sites that are defined such that
    # different Cores within the same Site have different Habitats (e.g., this is common with transect-type Sites).
    pass

if __name__ == "__main__":

    # Input file
    core_fp = args.input_fp

    # Additional labels
    if args.additional_labels is None:
        additional_labels = []
    else:
        additional_labels = args.additional_labels

    # Data type: (Depth-Info, Biogeochemistry, etc.)
    data_type = args.data_type
    data_rel_type = 'HAS_' + data_type.upper().replace('-', '_')
    if data_type.endswith('Biogeochemistry'):  # allows for different types of biogeochemistry: solid, porewater, pH, etc.
        data_type_short = 'Biogeochemistry'
    elif data_type in ['Depth-Info', 'Site-Info']:  # list of other valid data types -- add to this as necessary, but keep it limited
        data_type_short = data_type
    else:
        error('Invalid data_type specified: {}'.format(data_type))

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:

        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }

        try:
            metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                           update_existing_data=update_existing)
        except TypeError:
            print(file_metadata)
            error('TypeError when attempting to set DatasetID on Metadata node: {}'.format(file_metadata['DatasetID']))

        additional_labels += file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        additional_labels = list(set(additional_labels))  # remove any inadvertently-created duplicates


        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # Add column info nodes.
        if args.columns_fp is not None:
            columns_fp = args.columns_fp
            columns_df = pd.read_csv(columns_fp, sep=',', dtype=object)
            #del columns_df['Unnamed: 0']

            for index, column_info in columns_df.iterrows():
                # delete leading space from column name to store
                column_name = column_info['Column__']
                column_attrs = column_info.squeeze().fillna('NaN').to_dict()

                column_node = nodes.Column(session=session, name=column_name, metadata_node=metadata_node,
                                           data_dict=column_attrs)

                column_node.node = session.write_transaction(column_node.update_labels, id_node=column_node.node._id,
                                                             labels=additional_labels)

        # DATAFILE ----

        # Re-structured import process to include more verification and feedback at each level
        print('Importing coring information from {}'.format(core_fp))

        # Pre-process file
        csv_df = pd.read_csv(core_fp, sep=',', quotechar='"', dtype=object)

        # # NaN to 'NaN' string (necessary for any columns used directly for DataFrame.groupby())
        csv_df.DataSubsample__ = csv_df.DataSubsample__.fillna('NaN')
        # csv_df.DepthSubsample__ = csv_df.DepthSubsample__.fillna('NaN')
        # csv_df.DepthMin__ = csv_df.DepthMin__.fillna('NaN')
        # csv_df.DepthMax__ = csv_df.DepthMax__.fillna('NaN')
        # csv_df.GPS__ = csv_df.GPS__.fillna('NaN')
        # csv_df.DateC__ = csv_df.DateC__.fillna('NaN')
        # csv_df.DateP__ = csv_df.DateP__.fillna('NaN')
        # csv_df.DateOther__ = csv_df.DateOther__.fillna('NaN')

        # NEW: Fill all NaN in the whole dataframe (not just in selected columns) with 'NaN' strings
        csv_df.fillna('NaN')

        # Areas: If none specified, get from args.
        if 'Area__' not in csv_df.columns:
            if args.area is None:
                error('No Area specified. Must specify a site Area (e.g. Stordalen, APEX-Alpha, etc.)')
            else:
                area = args.area
                csv_df['Area__'] = area
                if area in area_groups.keys():
                    csv_df['AreaGroup__'] = area_groups[area]
                else:
                    csv_df['AreaGroup__'] = 'NaN'

        if 'Depth__' in csv_df.columns:
            # if there's no DepthSubsample__, make DepthComplete__ the same as Depth__
            if 'DepthSubsample__' not in csv_df.columns:
                csv_df['DepthComplete__'] = csv_df['Depth__']

            # otherwise, make DepthComplete__ column for combined Depth__ and DepthSubsample__ (if applicable)
            else:
                for index, row in csv_df.iterrows():
                    if row['DepthSubsample__'] == 'NaN':
                        csv_df.loc[index, 'DepthComplete__'] = row['Depth__']
                    else:
                        csv_df.loc[index, 'DepthComplete__'] = str(row['Depth__']) + '_' + str(row['DepthSubsample__'])

            # SampleID__ columns (if any)
            # TODO: Allow custom sampleID name formats.
            sampleID_cols = [col for col in csv_df.columns if col.endswith('SampleID__')]

        # if there's no DataSubsample__ column, add it, as DataSubsample__ is actually mandatory for
        #  Data nodes (at least as of 8/27/20; may change this later)
        if 'DataSubsample__' not in csv_df.columns:
            csv_df['DataSubsample__'] = 'NaN'


        for area, area_df in csv_df.groupby(by='Area__'):

            area_dict = dict(area_df[['AreaGroup__', 'Area__']].iloc[0])

            if area_dict['AreaGroup__'] in ['NaN', ''] or pd.isnull(area_dict['AreaGroup__']):
                del area_dict['AreaGroup__']

            area_node = nodes.Area(session=session, name=area, data_dict=area_dict)
            area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id,
                                                       labels=additional_labels)

            # Iterate through each site df, avoiding need to generate sites and then filter
            for site, site_df in area_df.groupby(by='Site__'):

                site_node = nodes.Site(session=session, name=site, area_node=area_node)  # Will automatically create if doesn't exist

                site_node.node = session.write_transaction(site_node.update_labels, id_node=site_node.node._id,
                                                           labels=additional_labels)
                if 'Core__' not in site_df.columns:
                    # If it's a site info data sheet, just connect Data nodes to the Sites.
                    # May add similar code later for core info (divided into cores but not depths), etc.
                    for subdata, data_df in site_df.groupby(by='DataSubsample__'):

                        if data_type != 'Site-Info':
                            error('Invalid data_type ({}) given for Site-level data. Must instead use "Site-Info".'.format(data_type))

                        if len(data_df) != 1:
                            error('Identified duplicate data rows from {}, {}, with no '
                                  'separating DataSubsample__.'.format(area, site))

                        if 'Habitat__' in csv_df.columns:
                            # Since we're now down to 1 row (verified in the previous step), and a Habitat__ column is
                            #  assumed to exist (it will throw an error if not), it's OK to assign habitat to site.
                            habitat = data_df['Habitat__'].iloc[0]

                        data_s = data_df.squeeze().fillna('NaN')

                        data_node = nodes.Data(session=session, update_existing=update_existing,
                                               metadata_node=metadata_node,
                                               source_node=site_node, source_id_key='SiteID',
                                               rel_type=data_rel_type, data_dict=data_s.to_dict(), subdata=subdata)

                        data_node.node = session.write_transaction(data_node.update_labels,
                                                                   id_node=data_node.node._id,
                                                                   labels=[habitat,
                                                                           data_type_short] + additional_labels)

                    continue  # keep iterating through sites; don't go into FieldSampling__, Core__, etc.

                for date, date_df in site_df.groupby(by='FieldSampling__'):

                    fieldsampling_dict = dict(date_df[['DateStart__', 'DateEnd__', 'FieldSampling__']].iloc[0])

                    if 'FieldTeam__' in csv_df.columns:
                        fieldsampling_dict['FieldTeam__'] = date_df['FieldTeam__'].iloc[0]  # assume it's the same throughout
                    elif area == 'Stordalen':
                        fieldsampling_dict['FieldTeam__'] = field_teams[date]
                    else:
                        warning('No FieldTeam__ information found for {} {} {} field sampling. Try to get this information '
                                'from collaborators so that it can be added later.'.format(area, site, date))

                    date_node = nodes.FieldSampling(session=session, name=date, area_node=area_node, data_dict=fieldsampling_dict)  # Will automatically create if doesn't exist

                    date_node.node = session.write_transaction(date_node.update_labels, id_node=date_node.node._id,
                                                               labels=additional_labels)

                    for core, core_df in date_df.groupby(by='Core__'):

                        gps = core_df['GPS__'].iloc[0]
                        dated = '{}|{}'.format(core, date)  # Should make unique and an identifier

                        # NEW: Assign habitat from dataframe, and at Core instead of Site level
                        if 'Habitat__' in csv_df.columns:
                            habitat = core_df['Habitat__'].iloc[0]

                        elif area == 'Stordalen':
                            # Some Stordalen sheets don't have Habitat__ column; habitat was instead assigned from common_vars.
                            # TODO: For the future, it would be better to just always have a Habitat__ column.
                            habitat = site_tag[site]['Habitat Type']

                            # If the current site's habitat is a dict (because it varies by year), need to get the habitat for *this* year
                            if type(habitat) == dict:
                                # save the dict to a new variable, then re-assign habitat to the current habitat
                                habitat_dict = habitat
                                habitat = habitat_dict[date[0:4]]

                        else:
                            error('No habitat identified for {}, {}, core {}'.format(area, site, dated))

                        # Try to get exact coring date. Do this by first making a list with the coring dates first, then
                        # general dates, then porewater dates, then other dates. Then use the first non-nan value.
                        possible_dates = []
                        if 'DateC__' in csv_df.columns:
                            possible_dates += list(core_df['DateC__'])
                        if 'Date__' in csv_df.columns:
                            possible_dates += list(core_df['Date__'])
                        if 'DateP__' in csv_df.columns:
                            possible_dates += list(core_df['DateP__'])
                        if 'DateOther__' in csv_df.columns:
                            possible_dates += list(core_df['DateOther__'])

                        possible_dates = [d for d in possible_dates if not(pd.isnull(d) or d == 'NaN')]

                        try:
                            specific_date = possible_dates[0]
                        except IndexError:
                            warning('Could not find any specific dates for this {} site and core: {} {}.'.format(area, site, dated))
                            specific_date = 'NaN'

                        core_dict = {'GPS__': gps, 'Habitat__': habitat, 'Date__': specific_date}

                        core_node = nodes.Core(session=session, name=dated, gps=gps, site_node=site_node, fieldsampling_node=date_node, data_dict=core_dict)

                        core_node.node = session.write_transaction(core_node.update_labels, id_node=core_node.node._id,
                                                                   labels=[habitat] + additional_labels)

                        for depth, depth_df in core_df.groupby(by='DepthComplete__'):

                            # let's hope the rows (if there's multiple) are identical for these columns
                            depth_dict = {
                                'DepthMin__': str(depth_df['DepthMin__'].iloc[0]),
                                'DepthMax__': str(depth_df['DepthMax__'].iloc[0]),
                                'DepthAvg__': str(depth_df['DepthAvg__'].iloc[0]),
                                'Depth__': str(depth_df['Depth__'].iloc[0])
                            }

                            for col in sampleID_cols:
                                if len(np.unique(depth_df[col])) == 1:  # assign only if there's only one unique value
                                    depth_dict.update({col: str(depth_df[col].iloc[0])})

                            if 'DepthSubsample__' in csv_df.columns:
                                subdepth = depth_df['DepthSubsample__'].iloc[0]
                                if not(pd.isnull(subdepth)) and subdepth != 'NaN':
                                    depth_dict['DepthSubsample__'] = str(subdepth)

                            # TODO: Generalize this to work with non-soil cores.
                            depth_node = nodes.CoreDepth(session=session, name=depth, kind='Soil-Depth',
                                                         core_node=core_node, data_dict=depth_dict,
                                                         additional_labels=[habitat] + additional_labels,
                                                         update_existing='toValues')

                            for subdata, data_df in depth_df.groupby(by='DataSubsample__'):

                                if len(data_df) != 1:
                                    error('Identified duplicate depths ({}) from {}, {}, core {}, with no '
                                          'separating DataSubsample__.'.format(depth, area, site, core))

                                data_s = data_df.squeeze().fillna('NaN')

                                data_node = nodes.Data(session=session, update_existing=update_existing, metadata_node=metadata_node,
                                                       source_node=depth_node, source_id_key='DepthID',
                                                       rel_type=data_rel_type, data_dict=data_s.to_dict(), subdata=subdata)

                                data_node.node = session.write_transaction(data_node.update_labels,
                                                                           id_node=data_node.node._id,
                                                                           labels=[habitat, data_type_short] + additional_labels)
