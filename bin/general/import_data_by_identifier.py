#!/usr/bin/env python
"""
This script imports data to be attached to a given node type, with specific attachment nodes determined by a unique
identifier or set of identifiers. The resulting node structure is as follows:

  Area-[*]->(attachment node)-->Data
                                 |
                                 v
                              Metadata

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import sys
import os
import pandas as pd
import argparse

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.data.common_vars import *
from tools.data.common_vars_A2A import *
from tools.imports import nodes_new as nodes

import warnings

# warnings.filterwarnings("ignore", category=DeprecationWarning)
# warnings.filterwarnings("ignore", category=FutureWarning)

parser = argparse.ArgumentParser(description="Import data by identifiers")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with coring information")

inputs.add_argument('--columns', dest='columns_fp', default=None,
                    help="File with metadata describing columns in main input file.")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'],
                    default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

inputs.add_argument('--additional-labels', dest='additional_labels', default=None,
                    nargs='+',  # nargs='+' allows a list of arguments to be passed
                    help="Additional labels to add to all nodes created with this script.")

inputs.add_argument('--upstream-label', dest='upstream_label',
                    help='Label for the upstream nodes to which the Data nodes will be attached.')

inputs.add_argument('--upstream-identifiers', dest='upstream_identifiers', nargs='+',
                    help='Keys that can be collectively used to uniquely identify the upstream node to which each Data '
                         'node will be attached. Can specify any number of keys from 1 to the maximum length of '
                         'data_dict in nodes.Vertex.check_unique_names_dict() (currently 4).')

inputs.add_argument('--dataset-type', dest='data_type',
                    help="Dataset type label to be added to Data nodes, and relationships pointing to them, created "
                         "with this script. "
                         "These allowed dataset types are limited so as to avoid too many different labels in the DB. "
                         "If another dataset type needs to be added, please edit the script to add it.")

# inputs.add_argument('--area', dest='area',
#                     help="Which Area node the data is for (if not specified in an Area__ column).")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'

update_existing = args.update_existing


if __name__ == "__main__":

    # Input file
    csv_fp = args.input_fp

    # Additional labels
    if args.additional_labels is None:
        additional_labels = []
    else:
        additional_labels = args.additional_labels

    # Data type: (Depth-Info, Biogeochemistry, etc.)
    data_type = args.data_type
    data_rel_type = 'HAS_' + data_type.upper().replace('-', '_')
    if data_type.endswith(
            'Biogeochemistry'):  # allows for different types of biogeochemistry: solid, porewater, pH, etc.
        data_type_short = 'Biogeochemistry'
    elif data_type in ['Depth-Info',
                       'Site-Info',
                       'Vegetation']:  # list of other valid data types -- add to this as necessary, but keep it limited
        data_type_short = data_type
    else:
        error('Invalid data_type specified: {}'.format(data_type))

    # Upstream labels and identifiers
    upstream_label = args.upstream_label  # due to how this arg was defined, this should be a string

    # upstream_id_key:
    # For now, assign in the same manner as in nodes.Vertex.create_node_with_properties.
    # May need to create special cases later.
    if '-' in upstream_label:
        upstream_id_key = upstream_label.split('-')[-1] + 'ID'
    else:
        upstream_id_key = upstream_label + 'ID'

    upstream_identifiers = args.upstream_identifiers
    # due to how this arg was defined, this should be a list, even if only one identifier was passed

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:

        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False,
                                dtype=object)
        file_metadata = file_list.loc[
            file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = {key: file_metadata[key] for key in file_metadata.keys() if not (key.startswith('_'))}

        try:
            metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                           update_existing_data=update_existing)
        except TypeError:
            print(file_metadata)
            error('TypeError when attempting to set DatasetID on Metadata node: {}'.format(file_metadata['DatasetID']))

        additional_labels += file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        additional_labels = list(set(additional_labels))  # remove any inadvertently-created duplicates

        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # Add column info nodes.
        if args.columns_fp is not None:
            columns_fp = args.columns_fp
            columns_df = pd.read_csv(columns_fp, sep=',', dtype=object)
            # del columns_df['Unnamed: 0']

            for index, column_info in columns_df.iterrows():
                # delete leading space from column name to store
                column_name = column_info['Column__']
                column_attrs = column_info.squeeze().fillna('NaN').to_dict()

                column_node = nodes.Column(session=session, name=column_name, metadata_node=metadata_node,
                                           data_dict=column_attrs)

                column_node.node = session.write_transaction(column_node.update_labels, id_node=column_node.node._id,
                                                             labels=additional_labels)

        # DATAFILE ----

        print('Importing data from {}'.format(csv_fp))

        # Pre-process file
        csv_df = pd.read_csv(csv_fp, sep=',', quotechar='"', dtype=object)

        # if there's no DataSubsample__ column, add it, as DataSubsample__ is actually mandatory for
        #  Data nodes (at least as of 8/27/20; may change this later)
        if 'DataSubsample__' not in csv_df.columns:
            csv_df['DataSubsample__'] = 'NaN'

        # # NaN to 'NaN' string (necessary for any columns used directly for DataFrame.groupby())
        csv_df.DataSubsample__ = csv_df.DataSubsample__.fillna('NaN')

        for identifier in upstream_identifiers:
            csv_df[identifier] = csv_df[identifier].fillna('NaN')



        # NEW: Fill all NaN in the whole dataframe (not just in selected columns) with 'NaN' strings
        csv_df.fillna('NaN')

        for upstream_props, upstream_df in csv_df.groupby(by=upstream_identifiers):

            identifier_dict = {identifier: upstream_df[identifier].iloc[0] for identifier in upstream_identifiers}

            upstream_node = nodes.UniqueNode(session=session, kind=upstream_label, id_key=upstream_id_key,
                                             data_dict=identifier_dict)

            # Store habitat, if it exists
            if 'Habitat__' in upstream_node.node._properties.keys():
                custom_labels = [upstream_node.node._properties['Habitat__'], data_type_short]
            else:
                custom_labels = [data_type_short]

            for subdata, data_df in upstream_df.groupby(by='DataSubsample__'):

                if len(data_df) != 1:
                    error('Identified duplicate rows with no separating DataSubsample__:\n'
                          '{}'.format(data_df))

                data_s = data_df.squeeze().fillna('NaN')

                data_node = nodes.Data(session=session, update_existing=update_existing,
                                       metadata_node=metadata_node,
                                       source_node=upstream_node, source_id_key=upstream_id_key,
                                       rel_type=data_rel_type, data_dict=data_s.to_dict(),
                                       subdata=subdata)

                data_node.node = session.write_transaction(data_node.update_labels,
                                                           id_node=data_node.node._id,
                                                           labels=custom_labels + additional_labels)
