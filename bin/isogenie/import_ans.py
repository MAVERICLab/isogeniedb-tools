#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import argparse
from datetime import datetime
from dateutil.parser import parse
import pandas as pd

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.imports import nodes_new as nodes
from tools.data.common_vars import *

parser = argparse.ArgumentParser(description="Import ANS information into a neo4j database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with ANS information")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('-t', '--type', dest='type', choices=['hourly', 'daily', 'auto'], default='hourly',
                    help="What type of ANS data is being imported, either hourly or daily.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'
    
update_existing = args.update_existing

ans_info = {
    'Name': 'Abisko Scientific Research Station',
    'GPS__': 'N 68 21.22247, E 18 48.92975',
    'dataProvider': 'Patrick Crill',
    'contact': 'Annika Kristoffersson (annika.kristoffersson@ans.polar.se)'
    }

if __name__ == "__main__":

    # Input file
    ans_fp = args.input_fp

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:
    
        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # DATAFILE ----


        # Re-structured import process to include more verification and feedback at each level

        print('Importing ANS information from {}'.format(ans_fp))

        area_node = nodes.Area(session=session, name='Stordalen')
        area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id,
                                                   labels=additional_labels)

        ans_site_node = nodes.Site(session=session, name='ANS', area_node=area_node, data_dict=ans_info)

        ans_site_node.node = session.write_transaction(ans_site_node.update_labels, id_node=ans_site_node.node._id,
                                                       labels=additional_labels)

        ans_df = pd.read_csv(ans_fp, sep=',', quotechar='"', comment="#", dtype=object, header=0)

        if args.type == 'daily':
            ans_df[['Year', 'Month', 'Day']] = ans_df[['Year', 'Month', 'Day']].astype(int)
            ans_df['Date__'] = ans_df.apply(
                lambda x: datetime(year=x['Year'], month=x['Month'], day=x['Day']).date().isoformat(), axis=1)

            # convert month, day, & year back to strings to avoid DB import errors
            ans_df[['Year', 'Month', 'Day']] = ans_df[['Year', 'Month', 'Day']].astype(str)

        elif args.type == 'hourly':
            ans_df['Date__'] = ans_df['Time(UTC)'].apply(lambda x: parse(x).date().isoformat())
            ans_df['Hour__'] = ans_df['Time(UTC)'].apply(lambda x: parse(x).strftime('%H:00'))

            # For the new Summary_Hourly_ANS_Wx_20050825-20171231.txt file, omit dates before 7/26/2005 as they're
            #  already covered with better continuity in the *_hourly.txt files.
            if ans_fp.endswith("Summary_Hourly_ANS_Wx_20050825-20171231.txt"):
                ans_df = ans_df[ans_df['Date__'] >= '2005-07-26']

        else:
            error('Type not hourly or daily?: {}'.format(args.type))

        for date, date_df in ans_df.groupby(by='Date__'):
            if args.type == 'daily':
                if len(date_df.index) == 1:
                    date_info = date_df.iloc[0].squeeze().fillna('NaN').to_dict()

                elif len(date_df.index) == 2 and date == "2015-06-09" and ans_fp.endswith("ANS_Daily_Wx_Jul84_Dec17.txt"):
                    # THIS SPECIFIC DATE IS A KNOWN DUPLICATE; just use the 2nd row for now (which is closer to the
                    #  values on the immediately preceding & following dates for more variables), but add quality flag.
                    date_info = date_df.iloc[1].squeeze().fillna('NaN').to_dict()

                    quality_flag = "This date had duplicate rows in ANS_Daily_Wx_Jul84_Dec17.txt. Selected the " \
                                   "second row to import, as more of its data values were closer (compared to the " \
                                   "first row) to the average of the immediately preceding and following dates."
                    date_info["QualityFlag__"] = quality_flag
                    warning(date + ": " + quality_flag)

                else:
                    error('Identified more than one row ({}) with the same date with ANS type=daily: {}'.format(len(date_df.index), date))

                date_node = nodes.Date(session=session, name=date, source_node=ans_site_node, source_id_key='SiteID',
                                       metadata_node=metadata_node,
                                       update_existing=update_existing, data_dict=date_info)

                date_node.node = session.write_transaction(date_node.update_labels, id_node=date_node.node._id,
                                                           labels=['Data'] + additional_labels)

            if args.type == 'hourly':
                # Should be faster to only check for Date nodes for each unique date, instead of every single line
                date_node = nodes.Date(session=session, name=date, source_node=ans_site_node, source_id_key='SiteID',
                                       update_existing=update_existing)  # only difference from daily is that we're not supplying a data_dict

                date_node.node = session.write_transaction(date_node.update_labels, id_node=date_node.node._id,
                                                           labels=additional_labels)

                for hour, hour_df in date_df.groupby(by='Hour__'):
                #for index, time_s in date_df.iterrows():
                    # Note that *MANY* sets of duplicate rows exist in Summary_Hourly_ANS_Wx_20050825-20171231.txt,
                    #  with temperatures that differ substantially; therefore the previous simple iteration over rows
                    #  is likely to give invalid results! (Previous separate "hourly" files up to 2005-07-25
                    #  had no duplicates; it appears only the version 1.0.0 "summary" file is affected.)

                    if len(hour_df.index) == 1:
                        time_info = hour_df.iloc[0].squeeze().fillna('NaN').to_dict()

                    elif len(hour_df.index) == 2 and ans_fp.endswith("Summary_Hourly_ANS_Wx_20050825-20171231.txt"):
                        # duplicate hours (only do this for this specific file)
                        # TODO: Should ask about actually fixing this file; the workaround below may not be ideal for
                        #  duplicate rows with smaller differences between air temperature (e.g. 2011-04-06)

                        # first get the air temperature from the most-recently-saved Hour node
                        # (based on a manual check, all affected times should have a non-NaN value for this)
                        previous_T = float(time_node.node._properties["AirTemperature"])
                        row0_diff = abs(float(hour_df.iloc[0]["AirTemperature"]) - previous_T)
                        row1_diff = abs(float(hour_df.iloc[1]["AirTemperature"]) - previous_T)

                        max_diff = 5  # somewhat arbitrary value for maximum allowable difference

                        if (row0_diff < row1_diff) and (row0_diff < max_diff):
                            best_rownum = 0

                        elif (row1_diff < row0_diff) and (row1_diff < max_diff):
                            best_rownum = 1

                        else:
                            warning("{} {}: Could not make a suitable choice for the best duplicated row to import. "
                                    "Skipping this timepoint.".format(date, hour))
                            continue

                        # If it gets to this point, an ideal row should have been selected.
                        time_info = hour_df.iloc[best_rownum].squeeze().fillna('NaN').to_dict()

                        quality_flag = "This timepoint had duplicate rows in Summary_Hourly_ANS_Wx_20050825-20171231.txt. " \
                                       "Selected row #{} (of 2) to import, based on its AirTemperature being closer to " \
                                       "(and within {} degC of) the previous timepoint.".format(str(best_rownum + 1), str(max_diff))
                        time_info["QualityFlag__"] = quality_flag

                    else:
                        # >2 duplicates, or a different file: just skip this hour (with a warning-- let's not stop it
                        #  entirely just for hourly data, which take longer to import and are less likely to be queried)
                        warning("{} {}: Duplicated times could not be resolved using current import methods. "
                                "Skipping this timepoint.".format(date, hour))
                        continue

                    # By this point, valid time_info should be ready to import (if it exists)

                    time_node = nodes.Hour(session=session, name=time_info['Hour__'], date_node=date_node, metadata_node=metadata_node,
                                           update_existing=update_existing, data_dict=time_info)

                    time_node.node = session.write_transaction(time_node.update_labels, id_node=time_node.node._id,
                                                               labels=['Data'] + additional_labels)

