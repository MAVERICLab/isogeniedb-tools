#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
import argparse

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.data.common_vars import *
from tools.imports import nodes_new as nodes

import warnings
# warnings.filterwarnings("ignore", category=DeprecationWarning)
# warnings.filterwarnings("ignore", category=FutureWarning)

parser = argparse.ArgumentParser(description="Import cores")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with coring information")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'
    
update_existing = args.update_existing

# Includes two functions: get_all_date_ranges and import_cores

# Suzanne's new function for getting inclusive date ranges from standardized coring sheets:
# The result (a list of unique start and end dates for ALL coring sheets) will be an additional argument to import_chemistry
# Then at the beginning of import_chemistry, the start and end dates will be added as columns in chemistry_df
# UPDATE 4/24/20: This function appears to be outdated, but waiting to comment out until the next time the imports are tested.
def get_all_date_ranges(coring_sheets):
    # in init.py, this will accept core_list as the coring_sheets argument

    # put ALL the start and end dates into a long list of length-2-lists
    dates_list_long = []
    for coring_sheet in coring_sheets:
        coring_sheet_df = pd.read_csv(coring_sheet, sep=',', quotechar='"')
        dates_df = coring_sheet_df[['DateStart__', 'DateEnd__']]
        dates_list_long += dates_df.values.tolist()

    # remove duplicates
    dates_list_unique = []
    for dates in dates_list_long:
        if dates not in dates_list_unique:
            dates_list_unique.append(dates)

    return dates_list_unique


if __name__ == "__main__":

    # Input file
    core_fp = args.input_fp

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:

        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # DATAFILE ----

        # Re-structured import process to include more verification and feedback at each level
        print('Importing coring information from {}'.format(core_fp))

        # Pre-process file
        csv_df = pd.read_csv(core_fp, sep=',', quotechar='"', dtype=object)

        # NaN to 'NaN' string
        csv_df.DepthSubsample__ = csv_df.DepthSubsample__.fillna('NaN')
        csv_df.DepthMin__ = csv_df.DepthMin__.fillna('NaN')
        csv_df.DepthMax__ = csv_df.DepthMax__.fillna('NaN')
        csv_df.GPS__ = csv_df.GPS__.fillna('NaN')
        csv_df.DateC__ = csv_df.DateC__.fillna('NaN')
        csv_df.DateP__ = csv_df.DateP__.fillna('NaN')
        csv_df.DateOther__ = csv_df.DateOther__.fillna('NaN')

        for index, row in csv_df.iterrows():
            # Make additional column for combined Depth__ and DepthSubsample__
            if row['DepthSubsample__'] == 'NaN':
                csv_df.loc[index, 'DepthComplete__'] = row['Depth__']
            else:
                csv_df.loc[index, 'DepthComplete__'] = str(row['Depth__']) + '_' + str(row['DepthSubsample__'])

        area_node = nodes.Area(session=session, name='Stordalen')
        area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id,
                                                   labels=additional_labels)

        sites = csv_df['Site__'].unique()

        # Iterate through each site df, avoiding need to generate sites and then filter
        for site, site_df in csv_df.groupby(by='Site__'):

            habitat = site_tag[site]['Habitat Type']

            if type(habitat) == str:
                # Most cases (old default behavior): The site only has a single habitat.
                site_node = nodes.Site(session=session, name=site, area_node=area_node,
                                       data_dict={'Habitat__': habitat},
                                       additional_labels=[habitat] + additional_labels)
            else:
                # If habitat is a dict (because it varies by year), don't put into the Site node.
                # We will still use the dict later to add the appropriate habitat to downstream nodes.
                site_node = nodes.Site(session=session, name=site, area_node=area_node,
                                       additional_labels=additional_labels)

            for date, date_df in site_df.groupby(by='FieldSampling__'):

                fieldsampling_dict = dict(date_df[['DateStart__', 'DateEnd__', 'FieldSampling__']].iloc[0])
                fieldsampling_dict['FieldTeam__'] = field_teams[date]

                date_node = nodes.FieldSampling(session=session, name=date, area_node=area_node, data_dict=fieldsampling_dict)  # Will automatically create if doesn't exist

                date_node.node = session.write_transaction(date_node.update_labels, id_node=date_node.node._id,
                                                           labels=additional_labels)

                # If the current site's habitat varies by year, need to get the habitat for *this* year
                if type(habitat) == dict:
                    # save the dict to a new variable, then re-assign habitat to the current habitat
                    habitat_dict = habitat
                    habitat = habitat_dict[date[0:4]]

                for core, core_df in date_df.groupby(by='Core__'):

                    gps = core_df['GPS__'].iloc[0]
                    dated = '{}|{}'.format(core, date)  # Should make unique and an identifier

                    # Try to get exact coring date. Do this by first making a list with the coring dates first, then
                    # porewater dates, then other dates. Then use the first non-nan value.
                    possible_dates = list(core_df['DateC__']) + list(core_df['DateP__']) + list(core_df['DateOther__'])
                    possible_dates = [d for d in possible_dates if not(pd.isnull(d) or d == 'NaN')]
                    try:
                        specific_date = possible_dates[0]
                    except IndexError:
                        warning('Could not find any specific dates for this site and core: {} {}.'.format(site, dated))
                        specific_date = 'NaN'

                    core_dict = {
                        'GPS__': gps,
                        'Date__': specific_date,
                        'CoreGroup__': core_df['CoreGroup__'].iloc[0],
                        'Habitat__': habitat  # by now, habitat should be a string
                    }

                    core_node = nodes.Core(session=session, name=dated, gps=gps, site_node=site_node,
                                           fieldsampling_node=date_node, data_dict=core_dict,
                                           additional_labels=[habitat] + additional_labels, update_existing='missing')

                    for depth, depth_df in core_df.groupby(by='DepthComplete__'):  # Let's hope the depths are unique

                        if len(depth_df) != 1:
                            error('Identified multiple unique depths ({}) for core {}'.format(depth, core))

                        depth_s = depth_df.squeeze().fillna('NaN')

                        depth_dict = {
                            'DepthMin__': str(depth_s['DepthMin__']),
                            'DepthMax__': str(depth_s['DepthMax__']),
                            'DepthAvg__': str(depth_s['DepthAvg__']),
                            'Depth__': str(depth_s['Depth__']),
                            'DepthSubsample__': str(depth_s['DepthSubsample__']),
                            'SampleID__': str(depth_s['SampleID__']),
                            'CoreGroup__': str(depth_s['CoreGroup__']),  # Redundant with core_dict, but adding separately here because it's a new property, and needs to be updated in downstream nodes later using run_add_missing_sample_metadata_from_soildepths()
                            'Month__': str(int(depth_s['Month__'])),  # There's a small possibility that Month__ may not be the same for all core depths (e.g., if porewater was sampled on a different date within a different calendar month), so adding here instead of at the Core level
                            'Year__': str(int(depth_s['Year__']))
                        }

                        subdata = str(depth_s['DataSubsample__'])

                        depth_node = nodes.CoreDepth(session=session, name=depth, kind='Soil-Depth',
                                                     core_node=core_node, data_dict=depth_dict,
                                                     additional_labels=[habitat] + additional_labels,
                                                     update_existing='toValues')

                        # depth_node.node = session.write_transaction(depth_node.update_labels,
                        #                                             id_node=depth_node.node._id,
                        #                                             labels=[habitat] + additional_labels)

                        # Data
                        data_node = nodes.Data(session=session, update_existing=update_existing, metadata_node=metadata_node,
                                               source_node=depth_node, source_id_key='DepthID',
                                               rel_type='HAS_DEPTH_INFO', data_dict=depth_s.to_dict(), subdata=subdata)

                        data_node.node = session.write_transaction(data_node.update_labels,
                                                                   id_node=data_node.node._id,
                                                                   labels=[habitat, 'Depth-Info'] + additional_labels)
