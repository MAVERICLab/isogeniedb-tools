#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
from dateutil.parser import parse
import argparse

#from sklearn.impute import SimpleImputer

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.imports import nodes_new as nodes
from tools.data.common_vars import *

#pr = nodes.pr

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

parser = argparse.ArgumentParser(description="Import lake temperature information into a neo4j database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH',
                    help="Input file with lake temperature information")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import lake data into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n"
                         "\nIn *this* script, 'none' argument is automatically changed to 'missing' to take care of times that break across CSVs.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'
    
update_existing = args.update_existing
# Unlike other data types, lake hours can break across CSVs with different minutes missing from each CSV.
# Therefore, update_existing needs to have a value of at least 'missing' if not more comprehensive.
if update_existing == 'none':
    update_existing = 'missing'

citation = 'Wik, Martin, et al. "Multiyear measurements of ebullitive methane flux from three subarctic lakes." ' \
               'Journal of Geophysical Research: Biogeosciences 118.3 (2013): 1307-1321.'

dataDict = {
        'Inre Harrsjon': {
#            'GPS__': 'N 68 21.5395, E 19 02.6958',
            'GPS__': 'N 68 21.5140, E 19 02.7227',  # in the README there were 2 GPSs, this one matches lake depth map and other metadata
            'Short': 'IH',
            'contact': 'Patrick Crill',
            'citation': citation},
        'Mellan Harrsjon': {
#            'GPS__': 'N 68 21.5062, E 19 02.4605',
            'GPS__': 'N 68 21.5063, E 19 02.5022',  # in the README there were 2 GPSs, this one matches lake depth map and other metadata
            'Short': 'MH',
            'contact': 'Patrick Crill',
            'citation': citation},
        'Villasjon': {
#            'GPS__': 'N 68 21.2802, E 19 03.0700',
            'GPS__': 'N 68 21.2772, E 19 03.1307',  # in the README there were 2 GPSs, this one matches lake depth map and other metadata
            'Short': 'VS',
            'contact': 'Patrick Crill',
            'citation': citation}
    }

if __name__ == "__main__":
    
    # Input file
    lake_temps_fp = args.input_fp

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)
    
    with driver.session() as session:  # Putting all the code in one session to ensure causal consistency; not sure if this is necessary
    
        # METADATA ----
        
        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"```', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()
        
        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }
        
        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)
        
        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)
        
        # DATAFILE ----
    
        # Re-structured import process to include more verification and feedback at each level
        print('Importing lake temperature information from {}'.format(lake_temps_fp))
    
        lake_temps_df = pd.read_csv(lake_temps_fp, header=0, delimiter=';', dtype=object)  # in pandas, object == string
        lake_temps_df.dropna(axis='index', how='all', inplace=True)
    
        # Identify erronous data lines
        lake_times = [col for col in lake_temps_df.columns if '_Time' in col]  # list of time column names
        if '#VALUE!' in lake_temps_df[lake_times].values:
        
            for index, row in lake_temps_df[lake_times].iterrows():
                if '#VALUE!' in row.values:
                    corrected_times = [val for val in row.values if val != '#VALUE!']
                    corrected_times = list(set(corrected_times))  # remove duplicates
                    if len(corrected_times) == 1:
                        for col in lake_times:
                            if lake_temps_df.loc[index, col] == '#VALUE!':
                                lake_temps_df.loc[index, col] = corrected_times[0]
                    else:
                        error('Could not identify the correct time to replace missing value:\n{}'.format(row))
    
        # Separate dfs according to their lake
        mh_cols = [col for col in lake_temps_df.columns if 'MH_' in col]
        vs_cols = [col for col in lake_temps_df.columns if 'VS_' in col]
        ih_cols = [col for col in lake_temps_df.columns if 'IH_' in col]
    
        mh_lake_df = lake_temps_df[mh_cols].copy()
        vs_lake_df = lake_temps_df[vs_cols].copy()
        ih_lake_df = lake_temps_df[ih_cols].copy()
    
        lake_sites = ['Mellan Harrsjon', 'Villasjon', 'Inre Harrsjon']
        
        area_node = nodes.Area(session=session, name='Stordalen')
        area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id, labels=additional_labels)
    
        for lake_site, lake_df in zip(lake_sites, [mh_lake_df, vs_lake_df, ih_lake_df]):
            
            if lake_df.empty:  # If the site doesn't exist in the data table, skip this site
                continue
            
            lake_df.fillna('NaN', inplace=True)
            
            lake_site_node = nodes.Site(session=session, name=lake_site, area_node=area_node, data_dict=dataDict[lake_site])
            
            lake_short_name = dataDict[lake_site]['Short']

            lake_site_node.node = session.write_transaction(lake_site_node.update_labels, id_node=lake_site_node.node._id,
                                                            labels=[lake_short_name]+additional_labels)
    
            # each lake might not have the same number of rows, which breaks import of the ENTIRE lake from that file
            # therefore remove rows where _Time is NaN
            lake_df = lake_df[lake_df['{}_Time'.format(lake_short_name)] != 'NaN']
    
            try:
                lake_df['Date__'] = lake_df['{}_Time'.format(lake_short_name)].apply(lambda x: parse(x).date().isoformat())
            except Exception as e:  # should have been prevented with the code above, so changed from warning to error
                error('Error "{}" encountered. No data was imported from {} in {}'.format(e, lake_site, lake_temps_fp))
                continue
    
            lake_df['Hour__'] = lake_df['{}_Time'.format(lake_short_name)].apply(lambda x: parse(x).strftime('%H:00'))
            lake_df['Minute__'] = lake_df['{}_Time'.format(lake_short_name)].apply(lambda x: parse(x).strftime('%H:%M'))
            lake_df['T_Minute__'] = lake_df['Minute__'].apply(lambda x: 'T_' + x)  # put T_ at the beginning,
              # since otherwise it's not obvious to someone unfamiliar with the DB that these are temperatures
    
            # Depths (for coring information) are associated with cores, but here they're associated as a 'location' in
            # the vertical sense
    
            # Treat each depth as part of the site
            depth_columns = [depth for depth in lake_df.columns if ' m' in depth]
    
            for depth_column in depth_columns:  # Go depth by depth
    
                depth = depth_column.replace('{}_'.format(lake_short_name), '')
    
                depth_node = nodes.SiteDepth(session=session, name=depth, kind='Water-Depth', site_node=lake_site_node,
                                             gps=dataDict[lake_site]['GPS__'])

                depth_node.node = session.write_transaction(depth_node.update_labels, id_node=depth_node.node._id,
                                                            labels=[lake_short_name]+additional_labels)
    
                headers = ['{}_Time'.format(lake_short_name), '{}_DOY'.format(lake_short_name), 'Date__', 'Hour__',
                           'T_Minute__', depth_column]
    
                # Remove any columns that might not exist, e.g. DOY
                headers = [col for col in headers if col in lake_df.columns]
    
                # Make df with times, dates and ONE depth
                depth_df = lake_df[headers]
    
                for date, date_df in depth_df.groupby(by='Date__'):
                        
                    # DAILY NODES
                    date_info = pd.Series(date_df['{}_{}'.format(lake_short_name, depth)].values,
                                              index=date_df['T_Minute__'].values)
                    date_node = nodes.Date(session=session, name=date, source_node=depth_node, source_id_key='DepthID',
                                           update_existing=update_existing, metadata_node=metadata_node,
                                           data_dict=date_info.to_dict(), additional_labels=[lake_short_name, 'Data']+additional_labels)
    
    #pr.dump_stats('profile_driver.pstat')

