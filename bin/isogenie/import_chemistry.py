#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
import argparse

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.imports import nodes_new as nodes
from tools.data.common_vars import *

import warnings
# warnings.filterwarnings("ignore", category=DeprecationWarning)
# warnings.filterwarnings("ignore", category=FutureWarning)

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

parser = argparse.ArgumentParser(description="Import geochemistry information into a neo4j database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH',
                    help="Input file with geochemistry information")

inputs.add_argument('--columns', dest='columns_fp', default=None, help="File with metadata describing columns in main input file.")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import data into.")

inputs.add_argument('-e', '--errors', dest='is_error', action='store_true')

inputs.add_argument('--error-rel-type', dest='error_rel_type',
                    choices=['HAS_STD_ERRORS', 'HAS_STD_DEVS', 'HAS_95CONF_INTERVALS'], default='HAS_STD_ERRORS')

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'
    
update_existing = args.update_existing

if __name__ == "__main__":

    # Input file
    chemistry_fp = args.input_fp

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:
    
        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # Add column info nodes.
        if args.columns_fp is not None:
            columns_fp = args.columns_fp
            columns_df = pd.read_csv(columns_fp, sep=',', dtype=object)
            #del columns_df['Unnamed: 0']

            for index, column_info in columns_df.iterrows():
                column_name = column_info['Column__']
                column_attrs = column_info.squeeze().fillna('NaN').to_dict()

                column_node = nodes.Column(session=session, name=column_name, metadata_node=metadata_node,
                                           data_dict=column_attrs)

                column_node.node = session.write_transaction(column_node.update_labels, id_node=column_node.node._id,
                                                             labels=additional_labels)

        # DATAFILE ----

        # Re-structured import process to include more verification and feedback at each level
        print('Importing chemistry information from {}'.format(chemistry_fp))

        chemistry_df = pd.read_csv(chemistry_fp, sep=',', quotechar='"', dtype=object)

        chem_type = False
        if 'peat_' in chemistry_fp:
            chem_type = 'HAS_SOLID_BIOGEOCHEMISTRY'
            date_col = 'DateC__'
        elif 'poregas_' in chemistry_fp:
            chem_type = 'HAS_POREGAS_BIOGEOCHEMISTRY'
            date_col = 'DateP__'
        elif 'porewater_' in chemistry_fp:
            chem_type = 'HAS_POREWATER_BIOGEOCHEMISTRY'
            date_col = 'DateP__'
        elif 'IRMS_' in chemistry_fp:
            chem_type = 'HAS_POREWATER_GAS_ISOTOPE_BIOGEOCHEMISTRY'
            date_col = False
        elif 'Date__' in chemistry_df.columns:  # generic date type, therefore generic chem_type
            chem_type = 'HAS_BIOGEOCHEMISTRY'
            date_col = 'Date__'
        elif 'MasterPeat' in chemistry_fp:  # Rachel's sheet has porewater + peat combined
            chem_type = 'HAS_BIOGEOCHEMISTRY'
            date_col = False
        else:
            chem_type = 'HAS_BIOGEOCHEMISTRY'
            date_col = False
            warning('Could not identify chemistry data type for {}. Using generic {}.'.format(chemistry_fp, chem_type))

        # No longer require script to determine if it's main data or errors; let the user decide

        # Change NaN values to 'NaN' strings
        chemistry_df.DepthSubsample__ = chemistry_df.DepthSubsample__.fillna('NaN')
        chemistry_df.DataSubsample__ = chemistry_df.DataSubsample__.fillna('NaN')
        chemistry_df.DepthMin__ = chemistry_df.DepthMin__.fillna('NaN')
        chemistry_df.DepthMax__ = chemistry_df.DepthMax__.fillna('NaN')

        if 'GPS__' in chemistry_df.columns:
            chemistry_df.GPS__ = chemistry_df.GPS__.fillna('NaN')
        else:  # If GPS__ column doesn't exist (differently-named GPS columns should be renamed by now), just make it a column of NaN's
            chemistry_df['GPS__'] = 'NaN'

        if 'IncubationRep__' in chemistry_df.columns:
            chemistry_df.IncubationRep__ = chemistry_df.IncubationRep__.fillna('NaN')
        else:  # If the column exists but is all NaN, still need to fill with NaN again or it won't recognize it for grouping, even though it's already filled with NaN in the csv...
            chemistry_df['IncubationRep__'] = 'NaN'  # add it if it doesn't exist, even if it's not applicable

        for index, row in chemistry_df.iterrows():
            # Make additional column for combined Depth__ and DepthSubsample__
            if row['DepthSubsample__'] == 'NaN':
                chemistry_df.loc[index, 'DepthComplete__'] = row['Depth__']
            else:
                chemistry_df.loc[index, 'DepthComplete__'] = str(row['Depth__']) + '_' + str(row['DepthSubsample__'])

        # Basically convert YYYY-MM-DD to YYYYMMDD

        # TODO There's a bit of 'logic' in the date_range, which requires pre-knowledge of the date ranges of the coring
        #  sheets. In the end it creates DateStart__ and DateEnd__ for the chemistry data, but doesn't have effect outside
        #  of additional contextual information.

        area_node = nodes.Area(session=session, name='Stordalen')
        area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id,
                                                   labels=additional_labels)

        for site, site_df in chemistry_df.groupby(by='Site__'):

            habitat = site_tag[site]['Habitat Type']

            if type(habitat) == str:
                # Most cases (old default behavior): The site only has a single habitat.
                site_node = nodes.Site(session=session, name=site, area_node=area_node,
                                       data_dict={'Habitat__': habitat},
                                       additional_labels=[habitat] + additional_labels)
            else:
                # If habitat is a dict (because it varies by year), don't put into the Site node.
                # We will still use the dict later to add the appropriate habitat to downstream nodes.
                site_node = nodes.Site(session=session, name=site, area_node=area_node,
                                       additional_labels=additional_labels)

            for date, date_df in site_df.groupby(by='FieldSampling__'):

                fieldsampling_dict = dict(date_df[['FieldSampling__']].iloc[0])
                try:
                    fieldsampling_dict['FieldTeam__'] = field_teams[date]  # field_teams from common_vars
                except KeyError:
                    fieldsampling_dict['FieldTeam__'] = 'NaN'
                    warning('Unknown field team for {} field sampling'.format(date))

                date_node = nodes.FieldSampling(session=session, name=date, area_node=area_node, data_dict=fieldsampling_dict)  # Will automatically create if doesn't exist

                date_node.node = session.write_transaction(date_node.update_labels, id_node=date_node.node._id,
                                                           labels=additional_labels)

                # If the current site's habitat varies by year, need to get the habitat for *this* year
                if type(habitat) == dict:
                    # save the dict to a new variable, then re-assign habitat to the current habitat
                    habitat_dict = habitat
                    habitat = habitat_dict[date[0:4]]

                for core, core_df in date_df.groupby(by='Core__'):
                    dated_core = '{}|{}'.format(core, date)

                    gps = core_df['GPS__'].unique().tolist()

                    if len(gps) > 1:
                        error('Identified more than 1 unique GPS {} for a single core: {}'.format(gps, core))
                    if pd.isnull(gps):
                        error('Unable to identify GPS {} for core: {}'.format(gps, core))

                    gps = gps[0]

                    # Try to get exact coring date. Do this by using the first non-nan value in date_col.
                    # Due to the way the Core class is written (data_dict only is used on new nodes), this will not
                    # override the dates from coring sheets, only add dates to new Cores.
                    if date_col:
                        possible_dates = list(core_df[date_col])
                        possible_dates = [d for d in possible_dates if not(pd.isnull(d) or d == 'NaN')]
                        try:
                            specific_date = possible_dates[0]
                        except IndexError:
                            specific_date = 'NaN'
                    else:
                        specific_date = 'NaN'

                    core_dict = {
                        'GPS__': gps,
                        'Date__': specific_date,
                        'Habitat__': habitat  # by now, habitat should be a string
                    }

                    core_node = nodes.Core(session=session, name=dated_core, gps=gps, site_node=site_node,
                                           fieldsampling_node=date_node, data_dict=core_dict,
                                           additional_labels=[habitat] + additional_labels)

                    # If GPS exists, update it on any nodes that don't have it (this includes core_node above).
                    # Don't need to do this if args.is_error, because it will have already been updated when importing non-error data.
                    if not args.is_error and not(pd.isnull(gps)) and gps != 'NaN' and gps != '':
                        session.write_transaction(set_property_by_id, IDkey='CoreID', IDvalue=core_node.id,
                                                  key='GPS__', value=gps, missing_only=True)

                    for depth, depth_df in core_df.groupby(by='DepthComplete__'):

                        # Moved the depth node creation before looping over DataSubsample__, for consistency and to more easily accomodate IncubationRep__
                        # The following attributes should be the same across differnent DataSubsamples and IncubationReps from the same Depth
                        depth_dict = {
                            'Depth__': depth_df['Depth__'].iloc[0],
                            'DepthSubsample__': depth_df['DepthSubsample__'].iloc[0],
                            'DepthMin__': depth_df['DepthMin__'].iloc[0],
                            'DepthMax__': depth_df['DepthMax__'].iloc[0],
                            'DepthAvg__': depth_df['DepthAvg__'].iloc[0],
                        }

                        depth_node = nodes.CoreDepth(session=session, name=depth, kind='Soil-Depth',
                                                     core_node=core_node, data_dict=depth_dict,
                                                     additional_labels=[habitat] + additional_labels)

                        # depth_node.node = session.write_transaction(depth_node.update_labels, id_node=depth_node.node._id,
                        #                                             labels=[habitat] + additional_labels)

                        for incub, incub_df in depth_df.groupby(by='IncubationRep__'):
                            if incub == 'NaN' or pd.isnull(incub):
                                # if it's not incubation data, then the Data node(s) will be attached directly to the depth_node
                                data_source_node = depth_node
                                data_source_id_key = 'DepthID'
                            else:
                                # If it's incubation data, then we need to create the associated nodes.
                                # Note this does not account for different amounts of elapsed time within the same incubation rep.
                                # Rachel's MasterPeat sheet only has one day for each rep, but we'll hopefully be getting more detailed data later, which will necessitate additional node classes.
                                incubation_node = nodes.IncubationRep(session=session, name=incub, depth_node=depth_node)

                                incubation_node.node = session.write_transaction(incubation_node.update_labels,
                                                                                 id_node=incubation_node.node._id,
                                                                                 labels=[habitat, 'Incubation'] + additional_labels)

                                data_source_node = incubation_node
                                data_source_id_key = 'IncubationRepID'

                            if len(incub_df) != 1:
                                # If there's still more than one line of data by this point, they need to have
                                #  differentiating DataSubsample__
                                if len(incub_df) != len(incub_df['DataSubsample__'].unique()):
                                    error('Identified duplicate depths ({}) or incubations ({}) to Core ({}) from Site ({}), with no '
                                          'separating DataSubsample__.'.format(depth, incub, dated_core, site))


                            for data, data_df in incub_df.groupby(by='DataSubsample__'):

                                chemistry_info = data_df.squeeze().fillna('NaN')

                                subdata = str(chemistry_info['DataSubsample__'])

                                if args.is_error:
                                    no_create = True
                                else:
                                    no_create = False

                                data_node = nodes.Data(session=session, source_node=data_source_node, metadata_node=metadata_node,
                                                       source_id_key=data_source_id_key, rel_type=chem_type,
                                                       data_dict=chemistry_info.to_dict(), subdata=subdata,
                                                       no_create=no_create, update_existing=update_existing)

                                data_node.node = session.write_transaction(data_node.update_labels,
                                                                           id_node=data_node.node._id,
                                                                           labels=[habitat, 'Biogeochemistry'] + additional_labels)

                                if data_source_id_key == 'IncubationRepID':
                                    data_node.node = session.write_transaction(data_node.update_labels,
                                                                               id_node=data_node.node._id,
                                                                               labels=['Incubation'])

                                if args.is_error:
                                    if not data_node.node:
                                        error('Could not find a Data node matching error data: depth: {}, core: {}, date: {}, site:'
                                              ' {}'.format(depth, core, date, site))

                                    error_node = nodes.Errors(session=session, name=subdata, update_existing=update_existing,
                                                              rel_type=args.error_rel_type, data_node=data_node, metadata_node=metadata_node,
                                                              error_dict=chemistry_info.to_dict())

                                    error_node.node = session.write_transaction(error_node.update_labels,
                                                                               id_node=error_node.node._id,
                                                                               labels=[habitat, 'Biogeochemistry'] + additional_labels)
