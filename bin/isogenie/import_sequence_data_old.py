#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import argparse
import time
from distutils.version import StrictVersion
import pandas as pd
from Bio import SeqIO
from py2neo.database import DatabaseError, NodeSelector
from py2neo.packages.httpstream.http import SocketError
from py2neo import Relationship
import gzip
import uuid
from datetime import datetime
import neobolt

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.utilities.utils import error, warning, printv
from tools.imports import nodes_old as nodes
# TODO: Make this use nodes_new, thus enabling the use of new functionality (particularly the option to specify
#  keepMetadataProps in get_upstreams()) and eliminating py2neo entirely. Also see if there's any way to make this use
#  instances of node classes (even if needing to add more options to initialize them quickly using a list of properties)
from tools.data.common_vars import *
from local_file_paths import config_file_1
from pprint import pprint

parser = argparse.ArgumentParser(
    description="Import sequence data information and [potentially] the sequences into the  neo4j graph database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH',
                    help="File containing information about the sequence data.  MUST include sufficient information to"
                         " identify a UNIQUE node. Will not create any new nodes except those of the sequence type. "
                         "Can optionally include 'target_path', which will overwrite location of the sequence data "
                         "source file. Useful if original import location is different than target location.")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

configs = parser.add_argument_group('Configurations')

configs.add_argument('--neo4j-driver', dest='neo4j_driver', action='store_true',
                     help="Use the neo4j driver instead of py2neo for importing the individual sequences. This is ~3x "
                          "faster and should work fine, but is new and doesn't use as much of the class functionality.")

configs.add_argument('--ignore-sequences', dest='ignore_sequences', action='store_true',
                     help="Ignore missing sequence files, (i.e. those specified in the sequence info file). This is "
                          "STRONGLY DISCOURAGED as it assumes the target location will have that file path available for"
                          " linking.")

configs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'],
                     default='none',
                     help="Whether to update existing nodes with new data, and which values to update."
                          "Options, in order of comprehensiveness:\n\n"
                          "none          Does nothing.\n"
                          "missing       Replaces ONLY missing data with new_data.\n"
                          "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                          "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

configs.add_argument('--dry-run', dest='dry', action='store_true',
                     help="Run the importer in dry mode, which will not create any new nodes but show their creation. "
                          "Useful for double-checking that 'source' nodes, that correspond to the sequencing data, "
                          "exist")  # This is causing lots of errors, so is not recommended.

configs.add_argument('--skip-individual-sequences', dest='skip_individual_sequences', action='store_true',
                     help="Run the importer in 'semi-dry' mode, which will not create any individual sequence nodes,"
                          "but will still create sequence collection and sequence origin nodes. Useful if wanting to "
                          "import these 'big picture' nodes quickly without having to wait for the creation of 1000s "
                          "of individual sequence nodes.")

configs.add_argument('--clear-sequences', dest='clear_sequences', action='store_true',
                     help="Remove existing sequence collections from the database. This is useful for testing or debug "
                          "or as a failsafe if import goes terribly wrong.")

configs.add_argument('--verbose', dest='verbose', action='store_true',
                     help="Display ALL messages. If not used, the many messages about adding or "
                          "reusing nodes are suppressed (doesn't affect warnings and errors).")

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'

# Suppress DeprecationWarning messages if not args.verbose
# if not args.verbose:
#     import warnings
#     warnings.filterwarnings("ignore", category=DeprecationWarning)

update_existing = args.update_existing


def check_names(graph, label, key, id):
    """Checks for multiple nodes in the database (graph) with label=label, property_key=key,
    and property_value=id. Returns nodes if they exist, or False if no node."""

    seconds = 0

    while True:

        try:
            nodes = graph.find(label, key, id)

            if not nodes:
                return False
            else:
                return nodes

        except SocketError:
            if seconds > 60:
                error('Too many socket errors.')
            else:
                print('Socket error. Trying again in 10 seconds.')
                seconds += 10
                time.sleep(10)

        except DatabaseError:
            if seconds > 60:
                error('Too many cypher errors.')
            else:
                print('Cypher error. Trying again in 10 seconds.')
                seconds += 10
                time.sleep(10)


def update_labels(node, attrs):
    # Get habitat associated with site
    if not isinstance(attrs, list):
        attrs = [attrs]

    for attr in attrs:
        if not node.has_label(attr):
            node.update_labels(attrs)
            node.push()


# These are those dependent on data type, but for the moment, only have 1 data type to associate with sequence data
# Eventually new sequence data may come from places other than soil-depths (10m in the air column, for ex.)
essential_headers = {
    'Soil-Depth': [
        'Area__',
        'Site__',
        'CoreDated__',
        'Depth__'
    ]
}

# These are unifying identifiers for each of the data source types. Used to 'mark' a node for subsequent node-"ship"
unique_keys = {
    'Soil-Depth': 'DepthID'
}


def validate_input(df: pd.DataFrame):
    mandatory_headers = {
        'node_label',
        'data_type',
        'Version__',
        'upload_path',  # This seems redundant with imported_file_href__, which is a mostly empty column (accept for one
                        #  link which appears wrong). Should probably use that instead, for consistency with other data.
        'imported_file_href__',
        'collection_name',
        'Quality__'
    }

    # Check for essential headers
    if not all(header in df.columns.tolist() for header in mandatory_headers):
        error('Unable to identify essential headers in the input file.')

    # Check if files exist at upload paths
    if args.ignore_sequences:
        warning('Skipping the file path validation due to --ignore-sequences argument.')
    else:
        all_fps = df['upload_path'].tolist()  # list of all entries under the upload_path column, some of which are themselves lists

        for fps in all_fps:

            if pd.isnull(fps):
                warning('The file import list is missing a file path.')
                continue

            # split any multi-entry file paths by semicolons
            fps_list = fps.split(';')

            for fp in fps_list:
                fp_cleaned = fp.replace(' ', '')  # delete residual spaces which are sometimes (but not always) after the semicolons in fps

                # Add root dir for test and local DBs
                if args.database in ['local', 'test']:
                    fp_cleaned = os.path.join(root_dir, fp_cleaned)

                if not os.path.exists(fp_cleaned):
                    if fp_cleaned.endswith('fastq.gz'):  # First see if another zipped/unzipped version exists
                        fp_alt = fp_cleaned[:-3]  # remove the last three characters to check for an unzipped file
                        if os.path.exists(fp_alt):
                            warning('The import list shows a nonexistent .gz file, but an unzipped version exists: \n{}\n'
                                    'This filename should be changed in the import list to reflect the unzipped version '
                                    'that actually exists.'.format(fp_cleaned))
                        else:
                            error('Unable to find {} specified in the input file.'.format(fp_cleaned))
                    else:
                        error('Unable to find {} specified in the input file.'.format(fp_cleaned))
        printv('Validated file paths')

    # Currently available sequence types, can include more when we know the breadth of sequence types in the project
    avail_sequence_types = {
        'Reads-Raw',
        'Reads-QC',
        'Contigs-General',
        'Contigs-Viral',
        'Contigs-Microbe',
        'MAG'
    }

    # Check to see if the data type and target node types are even valid
    in_data_types = df['data_type'].tolist()
    if not set(in_data_types).issubset(avail_sequence_types):
        error('Not all of the data types used in the input file were valid. Valid data types include:\n{}'.format(
            '\n'.join(avail_sequence_types))
        )

    # Check to ensure that the headers (associated with the target data type) are present
    target_types = df['node_label'].tolist()

    headers_to_search = set()

    for dtype in target_types:
        headers = set(essential_headers[dtype])
        headers_to_search.update(headers)

    # Now search for those headers
    if not all(header in df.columns.tolist() for header in headers_to_search):
        error('Unable to identify mandatory headers that are associated with the target node type in the input file.')

    return True


def check_relationships(graph, start_node, relationship, end_node):
    """
    Keep separate the checking, and creation of, sequence-data-affiliated nodes. Dry-run won't need to be implemented
    here, as there's no data creation, just returning nodes or need to creates.
    :param graph:
    :param start_node:
    :param relationship:
    :param end_node:
    :return:
    """
    relationships = [rel for rel in graph.match(start_node=start_node, rel_type=relationship, end_node=end_node)]

    if len(relationships) > 1:
        warning('Too many nodes identified in association with this sequence data. There should only be one. Skipping')
        return 'WARN'

    if len(relationships) == 1:
        # Need to check to see if need to update
        node = relationships[0].end_node()

        return node

    if len(relationships) == 0:
        printv('No node identified. This is fine if this is the first instance of this node.')  # This used to be a warning, which doesn't seem necessary
        return False  # Would like to have another term


def create_sequence_collection_node(graph, source_node, source_node_key, source_node_key_value, subdata,
                                    relation_type='HAS_SEQUENCE_DATA', data_dict={}, additional_labels=[], no_create=True):
    # 70% of Data class here is useful, but otherwise involves more checks for a singular node that doesn't exist
    # Hopefully this is a faithful representation of the data node, but more 'directly' created
    # Probably don't want to do this routinely
    if no_create:
        warning('Did not create sequence collection node attached to {} due to --dry-run'.format(source_node_key_value))
        return False, False

    node, node_id = nodes.Data.create_node(nodes.Vertex(name=subdata, kind='Collection'), graphDB=graph,
                                           label='Collection', metadata=subdata, sourceNodes=source_node,
                                           sourceIDKey=source_node_key, sourceID=source_node_key_value,
                                           rel_type=relation_type, dataDict=data_dict)

    # SUGGESTION: The kind for Data nodes **must** either == 'Data' or end in '-Data' (separated by a hyphen) so
    #  that the ID name can automatically be assigned as 'DataID' under the new create_node() method. Otherwise,
    #  if using e.g. kind='Raw Reads', the ID label then automatically becomes 'Raw ReadsID' instead of 'DataID'.
    # Better to keep the 'Data' label and then add any additional labels using update_labels (which I did below).
    # Unless you specifically *want* the ID to be called something custom, but then you'd need to write a new class
    # so that it's not using 'DataID' to check if a found node is the same node.
    # UPDATE 12/6/19: Ah, I now see the rationale for invoking create_node directly... otherwise have to separately find
    # the upstream nodes for depths, cores, etc., which is not really necessary unless the data import file would have
    # "new" depths (it does have "pooled" and "unknown" depths, but these are already not straightforward to import...),
    # plus it would necessitate doing the same complex process for any future non-soil-related microbial data.

    node.update_labels(additional_labels)
    node.push()

    printv('Created sequence "Collection" node: {}'.format(node_id))
    return node, node_id


def create_sequence_origin_node(graph, source_node, source_node_key, source_node_value, subdata, relation_type='HAS',
                                data_dict={}, additional_labels=[], no_create=True):
    # Unlike other node creation functions, sequence data is unique in that many of the "discretionary" arguments are
    # mandatory values
    # This is also unique in that the sequence data represents a collection of nodes, which correspond to individual
    # sequence files

    if no_create:
        warning('Did not create sequence node attached to {} due to --dry-run'.format(source_node_value))
        return False, False

    if relation_type.startswith('HAS_MAG'):
        data_type = 'MAG'
    else:
        data_type = relation_type.replace('HAS_', '')

    # Hopefully the following will be unique
    datasetID = data_type + '_' + subdata

    metadata_properties = {
        'DatasetID': datasetID,
        'MetadataID': str(uuid.uuid4()),
        'Metadata__': datasetID
        # Other Metadata properties (Version__, imported_file_href__, etc.) should already be in the CSV.
    }

    data_dict.update(metadata_properties)

    # Create 'origin' node that connects to all the actual sequence data
    node, node_id = nodes.Data.create_node(nodes.Vertex(name=subdata, kind='Data'), graphDB=graph,
                                           label='Data', metadata=subdata, sourceNodes=source_node,
                                           sourceIDKey=source_node_key, sourceID=source_node_value,
                                           rel_type=relation_type, dataDict=data_dict)

    if 'Metadata' not in additional_labels:
        additional_labels += ['Metadata']  # want to make sure these have BOTH 'Data' and 'Metadata' labels

    node.update_labels(additional_labels)
    node.push()

    printv('Created sequence origin node: {}'.format(node_id))

    return node, node_id


def read_nodes_driver(tx, query, node_variable='n'):
    # Unlike the other functions for the neo4j driver, this takes any query as its first argument.
    query_response = tx.run(query)
    return [record[node_variable] for record in query_response]


def create_node_driver(tx, metadata, kind, labels, dataDict, sourceNode):

    if kind not in labels:
        labels += [kind]

    labels_str = ':'.join(labels)

    # Some code below copied from Vertex.create_node()

    # Assign metadataType, the string appended to '__' or 'ID' to build the canonical ID and property keys.
    if '-' in kind:
        # Use of too-specific metadataType, resulting in properties like 'Soil-Depth__', is strongly discouraged.
        # Therefore use only the last part of the label after the hyphen ('Depth__' instead of 'Soil-Depth__')
        metadataType = kind.split('-')[-1]
    else:
        metadataType = kind

    # Make the canonical ID and __ keys
    metadataID = metadataType + 'ID'
    metadataKey = metadataType + '__'

    dataDict.update({
        'CreationDate__': datetime.now().strftime('%Y%m%d'),
        metadataID: str(uuid.uuid4()),
        metadataKey: str(metadata)
    })

    # remove null values, which cause problems with the neo4j browser rendering
    dataDict = {k: dataDict[k] for k in dataDict if not(pd.isnull(dataDict[k]))}

    upstream_dict = nodes.Vertex.get_upstreams(nodes.Vertex(), sourceNode=sourceNode, data_dict=dataDict)
        # although sourceNode is an instance of the py2neo Node class, the function get_upstreams doesn't actually
        #  interact with the database using py2neo
    dataDict.update(upstream_dict)

    query_string = "CREATE (n:%s) SET n={properties} RETURN n.%s" % (labels_str, metadataID)

    try:
        return tx.run(statement=query_string, parameters={'properties': dataDict}).single().value()
    except neobolt.exceptions.CypherSyntaxError:
        error('Invalid Cypher query: {}'.format(query_string))


def create_relationship_driver(tx, sourceLabels, sourceIDKey, sourceIDValue, endLabels, endIDKey, endIDValue, rel_type):
    query_string = "MATCH (a:%s {%s: '%s'}) " \
                   "MATCH (b:%s {%s: '%s'}) " \
                   "MERGE (a)-[:%s]->(b) " % (sourceLabels, sourceIDKey, sourceIDValue, endLabels, endIDKey, endIDValue, rel_type)

    try:
        return tx.run(query_string)
    except neobolt.exceptions.CypherSyntaxError:
        error('Invalid Cypher query: {}'.format(query_string))


if __name__ == "__main__":

    """
    Sequence data is unlike other data types, as those are usually single-value attributes. Even temporal data has a 
    value (i.e. temp) associated with a specific time. Sequence data has many "values" - all deriving from the same 
    depth. Additionally, sequence data can - and often is - larger than available storage capacities. The logic for 
    this import ASSUMES that the attachment point (neo4j node) exists, and will adjust its import strategy based on the
     size and number of sequence elements in the import file.

    One of the interesting aspects of sequencing data is that all [at least every element of data encountered thus far] 
    MUST derive from a depth of some sort, either Soil- or Lake-Depth. However, due to the database structure, all the 
    upstream nodes must be identified before sequencing data can be uploaded. Therefore, much of this code could clearly
     duplicate the import_cores script.
    """

    # Validate database
    graph_db, root_dir = load_config(args.config_fp, args.database)  # config_file (path) defined in local_file_paths

    if args.neo4j_driver:
        driver, root_dir = load_config_driver(config_file_1, args.database)

    # Input file
    seq_fp = args.input_fp

    # Check for file structure
    print('Validating sequencing information from {}'.format(seq_fp))
    seq_df = pd.read_csv(seq_fp, delimiter=',', header=0, index_col=0, dtype=object)

    # TODO: This file (seq_fp, usually "IsoGenieDB-Sequencing_Imports.csv") should be given its own Metadata node!!!
    #  Connect this Metadata node to the nodes containing all the non-standardized properties (BioSample, SRR, etc.) in
    #  that sheet, unless they're Sequence nodes. These would be the Data/Metadata nodes connected to the Collection
    #  nodes.

    res = validate_input(seq_df)

    if res:  # All fails are caught during the checks, so this is kinda response
        print('Input file successfully validated!')

    if args.clear_sequences:

        # SUZANNE UPDATE 12/12/19: Added the label 'Sequencing' to all sequence-related data, so this is now much simpler

        sequencing_delete = 'MATCH (n:Sequencing) DETACH DELETE (n)'
        response = graph_db.run(sequencing_delete).data()

        print('Removed all relationships and sequence nodes')

    time_start = time.time()

    # Re-structured import process to include more verification and feedback at each level
    print('Importing sequencing information from {}'.format(seq_fp))

    for index, row in seq_df.iterrows():

        # Find target node (i.e. node that the data will be associated with)
        # This is a huge redesign of how querying nodes has been done. It uses a cypher query directly instead of
        # iterately searching down the graph tree. It finds a SINGLE node (if it exists, otherwise error), returns the
        # node, then checks for relationships to it. If no sequence data relationship, make one, else update

        node_type = row['node_label']
        node_params = essential_headers[node_type]
        params_dict = {k: row[k] for k in node_params}
        collection_name = row['collection_name']
        additional_labels = row['Availability__'].split(', ') + ['Sequencing']  # Everything gets these labels

        # need to make dict string, but straight replacement is bad for cypher queries...
        params_str = ''
        for param, value in params_dict.items():
            params_str += " `{}`:'{}',".format(param, value)

        query = "MATCH (n:`{}` {{{}}}) RETURN n".format(node_type, params_str[:-1])

        # TODO: Make this work for non-exact depth matches, using the depth matching procedures in the CoreDepth class.
        #  This may require some substantial rewrites.

        try:
            response, = graph_db.run(query).data()
        except ValueError:  # No matches (could/should just not use , and get ACTUAL response
            warning('Unable to find target node for line {} from input file. This line was not imported.'.format(index))
            continue  # Should this error out?

        if len(response) > 1:
            error('Identified TWO target nodes for line {} from input file.'.format(index))

        # It's easier to do this than mess w/ py2neo's cursor
        node_df = pd.DataFrame.from_dict(response).squeeze()

        # Get UUID for attachment node
        unique_key = unique_keys[node_type]
        nodeID = node_df[unique_key]  # For node type, get its equivalent identifier

        # Get node - with specific ID, no need to search through everything
        # selector = NodeSelector(graph_db)  # Node selector *should* work, as the cypher equivalent works....
        # selected = selector.select(node_type, unique_key=nodeID)

        source_node = graph_db.find_one(node_type, unique_key, nodeID)  # Node that sequence data collection to attach

        if not source_node:
            warning('Could not identify target node to associate sequence data with. It will not be created.')
            continue
        else:
            printv('Identified target node for {}: {}'.format(unique_key, nodeID))

        # Check for sequence data node, HAS_SEQUENCE_DATA
        collection_n = check_relationships(graph_db, source_node, 'HAS_SEQUENCE_DATA', None)
        collection_nID = None

        # Create sequence collection to "collect" all the sequencing data generated by a specific depth
        if collection_n == 'WARN':
            collection_nID = collection_n['CollectionID']  # TODO JUST added
        elif not collection_n:
            collection_n, collection_nID = create_sequence_collection_node(
                graph_db, source_node=source_node, source_node_key=unique_key, source_node_key_value=nodeID,
                subdata='SequenceCollection', relation_type='HAS_SEQUENCE_DATA', data_dict={},
                additional_labels=additional_labels, no_create=args.dry)

        elif collection_n:
            collection_nID = collection_n['CollectionID']
            printv('Identified pre-existing Collection node ID: {}'.format(collection_nID))
            # Need to update sequence node with additional labels beyond 'Collection'

        # Since MAGs are a collection of their own (each MAG can consist of 1 - 100s of contigs), must create and
        # connect to
        mag_collection_nID = None
        if node_type == 'MAG':  # QUESTION: Is this a typo? Until now, node_type was for the source node
            mag_collection_n = check_relationships(graph_db, collection_n, 'HAS_MAG_DATA', None)
            unique_key = 'CollectionID'

            if mag_collection_n == 'WARN':
                mag_collection_nID = mag_collection_n['CollectionID']
                printv('Identified pre-existing Collection node ID: {}'.format(mag_collection_nID))
            elif not mag_collection_n:
                mag_collection_n, mag_collection_nID = create_sequence_collection_node(
                    graph_db, source_node=collection_n, source_node_key=unique_key,
                    source_node_key_value=collection_nID,
                    subdata=collection_name, relation_type='HAS_MAG_DATA', data_dict={},
                    additional_labels=additional_labels, no_create=args.dry)
                mag_collection_n.update_labels(['MAGs'])
                mag_collection_n.push()

                collection_n = mag_collection_n  # Need to ensure that MAGs are being connected to MAG Collection
                collection_nID = mag_collection_nID

        # Now that the sequence collection data node has been created OR found, search through for specific sequence
        #  data types.
        if row['data_type'] == 'MAG':
            # For all other data types
            adj_dtype = 'HAS_MAG_{}'.format(collection_name)  # QUESTION: Do we really want the full MAG name in the relationship name?
        else:
            adj_dtype = 'HAS_{}'.format(row['data_type'].replace('-', '_').upper())  # CONTIGS_VIRAL, READS_QC, READS_RAW

        # This checks if the "sequence data" collection node has that TYPE of sequence data associated with it.
        # So while they'll be only 1 sequence data node for each depth, there can be multiple origin_nodes that
        # correspond to each sequence data type (reads, contigs, MAGs, metaT, metaP, etc).
        origin_n = check_relationships(graph_db, collection_n, adj_dtype, None)
        # For MAGs, origin is an actual MAG collection, i.e. bog_213_acidobacteria and NOT contig1, contig2, contig3.

        # Needs to include values which would NOT have been included up to this point.
        # VERSIONS ARE INDIVIDUALLY SEQUENCE SPECIFIC. They get applied to entire dataset upon import, but any updates
        # can include a subset of those sequences, which have upgraded versions. This allows for multiple versions of
        # different sequences under the same collection.

        # Keys that need to be added to the specific sequence type's data node.
        # UPDATE 12/12/19: Instead of selecting keys to keep, just use the whole row by default and choose which ones to omit.
        #  As originally written, the DB didn't store some very important info such as BioSample numbers.

        # data_keys = ['node_label', 'data_type', 'Version__', 'upload_path', 'imported_file_href__',
        #              'collection_name', 'Quality__']
        #
        # data_dict = {k: row[k] for k in data_keys}
        data_dict = row.to_dict()

        # Delete the 'node_label' key, because it's only used for import purposes, and also could create confusion as it
        # doesn't refer to the node in which it's stored.
        try:
            del data_dict['node_label']
        except KeyError:
            pass

        if not origin_n:
            # Create data node
            # subdata between collection_n and sequence_origin_n must be identical or it will trigger an upstream
            # error and fail.
            # Actually still can fail, line 322 nodes.py - need to double-check.
            # If can't fix it, will *need* to write a specific class for Sequence data types.
            origin_n, origin_n_id = create_sequence_origin_node(
                graph_db, source_node=collection_n, source_node_key='CollectionID', source_node_value=collection_nID,
                subdata=collection_name, relation_type=adj_dtype, data_dict=data_dict,
                additional_labels=additional_labels+[row['data_type']], no_create=args.dry)
            # The origin node will have collection_n & _nID equal to the MAG if MAGs are imported

        else:
            origin_n_id = origin_n['DataID']
            nodes.Data.update_values(nodes.Vertex(name=collection_name, kind='Data'), node=origin_n, new_data=data_dict,
                                     update_existing=update_existing)

        # Now, let's crunch a lot of sequences
        if not args.skip_individual_sequences:
            individual_sequence_types = ['Contigs-General', 'Contigs-Viral', 'Contigs-Microbe', 'MAG']
            href_sequence_types = ['Reads-Raw', 'Reads-QC']
            if data_dict['data_type'] in individual_sequence_types:  # Here we go!

                # Add root dir for test and local DBs
                if args.database in ['local', 'test']:
                    input_fp = os.path.join(root_dir, row['upload_path'])
                else:
                    input_fp = row['upload_path']

                input_fh = None
                if any(input_fp.endswith(end) for end in ['.fasta', '.fna']):
                    input_fh = open(input_fp, 'rU')
                elif input_fp.endswith('.gz') and not input_fp.endswith('.tar.gz'):
                    input_fh = gzip.open(input_fp, 'rt')
                else:
                    error('Unable to identify sequence filetype')

                for record in SeqIO.parse(input_fh, 'fasta'):
                    record_id = record.id
                    record_desc = record.description
                    record_seq = record.seq

                    # Identify a node that matches those characteristics
                    sequence_query = "MATCH (n:`Data` {{`DataID`:'{}'}})-[r:`HAS_SEQUENCE`]->(t:`Sequence` {{`ID`: '{}'}}) RETURN n, t, r".format(
                        origin_n_id, record_id
                    )

                    try:

                        if args.neo4j_driver:
                            with driver.session() as session:
                                response = session.read_transaction(read_nodes_driver, query=sequence_query, node_variable='t')

                                if len(response) == 1:

                                    node_data_version = response[0]._properties['Version__']
                                    input_data_version = row['Version__']

                                    node_seq_id = response[0]._properties['SequenceID']

                                elif len(response) == 0:
                                    raise ValueError  # if didn't find anything, should proceed to "except ValueError" to create the node

                                else:
                                    error('{} existing sequence nodes identified with ID {}; there should be either 0 or 1.'.format(len(response), record_id))

                        else:
                            response, = graph_db.run(sequence_query)

                            df = pd.DataFrame.from_dict(response.data())  # n and t as columns

                            node_data_version = df.loc['Version__', 't']
                            input_data_version = row['Version__']

                            node_seq_id = df.loc['SequenceID', 't']

                        # TODO: Figure out how to make the code below work well with the "normal" mode of updating data
                        #  using update_existing (or if this is necessary).
                        if StrictVersion(node_data_version) < StrictVersion(input_data_version):  # Need to update
                            # Remove existing edge from old sequence node, add new sequence node, attach old node to
                            # new node and add "HAS_OLD_VERSION" or something like that

                            # TODO: Make a neo4j driver version of the code below (though won't encounter this until we
                            #  get a new version of existing sequence data).

                            print('Identified an out-of-date version of this sequence in the database ({} vs {}). If '
                                  'this was expected, then all is fine. If not, double-check the versioning.')

                            # Now, move old version
                            # Remove the relationship
                            outdated_node = graph_db.find_one('Sequence', 'SequenceID', node_seq_id)
                            outdated_rel = graph_db.match_one(start_node=origin_n,
                                                              rel_type='HAS_SEQUENCE',
                                                              end_node=outdated_node)
                            graph_db.separate(outdated_rel)
                            print('Removed old relationship to: {}'.format(node_seq_id))

                            # Do the same thing with the reverse relationship
                            outdated_rel2 = graph_db.match_one(start_node=outdated_node,
                                                              rel_type='HAS_METADATA',
                                                              end_node=origin_n)
                            graph_db.separate(outdated_rel2)
                            print('Removed old relationship from: {}'.format(node_seq_id))

                            # Create new node
                            data_dict.update(
                                {'ID': record_id,
                                 'Description': record_desc,
                                 'Sequence': str(record_seq)
                                 })

                            seq_node, seq_node_id = nodes.Data.create_node(
                                nodes.Vertex(name=record_id, kind='Sequence'), graphDB=graph_db,
                                label='Sequence', metadata=record_id, sourceNodes=origin_n, sourceIDKey='DataID',
                                sourceID=origin_n_id, rel_type='HAS_SEQUENCE', dataDict=data_dict)

                            # For now, to make sure these get the 'Data' label, just add it manually.
                            seq_node.update_labels(additional_labels + ['Data'])
                            seq_node.push()

                            # Make a reverse HAS_METADATA relationship for the same nodes
                            seq2origin = Relationship(seq_node, 'HAS_METADATA', origin_n)
                            graph_db.merge(seq2origin)
                            seq2origin.push()

                            # Connect 'outdated' node to new node
                            source2end = Relationship(seq_node, 'VERSION_{}'.format(node_data_version), outdated_node)
                            graph_db.merge(source2end)
                            source2end.push()

                            print('Updated sequence node {}'.format(seq_node_id))

                        else:
                            print('The data in the database is already the most up-to-date version. Not importing '
                                  'the new data.')

                    except ValueError:  # No matches (could/should just not use , and get ACTUAL response

                        printv('Unable to find sequence node {} in the database, creating!'.format(record_id))

                        data_dict.update(
                            {'ID': record_id,
                             'Description': record_desc,
                             'Sequence': str(record_seq)
                             })

                        if args.neo4j_driver:
                            try:
                                with driver.session() as session:
                                    seq_node_id = session.write_transaction(create_node_driver, metadata=record_id,
                                                                            kind='Sequence',
                                                                            labels=additional_labels + ['Data'],
                                                                            dataDict=data_dict, sourceNode=origin_n)

                                    # now create the relationships. identify origin_n using MetadataID, not DataID, because
                                    #  the DataIDs for sequence data are not unique (probably need to change this).

                                    session.write_transaction(create_relationship_driver, rel_type='HAS_SEQUENCE',
                                                              sourceLabels='Data:Metadata:Sequencing', sourceIDKey='MetadataID', sourceIDValue=origin_n['MetadataID'],
                                                              endLabels='Sequence', endIDKey='SequenceID', endIDValue=seq_node_id)

                                    session.write_transaction(create_relationship_driver, rel_type='HAS_METADATA',
                                                              sourceLabels='Sequence', sourceIDKey='SequenceID', sourceIDValue=seq_node_id,
                                                              endLabels='Data:Metadata:Sequencing', endIDKey='MetadataID', endIDValue=origin_n['MetadataID'])

                                printv('Created sequence node {}'.format(seq_node_id))

                            except neobolt.exceptions.ServiceUnavailable:  # should add this in other places too
                                print("ERROR: Connection refused when importing from line {}".format(index))
                                driver.close()
                                exit()

                        else:
                            seq_node, seq_node_id = nodes.Data.create_node(
                                nodes.Vertex(name=record_id, kind='Sequence'), graphDB=graph_db,
                                label='Sequence', metadata=record_id, sourceNodes=origin_n, sourceIDKey='DataID',
                                sourceID=origin_n_id, rel_type='HAS_SEQUENCE', dataDict=data_dict)

                            # For now, to make sure these get the 'Data' label, just add it manually.
                            seq_node.update_labels(additional_labels + ['Data'])
                            seq_node.push()

                            # Make a reverse HAS_METADATA relationship for the same nodes
                            seq2origin = Relationship(seq_node, 'HAS_METADATA', origin_n)
                            graph_db.merge(seq2origin)
                            seq2origin.push()

                            printv('Created sequence node {}'.format(seq_node_id))

            elif data_dict['data_type'] in href_sequence_types:  # Should just be reads...
                printv(
                    'Sequence data type is {}, and is therefore inappropriate to store as individual sequences within the '
                    'database. The node containing information to the flat file link has been created.'.format(
                        data_dict['data_type']))
            else:
                error('Unable to identify the sequence data type')

    if args.neo4j_driver:
        driver.close()

    time_end = time.time()
    print("Elapsed time was %i seconds" % (time_end - time_start))