#!/usr/bin/env python
"""
Description:
    This script identifies images corresponding to specific cores from the main standardized photo tags list
     (Photos-All_[daterange]_standardized.py), and then imports those file paths into the graph DB.
    Note that this script does NOT update the downstream nodes, only the Core nodes themselves. It's possible that
     later edits may be added to also update the downstream nodes (Soil-Depth, etc.) in addition to the Core nodes.

    UPDATES 7/13/2022:
    The process of updating core image filepaths in the graph DB has been simplified to no longer require merging with
     a separate Core cached query CSV, making redo_core_images_csv.py obsolete.
    Also, the filepaths are no longer imported as a single-item array containing a comma-delimited string (i.e.
     ["path1, path2, path3"]), as this string format makes the array unnecessary; the string is simply imported as-is
    ( i.e., "path1, path2, path3").


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import numpy as np
import pandas as pd
from datetime import datetime
import sys
import os
import argparse

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from local_file_paths import *
from tools.data.common_vars import *
from tools.imports import nodes_new as nodes
from tools.utilities.utils import error, warning, printv, BadDataError

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

parser = argparse.ArgumentParser(description="Updates core and vegetation quadrat image filepaths in the graph DB.")
inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH',
                    help='File path (can be either absolute, or relative to this repository) of the standardized photo '
                         'tags CSV, to be used for matching images with cores. The filename should start with '
                         '"Photos-All" and end with "_standardized.csv", and be the currently-most-up-to-date version.')

inputs.add_argument('--web-server-dir', dest='web_server_dir',
                    default='/var/www/isogenie/public/images/raw/',
                    help='Full path to the web server images directory (including trailing /).')

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import core images into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n"
                         "\nIn *this* script, an 'everything' argument is automatically changed to 'toValues' because the "
                         "function for updating core images (set_property_by_id() in common_vars) does not easily support "
                         "'everything' behavior. Future updates may remove this limitation.\n")


configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', default="IsoGenieDB.config",
                     help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'


if __name__ == "__main__":

    update_existing = args.update_existing

    if update_existing == 'everything':
        warning('update_existing="everything" is not yet supported for Update_Core_Images.py; proceeding with '
                '"toValues". If needing to delete image hrefs from the DB, this needs to be done manually until this '
                'script is updated.')
        update_existing = 'toValues'

    # now can set missing_hrefs_only
    if update_existing == 'none':
        print('No images will be updated due to update_existing = "none", but this script will still be run in '
              '"dry mode" to catch any errors that may appear.')
        missing_hrefs_only = None

    elif update_existing == 'missing':
        missing_hrefs_only = True

    else:
        missing_hrefs_only = False

    # Connect to DB, get info
    #graph_db, root_dir = load_config(config_file_1, loc)  # config_file (path) defined in local_file_paths
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:

        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False,
                                dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = {key: file_metadata[key] for key in file_metadata.keys() if not (key.startswith('_'))}

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        additional_labels = [label for label in additional_labels if label != '']

        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # DATAFILE ----

        photos_df = pd.read_csv(args.input_fp, sep=',', quotechar='"')

        # select only the rows with a core code
        cores_df = photos_df[~photos_df['Core Code'].isnull()]

        # create standardized columns for matching to DB
        for index, row in cores_df.iterrows():
            # dates
            if pd.isnull(row['Month']) or pd.isnull(row['Year']):
                error('Not enough information to determine the date for {}'.format(row['File Location']))

            if row['Month'] == 'August-September':
                month_str = '08'
            else:
                month_int = datetime.strptime(row['Month'], '%B').month
                if month_int <= 9:
                    month_str = '0' + str(month_int)
                else:
                    month_str = str(month_int)

            year_str = str(row['Year'])

            cores_df.loc[index, 'FieldSampling__'] = year_str + '-' + month_str

            # sites, cores
            core_code_components = row['Core Code'].split('_')
            if len(core_code_components) != 2:
                error("Invalid Core Code ({}) for photo: {}".format(row['Core Code'], row['File Location']))

            site_abbrev = core_code_components[0]
            site_name = np.nan
            for site, attrs in site_tag.items():
                if attrs['Short'] == site_abbrev:
                    site_name = site
                    break
            if pd.isnull(site_name):
                error("Could not identify the site from Core Code ({}) for photo: {}".format(row['Core Code'], row['File Location']))

            core_num = core_code_components[1]

            cores_df.loc[index, 'Site__'] = site_name
            cores_df.loc[index, 'Core__'] = core_num

            # Make a core_identifier column similar to the first part of the SampleID__ (without the depth) after the ".".
            #  For most cores this should be the same, but this is NOT the case for some 2010-08 cores that were actually
            #  sampled in September, for which the SampleID__ uses "201009" even though the FieldSampling__ is still "2010-08".
            #  Therefore this column is not used directly for matching with the graph DB, but only for verifying lists of
            #  core images as being from the same core.
            cores_df.loc[index, 'core_identifier'] = year_str + month_str + '_' + row['Core Code']

        print("Successfully determined FieldSampling__, Site__, and Core__ for all photos with core codes.")

        # Now get list of photos for each unique core
        for date, date_df in cores_df.groupby(by='FieldSampling__'):
            for site, site_df in date_df.groupby(by='Site__'):
                for core, core_df in site_df.groupby(by='Core__'):  # note the similar but different name to the main cores_df

                    # first verify that all rows now have the same core_identifier (which they all should, but just in case)
                    if len(set(list(core_df['core_identifier']))) != 1:
                        error('There should only be one unique core_identifier for this date, site, and core:\n'
                              '{}, {}, {}\n'
                              'The set of core_identifiers is:\n'
                              '{}\n'
                              'and the corresponding File Location for the first row is:\n'
                              '{}'.format(date, site, core, set(list(core_df['core_identifier'])), list(core_df['File Location'])[0]))

                    # Now get the lists of images and format as a string
                    # Do this separately for Core and Quadrat images, using those tags to select them
                    # TODO: When generating the path strings, possibly include full path? Or maybe not, since those will be
                    #  outdated soon anyway? But regardless, need to update other code to account for no longer storing
                    #  image_href__ as a 1-item array.

                    images_dict = {}

                    core_images_list = list(core_df[core_df['Core']=='X']['File Location'])
                    if len(core_images_list) > 0:
                        core_images_list = [args.web_server_dir + im for im in core_images_list]
                        core_images_str = ', '.join(core_images_list)
                        images_dict['image_href__'] = core_images_str

                    quadrat_images_list = list(core_df[core_df['Quadrat']=='X']['File Location'])
                    if len(quadrat_images_list) > 0:
                        quadrat_images_list = [args.web_server_dir + im for im in quadrat_images_list]
                        quadrat_images_str = ', '.join(quadrat_images_list)
                        images_dict['quadrat_image_href__'] = quadrat_images_str

                    # If images_dict has nothing in it, just continue (with a warning)
                    if images_dict=={}:
                        warning("None of the images for core {} are labeled as 'Core' OR 'Quadrat'; "
                                "skipping.".format(list(core_df['core_identifier'])[0]))
                        continue

                    # Now that core_df has been verified and images identified, find and update the corresponding Core node
                    # in the graph DB.
                    core_info = {'Area__': 'Stordalen',  # TODO: may later make this usable for non-Stordalen cores?
                                 'FieldSampling__': date,
                                 'Site__': site,
                                 'Core__': core}
                    try:
                        core_node = nodes.UniqueNode(session=session, kind='Core', id_key='CoreID', result_required=True,
                                                     data_dict=core_info)
                    except BadDataError:
                        warning("Could not identify any cores with the following info. Skipping:\n{}\n".format(core_info))
                        continue

                    # # core_node is of the UniqueNode class, which is intentionally "read-only" during initialization; but
                    # #  it should still be possible to use update_values from the Vertex class (from which all other node
                    # #  classes inherit).
                    # core_node.node = core_node.update_values(session=session, node=core_node.node,
                    #                                          new_data=images_dict,
                    #                                          update_existing='toValues')

                    for image_key, image_value in images_dict.items():
                        # For each image type, update all nodes matching the CoreID of the core_node
                        session.write_transaction(set_property_by_id, IDkey='CoreID', IDvalue=core_node.id,
                                                  key=image_key, value=image_value, missing_only=missing_hrefs_only)

