#!/usr/bin/env python
"""
Purpose: Rewrite of import_sequence_data.py to use node classes.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import argparse
import time
# from distutils.version import StrictVersion
import pandas as pd
# from py2neo.database import DatabaseError, NodeSelector
# from py2neo.packages.httpstream.http import SocketError
# from py2neo import Relationship
import uuid
from datetime import datetime
import neobolt

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.utilities.utils import error, warning, printv, BadDataError
from tools.imports import nodes_new as nodes
from tools.data.common_vars import *
from local_file_paths import config_file_1

parser = argparse.ArgumentParser(
    description="Import sequence data information and [potentially] the sequences into the  neo4j graph database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH',
                    help="File containing information about the sequence data.  MUST include sufficient information to "
                         "identify a UNIQUE node. Will not create any new nodes except those of the Sequencing type. "
                         "Can optionally include 'target_path', which will overwrite location of the sequence data "
                         "source file. Useful if original import location is different than target location.")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

configs = parser.add_argument_group('Configurations')

# configs.add_argument('--neo4j-driver', dest='neo4j_driver', action='store_true',
#                      help="Use the neo4j driver instead of py2neo for importing the individual sequences. This is ~3x "
#                           "faster and should work fine, but is new and doesn't use as much of the class functionality.")

configs.add_argument('--ignore-sequences', dest='ignore_sequences', action='store_true',
                     help="Ignore missing sequence files, (i.e. those specified in the sequence info file). This is "
                          "STRONGLY DISCOURAGED as it assumes the target location will have that file path available for"
                          " linking.")

configs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'],
                     default='none',
                     help="Whether to update existing nodes with new data, and which values to update.\n"
                          "In this sequence import script, this option is only applied to the sequence 'origin' "
                          "(Data/Metadata) nodes. The Sequence nodes have their own version-updating system, which "
                          "writes new versions to new nodes instead of updating the old, so that old sequences can "
                          "still be queried in the database.\n"
                          "Options, in order of comprehensiveness:\n\n"
                          "none          Does nothing.\n"
                          "missing       Replaces ONLY missing data with new_data.\n"
                          "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                          "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

configs.add_argument('--dry-run', dest='dry', action='store_true',
                     help="Run the importer in dry mode, which will not create any new nodes but show their creation. "
                          "Useful for double-checking that 'source' nodes, that correspond to the sequencing data, "
                          "exist. This option may generate a lot of errors, so is not recommended.")

# configs.add_argument('--skip-individual-sequences', dest='skip_individual_sequences', action='store_true',
#                      help="Run the importer in 'semi-dry' mode, which will not create any individual sequence nodes,"
#                           "but will still create sequence collection and sequence origin nodes. Useful if wanting to "
#                           "import these 'big picture' nodes quickly without having to wait for the creation of 1000s "
#                           "of individual sequence nodes.")
configs.add_argument('--import-individual-sequences', dest='import_individual_sequences', action='store_true',
                     help="Import individual sequences /contigs from MAG and other supported data types. "
                          "(This option was previously turned on by default and disabled with "
                          "--skip-individual-sequences; however, since these nodes aren't being used, and they take "
                          "awhile to import and are complex to update, these imports are now disabled by default.)")

configs.add_argument('--clear-sequences', dest='clear_sequences', action='store_true',
                     help="Remove existing sequence collections (and their child nodes) from the database. This is "
                          "useful for testing or debugging, or as a failsafe if import goes terribly wrong.")

configs.add_argument('--verbose', dest='verbose', action='store_true',
                     help="Display ALL messages. If not used, the many messages about adding or "
                          "reusing nodes are suppressed (doesn't affect warnings and errors).")

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'

# Suppress DeprecationWarning messages if not args.verbose
# if not args.verbose:
#     import warnings
#     warnings.filterwarnings("ignore", category=DeprecationWarning)
#     warnings.filterwarnings("ignore", category=FutureWarning)

if args.import_individual_sequences:
    # Individual sequence imports will likely be phased out, so these modules may not be installed on some machines.
    # Therefore only import SeqIO and gzip if needed.
    from Bio import SeqIO
    import gzip

update_existing = args.update_existing

# These are those dependent on data type, but for the moment, only have 1 data type to associate with sequence data
# Eventually new sequence data may come from places other than soil-depths (10m in the air column, for ex.)
essential_headers = {
    'Soil-Depth': [
        'Area__',
        'Site__',
        'CoreDated__',
        'Depth__'
    ]
}

# These are unifying identifiers for each of the data source types. Used to 'mark' a node for subsequent node-"ship"
unique_keys = {
    'Soil-Depth': 'DepthID'
}


def validate_input(df: pd.DataFrame):
    mandatory_headers = {
        'node_label',
        'data_type',
        'Version__',
        'upload_path',  # This seems redundant with imported_file_href__, which is a mostly empty column (accept for one
                        #  link which appears wrong). Should probably use that instead, for consistency with other data.
        'imported_file_href__',
        'collection_name',
        'Quality__'
    }

    # Check for essential headers
    if not all(header in df.columns.tolist() for header in mandatory_headers):
        error('Unable to identify essential headers in the input file.')

    # Check if files exist at upload paths
    if args.ignore_sequences:
        warning('Skipping the file path validation due to --ignore-sequences argument.')
    else:
        all_fps = df['upload_path'].tolist()  # list of all entries under the upload_path column, some of which are themselves lists

        for fps in all_fps:

            if pd.isnull(fps):
                warning('The file import list is missing a file path.')
                continue

            # split any multi-entry file paths by semicolons
            fps_list = fps.split(';')

            for fp in fps_list:
                fp_cleaned = fp.replace(' ', '')  # delete residual spaces which are sometimes (but not always) after the semicolons in fps

                # Add root dir for test and local DBs
                if args.database in ['local', 'test']:
                    fp_cleaned = os.path.join(root_dir, fp_cleaned)

                if not os.path.exists(fp_cleaned):
                    if fp_cleaned.endswith('fastq.gz'):  # First see if another zipped/unzipped version exists
                        fp_alt = fp_cleaned[:-3]  # remove the last three characters to check for an unzipped file
                        if os.path.exists(fp_alt):
                            warning('The import list shows a nonexistent .gz file, but an unzipped version exists: \n{}\n'
                                    'This filename should be changed in the import list to reflect the unzipped version '
                                    'that actually exists.'.format(fp_cleaned))
                        else:
                            error('Unable to find {} specified in the input file.'.format(fp_cleaned))
                    else:
                        error('Unable to find {} specified in the input file.'.format(fp_cleaned))
        printv('Validated file paths')

    # Currently available sequence types, can include more when we know the breadth of sequence types in the project
    avail_sequence_types = {
        'Reads-Raw',
        'Reads-QC',
        'Contigs-General',
        'Contigs-Viral',
        'Contigs-Microbe',
        'MAG'
    }

    # Check to see if the data type and target node types are even valid
    in_data_types = df['data_type'].tolist()
    if not set(in_data_types).issubset(avail_sequence_types):
        error('Not all of the data types used in the input file were valid. Valid data types include:\n{}'.format(
            '\n'.join(avail_sequence_types))
        )

    # Check to ensure that the headers (associated with the target data type) are present
    target_types = df['node_label'].tolist()

    headers_to_search = set()

    for dtype in target_types:
        headers = set(essential_headers[dtype])
        headers_to_search.update(headers)

    # Now search for those headers
    if not all(header in df.columns.tolist() for header in headers_to_search):
        error('Unable to identify mandatory headers that are associated with the target node type in the input file.')

    return True

if __name__ == "__main__":

    """
    Sequence data is unlike other data types, as those are usually single-value attributes. Even temporal data has a 
    value (i.e. temp) associated with a specific time. Sequence data has many "values" - all deriving from the same 
    depth. Additionally, sequence data can - and often is - larger than available storage capacities. The logic for 
    this import ASSUMES that the attachment point (neo4j node) exists, and will adjust its import strategy based on the
     size and number of sequence elements in the import file.

    One of the interesting aspects of sequencing data is that all [at least every element of data encountered thus far] 
    MUST derive from a depth of some sort, either Soil- or Lake-Depth. However, due to the database structure, all the 
    upstream nodes must be identified before sequencing data can be uploaded. Therefore, much of this code could clearly
     duplicate the import_cores script.
    """

    # Validate database
    # TODO: Remove need for graph_db entirely.
    # graph_db, root_dir = load_config(args.config_fp, args.database)  # config_file (path) defined in local_file_paths
    driver, root_dir = load_config_driver(config_file_1, args.database)

    # Input file
    seq_fp = args.input_fp

    # Check for file structure
    print('Validating sequencing information from {}'.format(seq_fp))
    seq_df = pd.read_csv(seq_fp, delimiter=',', header=0, index_col=0, dtype=object)

    # TODO: This file (seq_fp, usually "IsoGenieDB-Sequencing_Imports.csv") should be given its own Metadata node!!!
    #  Connect this Metadata node to the nodes containing all the non-standardized properties (BioSample, SRR, etc.) in
    #  that sheet, unless they're Sequence nodes. These would be the Data/Metadata nodes connected to the Collection
    #  nodes.

    res = validate_input(seq_df)

    if res:  # All fails are caught during the checks, so this is kinda response
        print('Input file successfully validated!')

    with driver.session() as session:

        if args.clear_sequences:

            # SUZANNE UPDATE 12/12/19: Added the label 'Sequencing' to all sequence-related data, so this is now much simpler

            sequencing_delete = 'MATCH (n:Sequencing) DETACH DELETE (n)'
            session.write_transaction(nodes.Vertex().run_fixed_query, query=sequencing_delete)  # need to test
            #response = graph_db.run(sequencing_delete).data()

            print('Removed all Sequencing nodes and relationships.')

        time_start = time.time()

        # Re-structured import process to include more verification and feedback at each level
        print('Importing sequencing information from {}'.format(seq_fp))

        # METADATA (Metadata node for sequencing list file) ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False,
                                dtype=object)
        file_metadata = file_list.loc[
            file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = {key: file_metadata[key] for key in file_metadata.keys() if not (key.startswith('_'))}

        try:
            master_metadata_n = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                               update_existing_data=update_existing)
        except TypeError:
            print(file_metadata)
            print(file_list_filename)
            error('TypeError when attempting to set DatasetID on Metadata node: {}'.format(file_metadata['DatasetID']))

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        additional_labels += ['Sequencing']
        additional_labels = list(set(additional_labels))  # remove any inadvertently-created duplicates

        master_metadata_n.node = session.write_transaction(master_metadata_n.update_labels, id_node=master_metadata_n.node._id,
                                                           labels=additional_labels)

        # DATAFILE ----

        for index, row in seq_df.iterrows():

            # Find target node (i.e. node that the data will be associated with)
            # This is a huge redesign of how querying nodes has been done. It uses a cypher query directly instead of
            # iterately searching down the graph tree. It finds a SINGLE node (if it exists, otherwise error), returns the
            # node, then checks for relationships to it. If no sequence data relationship, make one, else update

            node_type = row['node_label']  # source node type-- currently all Soil-Depth
            unique_key = unique_keys[node_type]  # currently all DepthID
            node_params = essential_headers[node_type]
            params_dict = {k: row[k] for k in node_params}  # parameters for identifying unique source node
            collection_name = row['collection_name']
            # re-define additional_labels for *this* row
            additional_labels = row['Availability__'].split(', ') + ['Sequencing']  # Everything gets these labels

            # sampleID: can be used for node matching if there's no match for params_dict
            sampleID = False
            for potentialID in [row['Title'], collection_name]:  # sampleID can be stored in either of these places
                if not(pd.isnull(potentialID)):
                    if potentialID.startswith('20') and len(potentialID) == 12:  # this seems like a good enough check for the format '20100900_E1S'
                        sampleID = potentialID
                        break

            try:
                # find the "source node" to which to attach the sequence collection
                # first try sampleID, because it should be faster to only have to match one property
                source_node = nodes.UniqueNode(session=session, kind=node_type, id_key=unique_key,
                                               data_dict={'Sequencing_SampleID-1__': str(sampleID)})
            except BadDataError:
                try:
                    # if sampleID fails, then try params_dict (although if it gets to this point, params_dict
                    #  most likely won't work either)
                    source_node = nodes.UniqueNode(session=session, kind=node_type, id_key=unique_key,
                                                   data_dict=params_dict)
                except BadDataError:
                    warning('Unable to find target node for line {} from input file. '
                            'This line was not imported.'.format(index))
                    continue  # Should this error out?

            printv('Identified source node with {}: {}'.format(unique_key, source_node.id))

            habitat = source_node.node._properties['Habitat__']

            # Add Soil to additional_labels (if it's a Soil node)
            if 'Soil' in source_node.node._labels:
                additional_labels += ['Soil']

            # Collection nodes
            collection_n = nodes.Collection(session=session, name='SequenceCollection', source_node=source_node,
                                            source_id_key=unique_key, rel_type='HAS_SEQUENCE_DATA', data_dict={})
            collection_n.node = session.write_transaction(collection_n.update_labels, id_node=collection_n.node._id,
                                                          labels=[habitat] + additional_labels)


            # The code below (from old import script) doesn't seem to apply to anything currently, so leave out for now.
            # # Since MAGs are a collection of their own (each MAG can consist of 1 - 100s of contigs), must create and
            # # connect to
            # mag_collection_nID = None
            # if node_type == 'MAG':  # QUESTION: Is this a typo? Until now, node_type was for the source node
            #     mag_collection_n = check_relationships(graph_db, collection_n, 'HAS_MAG_DATA', None)
            #     unique_key = 'CollectionID'
            #
            #     if mag_collection_n == 'WARN':
            #         mag_collection_nID = mag_collection_n['CollectionID']
            #         printv('Identified pre-existing Collection node ID: {}'.format(mag_collection_nID))
            #     elif not mag_collection_n:
            #         mag_collection_n, mag_collection_nID = create_sequence_collection_node(
            #             graph_db, source_node=collection_n, source_node_key=unique_key,
            #             source_node_key_value=collection_nID,
            #             subdata=collection_name, relation_type='HAS_MAG_DATA', data_dict={},
            #             additional_labels=additional_labels, no_create=args.dry)
            #         mag_collection_n.update_labels(['MAGs'])
            #         mag_collection_n.push()
            #
            #         collection_n = mag_collection_n  # Need to ensure that MAGs are being connected to MAG Collection
            #         collection_nID = mag_collection_nID

            # Now that the sequence collection data node has been created OR found, search through for specific sequence
            #  data types.
            # if row['data_type'] == 'MAG':
            #     # For all other data types
            #     adj_dtype = 'HAS_MAG_{}'.format(
            #         collection_name)  # QUESTION: Do we really want the full MAG name in the relationship name? NO
            # else:
            #     adj_dtype = 'HAS_{}'.format(
            #         row['data_type'].replace('-', '_').upper())  # CONTIGS_VIRAL, READS_QC, READS_RAW

            # Removing specific MAG name from relationship name, so now the assignment of adj_dtype is the same for all.
            adj_dtype = 'HAS_{}'.format(row['data_type'].replace('-', '_').upper())  # CONTIGS_VIRAL, READS_QC, READS_RAW, MAG

            data_dict = row.to_dict()

            # Delete the 'node_label' key, because it's only used for import purposes, and also could create confusion
            #  as it doesn't refer to the node in which it's stored.
            try:
                del data_dict['node_label']
            except KeyError:
                pass

            # Code below edited from create_sequence_origin_node -----------
            # TODO: Should a new DataMetadata class be created to force the creation of the metadata_properties below?

            # Hopefully the following will be unique.
            # For raw reads it will be "READS_RAW_[sampleID]", for MAGs it will be "MAG_[mag name]".
            # TODO: JUST REALIZED THE READS_RAW DatasetIDs WILL *NOT* BE UNIQUE ANYMORE, SINCE SAMPLES CAN HAVE MULTIPLE RAW READS VERSIONS.
            #  IN ADDITION, THIS NAMING STRUCTURE DOES *NOT* SPECIFY THE BASIC SEQUENCING TYPE (e.g. metaG or metaT).
            #  THEREFORE WILL NEED TO RENAME ALL OF THESE TO BE UNIQUE; MAYBE BY APPENDING ACCESSION?
            #  WILL ALSO NEED TO ADD THE SEQUENCING TYPE INTO A NEW PROPERTY.
            #  (Thankfully there aren't any nodes pointing to Reads-Raw nodes with source_DatasetIDs, so only these nodes themselves will need to be updated; and the MAG DatasetIDs should *hopefully* stay unique).
            origin_datasetID = row['data_type'].replace('-', '_').upper() + '_' + collection_name

            metadata_properties = {
                'DatasetID': origin_datasetID,
                'MetadataID': str(uuid.uuid4()),
                'Metadata__': origin_datasetID
                # Other Metadata properties (Version__, imported_file_href__, etc.) should already be in the CSV.
            }

            data_dict.update(metadata_properties)

            # With the code below, subdata=collection_name is stored in Data__ and DataSubsample__ (the latter not
            #   included in import_sequence_data_old); and origin_n.id is the DataID
            # Decided not to put old version nodes at this level, because the link to the master metadata node would
            #  create complications with versioning.
            origin_n = nodes.Data(session=session, subdata=collection_name, source_node=collection_n,
                                  source_id_key="CollectionID", rel_type=adj_dtype, metadata_node=master_metadata_n,
                                  data_dict=data_dict, update_existing=update_existing)

            # For now, to make sure these also get the 'Metadata' label, just add it manually.
            origin_n.node = session.write_transaction(origin_n.update_labels, id_node=origin_n.node._id,
                                                      labels=[habitat, 'Metadata'] + additional_labels)

            # Now, let's crunch a lot of sequences
            if args.import_individual_sequences:

                individual_sequence_types = ['Contigs-General', 'Contigs-Viral', 'Contigs-Microbe', 'MAG']
                href_sequence_types = ['Reads-Raw', 'Reads-QC']

                if data_dict['data_type'] in individual_sequence_types:  # Here we go!

                    # Add root dir for test and local DBs
                    if args.database in ['local', 'test']:
                        input_fp = os.path.join(root_dir, row['upload_path'])
                    else:
                        input_fp = row['upload_path']

                    # get input data version for version handling
                    input_data_version = row['Version__']

                    # Open the file
                    input_fh = None
                    if any(input_fp.endswith(end) for end in ['.fasta', '.fna']):
                        input_fh = open(input_fp, 'rU')
                    elif input_fp.endswith('.gz') and not input_fp.endswith('.tar.gz'):
                        input_fh = gzip.open(input_fp, 'rt')
                    else:
                        error('Unable to identify sequence filetype')

                    # Loop through records in the sequence file
                    for record in SeqIO.parse(input_fh, 'fasta'):
                        record_id = record.id
                        record_desc = record.description
                        record_seq = record.seq

                        data_dict.update(
                            {'ID': record_id,
                             'Description': record_desc,
                             'Sequence': str(record_seq),
                             'Version__': input_data_version
                             })

                        try:
                            # Create the sequence node

                            sequence_node = nodes.Sequence(session=session, name=record_id, data_dict=data_dict,
                                                           origin_node=origin_n, origin_id_key='DataID')

                            # For now, to make sure these get the 'Data' label, just add it manually.
                            sequence_node.node = session.write_transaction(sequence_node.update_labels,
                                                                           id_node=sequence_node.node._id,
                                                                           labels=additional_labels + ['Data'])

                        except neobolt.exceptions.ServiceUnavailable:
                            print("ERROR: Connection refused when importing from line {}".format(index))
                            driver.close()
                            exit()


                        # # OLD CODE BELOW ------------
                        #
                        # # Identify a node that matches those characteristics
                        # # sequence_query = "MATCH (n:`Data` {{`DataID`:'{}'}})-[r:`HAS_SEQUENCE`]->(t:`Sequence` {{`ID`: '{}'}}) RETURN n, t, r".format(
                        # #     origin_n.id, record_id
                        # # )
                        #
                        # # Three possibilities:
                        # # (1) there is no existing Sequence node at all
                        # # (2) there is an existing Sequence node with the same version number
                        # # (3) there is an existing Sequence node, but its version is different
                        # # These possibilities are denoted with their number in comments below.
                        #
                        # try:
                        #
                        #     # sequence_node = session.read_transaction(read_nodes_driver, query=sequence_query,
                        #     #                                     node_variable='t')
                        #     # todo: add optional rel_type to UniqueNode so that it finds the most recent one
                        #     sequence_node = nodes.UniqueNode(session=session, kind='Sequence', id_key='SequenceID',
                        #                                 data_dict={'DataID': origin_n.id, 'ID': record_id},
                        #                                 rel_type='HAS_SEQUENCE', result_required=False)
                        #     # result_required=False means that if it no node is found, sequence_node.node and
                        #     #   sequence_node.id will both be False, instead of the code just erroring out
                        #
                        #     if sequence_node.node:  # (2) or (3)
                        #
                        #         node_data_version = sequence_node.node._properties['Version__']
                        #         node_seq_id = sequence_node.id
                        #
                        #     else:  # (1)
                        #         raise ValueError
                        #         # if didn't find anything, should proceed to "except ValueError" to create the node
                        #
                        #     # else:
                        #     #     error(
                        #     #         '{} existing sequence nodes identified with ID {}; there should be either 0 or 1.'.format(
                        #     #             len(sequence_node), record_id))
                        #
                        #     # TODO: Figure out how to make the code below work well with the "normal" mode of updating data
                        #     #  using update_existing (or if this is necessary).
                        #     if StrictVersion(node_data_version) < StrictVersion(input_data_version):  # (3)
                        #         # Remove existing edge from old sequence node, add new sequence node, attach old node to
                        #         # new node and add "HAS_OLD_VERSION" or something like that
                        #
                        #         # TODO: Make a neo4j driver version of the code below (though won't encounter this until we
                        #         #  get a new version of existing sequence data).
                        #         # TODO: Can use create_relationship from import_sequence_samplecodes for anything not created
                        #         #  via the node classes.
                        #
                        #         print(
                        #             'Identified an out-of-date version of this sequence in the database ({} vs {}). If '
                        #             'this was expected, then all is fine. If not, double-check the versioning.')
                        #
                        #         # Now, move old version
                        #         # Remove the relationship
                        #         outdated_node = graph_db.find_one('Sequence', 'SequenceID', node_seq_id)
                        #         outdated_rel = graph_db.match_one(start_node=origin_n,
                        #                                           rel_type='HAS_SEQUENCE',
                        #                                           end_node=outdated_node)
                        #         graph_db.separate(outdated_rel)
                        #         print('Removed old relationship to: {}'.format(node_seq_id))
                        #
                        #         # Do the same thing with the reverse relationship
                        #         outdated_rel2 = graph_db.match_one(start_node=outdated_node,
                        #                                            rel_type='HAS_METADATA',
                        #                                            end_node=origin_n)
                        #         graph_db.separate(outdated_rel2)
                        #         print('Removed old relationship from: {}'.format(node_seq_id))
                        #
                        #         # Create new node.
                        #         # NOTE: This Sequence node's version number will differ from that of the origin node.
                        #         data_dict.update(
                        #             {'ID': record_id,
                        #              'Description': record_desc,
                        #              'Sequence': str(record_seq)
                        #              })
                        #
                        #         seq_node, seq_node_id = nodes.Data.create_node(
                        #             nodes.Vertex(name=record_id, kind='Sequence'), graphDB=graph_db,
                        #             label='Sequence', metadata=record_id, sourceNodes=origin_n, sourceIDKey='DataID',
                        #             sourceID=origin_n_id, rel_type='HAS_SEQUENCE', dataDict=data_dict)
                        #
                        #         # For now, to make sure these get the 'Data' label, just add it manually.
                        #         seq_node.update_labels(additional_labels + ['Data'])
                        #         seq_node.push()
                        #
                        #         # Make a reverse HAS_METADATA relationship for the same nodes
                        #         seq2origin = Relationship(seq_node, 'HAS_METADATA', origin_n)
                        #         graph_db.merge(seq2origin)
                        #         seq2origin.push()
                        #
                        #         # Connect 'outdated' node to new node
                        #         source2end = Relationship(seq_node, 'VERSION_{}'.format(node_data_version),
                        #                                   outdated_node)
                        #         graph_db.merge(source2end)
                        #         source2end.push()
                        #
                        #         print('Updated sequence node {}'.format(seq_node_id))
                        #
                        #     elif StrictVersion(node_data_version) > StrictVersion(input_data_version):  # bad
                        #         warning('Attempting to import a version of the sequence ({}) that is older than the '
                        #                 'most recent version in the database ({}). '
                        #                 'This sequence was not imported.'.format(input_data_version, node_data_version))
                        #     else:  # (2)
                        #         printv('The sequence in the database ({}, {}) is already the most up-to-date version. '
                        #                'Not importing the new sequence.'.format(origin_datasetID, record_id))
                        #
                        # except ValueError:  # (1) - No matches (could/should just not use , and get ACTUAL response
                        #
                        #     printv('Unable to find sequence node {} in the database, creating!'.format(record_id))
                        #
                        #     data_dict.update(
                        #         {'ID': record_id,
                        #          'Description': record_desc,
                        #          'Sequence': str(record_seq),
                        #          'Version__': input_data_version
                        #          })
                        #
                        #     try:
                        #         # Create the sequence node
                        #
                        #         # sequence_node = nodes.Data(session=session, kind='Sequence', subdata=record_id,
                        #         #                            source_node=origin_n, source_id_key='DataID',
                        #         #                            rel_type='HAS_SEQUENCE', data_dict=data_dict,
                        #         #                            update_existing='none', metadata_node=origin_n)
                        #
                        #         # It looks like the above will not work unless the Data class is re-written to
                        #         #  work with kind!='Data'. As currently written, the Data class checks for existing
                        #         #  nodes and decides whether they match based on DataID, whereas here we want it to
                        #         #  use SequenceID. Even after these re-writes, this checking may still be too
                        #         #  time-consuming. Therefore it might be best to just write a dedicated Sequence
                        #         #  class.
                        #
                        #         #  Rewrite:
                        #         sequence_node = nodes.Sequence(session=session, name=record_id, data_dict=data_dict,
                        #                                        origin_node=origin_n, origin_id_key='DataID')
                        #
                        #         # For now, to make sure these get the 'Data' label, just add it manually.
                        #         sequence_node.node = session.write_transaction(sequence_node.update_labels,
                        #                                                        id_node=sequence_node.node._id,
                        #                                                        labels=additional_labels + ['Data'])
                        #
                        #         printv('Created sequence node {}'.format(seq_node_id))
                        #
                        #     except neobolt.exceptions.ServiceUnavailable:  # should add this in other places too
                        #         print("ERROR: Connection refused when importing from line {}".format(index))
                        #         driver.close()
                        #         exit()

                elif data_dict['data_type'] in href_sequence_types:  # Should just be reads...
                    printv(
                        'Sequence data type is {}, and is therefore inappropriate to store as individual sequences within the '
                        'database. The node containing information to the flat file link has been created.'.format(
                            data_dict['data_type']))
                else:
                    error('Unable to identify the sequence data type')

        driver.close()

        time_end = time.time()
        print("Elapsed time was %i seconds" % (time_end - time_start))