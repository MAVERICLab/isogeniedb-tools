#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
import numpy as np
from dateutil.parser import parse
# from creates import *
# from graph import check_name
import argparse
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.imports import nodes_new as nodes
from tools.utilities.utils import error, warning, printv
from tools.data.common_vars import *

parser = argparse.ArgumentParser(description="Import autochamber information into a neo4j database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with autochamber information")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n"
                         "\nIn *this* script, 'none' argument is automatically changed to 'missing' to take care of times that break across CSVs.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'

update_existing = args.update_existing
# Similar to lake temperatures, autochamber data can have the same time in multiple CSVs.
# Therefore, update_existing needs to have a value of at least 'missing' if not more comprehensive.
if update_existing == 'none':
    update_existing = 'missing'

# which chamber number is at which site
chamber_map = {
    '1': 'Palsa Autochamber Site',
    '2': 'Sphagnum Autochamber Site',
    '3': 'Palsa Autochamber Site',
    '4': 'Sphagnum Autochamber Site',
    '5': 'Palsa Autochamber Site',
    '6': 'Sphagnum Autochamber Site',
    '7': 'Eriophorum Autochamber Site',
    '8': 'Eriophorum Autochamber Site',
    '9': 'Transitional Sphagnum Autochamber Site',
    '10': 'NO Chamber'
}


chamber_notes_default = 'Chamber was changed with QCL installation in 2011.'

# Still need to get the GPSs.
chamber_info = {
    '1': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '2': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '3': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '4': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '5': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '6': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '7': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '8': {
         'Notes': chamber_notes_default,
         'GPS__': 'NaN'
         },
    '9': {
         'Notes': 'Prior to QCL installation in 2011, chamber was over an E. vaginatum tussock in the bog. '
                 'In 2011, chamber was moved to a Sphagnum mound with some E. angustifolium which has slowly '
                 'become dominated by E. angustifolium and, thus, more fen like.',
         'GPS__': 'NaN'
         },
    '10': {
         'Notes': 'NO Chamber',
         'GPS__': 'NaN'
         }
}


def convert_date(date_series):
    # This function converts year + DOY into YYYY-MM-DD (accounts for leap years).

    # changed "day" to int("doy") to account for missing "day" column in KB sheets. "doy" (a float) exists in both.
    return datetime.strptime('{} {}'.format(date_series['year'], int(float(date_series['doy']))),
                             '%Y %j').date().isoformat()


if __name__ == "__main__":

    # Input file
    autochamber_fp = args.input_fp

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:
    
        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False, dtype=object)
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = { key: file_metadata[key] for key in file_metadata.keys() if not(key.startswith('_')) }

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data=update_existing)

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # DATAFILE ----

        # Re-structured import process to include more verification and feedback at each level
        print('Importing autochamber information from {}'.format(autochamber_fp))

        # Pre-process file
        autochamber_df = pd.read_csv(autochamber_fp, sep=';', dtype=object)  # Keep as string, or else pd tries to be smart

        autochamber_df['Site__'] = autochamber_df['chmbr'].map(chamber_map)
        autochamber_df['Date__'] = autochamber_df.apply(convert_date, axis=1)

        area_node = nodes.Area(session=session, name='Stordalen')
        area_node.node = session.write_transaction(area_node.update_labels, id_node=area_node.node._id,
                                                       labels=additional_labels)

        for site, site_df in autochamber_df.groupby(by='Site__'):

            printv('Processing autochamber site: {}'.format(site))

            # Get habitat from site name
            # NOTE: In the future (e.g. if a chamber location undergoes habitat transition without the chamber being
            #  moved), may need to update this to check for type(habitat)==dict, as is currently done for coring data
            #  imports. But for now, all autochamber sites should be defined to be constant-habitat.
            habitat = site_tag[site]['Habitat Type']
            if not(type(habitat) == str):
                print(habitat)
                error(f"The habitat for '{site}' (see above) is not a string. If it was changed to a dict due to "
                      f"habitat transition, then this script should be updated to get the current habitat.")

            site_node = nodes.Site(session=session, name=site, area_node=area_node, data_dict={'Habitat__': habitat})

            site_node.node = session.write_transaction(site_node.update_labels, id_node=site_node.node._id,
                                                       labels=[habitat] + additional_labels)

            # Each *set* of autochambers within a "site" - obviously has chambers
            for chamber, chamber_df in site_df.groupby(by='chmbr'):

                printv('Processing autochamber chamber: {} of site: {}'.format(chamber, site))

                chamber_node = nodes.Device(session=session, name=chamber, site_node=site_node, kind='Chamber', data_dict=chamber_info[chamber])
                chamber_node.node = session.write_transaction(chamber_node.update_labels, id_node=chamber_node.node._id,
                                                              labels=[habitat] + additional_labels)

                for date, date_df in chamber_df.groupby(by='Date__'):

                    date_node = nodes.Date(session=session, name=date, source_node=chamber_node, source_id_key='ChamberID', update_existing=update_existing)

                    date_node.node = session.write_transaction(date_node.update_labels, id_node=date_node.node._id,
                                                               labels=[habitat] + additional_labels)

                    for index, time_s in date_df.iterrows():

                        time_info = time_s.squeeze().fillna('NaN')
                        time_hour = time_s['hour']

                        # Time is a fraction of a day, not digital
                        if (not time_hour) or pd.isnull(time_hour):
                            time_hour = float(time_s['doy']) - int(time_s['doy'])

                        time_node = nodes.FractionalTime(session=session, name=time_hour, update_existing=update_existing,
                                                         metadata_node=metadata_node, data_dict=time_info.to_dict(), date_node=date_node)

                        time_node.node = session.write_transaction(time_node.update_labels, id_node=time_node.node._id,
                                                                   labels=[habitat, 'Data'] + additional_labels)
