#!/usr/bin/env python
"""
Purpose: Import sample metadata for sequenced samples into the graph database. This script assumes that much of the
 other soil data (and specifically the coring sheets) have already been imported, and updates all the 'Soil-Depth'
 nodes, plus all downstream nodes with the DepthID attribute, to include the sequencing metadata (if they exist).

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import sys
import os
import pandas as pd
import argparse
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from tools.data.common_vars import *
from tools.utilities.utils import error, warning, printv, BadDataError
from tools.imports import nodes_new as nodes  # probably won't use
from io_misc.Modify_nodes import run_add_missing_sample_metadata_from_soildepths  # probably won't use

import warnings

# warnings.filterwarnings("ignore", category=DeprecationWarning)
# warnings.filterwarnings("ignore", category=FutureWarning)

parser = argparse.ArgumentParser(description="Import sequencing sampleIDs, info on which metaGs exist and where/how "
                                             "they were sequenced, and other sample metadata.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_fp', metavar='FILEPATH', help="Input file with sample metadata")

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import sample metadata into.")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

# TODO: The --update argument here is ignored. It's still here because build_db.py automatically includes it, and
#  produces an error if it's not set. As written, this script acts like update_existing == 'toValues', because
#  that's the only way to easily update an arbitrary set of nodes all at once. Need to make it more flexible.
#  But since this script only updates a short list of properties, it's not as big a deal as for other imports.
inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'],
                    default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

args = parser.parse_args()

# Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
if args.verbose:
    os.environ['VERBOSE'] = 'True'
else:
    os.environ['VERBOSE'] = 'False'

#update_existing = args.update_existing


# Modified on 7/3/20 from run_add_missing_sample_metadata_from_soildepths() in io_misc.Modify_nodes.
# Modification removes the condition "WHERE NOT(EXISTS(n.`%s`))" from the MATCH statement, and changes print to printv.
def set_property(tx, depthID, key, value):
    # Sets a key:value property for all nodes with a given DepthID
    updatedate = datetime.now().strftime('%Y%m%d')

    result = tx.run(
        "MATCH (n {DepthID: $depthID}) SET n.`%s` = $value, n.UpdateDate__ = $updatedate RETURN n" % key,
        depthID=depthID, value=value, updatedate=updatedate)

    count = 0
    for record in result:
        count += 1

    if count > 0:
        printv('Updated {} = {} on {} nodes with DepthID = {}'.format(key, value, count, depthID))

# Modified on 9/11/20 from nodes_new.Vertex.create_relationship()
def create_relationship(tx, id_source, rel_type, id_end):
    tx.run("MATCH (a),(b) WHERE id(a)=%s AND id(b)=%s MERGE (a)-[:%s]->(b)" % (str(id_source), str(id_end), rel_type))


if __name__ == "__main__":

    # Input file
    samplemetadata_fp = args.input_fp

    # Validate database
    driver, root_dir = load_config_driver(args.config_fp, args.database)

    with driver.session() as session:

        # TODO: Record the existence of the sequencing_names file in the Metadata nodes. This file might eventually be
        #  revised and versioned, and should therefore have its own Metadata node.
        #  The only issue is that then it's not clear which nodes will point to that Metadata node. Technically it
        #  should be all of the ones that are updated, but since this script only adds a few new sample metadata
        #  properties, it seems like overkill to do it for *all* of them (e.g. the Biogeochemistry nodes).
        #  Maybe the best compromise would be to point from just the Soil-Depth and Sequencing (non-Sequence) nodes.
        #  A similar issue exists for the IsoGenieDB_Sequencing_Imports.csv file, which actually does not have its
        #  own Metadata node (even though it absolutely should). This issue has been flagged with a separate TO-DO in
        #  bin/isogenie/import_sequence_data.py.
        #  As both files involve similar challenges for import (e.g. lack of a dedicated line for file metadata in the
        #  file_list CSVs), this issue will be resolved at a later time.
        # TODO: Solution to above: Connect the Metadata node to the Depth-Info nodes. This would be semi-consistent with
        #  how the coring sheet data is connected to the source files--the entire node tree is populated based on the
        #  coring sheets, and this node tree includes standardized metadata from the coring sheets that's later
        #  propagated downstream to other nodes (e.g. Biogeochemisitry), but the Depth-Info nodes are the only ones that
        #  actually connect directly to the coring sheet Metadata node. Given that the sequencing sample IDs were
        #  built based on the data in Depth-Info nodes, it makes sense to attribute this metadata in the same way.
        #  Then, in import_sequence_data, connect the Metadata node for IsoGenieDB-Sequencing_Imports to whichever nodes
        #  contain the various non-standardized properties in that sheet.
        #
        # TODO: Decide whether to update Sequencing_SiteCode__ at the Site level. This seems logical at first glance,
        #  but has the disadvantage that the same site code isn't guaranteed to not be re-used for a different site in
        #  a different year (due to it being defined by people not directly involved with the DB). Because of this, and
        #  because updating it at the Soil-Depth level is easier (due to there being an existing function), it's just
        #  updated at the Soil-Depth level.

        # METADATA ----

        # get metadata from file_list (reuse code from build_db.py, then select only the row for this datafile)
        if args.database in ['local', 'test']:
            file_list_filename = 'file_list_test.csv'
        else:
            file_list_filename = 'file_list.csv'
        file_list = pd.read_csv(os.path.join(root_dir, file_list_filename), sep=',', quotechar='"', na_filter=False,
                                dtype=object)

        # find the row where the imported_file_href__ matches the end of the input_fp string
        file_metadata = file_list.loc[file_list['imported_file_href__'].apply(lambda x: args.input_fp.endswith(x) and x!='')].squeeze()

        # remove metadata keys used only internally (those starting with _ )
        file_metadata = dict(file_metadata)
        file_metadata = {key: file_metadata[key] for key in file_metadata.keys() if not (key.startswith('_'))}

        metadata_node = nodes.Metadata(session=session, name=file_metadata['DatasetID'], data_dict=file_metadata,
                                       update_existing_data="toValues")
        # TODO: Allow custom value for update_existing_data.

        additional_labels = file_metadata['Availability__'].split(', ') + file_metadata['Type__'].split(', ')
        # NOTE: additional_labels *only* applies to the Metadata node; these labels aren't applied to the nodes being updated.
        metadata_node.node = session.write_transaction(metadata_node.update_labels, id_node=metadata_node.node._id,
                                                       labels=additional_labels)

        # DATAFILE ----

        print('Importing sample metadata from {}'.format(samplemetadata_fp))
        samplemetadata_df = pd.read_csv(samplemetadata_fp, sep=',', quotechar='"', dtype=object)

        # Define lists of useful columns (EDIT THESE AS NEEDED)
        columns_to_add_str = ['Sequencing_SampleID-1__', 'Sequencing_SampleID-2__', 'DepthCode__',
                              'Sequencing_DepthCode-1__', 'Sequencing_SiteCode__']
        columns_to_add_bool = ['metaG_ACE__', 'metaG_JGI_HiSeq__', 'metaG_JGI_NovaSeq__']  # these are actually stored as strings ("TRUE" or "FALSE")

        columns_to_add = columns_to_add_str + columns_to_add_bool

        # Iterate through the import file rows
        for index, row in samplemetadata_df.iterrows():

            # Skip the row if it has no useful information
            if all(pd.isnull(row[columns_to_add_str])) and all(row[columns_to_add_bool] == 'FALSE'):
                continue

            else:
                # depthID = session.read_transaction(get_depthID_from_sample_metadata,
                #                                    row['Site__'], row['CoreDated__'], row['DepthComplete__'])

                # Identify Depth-Info node matching sample metadata, and get its DepthID
                try:
                    depth_info_node = nodes.UniqueNode(session=session, kind='Depth-Info', id_key='DataID',
                                                       data_dict={'Site__': row['Site__'],
                                                                  'CoreDated__': row['CoreDated__'],
                                                                  'DepthComplete__': row['DepthComplete__']})
                except BadDataError as e:
                    # before giving up, try different float/int versions of DepthComplete__
                    try:
                        new_depth = str(float(row['DepthComplete__']))
                    except ValueError:
                        error(e)  # this should print the BadDataError

                    try:
                        depth_info_node = nodes.UniqueNode(session=session, kind='Depth-Info', id_key='DataID',
                                                           data_dict={'Site__': row['Site__'],
                                                                      'CoreDated__': row['CoreDated__'],
                                                                      'DepthComplete__': new_depth})
                    except BadDataError as f:
                        # Last ditch effort: Try the int version of the depth (if it's the same as the float version)
                        if int(float(new_depth)) == float(new_depth):
                            new_depth = str(int(float(row['DepthComplete__'])))
                            depth_info_node = nodes.UniqueNode(session=session, kind='Depth-Info', id_key='DataID',
                                                               data_dict={'Site__': row['Site__'],
                                                                          'CoreDated__': row['CoreDated__'],
                                                                          'DepthComplete__': new_depth})
                        else:
                            error(f)

                depthID = depth_info_node.node._properties['DepthID']

                # Add relationship to Metadata node
                session.write_transaction(create_relationship, id_source=depth_info_node.node._id,
                                          rel_type='HAS_METADATA', id_end=metadata_node.node._id)

                # Assign new properties to all nodes with that depthID
                for col in columns_to_add:
                    if pd.isnull(row[col]):
                        continue
                    else:
                        session.write_transaction(set_property, depthID, col, row[col])
