#!/usr/bin/env python
"""
This script is used to import all data listed in a file list CSV into the DB.

To build the database from scratch using this script (first deleting any existing DB), run the following
from a terminal in the root isogenie-tools directory:
    python tools/bin/build_db.py --config IsoGenieDB.config --clear-db
To instead test the import of everything on top of the existing database, omit --clear-db from the above.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import sys
import os
import pandas as pd
import argparse
from glob import glob
import subprocess
from pprint import pprint
import datetime
import time

# import common_vars, which is in the root isogenie-tools directory
# append two '..' to the current directory of this file, because current directory is two levels down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from io_misc.Modify_nodes import run_add_missing_sample_metadata_from_soildepths, run_update_metadata
from local_file_paths import *
from tools.data.common_vars import *
from tools.imports import nodes_new as nodes
from tools.utilities.validate_filepaths import validate_filepaths


parser = argparse.ArgumentParser(description="Import entirety of data into a database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('--clear-db', dest='clear_db', action='store_true',
                    help="Delete the previous database before re-building from scratch. "
                         "Only works for local and test databases.")

inputs.add_argument('-u', '--update', dest='update_existing', choices=['none', 'missing', 'toValues', 'everything'], default='none',
                    help="Whether to update existing nodes with new data, and which values to update."
                         "Options, in order of comprehensiveness:\n\n"
                         "none          Does nothing.\n"
                         "missing       Replaces ONLY missing data with new_data.\n"
                         "toValues      Replaces all data with new_data, except when doing so would change a value to NaN.\n"
                         "everything    Replaces ALL data with new_data, even if doing so would change it to NaN.\n")

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('--new-soildepth-keys', dest='soildepth_keys', default=False, nargs='+',
                    help="Whether to run run_add_missing_sample_metadata_from_soildepths (in io_misc/Modify_nodes),"
                         "and if so, which keys to update in downstream nodes.")

inputs.add_argument('--cached-queries', dest='cached_queries', action='store_true',
                    help="Update cached queries immediately after finishing imports.")

inputs.add_argument('--updatestart', dest='update_start_date', default=False,
                    help="Earliest CreationDate__ or UpdateDate__ (format: YYYYMMDD) of nodes that should be queried "
                         "when updating cached queries. "
                         "This only applies if using the argument --cached-queries. "
                         "If specified, then only those nodes with a CreationDate__ or UpdateDate__ on or later than "
                         "that date will be queried. This option provides a straightforward way of updating only those "
                         "cached queries which actually include new (meta)data.")

inputs.add_argument('--labels', dest='specific_labels', default=False, nargs='+',
                    help="Specific node labels to query when updating cached queries. "
                         "This only applies if using the argument --cached-queries. "
                         "If specified, then only the nodes with ALL those labels will be queried, plus any labels "
                         "that overlap with that combination of labels (i.e., labels that share at least some of the "
                         "same nodes as the specified labels). 'Metadata' nodes are always queried regardless."
                         "It is the user's responsibility to make sure that the labels included here correspond with "
                         "the labels of the nodes actually updated during import (NOTE: The --updatestart argument "
                         "provides an easier way of doing this, and should now be used instead for this purpose). "
                         "If unspecified, then ALL labels will be queried.")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password")

configs.add_argument('-p', '--python', dest='python', help="Location of python executable to be used. Useful if python "
                                                           "was custom installed.")

args = parser.parse_args()

## Set args.verbose as an environment variable (must be a string) so that it's accessible by printv() in common_vars
#if args.verbose:
#    os.environ['VERBOSE'] = 'True'
#else:
#    os.environ['VERBOSE'] = 'False'

if __name__ == "__main__":

    # Validate database
    config_fp = os.path.realpath(args.config_fp)

    #graph_db, root_dir = load_config(config_fp, args.database)  # config_file (path) defined in local_file_paths
    driver, root_dir = load_config_driver(config_fp, args.database)

    if args.python:
        python = args.python
    else:
        python = 'python'
        
    # Verbose print outputs: send " --verbose" (if True) or an empty string (if False) to import commands
    if args.verbose:
        verbose_arg = ' --verbose'
    else:
        verbose_arg = ''
        
    # Send update arg to import commands
    update_arg = ' --update {}'.format(args.update_existing)

    # File paths to import scripts
    bin_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '../../bin')))

    if args.clear_db:
        if args.database in ['local', 'test']:
            with driver.session() as session:
                session.write_transaction(nodes.Vertex().run_fixed_query, query="MATCH (n) DETACH DELETE (n)")  # need to test
            #graph_db.delete_all()

# commented out the following because it might not work when running the script in the background
#         else:
#             proceed = ''
#             while proceed not in ['y', 'n', 'exit']:
#                 proceed = input('Are you sure you want to delete the database?\n'
#                                 '  y     = yes\n'
#                                 '  n     = no (and import without deleting)\n'
#                                 '  exit  = no (and abort the import)'.format(update_existing_data)).lower()
#             if proceed == 'y':
#                 graph_db.delete_all()
#                 
#             if proceed == 'exit':
#                 print('Exiting...')
#                 sys.stderr.flush()
#                 sys.exit(1)
            
    # Datafile metadata
    if args.database in ['local', 'test']:
        file_list_filename = 'file_list_test.csv'
    else:
        file_list_filename = 'file_list.csv'

    file_list_filepath = os.path.join(root_dir, file_list_filename)
    file_list = pd.read_csv(file_list_filepath, sep=',', quotechar='"', na_filter=False)

    # Validate file_list
    print('Validating file paths:')
    validate_filepaths(file_list=file_list_filepath, db=args.database)
        
    time_start = time.time()

    print("Importing files listed in {}".format(file_list_filename))

    # First separate out the metadata-only datasets (which don't have anything in the _script column), and update the
    #  Metadata nodes for those. Using _script rather than _imported column, as it's more of a fail-safe.
    file_list_metadata_only = file_list.loc[file_list['_script'] == '', ]
    file_list = file_list.loc[file_list['_script'] != '', ]  # reuse same name

    # First update the Metadata nodes for datasets where that's all that's needed
    with driver.session() as session:
        run_update_metadata(session=session, file_list_arg=file_list_metadata_only)

    # Loop through imported files in file_list. ***Files are imported in same order as listed in the CSV.***
    for index, row in file_list.iterrows():
        if args.database in ['local', 'test']:
            data_fp = os.path.join(root_dir, row['imported_file_href__'])
            if row['columns_file_href__'] != '':
                columns_fp = os.path.join(root_dir, row['columns_file_href__'])
            else:
                columns_fp = False  # re-assign to False to prevent it from carrying over from the last iteration
        else:
            data_fp = row['imported_file_href__']  # the server datafile paths are absolute because they're in multiple places
            if row['columns_file_href__'] != '':
                columns_fp = row['columns_file_href__']
            else:
                columns_fp = False  # re-assign to False to prevent it from carrying over from the last iteration
            
        script_fp = os.path.join(bin_dir, row['_script'])
        special_args = row['_special_args']
        if columns_fp:
            special_args = special_args + ' --columns ' + columns_fp

        import_cmd = '{} {} --input {} --db {} --config {}{}{}{}'.format(
                python, script_fp, data_fp, args.database, config_fp, special_args, update_arg, verbose_arg)
        
        subprocess.check_call(import_cmd, shell=True)

    if args.soildepth_keys:
        # TODO: Update run_add_missing_sample_metadata_from_soildepths() to be able to *overwrite* properties that
        #  already exist (this will be extremely useful when updating core group names in SampleID__).
        with driver.session() as session:
            print('Updating {} properties (if missing) in nodes associated with Soil-Depth nodes...'.format(
                args.soildepth_keys))
            run_add_missing_sample_metadata_from_soildepths(session=session, keys=args.soildepth_keys)
            
    time_end = time.time()
    print("Import finished. Elapsed time was %i seconds" % (time_end - time_start))

    if args.cached_queries:
        cached_query_cmd = '{} io_misc/Retrieve_labels_for_tables.py --db {}'.format(
            python, args.database)
        if args.update_start_date:
            cached_query_cmd += ' --updatestart ' + args.update_start_date
        if args.specific_labels:
            cached_query_cmd += ' --labels'
            for label in args.specific_labels:
                cached_query_cmd = cached_query_cmd + ' ' + label

        print('\n')
        print('Updating cached queries using the following command:')
        print('  ' + cached_query_cmd)

        subprocess.check_call(cached_query_cmd, shell=True)
