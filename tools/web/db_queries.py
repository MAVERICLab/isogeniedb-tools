#!/usr/bin/env python
"""
Runs and processes live queries launched from the web portal.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import argparse
from py2neo import Graph  # When running from Node on the server, this gives Permission Denied
from py2neo.packages.httpstream import http
import pandas as pd  # When running from Node on the server, this says 'Missing required dependencies'
from pyparsing import Literal, SkipTo
http.socket_timeout = 9999

pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 1000)


parser = argparse.ArgumentParser(description="Import entirety of data into a database")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help="Database to import cores into.")

inputs.add_argument('-n', '--node-labels', dest='labels', action='append', default=None,
                    help="Node labels to perform query against")

inputs.add_argument('-s', '--selection', dest='selection', choices=[None, 'ANS-Daily', 'ANS-Hourly', 'IH', 'MH', 'VS'],
                    help="", action='append')

inputs.add_argument('--verbose', dest='verbose', action='store_true',
                    help="Display ALL messages. If not used, the many messages about adding or "
                         "reusing nodes are suppressed (doesn't affect warnings and errors).")

inputs.add_argument('-f', '--format', dest='format', choices=[None, 'HTML', 'CSV'], default='HTML',
                    help="Output format")

configs = parser.add_argument_group('Configurations')

configs.add_argument('-c', '--config', dest='config_fp', help="Configuration file with username and password",
                     default='IsoGenieDB.config')

args = parser.parse_args()


def load_config(config_fn, location):
    config_df = pd.read_csv(config_fn, delimiter=',', index_col=0)

    graph_loc = config_df.loc[location, 'graph']
    graph_pass = config_df.loc[location, 'password']

    graph_db = Graph(graph_loc, password=graph_pass)
    root_dir = config_df.loc[location, 'rootdir']

    return graph_db, root_dir


def adjustIDs(column_list: list, search_id, search_name, label):
    #TODO: Annotate this.

    if (search_id in column_list) and (search_name not in column_list):

        column_list.insert(0, search_name)  # Insert at beginning of list to "sort"
        column_list.pop(column_list.index(search_id))  # Remove original name
        column_list.append(search_id)  # Move to end

        names = []
        for nameID in result_df[search_id]:
            if nameID:
                query, = graph_db.run("MATCH (n:{}) WHERE n.{}='{}' RETURN n.{} as {}".format(
                    label, search_id, nameID, search_name, search_name)).data()
                names.append(query[search_name])
            else:
                names.append('None')

        result_df[search_name] = names


def df_to_html(dframe: pd.DataFrame()):

    html = dframe.to_html(index=False, classes='my_class table-striped" id = "my_id')

    # Need to file write below
    # direct_html = html_template.substitute(html_table=html)

    # Find header so it can be copied to footer, as dataframe.to_html doesn't include footer
    start_header = Literal("<thead>")
    end_header = Literal("</thead>")

    text = start_header + SkipTo(end_header)

    new_text = ''
    for data, start_pos, end_pos in text.scanString(html):
        new_text = ''.join(data).replace(' style="text-align: right;"', '').replace('thead>',
                                                                                    'tfoot>\n  ') + '\n</tfoot>'

    # Get start and end positions to insert new text
    end_tbody = Literal("</tbody>")
    end_table = Literal("</table>")

    insertion_pos = end_tbody + SkipTo(end_table)

    final_html = ''
    for data, start_pos, end_pos in insertion_pos.scanString(html):
        final_html = html[:start_pos + 8] + '\n' + new_text + html[start_pos + 8:]

    return final_html


# Connect to DB, get info
graph_db, root_dir = load_config(args.config_fp, args.database)

# Shared info
lakes = ['IH', 'MH', 'VS']
ans = ['ANS-Daily', 'ANS-Hourly']

# Build dataframe from the input. Unlike retrieving labels/headers for single-label data, this requires going through
# each 'node' (really, Cursor) and building. Also unlike retrieving labels/headers, it doesn't require two separate
# queries against the database

if args.labels:
    label_query = 'MATCH (n:`{}`) RETURN n'.format('`:`'.join(str(args.labels[0]).split(',')))
    result = graph_db.run(label_query)  # .data()

    df_dict = {}
    for res in result:  # py2neo extends dict w/ RETURN n as key: n, with subdict of key, values
        data = res.data()['n']
        data.update({'labels': '|'.join(list(data.labels()))})

        df_dict[len(df_dict)] = data

    if len(df_dict) == 0:
        df_dict[0] = {'Matches': 'There are no matches for this(these) labels'}

    df = pd.DataFrame.from_dict(df_dict, orient='index')

    # Remove "extra" data
    columns = df.columns
    to_remove = [col for col in columns if (col.find('ID') == -1) and (col not in ['Area__'])]
    df = df[to_remove]

    if not args.format or args.format == 'HTML':
        html = df.to_html(index=False, classes='my_class" id = "selection')
        print(html)
    elif args.format == 'CSV':
        csv = df.to_csv(None, index=False)
        print(csv)


def select_to_df(selectors):  # name = key

    df_list = []

    for selector in selectors:

        key = None
        if any(args in ans for args in args.selection):
            key = 'ANS'  # Maybe add either Date or Hour label (depending on selector) plus the Data label?
        elif any(args in lakes for args in args.selection):
            key = ':'.join(['Data', 'Lake', selector])  # Changed to work with new lake temperature data structure
            #  (resolved by Date; all the lake Data nodes are also Date nodes) and also to include Lake label for
            #  consistency with ANS.

        # For ANS it's okay for key to be the only label, but other labels might not be so lucky, as `` surrounding will
        # yield no hits in DB.
        labels = '`{}`'.format(key) if '-' in key else '{}'.format(key)  # This won't work if it's a list of keys.
        # Won't encounter this anyway for the lake and ANS selectors used now, but may become a problem if adding other
        # data types.

        # This returns a list of keys that exist in the nodes with those labels:
        selection_query = 'MATCH (n:{}) unwind(keys(n)) as keys RETURN collect(distinct keys) as keys'.format(labels)

        result, = graph_db.run(selection_query).data()

        if key == 'ANS':
            filtered_result = sorted([x for x in result['keys'] if not any(y in x for y in [':', '__'])])
        elif any(x in key for x in lakes):
            filtered_result = sorted([x for x in result['keys'] if not any(y in x for y in ['__'])] + ['Depth__'])

        # Get properties as part of return, instead of iterating through the cypher object in py2neo
        return_properties = ['n.`{}`'.format(prop) for prop in filtered_result]

        new_query = 'MATCH (n:{}) RETURN {}'.format(labels, ', '.join(return_properties))
        final_result = graph_db.run(new_query).data()  # List of dicts w/ return_properties as keys

        df = pd.DataFrame.from_dict(final_result)  # no need to iterate through anything

        df_list.append(df)

    merged_df = pd.concat(df_list)

    return merged_df


if args.selection:

    result_df = select_to_df(args.selection)

    # Now, go through returned dataframe and clean up the columns: remove 'not useful' ones, convert IDs to actual
    # values where appropriate,

    # Remove pesky 'n' for everything
    result_df.rename(mapper=lambda x: x.replace('n.`', '').strip('`'), inplace=True, axis='columns')

    columns = list(result_df)

    # adjustIDs(columns, 'CoreID', 'Core', 'Core')  # "Core" isn't "Core__"?
    # adjustIDs(columns, 'SiteID', 'Site__', 'Site')
    adjustIDs(columns, 'DateID', 'Date__', 'Date')
    col_to_remove = ['DateID']
    if args.selection == 'ANS-Daily':
        col_to_remove.append('Time(UTC)')

    if args.selection == 'ANS-Hourly':
        adjustIDs(columns, 'HourID', 'Hour__', 'Hour')  # Takes a long time because each hour is unique across entirety
        col_to_remove.append('HourID')

    if any(arg in lakes for arg in args.selection):
        # COULD use adjustIDs to search for DepthID (from Depth__), but it's much faster to simply allow Depth__ to pass
        # through the filtered_result
        adjustIDs(columns, 'SiteID', 'Site__', 'Site')
        col_to_remove.extend(['AreaID', 'DepthID', 'SiteID'])

    for col in col_to_remove:
        try:
            columns.remove(col)
        except ValueError:
            pass

    result_df.drop(columns=col_to_remove, inplace=True)  # Already removed SiteID

    # Apply re-ordering of Columns
    result_df = result_df[columns]

    # Set up for later steps
    result_df.fillna(value=pd.np.nan, inplace=True)

    # Filter Month, Day, year for Daily
    # Filter UTC for Hourly
    if args.selection[0] == 'ANS-Daily':
        daily = result_df[pd.notnull(result_df['Year']) and pd.notnull(result_df['Month']) and pd.notnull(result_df['Day'])].copy()
        # If any all-null columns
        daily.dropna(axis='columns', how='all', inplace=True)  # Drop more than half the columns

        if not args.format or args.format == 'HTML':
            html = daily.to_html(index=False, classes='my_class" id = "selection')
            print(html)
        elif args.format == 'CSV':
            csv = daily.to_csv(None, index=False)
            print(csv)

    if args.selection[0] == 'ANS-Hourly':

        hourly = result_df[pd.notnull(result_df['Time(UTC)'])].copy()
        hourly.dropna(axis='columns', how='all', inplace=True)  # Drop more than half the columns

        if not args.format or args.format == 'HTML':
            html = hourly.to_html(index=False, classes='my_class" id = "selection')
            print(html)
        elif args.format == 'CSV':
            csv = hourly.to_csv(None, index=False)
            print(csv)

    if any(args in lakes for args in args.selection):

        # Adjust headers before merging/re-formatting data
        fix_headers = {
            'Depth__': 'Depth (m)',
            'Site__': 'Site',
            'Date__': 'Date'
        }

        result_df = result_df.rename(columns=fix_headers)
        result_df = result_df.rename(columns=lambda x: x.replace('T_', ''))
        result_df['Depth (m)'] = result_df['Depth (m)'].str.replace(' m', '')

        depth_summary = []
        for index, series in result_df.iterrows():

            # Need to half-zip dataframe Date rows with column headers (minutes), but not all
            # Did have a double dict comprehension - which worked awesomely, until wanted to add multiple lakes...

            # Save for later
            depth = series['Depth (m)']
            date = series['Date']
            site = series['Site']

            # Already saved, won't need for next piece
            series = series.drop(['Date', 'Depth (m)', 'Site'])

            # Iterate through 'columns' to merge day and time, with temperature as value for depth (which is col name)
            # Creates main "key" using date and k (which is column header), then a subdict with depth and temperature
            # ("v", which should be all that remains in the series), and then Site
            values = dict({'{} {}'.format(date, k): {depth: v, 'Site': site} for (k, v) in series.items()})

            new_df = pd.DataFrame().from_dict(values, orient='index')

            depth_summary.append(new_df)

        try:
            final_df = pd.concat(depth_summary, sort=False)
        except TypeError:
            final_df = pd.concat(depth_summary)
            
        final_df.index.name = 'Date'
        final_df.reset_index(inplace=True)

        final_cols = [col for col in final_df.columns if col not in ['Date', 'Site']]
        final_df = final_df.dropna(how='all', subset=final_cols, axis='index')

        melt_df = pd.melt(final_df, id_vars=['Date', 'Site'], var_name=['Depth'], value_name='Temperature')

        melt_df = melt_df.rename(columns={'Date': 'date', 'Depth': 'depth', 'Temperature': 'temperature', 'Site': 'site'})

        if not args.format or args.format == 'HTML':
            html = final_df.to_html(index=False, classes='my_class" id = "selection')
            print(html)
        elif args.format == 'CSV':
            csv = final_df.to_csv(None, index=False)
            # csv = melt_df.to_csv('example2.csv', index=False)
            print(csv)
