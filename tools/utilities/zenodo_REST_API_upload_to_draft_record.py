#!/usr/bin/env python
"""
PURPOSE: Submit dataset to Zenodo via their REST API.
This was written based on the instructions at https://developers.zenodo.org/#rest-api, and has been successfully tested
for uploading a file to an existing draft record with a reserved DOI. This is useful for uploading large files
directly from a server with good internet bandwidth but no web browser, to then publish manually via the Zenodo website.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the NSF-funded EMERGE Biology
Integration Institute. Copyright © 2024 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

from pathlib import Path
import requests
from urllib.parse import urljoin
import json
import os
import sys
import argparse
import pandas as pd
from datetime import datetime


parser = argparse.ArgumentParser(description="Upload a data file to Zenodo via their REST API. This assumes a DOI has "
                                             "already been reserved manually via the website. Note that this does NOT "
                                             "accept a DatasetID, data list filename, or any other EMERGE-DB-specific "
                                             "parameters (as does zenodo_REST_API_upload.py), but is designed to allow "
                                             "straightforward command-line uploads to existing draft depositions.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('--data-fp', dest='data_fp', help="Filepath of data file to upload.")

inputs.add_argument('--zenodo-info-root', dest='zenodo_info_root',
                    default='~/Zenodo_uploads/',
                    help="Directory in which to create a folder for the API-interfacing JSONs. This folder will be "
                         "named according to the format: zenodo-upload.[datafile.name].[yyyymmdd]. If it already exists,"
                         "then the existing folder will be re-used.")

inputs.add_argument('--reserved-doi', dest='reserved_doi',
                    help="Pre-reserved DOI for uploading files, or the trailing number from this DOI.")

inputs.add_argument('--token-path', dest='token_path', default='~/.ssh/id_zenodo',
                    help="Path to the access token file used to authenticate with the Zenodo REST API.")


args = parser.parse_args()

if __name__ == "__main__":

    data_fp = args.data_fp
    zenodo_info_root = Path(args.zenodo_info_root).expanduser()

    data_fn = os.path.basename(data_fp)
    zenodo_info_dir = os.path.join(zenodo_info_root, "zenodo-upload." + data_fn + "." + datetime.now().strftime('%Y%m%d'))

    # Verify that all the required paths exist
    if not (os.path.exists(data_fp)):
        print("ERROR: Could not locate data file {}".format(data_fp))
        exit()

    if not (os.path.exists(zenodo_info_dir)):
        print(f"Creating {zenodo_info_dir}")
        os.makedirs(zenodo_info_dir)

    # Create a subdirectory for the API submission files created by this script (if it doesn't already exist)
    api_files_dir = os.path.join(zenodo_info_dir, 'api_submission_files')
    if not (os.path.exists(api_files_dir)):
        os.makedirs(api_files_dir)

    # Import access token
    main_url = 'https://zenodo.org/'
    with open(Path(args.token_path).expanduser(), 'r') as access_token_fh:
        access_token = access_token_fh.read().strip()

    # Params: Set and forget
    headers = {"Content-Type": "application/json"}
    params = {'access_token': access_token}

    # The deposition_id is just a component of the reserved DOI
    doi = args.reserved_doi
    deposition_id = doi.replace('10.5281/zenodo.', '')

    # Retrieve the deposition resource JSON (with bucket URL for upload)
    # https://developers.zenodo.org/#retrieve

    deposition_json = Path(os.path.join(api_files_dir, 'deposition.json'))

    if not deposition_json.exists():
        retrieve_req = requests.get(urljoin(main_url, f'api/deposit/depositions/{deposition_id}'),
                                   params=params)

        # write retrieve_req to a new file located at the create_json path
        with open(deposition_json, 'w') as deposition_fh:
            json.dump(retrieve_req.json(), deposition_fh)

        # also save create_req to a variable
        submission_metadata = retrieve_req.json()

    else:
        # if the deposition_resource_json file already exists, just load it into a variable
        with open(deposition_json, 'r') as deposition_fh:
            submission_metadata = json.load(deposition_fh)


    # Get bucket for uploading file
    bucket_url = submission_metadata["links"]["bucket"] + '/'  # urljoin must have trailing / or it'll take base

    # Upload file(s) into bucket
    upload_fp = Path(data_fp)

    # The target URL is a combination of the bucket link with the desired filename seperated by a slash.
    upload_json = Path(os.path.join(api_files_dir, 'upload.' + os.path.basename(data_fp) + '.json'))

    if not upload_json.exists():
        print(f'Uploading {upload_fp} to {urljoin(bucket_url, upload_fp.name)}')

        # Write file to stream
        with open(upload_fp, 'rb') as upload_fh:
            upload_req = requests.put(urljoin(bucket_url, upload_fp.name),
                                      data=upload_fh,
                                      params=params,
                                      )

        # Save the upload_req info to json
        with open(upload_json, 'w') as upload_jfh:
            json.dump(upload_req.json(), upload_jfh)

        print(f"Upload attempt has finished. Check the draft record on the web, and see {upload_json}, to verify success.")

        # upload variable is commented out as it isn't being used, and would change each time with multiple files
        # upload = upload_req.json()

    else:
        print(f'Already uploaded; see {upload_json}.')
        # with open(upload_json, 'r') as upload_jfh:
        #     upload = json.load(upload_jfh)
