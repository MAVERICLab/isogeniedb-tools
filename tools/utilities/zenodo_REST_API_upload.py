#!/usr/bin/env python
"""
PURPOSE: Submit dataset to Zenodo via their REST API.
This was written based on the instructions at https://developers.zenodo.org/#rest-api, and has been successfully tested
for initial uploads.
If publishing a new version of a dataset, then separate api_submission_files folders might(?) need to be created for
each new version; probably the easiest way to do this is to rename the old version folders to include the version #s.
TODO: Need to figure out how to use the API to publish new versions of existing datasets, and then update this script
 accordingly.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the NSF-funded EMERGE Biology
Integration Institute. Copyright © 2023 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

from pathlib import Path
import requests
from pprint import pprint
from urllib.parse import urljoin
import json
import os
import sys
import argparse
import pandas as pd

# enable filepaths originating from the root isogeniedb-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

# local variables
from tools.utilities.utils import error, warning

parser = argparse.ArgumentParser(description="Submit dataset to Zenodo via their REST API. Out of caution, this only "
                                             "allows one dataset (as denoted by a DatasetID) to be submitted each time "
                                             "this script is run.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-root', '--datasets-root', dest='datasets_root',
                    default='test_data/IsoGenie_datasets/private_data/zenodo_submissions/',
                    help="Root directory containing dataset folders plus a _standardized.csv with the final dataset "
                         "details (including a list of filenames in the original_file_href__ column).")

inputs.add_argument('--data-list-fn', dest='data_list_fn', help="Filename of the _standardized.csv (containing "
                                                                "dataset submission responses with finalized metadata). "
                                                                "This is just the filename (not the full path), and it "
                                                                "must be inside the root_dir.")

inputs.add_argument('-id', '--dataset-id', dest='dataset_id', help="DatasetID of the dataset to upload. This dataset "
                                                                   "must have a directory (named with the DatasetID) "
                                                                   "inside the root_dir above, and that dataset "
                                                                   "directory must contain a .zenodo.json "
                                                                   "file plus all the data files listed in the "
                                                                   "_standardized.csv under original_file_href__.")

inputs.add_argument('--sandbox', dest='sandbox', action='store_true', help='Create a test upload at sandbox.zenodo.org '
                                                                           'instead of the real zenodo.org website.')

inputs.add_argument('--publish', dest='ready_to_publish', action='store_true', help='Whether to proceed with the final '
                                                                                    'publishing step.')

args = parser.parse_args()

if __name__ == "__main__":

    datasetID = args.dataset_id
    datasets_root = args.datasets_root
    dataset_dir = os.path.join(datasets_root, datasetID)
    data_list_fp = os.path.join(datasets_root, args.data_list_fn)

    # Verify that all the required files exist
    if not(os.path.exists(dataset_dir)):
        error("The dataset_dir ({}) does not exist.".format(dataset_dir))

    metadata_json_fp = os.path.join(dataset_dir, '.zenodo.json')
    if not(os.path.exists(metadata_json_fp)):
        error("The .zenodo.json file does not exist for {}.".format(datasetID))

    if not(os.path.exists(data_list_fp)):
        error("The data_list_fp ({}) does not exist.".format(data_list_fp))

    # Data files: Need to get these from the data list CSV
    datasets_df = pd.read_csv(data_list_fp, sep=',', quotechar='"', na_filter=False, dtype=object)
    data_files = datasets_df.loc[datasets_df['DatasetID']==datasetID, 'original_file_href__']  # returns a series (but should be just one value)
    if len(data_files) == 1:
        # need to reset the index so that it starts at 0, then get the first item
        data_files.reset_index(drop=True, inplace=True)
        data_files = data_files[0]  # should now be a string
    else:
        error("Could not find one unique set of data files in the input CSV. Found: {}".format(data_files))

    # split the comma-delineated file list string into an actual list for checking file existence
    data_files = data_files.split(', ')

    for file in data_files:
        if not(os.path.exists(os.path.join('test_data', file))):
            error("Could not locate data file {}".format(file))


    # Create a subdirectory for the API submission files created by this script (if it doesn't already exist)
    if args.sandbox:
        api_files_dir = os.path.join(dataset_dir, 'sandbox_submission_files')
    else:
        api_files_dir = os.path.join(dataset_dir, 'api_submission_files')
    if not(os.path.exists(api_files_dir)):
        os.makedirs(api_files_dir)

    # Import access token
    if args.sandbox:
        main_url = 'https://sandbox.zenodo.org/'
        with open(Path('~/.ssh/id_zenodo_sandbox').expanduser(), 'r') as access_token_fh:
            access_token = access_token_fh.read().strip()
    else:
        main_url = 'https://zenodo.org/'
        with open(Path('~/.ssh/id_zenodo').expanduser(), 'r') as access_token_fh:
            access_token = access_token_fh.read().strip()

    # Set and forget
    headers = {"Content-Type": "application/json"}
    params = {'access_token': access_token}

    # Find community name
    # response = requests.get('https://sandbox.zenodo.org/api/communities',
    #                         params={'q': 'emerge-bii',
    #                                 'access_token': sandbox_token})
    # pprint(response.json())

    # Create new, empty upload
    create_json = Path(os.path.join(api_files_dir, 'create.json'))

    if not create_json.exists():
        create_req = requests.post(urljoin(main_url, 'api/deposit/depositions'),
                                   params=params,
                                   json={},
                                   headers=headers)

        # write create_req to a new file located at the create_json path
        with open(create_json, 'w') as create_fh:
            json.dump(create_req.json(), create_fh)

        # also save create_req to a new variable
        submission_metadata = create_req.json()

    else:
        # if the create_json file already exists, just load it into a new variable
        with open(create_json, 'r') as create_fh:
            submission_metadata = json.load(create_fh)

    # Get bucket
    bucket_url = submission_metadata["links"]["bucket"] + '/'  # urljoin must have trailing / or it'll take base

    # Get deposition id
    deposition_id = str(submission_metadata['id'])

    # Upload file(s) into new bucket
    for filepath in data_files:
        # root_dir = Path('~/Downloads/').expanduser()
        # upload_fp = root_dir / 'test_data.zip'
        upload_fp = Path(os.path.join('test_data', filepath))

        # The target URL is a combination of the bucket link with the desired filename
        # seperated by a slash.
        # upload_json = Path('upload.json')
        upload_json = Path(os.path.join(api_files_dir, 'upload.' + os.path.basename(filepath) + '.json'))

        if not upload_json.exists():

            print(f'Uploading {upload_fp} to {urljoin(bucket_url, upload_fp.name)}')

            # Write file to stream
            with open(upload_fp, 'rb') as upload_fh:
                upload_req = requests.put(urljoin(bucket_url, upload_fp.name),
                                          data=upload_fh,
                                          params=params,
                                          )

            with open(upload_json, 'w') as upload_jfh:
                json.dump(upload_req.json(), upload_jfh)

            # upload variable is commented out as it isn't being used, and would change each time with multiple files
            # upload = upload_req.json()

        else:
            print(f'Reusing upload information in {upload_json}.')
            # with open(upload_json, 'r') as upload_jfh:
            #     upload = json.load(upload_jfh)

    # Add metadata - for a full list: https://developers.zenodo.org/#representation
    with open(metadata_json_fp) as metadata_json_fh:
        metadata_dict = json.load(metadata_json_fh)
    metadata_data = {
        'metadata': metadata_dict
    }

    metadata_submission_json = Path(os.path.join(api_files_dir, 'metadata_submission.json'))

    if not metadata_submission_json.exists():

        print(f'Adding metadata (via put) for {bucket_url}')

        metadata_req = requests.put(urljoin(main_url, 'api/deposit/depositions/' + deposition_id),
                                    params=params,
                                    data=json.dumps(metadata_data),
                                    headers=headers)

        with open(metadata_submission_json, 'w') as metadata_fh:
            json.dump(metadata_req.json(), metadata_fh)

        metadata_response = metadata_req.json()

    else:
        with open(metadata_submission_json, 'r') as metadata_fh:
            metadata_response = json.load(metadata_fh)

    # just to be safe, don't start the publishing step until everything else is ok!
    if args.ready_to_publish:

        # Publish
        publish_req = requests.post(f'{main_url}api/deposit/depositions/{deposition_id}/actions/publish',
                                    params=params)

        publish_json = Path(os.path.join(api_files_dir, 'publish.json'))
        with open(publish_json, 'w') as publish_fh:
            json.dump(publish_req.json(), publish_fh)

        print(f'Published under {publish_req.status_code}')

    else:
        print("Not yet publishing, but all the information should be ready. Use --publish to publish it.")
