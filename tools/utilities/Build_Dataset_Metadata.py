"""
PURPOSE: Convert dataset submission form responses into file metadata for:
(1) uploading to Zenodo as JSON
(2) importing into Neo4j as Metadata nodes
(3) (future) automated parsing of files for granular import into Neo4j
Before running this script on the form responses, first run Collect_New_Datasets.py to update the local form responses
file (if necessary) and assign the DatasetID prefixes, then manually fill in the rest of the DatasetID strings and
assign DatasetPackageIDs.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the NSF-funded EMERGE Biology
Integration Institute. Copyright © 2022 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
import sys
import json
import argparse
import pandas as pd
import re
import requests
import gdown
from urllib import request

# json schema validation (and templating?)
import jsonschema

# enable filepaths originating from the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

# local variables
from tools.data.private.private_vars import people_list_csv_url
from tools.utilities.utils import error, warning

parser = argparse.ArgumentParser(description="Build dataset metadata from Google form responses, in the form of both a "
                                             "JSON for uploading to Zenodo, and updates to file_list_all.csv. "
                                             "For security reasons, this will not update file_list_all.csv directly, "
                                             "but any updates to the file itself will be put into a temporary copy "
                                             "with the suffix '_updates_[today's date].csv'.")

inputs = parser.add_argument_group('Inputs')

# NOTE: Some values in the imported file below may have been manually edited based on emailed instructions, or to
#  ensure consistent values for parsing.
# TODO: Might want to create more new columns for *all* manually-edited values, to avoid manipulating user-submitted
#  responses?
inputs.add_argument('-i', '--input', dest='input_file', default='default', help="File path of form response sheet. "
                                                                                "This should be the *standardized* "
                                                                                "version (from Collect_New_Datasets.py, "
                                                                                "plus manual edits to columns), in "
                                                                                "as final a version as possible.")

# Option for downloading the data files.
# TODO: Need to figure out a good way to specify download of only *some* of the datasets. Might implement this in tandem
#  with versioning, possibly using a combination of checks on the response timestamp, and the data file md5sums?
inputs.add_argument('-rdf', '--refresh-data-files', action='store_true', help='Download the data files using the URLs specified '
                                                                              'under "Please upload your data files...". '
                                                                              'To avoid using excess internet bandwith by '
                                                                              're-downloading the files every time the metadata is '
                                                                              're-built, this option is turned off by default; so'
                                                                              'it should be explicitly specified when needing to '
                                                                              'download a new file (or new version of a file).')

inputs.add_argument('-rpl', '--refresh-people-list', action='store_true', help='Re-download the people list with '
                                                                               'details of EMERGE project members (for '
                                                                               'getting affiliations, ORCIDs, etc.)')

# JSON schema validation options.
# These are not currently usable due to the lack of a usable schema file from Zenodo that can successfully validate the
#  already-tested-and-found-to-be-valid JSON format on the EMERGE Github (https://github.com/emerge-bii/test-zenodo).
inputs.add_argument('-js', '--json-schema', default=None, help="File path of Zenodo JSON schema to validate against,"
                                                               "or 'default' to use the default filepath. "
                                                               "Currently there does not appear to be any schema file "
                                                               "from Zenodo that actually works, so best to skip this "
                                                               "argument; however it's still kept as a placeholder for "
                                                               "now.")
"""
NOTE ON ZENODO'S JSON SCHEMAS FOR FILE METADATA:
There are a few different versions of the JSON schema on Zenodo's website:
(1) https://zenodo.org/schemas/deposits/records/legacyrecord.json, linked from https://developers.zenodo.org/#github.
    This one fails validation with "Additional properties are not allowed ('version' was unexpected)", EVEN THOUGH 
    "version" is included in the attributes listed at https://developers.zenodo.org/#representation (this issue also 
    applies to everything else listed after "version" under "Deposit Metadata"). Assume this schema version is outdated 
    (as implied by the URL text).
(2) https://zenodo.org/schemas/records/record-v1.0.0.json, linked from https://about.zenodo.org/policies/.
    This one appears to be more official based on the filename. It also includes the "version" attribute. 
    However, there is no "upload_type" attribute (as listed at https://developers.zenodo.org/#representation); this 
    appears to have been replaced with "resource_type". 
    ALSO "recid" IS REQUIRED, BUT UNSURE WHETHER IT WOULD EVEN *EXIST* UNTIL UPLOAD IS SUCCESSFUL; SO THIS ONE ALSO
    FAILS VALIDATION.
(3) https://zenodo.org/schemas/deposits/records/record-v1.0.0.json, URL generated manually based on (1) and (2) above.
    This one seems pretty official, and is labeled as a "deposit schema" like (1) above (as opposed to a "record schema" 
    like (2)), but isn't linked from anywhere. As with (2), "upload_type" appears to have been replaced with "resource_type".
    BUT ALSO IT REQUIRES A NEW ATTRIBUTE, "_deposit", WHICH DOESN'T HAVE WELL-DOCUMENTED USAGE; AND GIVEN THE LEADING _
    (plus this document not being linked from developers.zenodo.org), THIS MAY INDICATE AN INTERNAL PROPERTY, WHICH 
    WOULD MEAN THIS MAY *NOT* IN FACT BE THE OFFICIAL UPLOAD SCHEMA?
    
Might need to contact Zenodo to ask which schema to use for validation (if one exists at all)? 
Note that a previous successful test script by Ben B. (using the REST API) just used a dict in a format consistent with 
(1) above, and no version #, so (1) might be the most up-to-date? But then again, this was on the *sandbox* website.
Might just have to run a test upload with a JSON in order to find the correct format... Maybe try adding/editing a JSON 
with "version" and other attributes for Ben W's test Zenodo GitHub repository?
"""

args = parser.parse_args()

def validate_json(json_fp, schema):
    # test
    # change the first filename below depending on which schema is being tested

    if schema == 'default':
        json_schema_fp = "test_data/IsoGenie_datasets/private_data/zenodo_submissions/json_schemas/zenodo_schema_legacyrecord.json"
        # json_schema_fp = "test_data/IsoGenie_datasets/private_data/zenodo_submissions/json_schemas/zenodo_schema_deposit_record-v1.0.0.json"
    else:
        json_schema_fp = schema

    with open(json_schema_fp) as zenodo_schema_file:
        zenodo_schema = json.load(zenodo_schema_file)

    with open(json_fp) as zenodo_json_file:
        zenodo_json = json.load(zenodo_json_file)

    jsonschema.validate(instance=zenodo_json, schema=zenodo_schema)

if __name__ == "__main__":

    # The file should be as final of a version as possible.
    responses = pd.read_csv(args.input_file, sep=',', quotechar='"', na_filter=False, dtype=object)

    # make Dataset__ column (identical to "Dataset name") for DB (if it doesn't already exist)
    if 'Dataset__' not in responses.columns:
        responses['Dataset__'] = responses['Dataset name']

    # Probably easiest to make folders named with DatasetIDs in test_data (can move them into "real" folders later).
    # These will also be used for the filepaths listed in a newly-created "original_file_href__" column in the
    #  standardized responses CSV for downloaded data files (consistent with the filepaths used for test DB imports,
    #  although those don't include the "test_data/" component of the filepath)
    # TODO: Need a way to automate knowing where to put the final dataset folders within the existing web directory structure
    datasets_root = 'test_data/IsoGenie_datasets/private_data/zenodo_submissions/'

    # Import the people list from the web, for getting affiliations, ORCIDs
    people_list_fn = "people_list.csv"
    people_list_fp = os.path.join("test_data", "IsoGenie_datasets", "private_data", "zenodo_submissions", people_list_fn)

    # Import the list of institution/affiliation abbreviations. TODO: This list is not yet comprehensive.
    with open(os.path.join("test_data", "IsoGenie_datasets", "private_data", "zenodo_submissions", "affiliation_abbrevs.json")) as affiliation_abbrevs_file:
        affiliation_abbrevs = json.load(affiliation_abbrevs_file)

    if args.refresh_people_list:
        # download the file, decode binary into text, and then save it
        # works with plain-text file URLs
        with request.urlopen(people_list_csv_url) as incoming_download:
            people_list_csv_str = incoming_download.read().decode()
        with open(people_list_fp, 'w') as people_list_file:
            people_list_file.write(people_list_csv_str)

    people_list_df = pd.read_csv(people_list_fp, sep=',', quotechar='"', skiprows=1, na_filter=False, dtype=object)

    for index, row in responses.iterrows():

        datasetID = row['DatasetID']

        # Create the dataset directory (if it doesn't already exist)
        dataset_dir = os.path.join(datasets_root, datasetID)
        if not os.path.exists(dataset_dir):
            os.makedirs(dataset_dir)

        # Generate JSON with basic details.
        # TODO: Need to gather full author details for those listed under ContactName__, including ContactEmail__
        #  (for DB), and Institution and ORCIDs (for Zenodo). Best way to automate might be to parse the existing
        #  "EMERGE Who's Who" list, but this will still require some work to convert institution abbreviations to full
        #  names, deal with alternate initials/spellings for people names, etc.

        # start with the easy properties
        zenodo_metadata = {
            "upload_type": "dataset",
            "title": row["Dataset__"],  # should this be edited? maybe not, as the form says "as you would like it to appear on the website"
            "keywords": ["EMERGE Biology Integration Institute", "Stordalen Mire"],  # from EMERGE Authorship Agreement
            "communities": [{"identifier": "emerge-bii"}]
        }

        # Description: This will be a concatenated long string containing the various description-type responses
        # (WILL NEED TO CHECK THE FINAL DESCRIPTIONS MANUALLY FOR ANY INTERNALLY-SUBMITTED INFO)
        # TODO: Later, might also need to parse other columns that are currently all blank or non-description-type (e.g., file/sample name formats).
        description_cols = {"methods": "Please describe any methods information that are needed to distinguish this dataset from other, very similar datasets.",
                            "column_defs": "Please describe any columns or other fields in this dataset that we didn't already ask about (and that aren't self-explanatory).",
                            "other_info": "Is there any other information you would like us to know about this dataset?",
                            "funding": "Was this dataset collected using any funding sources besides EMERGE?"}
        description_components = []

        # For now, just go through each one individually so that custom headers can be added.
        # To account for any responses with accidentally-entered spaces or newlines, use presence of letters to identify non-empty responses.
        if re.search(r'[A-Za-z]', row[description_cols['methods']]) is not None:
            description_components.append('METHODS:\n' + row[description_cols['methods']])

        if re.search(r'[A-Za-z]', row[description_cols['column_defs']]) is not None:
            description_components.append('COLUMN DEFINITIONS:\n' + row[description_cols['column_defs']])

        if re.search(r'[A-Za-z]', row[description_cols['other_info']]) is not None:
            description_components.append(row[description_cols['other_info']])

        # Funding acknowledgements will ALWAYS need to be included, even if just for EMERGE.
        # NOTE: Will likely need to manually add an additional IsoGenie funding acknowledgment for many datasets
        #  (hopefully they will have specified any other, less-obvious additional funding sources).
        funding_acknowledgment = "FUNDING:\n" \
                                 "This research is a contribution of the EMERGE Biology Integration Institute, " \
                                 "funded by the National Science Foundation, Biology Integration Institutes Program, " \
                                 "Award # 2022070.\n" \
                                 "We thank the Swedish Polar Research Secretariat and SITES for the support of the " \
                                 "work done at the Abisko Scientific Research Station. SITES is supported by the " \
                                 "Swedish Research Council’s grant 4.3-2021-00164."
        if re.search(r'[A-Za-z]', row[description_cols['funding']]) is not None:
            if row[description_cols['funding']].lower() not in ['no', 'no.']:
                # TODO: There are still some non-standard responses (e.g. "The funding of paper...") which will need to
                #  be corrected manually.
                funding_acknowledgment = funding_acknowledgment + "\n" + row[description_cols['funding']]
        description_components.append(funding_acknowledgment)

        # Put it all together
        # NOTE: This automatically creates Unicode escape sequences for special characters, which appear to render
        # correctly when uploading via GitHub; however, they might not work for copying and pasting to the web. So
        # for now, just print it to the console (which *does* display the rendered characters) for copying/pasting.
        final_description = "\n\n".join(description_components)
        # # print to console
        # print("\n---------------------------------")
        # print(row['DatasetID'])
        # print("---------------------------------")
        # print(final_description)
        # print("---------------------------------\n")

        # put description into README for the DB
        # https://stackabuse.com/writing-to-a-file-with-pythons-print-function/
        original_stdout = sys.stdout  # Save a reference to the original standard output
        description_readme_fp = os.path.join(dataset_dir, 'README_' + datasetID + '.txt')
        with open(description_readme_fp, 'w') as f:
            sys.stdout = f  # Change the standard output to the file we created.
            print(row['Dataset__'])
            print("\n")
            print(final_description)
            sys.stdout = original_stdout  # Reset the standard output to its original value


        # Before updating the JSON, replace \n newlines with HTML line breaks (based on test-zenodo results)
        final_description = final_description.replace("\n", "<br>")
        zenodo_metadata.update({"description": final_description})

        # Authors
        # TODO: Need to be able to convert between first initials and names, and between alternate first names. The
        #  HumanName class (in module: nameparser) might be useful for this sort of thing. For now, just use the raw
        #  ContactName__ field (which should already be a comma-separated list of "First Last" or "[First Initials]
        #  Last"), assume it includes full first names, and match those to the people list.

        # First verify that it's in a format that's readily parsable here (i.e. if there's semicolons,
        #  then we can assume that parsing by commas below won't work)
        creators_str = row["ContactName__"]
        if ";" in creators_str:
            error("Invalid ContactName__ format: {}".format(creators_str))

        creators_names = creators_str.split(", ")
        creators_emails = []
        creators_list = []
        for name in creators_names:
            # Loop through people_list_df until a match is found (probably not the most efficient way, but should work)
            name_found = False
            #name_lastfirst = None
            affiliation_abbr = None
            orcid = None
            email = 'UNDEFINED'  # placeholder text for easily searching/replacing in final dataframe
            for person_index, person_row in people_list_df.iterrows():
                first = person_row["First"]
                last = person_row["Last"]
                # check for (and skip) any rows with empty names before proceeding further
                if (re.search(r'[A-Za-z]', first) is None) or (re.search(r'[A-Za-z]', last) is None):
                    # all first and last names should contain at least one letter
                    continue
                if name.startswith(first) and name.endswith(last):
                    name_found = True
                    # name_lastfirst = last + ', ' + first  # not used yet

                    if re.search(r'[0-9]', person_row['ORCID']) is not None:
                        # ORCIDs should have at least a digit. TODO: Obviously it's more criteria than that, but just use this one for now.
                        orcid = person_row['ORCID'].replace("https://orcid.org/", "").replace("/", "")

                    if re.search(r'[A-Za-z]', person_row['Institution (abbreviation)']) is not None:
                        # affiliations should have at least a letter
                        affiliation_abbr = person_row['Institution (abbreviation)'].upper()

                    if '@' in person_row['Email']:
                        # Zenodo doesn't accept creator emails; these are just for the DB.
                        email = person_row['Email']  # let's just hope it's a valid email address

                    break

            if not name_found:
                warning('The name "{}" could not be found in the people list.'.format(name))
                name_components = name.split(" ")
                # name_lastfirst = name_components[-1] + ", " + name_components[0]
                # if len(name_components) != 2:
                #     warning('Could not straightforwardly identify the first and last name from "{}". '
                #             'Proceeding using the first and last space-separated elements, the name to be used is: '
                #             '"{}"'.format(name, name_lastfirst))

            # For now, just ignore the name_lastfirst variable (& warning commented out above), and use the original
            # name instead, as (1) reformatting involves a potential source of error, (2) some shortened nicknames
            # were found in the people list "First" field, and (3) it's probably safest to assume that the format
            # given by the data submitter is the one they want to use. Although the Zenodo metadata tutorial uses the
            # "Last, First" format in their example, some data records also exist that use the "First Last" format.
            creator_dict = {"name": name}

            if orcid is not None:
                creator_dict.update({"orcid": orcid})  # For now, just assume it's valid, and check manually

            if affiliation_abbr is not None:
                # Get the full institution name from the abbreviation
                # Note that some people have multiple affiliations, but not sure if Zenodo allows a list to be passed.
                #  So for now, just parse them into a single string (using a semicolon to separate).
                affiliation_abbrs = affiliation_abbr.split("/")
                affiliation_names = []

                for abbr in affiliation_abbrs:
                    try:
                        affiliation_names.append(affiliation_abbrevs[abbr])
                    except KeyError:
                        warning('Could not find a match for the affiliation abbreviation "{}". Skipping this one.'.format(abbr))

                if len(affiliation_names) > 0:
                    creator_dict.update({"affiliation": "; ".join(affiliation_names)})

            creators_list.append(creator_dict)  # can't use += to make a list of dicts
            creators_emails.append(email)

        zenodo_metadata.update({"creators": creators_list})
        responses.loc[index, 'ContactEmail__'] = ', '.join(creators_emails)

        # access rights
        access_response = row["When do you want the dataset to become publicly available?"]
        license_response = row["Once publicized, what license would you like the dataset to be published under?"]

        # Convert license_response to Zenodo's controlled vocabulary
        if license_response == "CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)":
            license_id = "cc-by-4.0"
            zenodo_metadata.update({"license": license_id})
        else:
            # TODO: Need to automate getting the IDs of other licenses. Currently this is best done manually by looking
            #  for a string in the license that looks like "cc-by-4.0" above, and then plugging it into the URL
            #  https://zenodo.org/api/licenses/[license_id] to make sure it's valid.
            error("unknown license selected; need to get details")

        if access_response.startswith("It's already public") or access_response.startswith("It should be publicized ASAP"):
            zenodo_metadata.update({"access_right": "open"})
            responses.loc[index, 'Availability__'] = 'IsoGenie, A2A, Public'
        else:
            # If not one of the above, assume it's restricted, just to be safe
            # (not embargoed, since publication date isn't yet known)
            zenodo_metadata.update({"access_right": "restricted",
                                    "access_conditions": "This data will become publicly available upon manuscript "
                                                         "publication. Until then, only reviewers of the manuscript "
                                                         "and EMERGE project members may access this data. Please "
                                                         "contact us if you fall under one of these categories."})
            responses.loc[index, 'Availability__'] = 'IsoGenie'

        # TODO: other properties e.g. notes? (maybe not for now)

        # Download data files
        if args.refresh_data_files:
            data_urls = row["Please upload your data files (and any READMEs) here."].split(', ')
            filepaths = []

            for data_url in data_urls:
                # Google Drive links require some special handling; the code below is modified from:
                #  https://stackoverflow.com/questions/38511444/python-download-files-from-google-drive-using-url (for downloading files with gdown) and
                #  https://stackoverflow.com/questions/52880111/python-google-drive-file-download-without-specifying-destination-filename (for getting filenames)

                # Convert URL for downloading (all URLs *should* be in the format below, since this field is populated
                #  automatically as files are uploaded)
                request_url = data_url.replace("https://drive.google.com/open?id=", "https://drive.google.com/uc?id=")

                # Get the originally-uploaded filename for saving
                header = requests.get(request_url).headers['Content-Disposition']
                file_name = re.search(r'filename="(.*)"', header).group(1)
                # Replace spaces with underscores before downloading (as they mess with EMERGE-DB filepaths)
                # TODO: Also check for and replace other special characters?
                file_name = file_name.replace(' ', '_')
                print('Preparing to download "' + file_name + '" ...')
                filepaths.append(os.path.join(dataset_dir.replace("test_data/", ""), file_name))

                # Download the file into the dataset folder
                # TODO: Should figure out a way to track versions of these?
                download_fp = os.path.join(dataset_dir, file_name)
                gdown.download(request_url, download_fp, quiet=False)

            # Update the responses dataframe with the filepaths
            responses.loc[index, "original_file_href__"] = ', '.join(filepaths)

        # Versions (for now, just use whatever was manually entered into the sheet)
        zenodo_metadata.update({"version": row["Version__"]})

        # See if a completed Zenodo upload already exists with the same version #, and if so, update the DB metadata
        #  (this would only be applicable if re-running this script to obtain post-Zenodo-submission info for the DB)
        zenodo_publish_json_fp = os.path.join(dataset_dir, "api_submission_files", "publish.json")
        if os.path.exists(zenodo_publish_json_fp):
            with open(zenodo_publish_json_fp) as zenodo_publish_json_file:
                zenodo_publish_info = json.load(zenodo_publish_json_file)
            version_zenodo_current = zenodo_publish_info['metadata']['version']

            if version_zenodo_current == row["Version__"]:
                # Use the publication date & DOI for this specific version, just to keep things consistent
                # (FYI, the "cite all versions" would be in "conceptdoi" instead of "doi")
                responses.loc[index, 'VersionDate__'] = zenodo_publish_info['metadata']['publication_date']
                responses.loc[index, 'ExternalRepositoryURL__'] = zenodo_publish_info['links']['doi']

            else:
                warning(f"Re-building a new DRAFT.zenodo.json for {datasetID} with a different version ({row['Version__']}) "
                        f"than the currently active one ({version_zenodo_current}). If this is intentional, then it's "
                        f"strongly recommended to put the old data/metadata into a new folder named with the last uploaded "
                        f"version, before creating the new version.")

        # Write JSON to file
        json_fp = os.path.join(dataset_dir, "DRAFT.zenodo.json")  # use "DRAFT" prefix to protect the final versions (which may need some manual edits)
        with open(json_fp, 'w') as json_fh:
            json.dump(zenodo_metadata, json_fh)

        if args.json_schema is not None:

            # test an already-found-to-be-valid JSON (modified from https://github.com/emerge-bii/test-zenodo)
            test_json = "test_data/IsoGenie_datasets/private_data/zenodo_submissions/test_jsons/metadata_zenodo_test.json"
            validate_json(json_fp=test_json, schema=args.json_schema)

            # TODO: If Zenodo releases a valid schema (i.e. one that successfully validates the test_json above), then
            #  comment out the test_json validation above, and uncomment the real json validation below.
            #validate_json(json_fp=json_fp, schema=args.json_schema)

        else:
            pass  # print("Bypassing JSON validation.")

    # Re-write the responses CSV as a new file containing the new Metadata node properties, plus the downloaded file
    # filepaths (if applicable).
    # Saving it with a new name in order to protect the manually-standardized version against accidental changes.

    # However, this means that if further manual edits are made to the standardized.csv, then this script will need to
    # be re-run in order to re-create the standardized_forDB.csv file. If the data files are not being re-downloaded,
    # then this will overwrite the _forDB file to delete those filepaths!
    # Therefore, it's probably best to just copy these new folder paths into the old standardized.csv and then re-save
    # it; this way, additional files not from the Google Drive uploads can also be manually added to the
    # standardized.csv while still protecting it from manipulation via this script.

    # TODO: Once versioning is implemented, may also need to change the way the edited list is saved.

    # export
    standardized_metadata = responses[[col for col in responses.columns if (col.endswith('ID') or col.endswith('__'))]]
    standardized_metadata.to_csv(args.input_file.replace("standardized.csv", "standardized_forDB.csv"), index=False, quotechar='"', sep=',')