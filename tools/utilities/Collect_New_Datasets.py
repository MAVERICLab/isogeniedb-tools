"""
PURPOSE: Download dataset submission form responses as a CSV, and assign DatasetID number prefixes to each submitted dataset.
Once this is done, the next step is to manually expand these into full DatasetIDs, then group them together and assign
these groups DatasetPackageIDs.
Then the manually-edited CSV is imported into Build_Dataset_Metadata.py and used to update the file list CSV and
generate a JSON for Zenodo submission.

This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the NSF-funded EMERGE Biology
Integration Institute. Copyright © 2022 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import argparse
import pandas as pd
import os
import sys
import json
from datetime import datetime
from urllib import request

# enable filepaths originating from the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

# local variables
#from tools.data.common_vars import *
from tools.data.private.private_vars import form_responses_csv_url

parser = argparse.ArgumentParser(description="Collect the latest dataset submission Google form responses and assign "
                                             "DatasetID prefixes to new datasets.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('--file-list', default='test_data/file_list_all.csv', help='File path of existing file list CSV. Default: test_data/file_list_all.csv')

inputs.add_argument('--range', default='0000', help='Range of DatasetIDs (as first value in a range of 500, e.g. '
                                                       '"0000" [default] or "0500") for which to find the next set of '
                                                       'available DatasetID prefixes.')

#inputs.add_argument('--n-ids', default=1, help='Number of new DatasetID prefixes to return (default: 1).')

inputs.add_argument('-rfr', '--refresh-form-responses', action='store_true', help='Re-download the dataset submission form responses from the web. '
                                                                                  'If not set, it instead looks for an existing file located at '
                                                                                  'test_data/IsoGenie_datasets/private_data/zenodo_submissions/dataset_submission_responses.csv.')

args = parser.parse_args()


def next_DatasetIDs(file_list='test_data/file_list_all.csv', id_range='0000', n_ids=1):
    """
    This function looks at the existing list of DatasetID numbers, and selects the next available n numbers in the
     id_range specified (after making sure it doesn't duplicate any existing numbers e.g. from another range).

    More info about these id ranges:

    DatasetID number ranges refer to the range of the 4 digits at the beginning of each DatasetID.
    These digits serve as a (somewhat redundant) means of ensuring that each DatasetID is unique (i.e. to avoid relying
     entirely on the unnumbered 'text' portion), and are grouped into ranges based on broad project and database categories.
    Note that these ranges were previously implemented manually, and are not always consistent; therefore they
     shouldn't be exclusively relied upon to group datasets together, but exist mainly for convenience.

    The existing ranges are as follows:

    MAIN DATASETS IN OFFICIAL GRAPH DB (i.e. 'db'):
      0000: Main IsoGenie/EMERGE datasets, plus some A2A datasets.
      0500: A2A datasets (note: this does not appear to have been used consistently, although it should have been).
      1000: Main datasets that are handled entirely "internally" to these scripts (as opposed to submitted by other
       project members), e.g. the photo tags list.

    TEST DATASETS FOR LOCAL GRAPH DB (i.e. local, test):
      9000: TEST versions of IsoGenie/EMERGE datasets (for test DB only).
      0900: Another set of TEST datasets (not sure why it's different from 9000 group above; probably a mistake)

    Note: These number prefixes are not assigned to DatasetIDs generated for Data/Metadata nodes representing
     sequencing files for individual samples.

    """

    df = pd.read_csv(file_list, sep=',', quotechar='"', na_filter=False)

    datasetIDs = df['DatasetID'].tolist()
    datasetID_nums = [int(id[0:4]) for id in datasetIDs]

    min_value = int(id_range)
    max_value = min([min_value + 499, 9999])  # use ranges of 500 for consistency with existing ranges, but don't allow > 9999.

    # all sequential numbers between min_value and max_value
    values_in_range = range(min_value, max_value+1)

    # Available DatasetID numbers (in the specified range)
    available_values = [num for num in values_in_range if num not in datasetID_nums]

    # Select and return the next set of n available numbers, as strings with leading 0s.
    results_nums = available_values[0:int(n_ids)]
    results_strs = [str(num).zfill(4) for num in results_nums]

    return results_strs


if __name__ == "__main__":

    form_responses_fn = "dataset_submission_responses.csv"
    form_responses_fp = os.path.join("test_data", "IsoGenie_datasets", "private_data", "zenodo_submissions", form_responses_fn)

    # TODO: Need to be able to compare the current form responses with older versions (to be saved with datestamps at the end of this script).
    #  Or maybe since not all submissions may be processed at the same time (e.g. known pending edits to responses), might be better to compare
    #  with the file list csv? Or maybe use the "Timestamp" column in the form responses to compare with datestamps on previous versions?
    #  Need to decide the best way to proceed. See also the to-do for Version #s below.
    if args.refresh_form_responses:
        # download the file, decode binary into text, and then save it
        # works with plain-text file URLs
        with request.urlopen(form_responses_csv_url) as incoming_download:
            form_responses_csv_str = incoming_download.read().decode()
        with open(form_responses_fp, 'w') as form_responses_file:
            form_responses_file.write(form_responses_csv_str)

    # Read the responses into a dataframe
    form_responses_df = pd.read_csv(form_responses_fp, sep=',', quotechar='"')

    # Get the next set of n available DatasetID number prefixes, where n = # of rows in dataframe
    # TODO: Should rows with >1 linked data file be split up? Perhaps it's OK if not, but in some cases this may cause issues when actually importing data. Then again, some non-fully-imported datasets already link to multiple files...
    n_datasets = len(form_responses_df.index)
    new_DatasetID_nums = next_DatasetIDs(args.file_list, args.range, n_datasets)
    print(new_DatasetID_nums)

    # Add a temporary column with new DatasetID number prefixes (plus the "_" to prevent conversion back to ints)
    form_responses_df['_datasetID_prefix'] = [numstr + '_' for numstr in new_DatasetID_nums]

    # Add empty columns for creating the full DatasetIDs and DatasetPackageIDs (to be later filled in manually)
    empty_column = [''] * n_datasets
    form_responses_df['_datasetID_suffix'] = empty_column
    form_responses_df['DatasetID'] = empty_column
    form_responses_df['DatasetPackageID'] = empty_column

    # Versions - For now, just make a blank column and update them manually.
    # TODO: Figure out the best way to update version #s, integrated into comparisons with older versions of form responses.
    #  Note that not all changes to responses will result in a new version #, depending on whether the dataset has
    #  already been fully processed and updated in the DB; and also new version #s will need to be manually assigned
    #  as "major" or "minor" revisions based on the current system (see "How do we track file versions?" dropdown at
    #  https://emerge-db.asc.ohio-state.edu/datasource_groups).
    form_responses_df['Version__'] = empty_column

    # Ugh, just realized there are some non-standard responses to some questions e.g. author list... so also need to
    #  make a new column for a *standardized* author list that matches the existing ContactName__ property in the DB
    #  (may also need to do something similar for other free-form response fields).
    #  TODO: For the authors specifically, also need to convert "Same as paper authors" into the actual author list.
    #   Probably best to store these in a static file, as not all papers will have preprints from which to grab the
    #   authors from a RIS file before publication? Currently using "draft" author lists though.
    #   Also need to figure out how to deal with group author designations?
    with open(os.path.join("test_data", "IsoGenie_datasets", "private_data", "zenodo_submissions", "manuscript_info.json")) as manuscript_info_file:
        manuscript_info = json.load(manuscript_info_file)

    form_responses_df['PublishingNotes__'] = empty_column
    # EDIT 2/21/23: Not using ManuscriptCitation__ if there's no ManuscriptURL__, because this might not display properly on the website.
    # TODO: Add steps for parsing ManuscriptCitation__ and ManuscriptURL__ for *published* manuscripts ONLY.
    # form_responses_df['ManuscriptCitation__'] = empty_column
    # form_responses_df['ManuscriptURL__'] = 'NaN'  # instead of a blank string, use this default value for manuscripts that don't yet have URLs
    form_responses_df['ContactName__'] = empty_column

    for index, row in form_responses_df.iterrows():
        manuscript_nickname = row["Which paper(s) is this dataset for?"]

        # add manuscript citations (more may be added manually)
        form_responses_df.loc[index, 'PublishingNotes__'] = manuscript_info[manuscript_nickname]["citation_short"]

        if row["Who should be listed as contributors for this dataset?"].strip() == "Same as paper authors":  # not sure why, but some instances of this have a space at the end?
            form_responses_df.loc[index, 'ContactName__'] = manuscript_info[manuscript_nickname]["authors"]

    # Save as a new CSV with a datestamp corresponding to the creation date of the original file
    datestamp = datetime.fromtimestamp(os.path.getctime(form_responses_fp)).strftime('%Y-%m-%d')
    form_responses_fp_new = form_responses_fp.replace('.csv', '_' + datestamp + '.csv')
    form_responses_df.to_csv(form_responses_fp_new, index=False, quotechar='"', sep=',')

