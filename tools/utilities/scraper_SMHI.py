"""
OVERVIEW:
This script automates download of Abisko Naturvetenskapliga Station (ANS) meteorological data from the
Swedish Meteorological and Hydrological Institute (SMHI) data portal.


This script was created by Sachit Kshatriya (Case Western Reserve University), with notes and minor modifications added
by Dr. Suzanne Hodgkins (The Ohio State University), and with support from the NSF-funded EMERGE Biology Integration
Institute.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

# %%
import requests
import datetime
from pathlib import Path

# %%

t = datetime.datetime.now()
#datecode = f"{t.year}{t.month}{t.day}"
datecode = t.strftime('%Y%m%d')  # this format puts leading 0's on month and day, ensuring correct folder sorting

# These parameter numbers should always correspond to the same measurement type regardless of station; see
#  https://opendata.smhi.se/apidocs/metobs/parameter.html for a full list.
# Some are grouped together because they're closely related (e.g. daily maximum and minimum temperature).
# If any parameter in a group exists, then the others should too (this was tested manually on 2024-04-24 using manual
#  URL checks in the browser).
# But even if some parameters in a group didn't exist, the other parameters in the same group would still be downloaded,
#  with the resulting file being identical (based on md5sum) regardless of whether the non-existent parameters had been
#  attempted (based on test download of station 188800 param "1" only, and "1,6" [with 6 being non-existent]).
param_key = [
    "1",
    "2",
    "3,4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10,11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19,20",
    "21",
    "22",
    "23",
    "24",
    "25",
    "26,27",
    "28,30,32,34,29,31,33,35",
    "36",
    "37",
    "38",
    "39",
    "40",
]

stat_list = [
    188800,
    188790,
]

# Make station directories if they don't exist
for stat in stat_list:
    Path(f"test_data/IsoGenie_datasets/private_data/ANS/SMHI/station_{stat}").mkdir(parents=True, exist_ok=True)

# od_hist = Path("./historical")
# od_hist.mkdir(exist_ok=True, parents=True)

# %%


def pull_records(datecode: str, rectype: str = "historical", stationid: int = 188790):

    # Downloading into private_data (even though it's public in SMHI) due to potential large size, and to ensure that
    #  it's not shared without proper attribution (which will be added when sharing via the EMERGE-DB website).
    od = Path(f"test_data/IsoGenie_datasets/private_data/ANS/SMHI/station_{stationid}/{rectype}_{datecode}")
    od.mkdir(exist_ok=True, parents=True)
    print(f"Pulling data for station {stationid}")

    for val in param_key:

        historical = f"https://opendata-download.smhi.se/stream?type=metobs&parameterIds={val}&stationId={stationid}&period=corrected-archive"
        # The URL format above is the same one used for downloading parameters manually via a web browser.
        #  These manual downloads can be performed by going to the station URL (e.g.
        #  https://www.smhi.se/data/meteorologi/ladda-ner-meteorologiska-observationer#param=airtemperatureInstant,stations=all,stationid=188790),
        #  selecting a parameter, and clicking the button:
        #  "Ladda ner historisk data, förutom de senaste 3 månaderna (.csv)" (Swedish) or
        #  "Download historical data, except for the last 3 months (.csv)" (English).
        # To actually view the URLs, first download the files manually as described above, then go to the download
        #  history and right-click the downloaded filename to copy the URL.

        # Terms of use for downloaded data: https://www.smhi.se/data/oppna-data/villkor-for-anvandning-1.30622

        # Potential caveat: Note that these Terms of Use state,
        #   "You shall only use documented APIs. For example, it is not permitted to use services for downloading
        #   open data that are not in the open data service directory."
        # Since the files downloaded using the URL format above are identical to manual browser downloads (which anyone
        #  can easily access), it's likely still OK to use them here. However, SMHI strongly recommends use of the API
        #  for automated downloads. As of 2024-04-24, the URLs used by the API are formatted as follows:
        # historical_api = f"https://opendata-download-metobs.smhi.se/api/version/latest/parameter/{val}/station/{stationid}/period/corrected-archive/data.csv"
        # Note that the API doesn't support grouped parameters in the URL, so {val} for groups must be replaced by
        #  single parameter numbers. Also, the specific response text will differ from the code below, and the downloaded
        #  files may have slightly different notes formatting (based on a test download of parameter 21 from 188790).
        # See https://opendata.smhi.se/apidocs/metobs/index.html for full API documentation.

        try:
            flag = 0
            r = requests.get(historical)
            r.encoding = 'utf-8'  # change encoding from ISO-8859-1 to utf-8 to ensure filenames are saved correctly

            namen = ""
            for l in r.iter_lines(decode_unicode=True):
                # print(str(l))
                if flag:
                    namen = str(l).split(";")[0]
                    break
                if str(l).startswith("<!doctype html>"):
                    print(f"No data found for param: {val}")
                    break
                if str(l).startswith("Parameternamn"):
                    print(f"Success for: {val}")
                    flag = 1

        except Exception as e:
            print(f"Failed for: {val}")
            print(e)
            print("\n\n")
            continue

        if flag:
            opath = od.joinpath(f"{val}_{namen}.csv")
            with open(opath, "w", encoding="utf-8") as f:
                f.write(r.text)


# %%

for stat in stat_list:
    pull_records(datecode, "historical", stat)

print("Downloads complete.")
