#!/usr/bin/env python
"""
This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import pandas as pd
import numpy as np
import re

# enable filepaths originating from the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

class BadDataError(Exception):
    pass


def error(msg):
    # Edited this so that it can be caught by try-except statements. In the future, it's better to raise BadDataError
    # directly from the code that caused it, but it's also called here for backward compatibility.
    raise BadDataError(msg)
    # sys.stderr.write("\nERROR: " + msg + os.linesep + '\n')
    # sys.stderr.flush()
    # sys.exit(1)


def warning(msg):
    sys.stderr.write("\nWARNING: " + msg + os.linesep + '\n')
    sys.stderr.flush()
    
    
def printv(msg):
    verbose = os.environ.get('VERBOSE', 'True')
    # can't use bool(verbose) because this ALWAYS evaluates to True even if verbose='False'
    if verbose == 'True':
        print(msg)


def validate_csv(path):
    """
    General csv reader to identify major issues overall
    Design for all CSV data? Or for specific data types?
    :param path:
    :return:
    """
    pass


def lookup_id_from_df(value_lookup, colname_lookup, colname_result, df):
    """
    Look up a result value from a dataframe based on a value in a lookup column, returning 'NaN' if no match is found,
      or throwing an error if multiple matches are found.
    :param value_lookup: The value for which to look up a match.
    :param colname_lookup: Name of the column in which to search for value_lookup.
    :param colname_result: Name of the column containing the result to return.
    :param df: Dataframe in which to look.
    :return: Value under colname_result that matches value_lookup (i.e. from the same row as value_lookup).
    """
    result = df[(df[colname_lookup] == value_lookup) & (~df[colname_lookup].isna())][colname_result]
    # result is a series, which can be length 0 if no match is found

    try:

        if len(result) == 1:  # best case scenario
            result.reset_index(drop=True, inplace=True)  # resets the index to 0
            return result[0]

        elif len(result) == 0:  # no match; this may be fine though
            return "NaN"
            # return as a string to simplify comparison down the line
            # hopefully this will remain a string; if not, switch to np.nan to avoid confusion

        else:  # multiple matches
            error(f'Multiple ({len(result)}) matches found for {colname_lookup}="{value_lookup}"')

    except:
        print(result)
        error("Result (see above) could not be parsed correctly!")

def parse_official_SampleID(sampleID, data_source=None, parse_str=True):
    """
    From an official SampleID__ string, retrieve the standardized sample metadata (__) properties.
    :param sampleID:    SampleID__ string to look up.
    :param data_source: Dataframe in which to look up sample metadata (optional if parse_str==True).
                         This should be a query export for the Soil-Depth or Depth-Info label in the DB.
    :param parse_str:   Whether to directly parse the SampleID__ string text (beyond just the SampleGroup__).
                         If True (default), the sample metadata will be obtained directly from the ID string, and cross-checked with the data_source if one was provided.
                         If False, the information from the data_source will be used exclusively (except for the initial determination of SampleGroup__), throwing an error if there's no data_source.
    :return:            A dict containing standardized sample metadata.
    """

    # Apparently importing common_vars at the top of scripts that call this function, PLUS at the top of *this* file, is not enough;
    #  external variables must be imported BY NAME (not with *), & INSIDE THE FUNCTION, or else they won't be recognized!
    from tools.data.common_vars import field_sample_groups, incubation_sample_groups, site_abbrevs

    if data_source is None and not parse_str:
        error("Please either specify a data_source (recommended), or use parse_str=True.")

    useful_keys = ["FieldSampling__", "Date__", "Month__", "Year__",
                   "Site__", "Core__",
                   "Depth__", "DepthMin__", "DepthMax__", "DepthAvg__", "DepthSubsample__", "DepthCode__",
                   "CoreGroup__", "SampleGroup__",
                   "SampleType__"]  # SampleType__ is new as of this function; used for delineating "Field" vs. "Incubation/Bioreactor" samples.

    sample_dict = dict()
    for key in useful_keys:
        if key not in sample_dict.keys():
            sample_dict[key] = np.nan

    sampleID_split = sampleID.split(".")

    if len(sampleID_split) != 2:
        error(f"SampleID__ should have 2 components when split by '.' Invalid ID: {sampleID}")
        # TODO: Hopefully there won't be exceptions, but deal with them if so!

    sample_group = sampleID_split[0]
    sample_dict["SampleGroup__"] = sample_group

    quality_flag = ''

    if sample_group in field_sample_groups:
        sample_dict["SampleType__"] = "Field"
        sample_dict["CoreGroup__"] = sample_group
        # TODO: Finish filling other metadata, first from data_source and then by parsing the ID (if applicable).
        #  If there are mismatches, the data_source should prevail; and output a flag (string) listing any mismatched properties.

        if sample_group == "MalhotraTransectPerryman":  # non-standard ID format; skip for now. TODO: Need to instead parse those from the metadata file.
            quality_flag += "The information needed for parsing this SampleID__ is not yet finalized."

        else:
            # (1) Lookup from data_source
            datasource_properties = None

            if data_source is not None:

                datasource_result = data_source[(data_source["SampleID__"] == sampleID) & (~data_source["SampleID__"].isna())][[key for key in useful_keys if key in data_source.columns]]
                # result should be a dataframe?
                datasource_result.reset_index(drop=True, inplace=True)

                if len(datasource_result.index) == 1:
                    datasource_properties = datasource_result.squeeze().to_dict()

                elif len(datasource_result.index) == 0:
                    datasource_result = None
                    # TODO: Note this result in "flag" text.
                    quality_flag += "This SampleID__ does not yet exist in the DB."

                else:
                    quality_flag += f"This SampleID__ has multiple ({len(datasource_result.index)}) matches; skipping lookup of attributes."
                    # TODO: This shouldn't happen, but if it does, maybe see if the matches are identical before giving up?

            # (2) Parse sampleID components
            sampleID_properties = dict()
            if parse_str:
                sampleID_properties["CoreGroup__"] = sample_group  # same as sampleID_split[0]

                sampleID_components = sampleID_split[1].split("_")
                invalid_components = False

                if len(sampleID_components) == 4:

                    # Date
                    date_component = sampleID_components[0]
                    # This assumes the date is the actual month & year (not FieldSampling__). TODO: If any SampleIDs use non-standard FieldSampling__ (with extra text) for the date component, those will need to be parsed differently.
                    if re.search(r'^20\d{4}$', date_component) is not None:
                        sampleID_properties.update({"Year__": date_component[0:4],
                                                    "Month__": str(int(date_component[4:6]))})

                        # As of the 2023 field sampling, there should be the only 2 exceptions to the general rule that FieldSampling month == actual month.
                        # Any other exceptions should be caught when comparing with datasource_properties.
                        if date_component == "201009":
                            sampleID_properties.update({"FieldSampling__": "2010-08"})
                        elif date_component == "201808":
                            sampleID_properties.update({"FieldSampling__": "2018-07"})
                        else:  # most common case
                            sampleID_properties.update({"FieldSampling__": date_component[0:4] + "-" + date_component[4:6]})
                    else:
                        invalid_components = True

                    # Site
                    site_component = sampleID_components[1]
                    try:
                        sitename = site_abbrevs[site_component]
                        sampleID_properties.update({"Site__": sitename})
                    except KeyError:
                        invalid_components = True

                    # Core
                    core_component = sampleID_components[2]
                    # pretty sure all the core components can be read at face value
                    # TODO: CoreDated__? But this isn't as useful for filtering.
                    sampleID_properties.update({"Core__": core_component})

                    # Depth
                    # TODO: Check for non-numeric values?
                    depth_component = sampleID_components[3]
                    depth_converted = depth_component.replace("p", ".")
                    if re.search(r'to', depth_converted) is not None:
                        depth_min = depth_converted.split("to")[0]
                        depth_max = depth_converted.split("to")[1]
                        sampleID_properties.update({"DepthMin__": depth_min})
                        sampleID_properties.update({"DepthMax__": depth_max})
                        #sampleID_properties.update({"Depth__": depth_min + "-" + depth_max})  # placeholder just in case; depths can be variabe so don't use for now
                    else:
                        sampleID_properties.update({"DepthAvg__": depth_converted})

                else:
                    invalid_components = True

                if invalid_components:
                    quality_flag += " SampleID__ had one or more invalid components."

            # Check for any inconsistencies between datasource_properties and sampleID_properties
            if datasource_properties is not None and parse_str:
                inconsistent_properties = []
                for prop_key, prop_value_SID in sampleID_properties.items():
                    if prop_key in datasource_properties.keys():
                        if prop_value_SID == datasource_properties[prop_key]:
                            continue
                        else:
                            try:
                                # first see if they're just inconsistently-formatted numbers
                                if float(prop_value_SID) == float(datasource_properties[prop_key]):
                                    continue
                                else:
                                    inconsistent_properties.append(prop_key)
                            except ValueError:
                                inconsistent_properties.append(prop_key)
                if inconsistent_properties:
                    quality_flag += (" Inconsistent properties were found between the SampleID__ and corresponding properties in the DB: " + ", ".join(inconsistent_properties))

            # update sample_dict
            if datasource_properties is not None:
                # datasource_properties takes precedence, if it exists
                sample_dict.update(datasource_properties)
            elif len(sampleID_properties) > 0:
                sample_dict.update(sampleID_properties)
                quality_flag += " Obtained properties directly from SampleID__ in lieu of lookup from the DB,"
                if data_source is None:
                    quality_flag += " as no DB source was provided."
                else:
                    quality_flag += " as there was no match found in the DB."
            else:
                quality_flag += " NO PROPERTIES IDENTIFIED for this SampleID__."

    elif sample_group in incubation_sample_groups:
        sample_dict["SampleType__"] = "Incubation/Bioreactor"

    else:
        error(f'Invalid SampleGroup__: "{sample_group}" (in "{sampleID}")')

    if quality_flag != "":
        # could also put into a simple "QualityFlag__" property, but perhaps it's better to reserve that property for other uses
        sample_dict["sample_metadata_QualityFlag__"] = quality_flag.strip()  # remove leading space if one exists

    return sample_dict
