#!/usr/bin/env python
"""
OVERVIEW:
From excel-formatted (*.XLS, *.XLSX), manually-curated and triple-verified files were converted into CSV using Excel's
export functionality. These CSVs are then read in, and then data headers are "re-mapped" to standardized nomenclature.
These headers are then saved to a new CSV so that future work will no longer need to go through this process. This new
"standardized" CSV is then used for importing the data into the database.

Because this standardization is done once at this stage, the actual import process is faster, and the same import
scripts can also be reused for other standardized data sheets.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import re
import numpy as np
import pandas as pd
import argparse
#import LatLon  # doesn't work in Python 3
from dateutil.parser import parse as date_parse
from datetime import datetime
import warnings

# suppress FutureWarning
warnings.filterwarnings("ignore", category=FutureWarning)

#from pprint import pprint

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
from local_file_paths import *
from tools.data.common_vars import *

parser = argparse.ArgumentParser(description="Standardize coring sheets")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_file', default='default', help="File name of coring sheet CSV to standardize")

args = parser.parse_args()


def error(message):
    sys.stdout.write('ERROR: {}\nExiting...'.format(message))
    sys.exit(1)


# Suzanne's new function for getting ranges of close-together dates from a dataframe of dates
def get_date_ranges(df, gap=7):
    # Arguments:
    # df = dataframe with date columns only
    # gap = minimum # of days needed to separate date ranges (default 1 week)
    
    # Put all the values in df into one long list
    alldates = []
    for column in df:
        alldates += list(df[column])
    
    # Convert alldates to integers (if not already)
    for index, date in enumerate(alldates):
        alldates[index] = int(alldates[index])
    
    # Remove duplicates from alldates and sort ascending
    alldates = set(alldates)
    alldates = list(alldates)
    alldates.sort()
    
    # Convert dates to date format for getting differences in # of days
    alldates_int = alldates.copy()
    for index, date in enumerate(alldates):
        alldates[index] = datetime.strptime(str(date), "%Y%m%d")
    
    # Find the breakpoints between the date ranges
    isStartDate = []
    for index, date in enumerate(alldates):
        if index == 0:
            # the earliest date is always a start date
            isStartDate.append(True)
        # elif index == len(alldates)-1:
        #     # the latest date is never a start date
        #     # EDIT: Unless it's only date during that field sampling! So get rid of this condition.
        #     isStartDate.append(False)
        else:
            # all other dates
            if (alldates[index] - alldates[index-1]).days >= gap:
                isStartDate.append(True)
            else:
                isStartDate.append(False)
                
    # Slice alldates_int (unique sorted dates in integer format) into discrete ranges
    alldates_sep = []
    indices_StartDates = [i for i, x in enumerate(isStartDate) if x]
    indices_StartDates.append(len(alldates_int))
    for j, index_date in enumerate(indices_StartDates):
        if index_date == len(alldates_int):
            # if at the end of indices_StartDates, break out of the loop
            break
        else:
            # get the sublist of alldates_int
            daterange = alldates_int[indices_StartDates[j]:indices_StartDates[j+1]]
            # keep the first and last dates only
            daterange = [daterange[0], daterange[len(daterange)-1]]
            alldates_sep.append(daterange)
            
    return alldates_sep

def parse_dates_Ymd(date):
    # convert arbitrary date format to YYYYMMDD
    if not(pd.isnull(date)):
        date = date_parse(date).strftime('%Y%m%d')
    return(date)

def formatDate(intdate):
    # convert YYYYMMDD to YYYY-MM-DD, or YYYYMM to YYYY-MM
    if not(pd.isnull(intdate)):
        intdate = str(intdate)
        
        if len(intdate) == 8:
            formatted = datetime.strptime(intdate, '%Y%m%d').strftime('%Y-%m-%d')
        elif len(intdate) == 6:
            formatted = datetime.strptime(intdate, '%Y%m').strftime('%Y-%m')
        else:
            error("Could not format date {}".format(intdate))
    else:
        formatted = 'NaN'
        
    return formatted


# For converting string WTDs and ALDs to numeric.
# Code below copied from make_metadata_sheet.R, then translated into Python.
def convert_depth_to_numeric_cm(value, abs_val=False):
    original_value = value

    value = str(value)

    # convert any "1m" values to "100"
    value = re.sub(" 1m", "100", value)
    value = re.sub("1m", "100", value)
    value = re.sub("1 m", "100", value)

    # remove any extraneous "below" at the end of the string
    value = re.sub(" below$", "", value)

    # remove any "~" (meaning "approximately") at the beginning of the string
    value = re.sub("^\\~", "", value)

    # remove any extraneous "cm"
    value = re.sub(" *cm", "", value)

    # remove "?"
    value = re.sub("\?", "", value)

    # convert " to " and " or " to hyphens
    value = re.sub(" to ", "-", value)
    value = re.sub(" or ", "-", value)

    if pd.isnull(original_value) or original_value in [np.nan, "NaN", "nan", "", "Not Measured", "No core"]:
        # For some reason, NaNs are not always caught by the "in" statement, so need to also use pd.isnull(original_value)
        value = "Not Measured"

    try:
        result = float(value)

    except ValueError:

        if value.startswith(">"):  # greater than X
            if value == ">X":
                result = 'Below Detection'
            else:
                result = 'Below Detection (' + value + ')'

        elif original_value in ["?", "+/- neg, see notes", "Difficult to tell, fluffy sphag layer floated in/above the water surface."]:
            result = original_value

        elif original_value in ["N/D", "Not Detected"]:
            result = "Not Detected"

        elif value == "Not Measured":
            result = value

        elif bool(re.search("\\-", value)) and not (value.startswith("-")):  # X-Y, with "-" NOT at beginning of string

            try:
                depthrange = [float(v) for v in value.split('-')]
                result = sum(depthrange) / len(depthrange)

            except ValueError:
                warning('Could not convert WTD or ALD "' + original_value + '" to numeric.')
                result = original_value

        else:  # anything else
            warning('Could not convert WTD or ALD "' + original_value + '" to numeric.')
            result = original_value

    if abs_val and (type(result) in [float, int]):
        return abs(result)
    else:
        return result

def depth_str_for_IDs(depth, leading_0=False, replace_p=True):
    # For making sample IDs

    depth = float(depth)

    if depth == int(depth):

        if depth < 10 and depth >= 0 and leading_0:
            # add leading 0 to positive single-digit depths
            depth_str = '0' + str(int(depth))
        else:
            depth_str = str(int(depth))

    else:  # decimal depths
        if replace_p:  # convert . to p
            depth_str = str(depth).replace('.', 'p')
        else:
            depth_str = str(depth)

    return depth_str


def generate_official_SampleID(sample_dict):
    # Generates SampleID__ from standardized sample properties; returns dict with SampleID__ and related info.
    # This function only supports field samples. For incubation & bioreactor samples, the properties will be different
    #  for each sample group, so those IDs should be defined using a different method.
    # To do the reverse, use parse_official_SampleID() in tools.utilities.utils

    # make sure all useable keys are filled in (even if with NaN)
    useful_keys = ["FieldSampling__", "Date__", "Site__", "Core__",
                   "DepthMin__", "DepthMax__", "DepthAvg__", 'DepthSubsample__']
    for key in useful_keys:
        if key not in sample_dict.keys():
            sample_dict[key] = np.nan


    # Date component
    # also: Month__, Year__
    #if not(pd.isnull(sample_dict['Date__']) or sample_dict['Date__'] == 'NaN'):
    if sample_dict['Date__'] not in [np.nan, 'NaN']:
        # First see if we can use the full date, for consistency with previously-assigned IDs
        date_str = sample_dict['Date__'].replace("-", "")[0:6]
    #elif not(pd.isnull(sample_dict['FieldSampling__']) or sample_dict['FieldSampling__'] == 'NaN'):
    elif sample_dict['FieldSampling__'] not in [np.nan, 'NaN']:
        # If full date is unknown, then can use FieldSampling__
        date_str = sample_dict['FieldSampling__'].replace("-", "")
    else:
        date_str = np.nan
        print(sample_dict)
        error("No dates (Date__ or FieldSampling__) could be identified from this info.")

    sample_dict['Month__'] = str(int(date_str[4:6]))
    sample_dict['Year__'] = str(int(date_str[0:4]))
    # (converting to ints gets rid of leading 0 in month, and then converting back to strings prevents automatic conversion to floats)

    # Site component
    short_site_name = site_tag[sample_dict['Site__']]['Short']

    # Core component is simply the Core__, so no need to re-format

    # Depth component
    #if not (pd.isnull(sample_dict['DepthMin__']) or sample_dict['DepthMin__'] == 'NaN'):
    if sample_dict['DepthMin__'] not in [np.nan, 'NaN']:
        depth_range = depth_str_for_IDs(sample_dict['DepthMin__']) + 'to' + \
                      depth_str_for_IDs(sample_dict['DepthMax__'])

        # calculate DepthAvg__ for the output if it doesn't exist yet
        if sample_dict['DepthAvg__'] in [np.nan, 'NaN']:
            sample_dict['DepthAvg__'] = (float(sample_dict['DepthMin__']) + float(sample_dict['DepthMax__'])) / 2

    elif sample_dict['DepthAvg__'] not in [np.nan, 'NaN']:
        depth_range = depth_str_for_IDs(sample_dict['DepthAvg__'])
    else:
        depth_range = "NaN"
        print(sample_dict)
        error("No depths (DepthMin__ & DepthMax__, or DepthAvg__) could be identified from this info.")

    if sample_dict['DepthSubsample__'] not in [np.nan, 'NaN']:
        depth_range += ("_" + sample_dict['DepthSubsample__'])

    # CoreGroup__ component (e.g., "MainAutochamber")

    #  structured code this way for easy adding of more core groups
    core_group = "NaN"
    if sample_dict['Site__'].endswith('Autochamber Site'):
        if str(sample_dict['Core__']) in ['1', '2', '3', '1a', '1b']:
            core_group = 'MainAutochamber'
        elif str(sample_dict['Core__']).startswith('Inc'):  # 2014 incubation material
            core_group = 'IncubationMaterial'
        else:
            core_group = 'AdditionalAutochamber'
    elif sample_dict['Site__'].endswith('Optimization Site'):
        core_group = 'Optimization'
    elif 'Palsa Hole' in sample_dict['Site__'] or 'Collapsed Palsa' in sample_dict['Site__']:
        core_group = 'PalsaHole'
    elif sample_dict['Site__'].startswith('Avni Transect'):
        core_group = 'MalhotraTransect'
    elif sample_dict['Site__'].startswith('Inc-') or sample_dict['Site__'].startswith('Bioreactor-'):
        core_group = 'IncubationMaterial'
    elif sample_dict['Site__'] in ['Bog1', 'Fen1', 'Fen2']:
        core_group = 'Hodgkins'
    elif sample_dict['Site__'].startswith('AJ-'):
        core_group = 'Garnello'
    else:
        error('CoreGroup__ could not be identified for site "{}", core "{}".'.format(sample_dict['Site__'], sample_dict['Core__']))
        # change to warning?

    sample_dict['CoreGroup__'] = core_group

    # Put together the final SampleID__
    # NOTE: Based on the year & month used to generate the coreID below, this is based on the
    #   *actual* sampling date (not FieldSampling__).

    # assign coreID component to a separate property, to help with later filling-out of empty non-depth-specific coring sheet rows
    coreID_tmp = date_str + "_" + short_site_name + "_" + str(sample_dict['Core__'])
    sample_dict['coreID_tmp'] = coreID_tmp

    sampleID = core_group + "." + coreID_tmp + "_" + depth_range

    # Deal with one exception (same depth in two rows of a coring sheet, for which the first SampleID__ should be used)
    if sampleID.endswith("201106_Fen1_1_7"):
        sampleID = sampleID.replace("201106_Fen1_1_7", "201106_Fen1_1_5to8")

    sample_dict['SampleID__'] = sampleID

    # additional keys used by parse_official_SampleID(), to make sure the output of both functions is consistent
    # (these are valid only for field samples, for which this function is designed)
    sample_dict["SampleGroup__"] = core_group
    sample_dict["SampleType__"] = "Field"

    return sample_dict

if __name__ == "__main__":
    
    source_dir = source_cores_dir  # from local_file_paths.py
    
    # Gather all the CSVs
    core_file = args.input_file
    
    data_csvs = []
    
    if core_file == 'default':
        for root, dirs, files in os.walk(source_dir):
            for fname in files:
                if fname.endswith('_BB.csv'):
                    data_csvs.append(os.path.join(root, fname))
                if fname.endswith('_formatted.csv'):
                    data_csvs.append(os.path.join(root, fname))
    else:
        data_csvs.append(os.path.join(source_dir, core_file))
    
    
    # Read each CSV into a dataframe
    data_df_list = []
    for data_csv in data_csvs:
        print(data_csv)
        csv_df = pd.read_csv(data_csv, sep=',', quotechar='"')

        csv_df.replace({'---': np.nan, '`': np.nan, 'n/a': np.nan, 'n/s': np.nan, 'na':np.nan, 'NA':np.nan, '-':np.nan}, inplace=True)
    
        # Drop empty rows and columns (byproduct of Excel's "save as csv") for some sheets
        csv_df.dropna(axis='index', how='all', inplace=True)
        csv_df.dropna(axis='columns', how='all', inplace=True)
    
        # Create standardized column headers
        new_columns = []
        for column in csv_df.columns:

            new_column = False
            for official_column_name, variants in column_syn.items():
                if column in variants:
                    new_column = official_column_name
                    break

            if new_column:
                new_columns.append(new_column)
            else:
                new_columns.append(column)

        try:
            csv_df.columns = new_columns
        except ValueError:
            print('Old columns: {}'.format(csv_df.columns))
            print('New columns: {}'.format(new_columns))
            exit()
    
        for index, row in csv_df.iterrows():  # iterate over rows (index is the index of each row)
    
            # GPS
            try:
                adjusted_coords = standardize_GPS(row['GPS'])
                # New code without using latlon:
                csv_df.loc[index, 'GPS__'] = adjusted_coords

            except ValueError:
                print(row['GPS'])
                error('Unable to identify/determine GPS information. ALL cores must have a value, even if "no core"')

            except KeyError:

                try:
                    # 2017+ sheets have lat & lon in their own columns
                    lat = row['GPS_Latitude']
                    lon = row['GPS_Longitude']

                    try:  # the following assumes it's decimal degrees

                        # first get rid of trailing "N" and "E" (if applicable)
                        if type(lat) == str:
                            lat = float(lat.replace("N", ""))
                            lon = float(lon.replace("E", ""))

                        # round minutes to 10 decimal places (more than enough precision even for cm-resolution GPS)
                        #  to get rid of extra digits caused by floating point arithmetic
                        lat_min = round((lat - int(lat))*60, 10)
                        lon_min = round((lon - int(lon))*60, 10)
                        # format: N 68 21.1959, E 19 02.7974
                        adjusted_coords = 'N ' + str(int(lat)) + ' ' + str(lat_min) + ', E ' + str(int(lon)) + ' ' + str(lon_min)
                        # add a zero before single-digit numbers, e.g. "2.8" becomes "02.8"
                        # with a decimal point
                        adjusted_coords = re.sub(' (?=\d\.)', ' 0', adjusted_coords)
                        # without a decimal point (not likely to appear, but just in case)
                        adjusted_coords = re.sub(' (?=\d )', ' 0', adjusted_coords)
                        csv_df.loc[index, 'GPS__'] = adjusted_coords

                    except ValueError:  # non-decimal lat and lon
                        coords = str(lat) + ', ' + str(lon)  # this should result in something similar to row['GPS'] for the other years
                        adjusted_coords = standardize_GPS(coords)
                        csv_df.loc[index, 'GPS__'] = adjusted_coords

                except KeyError:
                    print(row['GPS'])
                    error('Unable to locate any GPS information.')

            except TypeError:
                error('Unable to parse GPS "{}" of type {}'.format(row['GPS'], type(row['GPS'])))
    
            # Date
            other_date = np.nan
    
            try:
                coring_date = row['Date_Coring']

            except KeyError:
                coring_date = np.nan
                    
            try:
                porewater_date = row['Date_Porewater']
                if ' and ' in porewater_date:  # 2 dates listed: applies to a few rows in 2012
                    other_date = porewater_date.split(' and ')[1]  # put the second date in 'other_date'
                    porewater_date = porewater_date.split(' and ')[0]  # put the first date in 'porewater_date'

            except KeyError:
                porewater_date = np.nan
            except TypeError:  # use because checking ' and ' in porewater_date raises TypeError for nan
                porewater_date = np.nan
                
            try:
                watertable_date = row['Date_WTD']
            except KeyError:
                watertable_date = np.nan
                    
            if pd.isnull(other_date):
                other_date = watertable_date  # still might be null, but use it if it exists
                    
            # standardize date formats
            coring_date = parse_dates_Ymd(coring_date)
            porewater_date = parse_dates_Ymd(porewater_date)
            other_date = parse_dates_Ymd(other_date)
            watertable_date = parse_dates_Ymd(watertable_date)
            
            # take care of some erroneous rows in 2013 sheet where porewater_date is in 2014
            if porewater_date == '20140727' and coring_date == '20130727':
                porewater_date = '20130727'
    
            # Save these dates to the dataframe (while some are still null) before assigning values to all the dates
            #  in order to get date ranges.
            # While saving, convert dates from YYYYMMDD to YYYY-MM-DD; but don't change the inputs directly because the
            #  int-like format is needed to get the date ranges.
            csv_df.loc[index, 'DateC__'] = formatDate(coring_date)
            csv_df.loc[index, 'DateP__'] = formatDate(porewater_date)
            csv_df.loc[index, 'DateOther__'] = formatDate(other_date)
            
            # To prepare to get date ranges, make sure coring_date and porewater_date are always filled in, even if from another date
            if pd.isnull(coring_date):

                if pd.isnull(porewater_date):

                    if pd.isnull(watertable_date):  # has no dates
                        print(row)
                        error('No dates identified. Tried {}, {}, {}'.format(coring_date, porewater_date, watertable_date))

                    else:  # has watertable_date only
                        coring_date = watertable_date
                        porewater_date = watertable_date

                else:  # has porewater_date, but no coring_date
                    coring_date = porewater_date

            else:  # has coring_date

                if pd.isnull(porewater_date): # has coring_date, but no porewater_date
                    porewater_date = coring_date
                    
            # fill in other_date if it's not already filled in
            if pd.isnull(other_date):
                if pd.isnull(watertable_date):
                    other_date = coring_date
                else:  # if other_date isn't already filled and watertable_date exists, use watertable_date
                    other_date = watertable_date
                    
            # Now save these to temporary columns
            csv_df.loc[index, 'DateC_tmp'] = coring_date
            csv_df.loc[index, 'DateP_tmp'] = porewater_date
            csv_df.loc[index, 'DateOther_tmp'] = other_date
    
            # Sites and Habitats
            potential_site = np.nan
            habitat = np.nan
            identified = False
            site_column = False
    
            if (not identified) and ('Site' in row):
                # Put this one first because it's the simplest case, and others are more likely to result in wrong names
                if not pd.isnull(row['Site']):
                    potential_site = row['Site']

                    # If it's "Bog" or "Fen", change to "Sphagnum" or "Eriophorum" so that it can be found in site_syn.
                    # Because those habitat labels are extremely general and therefore discouraged, this should
                    #   ONLY be done if those strings are found under a "Site" column (and that's why they're
                    #   not simply added to site_syn).
                    if potential_site == 'Bog':
                        potential_site = 'Sphagnum'
                    if potential_site == 'Fen':
                        potential_site = 'Eriophorum'

                    identified = True
                    site_column = True
    
            if (not identified) and ('Sample_Name' in row):
                if not pd.isnull(row['Sample_Name']):
                    potential_site = row['Sample_Name']
                    identified = True
    
            if (not identified) and ('Sample_ID' in row):  # If still not identified but there is a alternative site
                if not pd.isnull(row['Sample_ID']):  # Only if there's something in the row will it be true
                    potential_site = row['Sample_ID']
                    identified = True
    
            if (not identified) and ('Habitat' in row):
                potential_site = row['Habitat']
                identified = True
    
            if not identified:
                error('Unable to select site data.')
    
            name_changed = False
    
            if not site_column:  # New: Go through this only if Site came from a column other than 'Site'
                if any(frag in potential_site for frag in ['P1-', 'P2-', 'P3-']):
                    potential_site = 'Palsa'
                    name_changed = True
                elif any(frag in potential_site for frag in ['E1-', 'E2-', 'E3-']):
                    potential_site = 'Eriophorum'
                    name_changed = True
                elif any(frag in potential_site for frag in ['S1-', 'S2-', 'S3-']):
                    potential_site = 'Sphagnum'
                    name_changed = True
                elif any(frag in potential_site for frag in ['Palsa-opt', 'Sphagnum-opt', 'Eriophorum-opt']):
                    if 'Palsa' in potential_site:
                        potential_site = 'Palsa Optimization Site'
                    if 'Sphagnum' in potential_site:
                        potential_site = 'Sphagnum Optimization Site'
                    if 'Eriophorum' in potential_site:
                        potential_site = 'Eriophorum Optimization Site'
                    name_changed = True
                elif 'AJ-' in potential_site:
                    potential_site = potential_site.rsplit('-', 2)[0]  # NOT NAME CHANGE, CORRECTION
                elif 'Inc-' in potential_site:
                    potential_site = potential_site.rsplit('-', 2)[0]
                elif any(frag in potential_site for frag in ['.1', '.2', '.3']):
                    potential_site = potential_site.split('.')[0]
                elif 'time' in potential_site:
                    potential_site = row['Habitat']
                elif any(frag in potential_site for frag in ['PHS ', 'PHB ', 'Bog1 ', 'Fen1 ', 'Fen2 ', 'SOS ', 'EOS ']):  # sample names with depths in them
                    # print(data_csv)
                    potential_site = potential_site.split(' ')[0]
                else:
                    potential_site = potential_site.split('-')[0]
    
            # print(potential_site)
            
            # correct a misspelling
            if 'Anvi' in potential_site:
                potential_site = potential_site.replace('Anvi', 'Avni')
                
            # Sphagnum High Resolution "site" = regular Sphagnum site; should be differentiated by core name instead
            if potential_site == 'Sphagnum High Resolution Core':
                potential_site = 'Sphagnum'
                core_prefix = 'SHR'
            else:
                core_prefix = ''
                
            for site_name, variants in site_syn.items():
                if potential_site in variants:
                    potential_site = site_name
                    name_changed = True
                    break
    
            if name_changed:
                habitat = site_tag[potential_site]['Habitat Type']
            else:
                error('Could not determine site from: \n{}'.format(row))
                # Try parsing sample name...
    
            if pd.isnull(potential_site):
                error('Could not identify site')
            if pd.isnull(habitat):
                error('Could not identify habitat')

            # If habitat varies by year, need to get the habitat for *this* year
            if type(habitat) == dict:
                habitat_dict = habitat
                habitat = habitat_dict[coring_date[0:4]]
    
            csv_df.loc[index, 'Site__'] = potential_site
            csv_df.loc[index, 'Habitat__'] = habitat
    
            core_num = row['Core']
    
            try:
                int(core_num)
            except ValueError:  # String
                if core_num == 'Optimization':
                    core_num = '1'
                elif core_num == 'TIME-SERIES':
                    core_num = 'TimeSeries' + date_parse(row['Time_coring_begun']).strftime('%H%M')
                elif core_num == '1, second try':
                    core_num = '1b'
                elif core_num == '?':
                    # warning('Unidentified core for site {} in file {}'.format(potential_site, data_csv))
                    pass
                else:
                    error('Could not identify Core # from {}'.format(core_num))
                          
            core_num = core_prefix + str(core_num)
    
            csv_df.loc[index, 'Core__'] = str(core_num)  # converting to string prevents Pandas from automatically converting ints to floats

            # -----------------------------
            # Fix WTD signs
            # Dates, sites, core #s already done; depths and quality flags not yet.
            # Exact column names might not exist yet in row object.
            # Code below copied from make_metadata_sheet.R, then translated into Python and adapted to this script

            # First fill in the "original" WTD and ALD columns for 2013 collapsed palsa sites, which were missed in the "formatted" sheet
            if coring_date[0:6] == '201307' and 'Collapse Hole' in csv_df.loc[index, 'Site__']:
                wtd = 0
                row['Water_Table_Depth_WTD.cm'] = wtd
                csv_df.loc[index, 'Water_Table_Depth_WTD.cm'] = wtd

                ald = '>X'
                row['Active_Layer_Depth_ALD.cm'] = ald
                csv_df.loc[index, 'Active_Layer_Depth_ALD.cm'] = ald

            final_wtd = np.nan  # Takes care of cases where one of the WTD columns below exists, but its value is NaN
            wtd_quality_flag = ''

            if 'Water_Table_Depth_WTD.cm_neg_is_below_sfc' in row:
                wtd_neg_below = row['Water_Table_Depth_WTD.cm_neg_is_below_sfc']

                if wtd_neg_below == ">1m below":
                    converted_wtd = "Below Detection (<-100)"    # this should be handled separately due to sign switch
                else:
                    converted_wtd = convert_depth_to_numeric_cm(wtd_neg_below)

                final_wtd = converted_wtd
                
            elif 'Water_Table_Depth_WTD.cm_neg_is_ABOVE_sfc' in row:
                wtd_neg_above = row['Water_Table_Depth_WTD.cm_neg_is_ABOVE_sfc']

                if wtd_neg_above == "-6 / -2" and coring_date[0:6] == '201108' and csv_df.loc[index, 'Site__'] == "Eriophorum Autochamber Site" and csv_df.loc[index, 'Core__'] == '1':
                    final_wtd = 2  # Special case: coring location WTD was 2, porewater location WTD was 6. Use coring location.

                else:
                    converted_wtd = convert_depth_to_numeric_cm(wtd_neg_above)

                    if not(pd.isnull(converted_wtd)):

                        if type(converted_wtd) in [float, int]:
                            final_wtd = -1*converted_wtd    # flip the sign

                        else:  # text doesn't need to have the sign flipped (BUT THIS COULD BE AN ISSUE IF THE STRING CONTAINS A NUMBER AND COULDN'T BE CONVERTED)
                            final_wtd = converted_wtd

            elif 'Water_Table_Depth_WTD.cm' in row:
                wtd_unknown_sign = row['Water_Table_Depth_WTD.cm']

                converted_wtd = convert_depth_to_numeric_cm(wtd_unknown_sign)

                if float(coring_date[0:4]) < 2020:
                    wtd_quality_flag = 'Water table depth (WTD) note: The WTD sign convention for this field campaign was ambiguous, ' \
                                       'and/or the WTD signs for some of the sites were questionable. The originally-recorded ' \
                                       'WTDs are in the column "Water_Table_Depth_WTD.cm", and the sign-resolved WTDs ' \
                                       '(based on emails between Virginia Rich and Suzanne Hodgkins) are in the column ' \
                                       '"WTD.cm_neg_is_below_sfc__".'

                if not(pd.isnull(converted_wtd)):

                    if converted_wtd in [0, 'Not Measured', 'Not Detected', 'Difficult to tell, fluffy sphag layer floated in/above the water surface.']:
                        final_wtd = converted_wtd

                    elif float(coring_date[0:4]) >= 2020:  # 2020-2021 WTDs look OK

                        if float(coring_date[0:4]) >= 2022:
                            # make sure the sign matches "above or below surface"
                            if 'WTD_above_or_below_surface' in row:
                                if converted_wtd > 0:
                                    wtd_position_from_value = "above"
                                else:  # exactly 0 has already been handled above
                                    wtd_position_from_value = "below"
                                # check
                                if row['WTD_above_or_below_surface'] != wtd_position_from_value:
                                    warning(f"In row {index}, the sign of the WTD value ({converted_wtd}) does not "
                                            f"match 'above or below' designation ({row['WTD_above_or_below_surface']}). "
                                            f"Reversing the sign to match the text.\n"
                                            f"If WTD '{row['WTD_above_or_below_surface']}' (for a {habitat}) is "
                                            f"unexpected, check the file and redo.")
                                    converted_wtd = -1*converted_wtd

                        final_wtd = converted_wtd

                    elif habitat in ['Palsa', 'Bog']:
                        final_wtd = -1*abs(converted_wtd)    # all unknown palsa and bog samples have been resolved to BELOW surface

                        wtd_quality_flag += (' For this core, the WTD was assumed to be below the surface because the site is a ' + habitat.lower() + '.')

                        if csv_df.loc[index, 'Site__'] == 'Sphagnum Autochamber Site':
                            if coring_date[0:6] == '201307':
                                wtd_quality_flag += ' In addition, there was no porewater pH recorded at 3cm.'

                            elif coring_date[0:6] == '201407':
                                wtd_quality_flag += ' In addition, there was no porewater pH recorded at 3cm or 7cm.'

                            elif coring_date[0:6] == '201707':
                                wtd_quality_flag += ' In addition, there was no porewater pH recorded for the surface samples. ' \
                                                    'However, the WTD being below isn’t 100% certain, as Nicole’s note says ' \
                                                    '"positive=above surface," and also the site in general was unusually wet this year. ' \
                                                    'Note from Virginia Rich: "I think that is above; Nicole was attentive ' \
                                                    'to detail and her note is unambiguous, and it was reported as a wet time. ' \
                                                    'I would feel very certain of this but 11cm over the surface seems like a lot. ' \
                                                    'It’s worth looking at the site photo of that time. Nicole loves photography so ' \
                                                    'I’d think there’s good pics for that year." In response, Suzanne looked in the ' \
                                                    'Google Drive folder for 2017 and didn’t find any photos; however, Patrick Crill’s ' \
                                                    'sensor data says the WTD was 9cm below the surface on that same date. This, ' \
                                                    'combined with the lack of porewater measurements at the surface, led to the ' \
                                                    'consensus that the WTD for these cores was below the surface.'

                            elif coring_date[0:6] == '201807' and csv_df.loc[index, 'Core__'] == '1':
                                wtd_quality_flag += ' Although porewater pH exists only at 22 & 32cm (which would be above a WTD of ' \
                                                    '39cm below surface), it seems doubtful that a bog would be inundated ' \
                                                    'with 39cm of water above the surface. Note from Virginia Rich: ' \
                                                    '"I believe these are 39cm below. Sometimes the porewater isn’t ' \
                                                    'collected exactly where the core is taken because they tried and ' \
                                                    'the first core attempt failed so they had to move over a bit. And, ' \
                                                    'often the porewater is taken the day before, such that it might have ' \
                                                    'dried out some overnight. They noted it was full sun, hot and ' \
                                                    'windy on the coring dayt so I think that corroborates that hypothesis."'

                            elif coring_date[0:6] == '201907':

                                wtd_quality_flag += ' Note from Virginia Rich: "I think you’re probably right ' \
                                                    'about them being below, I think it would be reassuring to ' \
                                                    'see the photo to confirm no standing water. The note-taker ' \
                                                    'might have just forgotten from the day before (when they ' \
                                                    'cored the fen), or more likely, been a different notetaker." ' \
                                                    'Suzanne looked at the photos and confirmed no standing water.'

                        elif csv_df.loc[index, 'Site__'] == 'Palsa Autochamber Site':

                            if coring_date[0:6] == '201807' and csv_df.loc[index, 'Core__'] == '1':

                                wtd_quality_flag += ' However, |WTD| (=76) > |ALD| (=41), and notes for 40-44cm ' \
                                                    'sample say "hole too soggy … visible water," so not sure if ' \
                                                    'there is a typo somewhere?'

                    elif coring_date[0:6] == '201307' and csv_df.loc[index, 'Site__'] == "Eriophorum Autochamber Site" and csv_df.loc[index, 'Core__'] == '3':
                        final_wtd = -1*abs(converted_wtd)    # this core resolved to BELOW (but is only =2, so doesn't matter much if wrong)
                        wtd_quality_flag += ' For this core, the WTD was assumed to be below the surface. Note from Virginia Rich: ' \
                                            '"I think we should assume below. It was collected the day before other ' \
                                            'cores where you have deduced the positive notation must mean below. ' \
                                            'And, at 2cm, It’s near-surface regardless so it’s not much ecological ' \
                                            'difference either way."'

                    elif coring_date[0:6] in ['201707', '201807'] and csv_df.loc[index, 'Site__'] == 'Eriophorum Autochamber Site':
                        final_wtd = abs(converted_wtd)    # 2017 & 2018 fen resolved to ABOVE
                        wtd_quality_flag += (
                            ' For this core, the WTD was assumed to be above the surface because the site is a fen.')

                        if coring_date[0:6] == '201707':
                            wtd_quality_flag += ' In addition, porewater pH exists at all depths, and notes say the site was unusually wet this year.'

                        else:
                            wtd_quality_flag += ' In addition, porewater pH exists at surface depths.'

                    else:
                        if type(converted_wtd) == float or type(converted_wtd) == int:
                            final_wtd = str(converted_wtd) + " or " + str(-1*converted_wtd) + ", pending resolving of unknown sign convention"
                        else:
                            final_wtd = "+/- " + str(converted_wtd) + ", pending resolving of unknown sign convention"

                        if coring_date[0:6] == '201907' and csv_df.loc[index, 'Site__'] == 'Eriophorum Autochamber Site':
                            wtd_quality_flag += ' For these cores, the dense vegetation made it difficult to tell ' \
                                                'based on the photos whether there was standing water.'

                            if csv_df.loc[index, 'Core__'] == '1':
                                wtd_quality_flag += ' This one is likely above the surface, as |WTD|=18cm and ' \
                                                    'porewater pH exists at surface depths.'
                            else:
                                wtd_quality_flag += ' Note from Virginia Rich: "I think we should asssume these were ' \
                                                    'below, since there was likely the same notetake for the full day ' \
                                                    'and it’s clear that the -18 for core #1 that day was above. So, ' \
                                                    'these are positive #s, I think we infer just below the peat surface. ' \
                                                    'And again, since it’s nearly right at the surface, it doesn’t make ' \
                                                    'an ecological difference anyway."'

            else:
                final_wtd = np.nan

            # Assign final_wtd to a standardized column name (to be consistent with what was done in original R script)
            csv_df.loc[index, 'WTD.cm_neg_is_below_sfc__'] = final_wtd

            # Also make standardized ALD column
            if 'Active_Layer_Depth_ALD.cm' in row:
                csv_df.loc[index, 'ALD.cm__'] = convert_depth_to_numeric_cm(row['Active_Layer_Depth_ALD.cm'], abs_val=True)

            # -----------------------------
    
            # Depths
            core_depth = row['Depth_of_Core_Section.cm_below_peat_soil_surface']
            
            min_depth = 'NaN'
            max_depth = 'NaN'  # will assign these later
    
            try:
                if pd.isnull(core_depth) or ('--' in str(core_depth)):  # No data

                    try:  # Grab from porewater
                        core_depth = row['Depth_of_Porewater_Sample.cm_below_peat_soil_surface']

                        if pd.isnull(core_depth) or ('--' in str(core_depth)):  # Is porewater empty?

                            try:
                                core_depth = row['Depth_of_Temperature_Measurement.cm_below_peat_soil_surface']

                                if pd.isnull(core_depth) or ('--' in str(core_depth)):

                                    try:  # Last ditch effort, try sample name
                                        core_depth = row['Sample_Name']

                                    except KeyError:
                                        error('Unable to identify the depth for row:\n{}'.format(row))
                            except KeyError:

                                try:  # Last ditch effort, try sample name
                                    core_depth = row['Sample_Name']
                                except KeyError:
                                    error('Unable to identify the depth for row:\n{}'.format(row))

                    except KeyError:
                        error('Unable to identify the depth for row:\n{}'.format(row))

                    if type(core_depth) == str and '-' in str(core_depth):
                        components = re.findall(r"[.\w]+", core_depth)  # Make sure decimals (.) are counted within the numbers.

                        if len(components) == 2:  # start-end
                            if components[0].isdigit() and components[1].isdigit():
                                min_depth = components[0]
                                max_depth = components[1]
                                core_depth = '{}-{}'.format(min_depth, max_depth)
                            elif 'CP' in core_depth:  # CP-##
                                core_depth = components[-1]

                                if core_depth.isdigit():
                                    core_depth = components[-1]
                                else:
                                    core_depth = 'None'
                            else:
                                core_depth = 'None'
                        else:
                            core_depth = 'None'
    
                else:  # Something is there
                    if type(core_depth) == str and '-' in str(core_depth):
                        components = re.findall(r"[.\w]+", core_depth)  # Make sure decimals (.) are counted within the numbers.
    
                        if len(components) == 2:  # start-end

                            try:
                                test = float(components[0]) + float(components[1])  # See if they can be converted to floats
                                min_depth = components[0]
                                max_depth = components[1]
                                core_depth = '{}-{}'.format(min_depth, max_depth)
                            except ValueError:
                                print('not passed', components)

                        elif len(components) == 4:  # depth code, start, end, 'cm'
                            try:
                                test = float(components[1]) + float(components[2])  # See if they can be converted to floats
                                min_depth = components[1]
                                max_depth = components[2]
                                core_depth = '{}-{}'.format(min_depth, max_depth)
                            except ValueError:
                                print('not passed', components)
                        elif '(geochem)' in core_depth:
                            min_depth = components[0]
                            max_depth = components[1]
                            core_depth = '{}-{}'.format(min_depth, max_depth)
                        elif 'unlikely to get used' in core_depth:
                            min_depth = components[1]
                            max_depth = components[2]
                            core_depth = '{}-{}'.format(min_depth, max_depth)
                        else:
                            error('Unable to identify a depth from any of the data')
    
            except ValueError:
                error('Value error for {}'.format(core_depth))
    
            if core_depth == 'surface' or core_depth == 'overlying water' or core_depth == 'standing water':
                core_depth = 'above peat'
    
            csv_df.loc[index, 'Depth__'] = core_depth
            csv_df.loc[index, 'DepthMin__'] = min_depth
            csv_df.loc[index, 'DepthMax__'] = max_depth

            if csv_df.loc[index, 'Depth__'] == 'None':  # "None" depths will be deleted anyway
                continue  # this continues to the next row (thus preventing further errors with this one)

            # Make a DepthAvg__ column, which is always a single number
            csv_df.loc[index, 'DepthAvg__'] = 'NaN'  # set it to NaN first, just in case it's never assigned later

            # First try to convert all the depth values to floats, and change to NaN if this isn't possible.
            # This avoids having to check for both NaN *and* non-numeric depths separately when determining DepthAvg__.
            try:
                core_depth_num = float(core_depth)
            except ValueError:
                core_depth_num = 'NaN'

            try:
                min_depth_num = float(min_depth)
                max_depth_num = float(max_depth)
            except ValueError:
                min_depth_num = 'NaN'
                max_depth_num = 'NaN'

            # Now calculate a numeric DepthAvg__ if possible
            if not (pd.isnull(core_depth_num) or core_depth_num == 'NaN'):
                csv_df.loc[index, 'DepthAvg__'] = core_depth_num
            elif not (pd.isnull(min_depth_num) or min_depth_num == 'NaN'):  # check if min_depth_num is NaN; assume max_depth_num is the same
                csv_df.loc[index, 'DepthAvg__'] = (min_depth_num + max_depth_num) / 2  # average of min & max
            else:  # NONE of them are numeric values
                if core_depth == 'above peat':
                    # if the original core_depth is "above peat," try to get numeric using midpoint between surface and
                    # WTD (if the WTD is above the surface)
                    try:
                        if float(final_wtd) > 0:
                            csv_df.loc[index, 'DepthAvg__'] = -0.5 * float(final_wtd)
                        else:
                            warning('Could not determine DepthAvg__ for "above peat" depth based on WTD, as the WTD is below the surface.')

                    except ValueError:  # non-numeric WTD?
                        warning('WTD is non-numeric; cannot use it to determine numeric value for "above peat" sample depth: \n{}'.format(row))

                else:  # not an "above peat" or a "None" depth; therefore something else is weird
                    error('Could not determine DepthAvg__ from the available data; defaulting to NaN: \n{}'.format(row))

            if 'QualityFlag__' in csv_df.columns:
                quality_flag = csv_df.loc[index, 'QualityFlag__']  # if it's already there, store it so it isn't lost
                if pd.isnull(quality_flag) or quality_flag == 'NaN':
                    quality_flag = ''
            else:
                quality_flag = ''
            
            # correct one mislabeled row -- some of these checks are redundant, but don't want to change the wrong thing!
            if (csv_df.loc[index, 'Site__'] == 'Fen1' and porewater_date == '20110614' and 
                csv_df.loc[index, 'Depth__'] == 'above peat' and csv_df.loc[index, 'GPS__'] == 'N 68 21.277, E 19 02.911'):
                
                csv_df.loc[index, 'Site__'] = 'Fen2'
                quality_flag += 'Corrected site from "Fen1" to "Fen2". '

            # correct a bad GPS (corrected GPS is from biogeochemistry sheet)
            if (csv_df.loc[index, 'Site__'] == 'Fen2' and porewater_date == '20110614' and
                    csv_df.loc[index, 'GPS__'] == 'N 68 21.277, E 19 02.911'):
                csv_df.loc[index, 'GPS__'] = 'N 68 21.2748, E 19 02.9730'
                quality_flag += 'Corrected GPS from "N 68 21.277, E 19 02.911" to "N 68 21.2748, E 19 02.9730" based on biogeochemistry data.'

            # add WTD quality flags (if any)
            if wtd_quality_flag != '':
                if quality_flag == '':
                    quality_flag = wtd_quality_flag
                else:
                    quality_flag += (' ' + wtd_quality_flag)

            # assign final quality flag
            if quality_flag != '':
                csv_df.loc[index, 'QualityFlag__'] = quality_flag
    
            # Depth subsamples (samples from the same core & numerical depth that should be counted as separate Depth nodes
            subdepth = 'NaN'
            # if core_num == 'Time-Series':
            #     subdepth = date_parse(row['Time_coring_begun']).strftime('%H:%M')
            
            csv_df.loc[index, 'DepthSubsample__'] = subdepth
            
            # Data subsamples (sim. to depth subsamples, but if instead wanting separate Data nodes within same Depth node; e.g. some Aug. 2012 geochem)
            # In coring sheets, this is used for cases where peat and porewater info for the same depth is in different rows
            subdata = 'NaN'
            if csv_df.loc[index, 'Site__'] == 'Fen1' and '201106' in porewater_date:
                if 'porewater' in csv_df.loc[index, 'Samples_Taken']:
                    subdata = 'porewater'
                else:
                    subdata = 'peat'
                    
            csv_df.loc[index, 'DataSubsample__'] = subdata


            # Other standardized columns originally created when making Sample Metadata Sheet:
            # Code below copied from make_metadata_sheet.R, then translated into Python and adapted to this script.
            # Was going to also copy the SampleID__ generation to Standardize_Chemistry_Sheets.py, but decided against
            #  it because this would create a lot of conflicts for not-quite-matching depths (for which the coring
            #  sheet SampleID__ should be considered the "official" one).
            # NEW AS OF 2024-01-10: SampleID__ generation now in a separate function, which can be called by other scripts.
            
            # SampleID__

            sampleID_dict = None  # reset in case the mutable 'dict' type causes issues

            sampleID_dict = dict(csv_df.loc[index, ['Site__', 'Core__',
                                                    'DepthMin__', 'DepthMax__', 'DepthAvg__', 'DepthSubsample__']])
            sampleID_dict['Date__'] = formatDate(coring_date)

            # Should probably make sure this won't cause issues due to dicts being mutable, but it's probably OK
            #  since the function only adds new keys (& doesn't modify existing ones)...
            sampleID_dict = generate_official_SampleID(sampleID_dict)

            csv_df.loc[index, 'Month__'] = sampleID_dict['Month__']
            csv_df.loc[index, 'Year__'] = sampleID_dict['Year__']
            csv_df.loc[index, 'CoreGroup__'] = sampleID_dict['CoreGroup__']
            csv_df.loc[index, 'coreID_tmp'] = sampleID_dict['coreID_tmp']
            csv_df.loc[index, 'SampleID__'] = sampleID_dict['SampleID__']

    
        # Suzanne's new method for dates: Consolidate dates that "go together" to make summarized DateStart__ and DateEnd__ columns
        date_ranges = get_date_ranges(csv_df[['DateC_tmp', 'DateP_tmp', 'DateOther_tmp']])
        for index, row in csv_df.iterrows():
            date_int = int(row['DateC_tmp'])
            date_start = np.nan
            for date_range in date_ranges:
                if date_int >= date_range[0] and date_int <= date_range[1]:
                    date_start = date_range[0]
                    date_end = date_range[1]
                    break
    
            if np.isnan(date_start):
                error('Date out of range: ', str(date_int))
            else:
                date_start = str(date_start)
                date_end = str(date_end)
                csv_df.loc[index, 'DateStart__'] = formatDate(date_start)
                csv_df.loc[index, 'DateEnd__'] = formatDate(date_end)
                csv_df.loc[index, 'FieldSampling__'] = formatDate(date_start[0:len(date_start)-2])  # month and year only
                
        # Drop 'None' depths
        csv_df = csv_df[csv_df.Depth__ != 'None']

        # Fill in non-depth-specific data throughout each whole core (for things like air T that were sometimes only filled in for the first depth)
        # Code below copied from make_metadata_sheet.R, then translated into Python and adapted to this script.
        non_depth_specific_cols = ['ALD.cm__',
                                   'WTD.cm_neg_is_below_sfc__',
                                   'WTD_above_or_below_surface',
                                   'T_air.deg_C',
                                   'Weather_Conditions_on_Day_of_Coring',
                                   'Core_Length.cm',
                                   'Distance_to_Chamber_and_Which_Chamber',
                                   'Distance_to_Chamber.cm']

        non_depth_specific_cols = [col for col in non_depth_specific_cols if col in csv_df.columns]

        for coreID, core_df in csv_df.groupby(by='coreID_tmp'):
            for col in non_depth_specific_cols:

                values_for_core = core_df[col].replace(to_replace='Not Measured', value=np.nan, inplace=False)

                if any(pd.isnull(values_for_core)) and not(all(pd.isnull(values_for_core))):
                    unique_values = list(set([val for val in values_for_core if not(pd.isnull(val))]))
                    unique_values.sort()

                    # take care of a special case of weather in 2 rows (also with a misspelling)
                    if unique_values == ['breif morning rain', 'very cloudy']:
                        unique_values = ['brief morning rain, very cloudy']

                    if (len(unique_values) > 1):
                        print(unique_values)
                        error("Multiple unique '{}' values for core '{}' (see above)".format(col, coreID))
                    else:
                        csv_df.loc[csv_df.coreID_tmp == coreID, col] = unique_values[0]

    
        # Drop temporary columns
        csv_df.drop(columns=['DateC_tmp', 'DateP_tmp', 'DateOther_tmp', 'coreID_tmp'], inplace=True)
    
        # Remnant from when aggregating all Cores columns to best consolidate "duplicates"
        if len(csv_df.columns) > len(set(csv_df.columns)):
            error('Duplicate columns identified in {}. Full list of columns: {}'.format(data_csv, csv_df.columns))
    
        # Set the csv export filename by putting "_standardized" at the end of the original filename.
        updated_csv = data_csv.replace('.csv', '_standardized.csv')
        
        # Export to csv.
        csv_df.to_csv(updated_csv, index=False, quotechar='"', sep=',')
    
        data_df_list.append(csv_df)

