#!/usr/bin/env python
"""
OVERVIEW:
From excel-formatted (*.XLS, *.XLSX), manually-curated and triple-verified files were converted into CSV using Excel's
export functionality. These CSVs are then read in, and then data headers are "re-mapped" to standardized nomenclature.
These headers are then saved to a new CSV so that future work will no longer need to go through this process. This new
"standardized" CSV is then used for importing the data into the database.

Because this standardization is done once at this stage, the actual import process is faster, and the same import
scripts can also be reused for other standardized data sheets.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""



import os
import sys
import re
import numpy as np
import pandas as pd
import argparse
#import LatLon  # doesn't work in Python 3
from dateutil.parser import parse as date_parse
#from natsort import natsorted

from pprint import pprint

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
from local_file_paths import *
from tools.data.common_vars import *
from tools.utilities.standardize.Standardize_Column_Info import standardize_column_info


parser = argparse.ArgumentParser(description="Standardize coring sheets")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_file', default='default',
                    help="File path of geochemistry sheet CSV to standardize. "
                         "If left blank, will loop through files ending in '_BB.csv' in source_chem_dir. "
                         "If specifying a specific file, need to provide the full file path from whichever "
                         "directory this script is being run.")

inputs.add_argument('-f', '--fieldsampling', default=None,
                    help="Custom FieldSampling__ to use if dates are missing from the input file but are "
                         "otherwise known (e.g. from publication methods). This should equal or begin with a date "
                         "string in the form YYYY-MM, with additional text allowed at the end to separate from the "
                         "'main' field sampling from the same month & year (e.g. from side-projects)."
                         "Note that this field is mandatory if the file doesn't contain specific dates.")

inputs.add_argument('-d', '--date', default=None,
                    help="Custom Date__ to use if dates are missing from the input file but are "
                         "otherwise known (e.g. from publication methods). This should be in the form YYYY-MM-DD, and "
                         "is distinct from --fieldsampling in that it refers to a specific date rather than a general "
                         "field sampling identifier. "
                         "Both --date and --fieldsampling should only be used if the value is the same for all "
                         "rows in the file; otherwise the dates should be added manually during formatting.")

inputs.add_argument('-ss', '--site-syn-alt', default='default',
                    help="Alternative set of site synonyms (named subcategory in site_syn_extras.json) "
                         "to use instead of the default (site_syn.json). "
                         "Useful for importing data collected from an alternate set of sites "
                         "which are named identically to the 'main' set of sites (e.g., called "
                         "'Sphagnum' but not from the main Sphagnum Autochamber Site; this "
                         "practice is discouraged, but sometimes it still happens).")

inputs.add_argument('-cs', '--column-syn-alt', default='default',
                    help="Alternative set of column synonyms (named subcategory in column_syn_geochem_groups.json) "
                         "to use *in addition to* the default category (which includes all synonyms with known units). "
                         "Useful for importing data with units that aren't specified in the column header itself, "
                         "but are otherwise known; and then adding these units to the headers. "
                         "While column_syn.json is reserved for importing field sheet data, "
                         "column_syn_geochem_groups.json can be used to import from a variety of other data sources."
                         "For backward compatibility, no columns are renamed if input_file='default' or contains the "
                         "string 'errors_'.")

inputs.add_argument('--rename-columns-inplace', action='store_true',
                    help="Allow data column headers to be renamed inplace with their 'official' names from "
                         "column_syn_geochem_groups.json. If not specified, all new standardized columns will "
                         "be copied into new columns, retaining the original columns with their original headers, "
                         "even if the values are all unchanged. \n"
                         "Regardless of this setting, files containing measurement errors (these should have filenames "
                         "starting with 'errors_') will always have '_err' appended to their headers inplace, and any "
                         "standardizations that may involve changes to values (e.g. site names) will still go into "
                         "separate columns.")

inputs.add_argument('-cf', '--column-file', dest='column_info_file', default=None,
                    help="File path of an existing column info file to standardize. It should already include headers "
                         "with names standardized for Column nodes in the graph DB, with the current data column "
                         "names (with any typos corrected to match the data input file) under 'OriginalColumnName__'. "
                         "This file will be entered as the existing_column_csv argument in standardize_column_info(), "
                         "(see Standardize_Column_Info.py for details), and will be updated to include info on any "
                         "additional whole data column standardization performed using the current script.")

args = parser.parse_args()

def error(message):
    sys.stdout.write('ERROR: {}\nExiting...\n'.format(message))
    sys.exit(1)

if __name__ == "__main__":
    #site_info_df = pd.DataFrame.from_dict(site_info, orient='index')  # defined but not used here?

    # Gather all the CSVs
    chemistry_file = args.input_file
    chemistry_csvs = []

    if chemistry_file == 'default':
        # TODO: Maybe phase this out, to avoid accidentally updating too many files at once?
        source_dir = source_chem_dir  # from local_file_paths.py
        for root, dirs, files in os.walk(source_dir):
            for fname in files:
                if '_BB.csv' in fname:
                    chemistry_csvs.append(os.path.join(root, fname))

        # In addition, also turn off ALL whole-data-column standardization (essential sample metadata will still be
        #  standardized). This prevents unintentional changes to the files that were previously standardized using
        #  this 'default' batch setting.
        # As of now (May 4 2023), this also prevents the automatic creation of standardized column info files, as
        #  manually-generated files already exist under very similar names.
        standardize_whole_data_columns = False

    else:
        chemistry_csvs.append(chemistry_file)
        standardize_whole_data_columns = True

    # Import / update column_syn_geochem
    column_syn_alt = args.column_syn_alt

    # always include the default synonym set
    with open('tools/data/column_syn_geochem_groups.json') as column_syn_geochem_file:
        column_syn_geochem_all = json.load(column_syn_geochem_file)
        column_syn_geochem = column_syn_geochem_all['default']

    # add to the default synonyms if a specific group was specified
    if column_syn_alt != "default":
        column_syn_geochem_alt = column_syn_geochem_all[column_syn_alt]
        for key, value in column_syn_geochem_alt.items():
            if key in column_syn_geochem.keys():
                column_syn_geochem[key] += column_syn_geochem_alt[key]  # append to existing
            else:
                column_syn_geochem[key] = column_syn_geochem_alt[key]  # add

    # Update site_syn if an alternate version was specified
    #  (otherwise, defaults to the one loaded from site_syn.json in common_vars)
    site_syn_alt = args.site_syn_alt
    if site_syn_alt != "default":
        with open('tools/data/site_syn_extras.json') as site_syn_extras_file:
            site_syn_extras = json.load(site_syn_extras_file)
            site_syn = site_syn_extras[site_syn_alt]


    # Loop through the CSVs
    for chemistry_csv in chemistry_csvs:
        print(chemistry_csv)

        chemistry_df = pd.read_csv(chemistry_csv, sep=',', quotechar='"')

        # Drop empty columns (byproduct of Excel's "save as csv") for some sheets
        chemistry_df.dropna(axis='columns', how='all', inplace=True)

        # Drop empty rows
        chemistry_df.dropna(axis='index', how='all', inplace=True)

        # Rename columns and save renaming info for Column nodes ----
        # TODO: Decide whether to create duplicates with new name (vs. just renaming, as is done below)?
        #  The original intention of the "__" headers was that they would duplicate (not replace) the
        #  original ones; but this wasn't always the case historically, and it's only strictly
        #  necessary if the values under the headers are changed.
        #  Currently this behavior is user-specified, unless overridden for backward compatibility with matched Data &
        #  Errors (see below).

        # First control for the "default" file set, which contains separate main data and errors files (so far this
        #  is the only set of files like this)
        chemistry_fn = os.path.basename(chemistry_csv)
        if (chemistry_fn.startswith("errors_") or
            chemistry_fn.startswith("porewater_updated") or
            chemistry_fn.startswith("poregas_updated") or
            chemistry_fn.startswith("peat_updated")) \
                and chemistry_fn.endswith("_BB.csv"):  # lots of redundant checks, just to be safe
            if (args.rename_columns_inplace):
                warning("Overriding user-specified --rename-columns-inplace in order to avoid mismatches between the "
                        "Data column headers and their corresponding Errors column headers (ending in _err).")
            rename_columns_inplace = False

        else:
            rename_columns_inplace = args.rename_columns_inplace

        # start a list of dicts, each corresponding to a column header, in the form
        #   (for eventual conversion to data frame using pd.DataFrame.from_dict()):
        #
        # [{"Column__": "d13C_CH4__",
        #   "OriginalColumnName__": "d13CH4",
        #   "OriginalColumnRenamed__": True}]
        #
        # For columns standardized into "official" names, OriginalColumnRenamed__ should match the value of
        # rename_columns_inplace (otherwise it's False), and tells the end-user of the graph DB whether
        #  the column was renamed inplace (or if the original name still exists as a separate column).
        # Note that if any values are changed (as is done later in this script), the changes MUST go into a new column.
        # TODO: Maybe also add a property that says whether any values may have been changed?
        column_info_list = []
        for header in chemistry_df.columns:
            column_info_list.append({"OriginalColumnName__": header})

        # rename
        for specific_column_info in column_info_list:
            header = specific_column_info["OriginalColumnName__"]
            official_name_exists = False
            for official_header, synonyms in column_syn_geochem.items():
                if header in synonyms:
                    official_name_exists = True
                    break
            if official_name_exists and standardize_whole_data_columns:
                if rename_columns_inplace:
                    chemistry_df.rename(columns={header: official_header}, inplace=True)
                else:
                    chemistry_df[official_header] = chemistry_df[header]

                # Yes, updating specific_column_info actually updates the column_info_list item
                specific_column_info.update({"Column__": official_header,
                                             "OriginalColumnRenamed__": rename_columns_inplace})

            else:
                # Note that if standardize_whole_data_columns=False, column_info_list won't be used; but
                #  it's still updated below in case we decide to use it later, and also to cover cases where
                #  standardize_whole_data_columns=True but no official name was found for this specific column.
                specific_column_info.update({"Column__": header,
                                             "OriginalColumnRenamed__": False})

        # Fill in user-specified date and field sampling (if applicable)
        if args.fieldsampling:
            if 'FieldSampling__' not in chemistry_df.columns:
                chemistry_df['FieldSampling__'] = args.fieldsampling
                column_info_list.append({"Column__": "FieldSampling__",
                                         "OriginalColumnName__": "NaN",
                                         "OriginalColumnRenamed__": "NaN"})
            else:
                error("User-specified --fieldsampling would override an existing FieldSampling__ column. "
                      "Please rerun without this argument.")
        if args.date:
            if ('Date__' not in chemistry_df.columns) and \
                    ('DateC__' not in chemistry_df.columns) and \
                    ('DateP__' not in chemistry_df.columns):
                chemistry_df['Date__'] = args.date
                column_info_list.append({"Column__": "Date__",
                                         "OriginalColumnName__": "NaN",
                                         "OriginalColumnRenamed__": "NaN"})
            else:
                error("User-specified --date would override an existing date column (Date__, DateC__, or DateP__)."
                      "Please rerun without this argument.")

        # Check the new headers for any "mandatory" sample metadata that can be skipped when standardizing each row.
        skip_individual_fieldsamplings = False
        skip_individual_dates = False
        skip_individual_sites = False
        skip_individual_cores = False
        skip_individual_depthminmax = False
        skip_individual_depthfull = False
        skip_individual_depthavg = False

        if 'FieldSampling__' in chemistry_df.columns:
            skip_individual_fieldsamplings = True
            print("INFO: Not standardizing individual field samplings, as a FieldSampling__ column already exists. "
                  "Please check the final file to ensure correct formatting, noting that this CANNOT be blank.")

        if ('Date__' in chemistry_df.columns) or \
                ('DateC__' in chemistry_df.columns) or \
                ('DateP__' in chemistry_df.columns):
            skip_individual_dates = True
            print("INFO: Not standardizing individual dates, as a column already exists (Date__, DateC__, and/or DateP__). "
                  "Please check the final file to ensure correct formatting.")

        if 'Site__' in chemistry_df.columns:
            skip_individual_sites = True
            print("INFO: Not standardizing individual site names, as a Site__ column already exists. "
                  "Please check the final file to ensure correct formatting, noting that this CANNOT be blank.\n"
                  "(WARNING: Manually-standardized sites are particularly dangerous due to the existence of site synonyms.")

        if 'Core__' in chemistry_df.columns:
            skip_individual_cores = True
            print("INFO: Not standardizing individual core names, as a Core__ column already exists. "
                  "Please check the final file to ensure correct formatting, noting that this CANNOT be blank.")

        if ('DepthMin__' in chemistry_df.columns) and ('DepthMax__' in chemistry_df.columns):
            skip_individual_depthminmax = True
            print("INFO: Not standardizing individual DepthMin__ and DepthMax__, as these columns already exist. "
                  "Please check the final file to ensure correct formatting.")
        elif ('DepthMin__' in chemistry_df.columns) or ('DepthMax__' in chemistry_df.columns):
            error("Only one of the columns DepthMin__ and DepthMax__ exists; they must either both exist, or neither.")
        else:
            pass

        if 'Depth__' in chemistry_df.columns:
            skip_individual_depthfull = True
            print("INFO: Not standardizing individual Depth__, as this column already exists. "
                  "Please check the final file to ensure correct formatting, noting that this CANNOT be blank.")

        if 'DepthAvg__' in chemistry_df.columns:
            skip_individual_depthavg = True
            print("INFO: Not standardizing individual DepthAvg__, as this column already exists. "
                  "Please check the final file to ensure correct formatting, noting that this CANNOT be blank.")

        # TODO: Use column_info_list (in combination with contributor-provided column info) to create standardized column info files.
        #  Also save and include the info from sample metadata standardization below.

        # Iterate over rows ----
        for index, row in chemistry_df.iterrows():

            # Field sampling, Month, Year ----
            # TODO: Add standardized Month__ (numeric, based on actual sampling date), Year__, and other columns
            #   currently added to coring sheets but not chemistry sheets (e.g. CoreGroup__, SampleID__).
            #   Or maybe it's better to do this within the import script?

            if not skip_individual_fieldsamplings:
                # The code below assumes that 'Month' and 'Row' columns exist, which may not always be the case?
                try:
                    monthyear = date_parse('{} {}'.format(row['Month'], int(row['Year']))).strftime('%Y-%m')
                except ValueError:
                    error('ValueError: Could not parse month and year "{} {}" in row {}.'.format(row['Month'],
                                                                                                 int(row['Year']), index))

                # TODO: Create "special case" FieldSampling__ for same-month extra samplings (e.g. monolith cores)?
                chemistry_df.loc[index, 'FieldSampling__'] = monthyear


            # Specific Dates ----

            if not skip_individual_dates:

                if 'Date__' in row:
                    date = row['Date__']
                    datecolumn = 'Date__'
                elif 'Date_core' in row:
                    date = row['Date_core']
                    datecolumn = 'DateC__'
                elif 'Date_pw' in row:
                    date = row['Date_pw']
                    datecolumn = 'DateP__'
                elif 'Date_pg' in row:
                    date = row['Date_pg']
                    datecolumn = 'DateP__'
                else:
                    date = False
                    datecolumn = False

                if pd.isnull(date) or not datecolumn:
                    # pd.isnull(date) happens if a date column exists, but didn't contain anything
                    # datecolumn=False (and therefore date=False, per above) happens if no date column was found
                    if "FieldSampling__" in row:
                        # OK to have unknown dates as long as FieldSampling__ exists
                        date = 'NaN'
                    else:
                        print(row.to_dict())
                        error('Could not identify a date (column: {}) for row {}.\n'.format(datecolumn, index))

                if datecolumn:
                    # Put date into official format
                    if date == 'NaN':
                        adjusted_date = 'NaN'
                    else:
                        adjusted_date = date_parse(date).strftime('%Y-%m-%d')

                    # Assign final date
                    chemistry_df.loc[index, datecolumn] = adjusted_date

                else:
                    # If no datecolumn, give a warning on the first row
                    if index==0:
                        warning("No date columns found. If this is unexpected, check the file for the correct column "
                                "name and add it to the this script's conditional that assigns 'datecolumn'.")


            # Depths ----

            # DepthMin__, DepthMax__
            if not skip_individual_depthminmax:
                if 'Depth_Min.cm' in row:
                    min_depth = row['Depth_Min.cm']
                    max_depth = row['Depth_Max.cm']
                else:
                    min_depth = 'NaN'
                    max_depth = 'NaN'
                # Unlike the other Depth columns, it's OK if these are NaN.

                chemistry_df.loc[index, 'DepthMin__'] = min_depth
                chemistry_df.loc[index, 'DepthMax__'] = max_depth

            else:
                # still get the values for possible later use
                min_depth = row['DepthMin__']
                max_depth = row['DepthMax__']

            # convert any pd.isnull() values to string NaN for easier use
            if pd.isnull(min_depth):
                min_depth = 'NaN'
            if pd.isnull(max_depth):
                max_depth = 'NaN'

            # Depth__
            if not skip_individual_depthfull:

                #if 'Date_core' in row:
                if not(min_depth=='NaN') and not(max_depth=='NaN'):
                    depth = '{}-{}'.format(min_depth, max_depth)
                elif 'DepthAvg__' in row:
                    depth = row['DepthAvg__']
                elif 'Depth.cm' in row:
                    depth = row['Depth.cm']
                else:
                    error('Could not identify a depth from row {}:\n{}'.format(index, row))

                chemistry_df.loc[index, 'Depth__'] = depth

            else:
                # still get the Depth__ value for possible later use
                depth = row['Depth__']


            # DepthAvg__
            # (this is always a single number)
            if not skip_individual_depthavg:
                # set it to NaN first, just in case it's never assigned later
                chemistry_df.loc[index, 'DepthAvg__'] = 'NaN'

                # First try to convert all the depth values to floats, and change to NaN if this isn't possible.
                # This avoids having to check for both NaN *and* non-numeric depths separately when determining DepthAvg__.
                try:
                    depth_num = float(depth)
                except ValueError:
                    depth_num = 'NaN'

                try:
                    min_depth_num = float(min_depth)
                    max_depth_num = float(max_depth)
                except ValueError:
                    min_depth_num = 'NaN'
                    max_depth_num = 'NaN'

                # convert any pd.isnull() values back to string NaN for easier use
                #  (because doing float('NaN') above converts to null)
                if pd.isnull(min_depth_num):
                    min_depth_num = 'NaN'
                if pd.isnull(max_depth_num):
                    max_depth_num = 'NaN'
                if pd.isnull(depth_num):
                    depth_num = 'NaN'

                # Now calculate a numeric DepthAvg__ if possible
                if not (depth_num == 'NaN'):
                    chemistry_df.loc[index, 'DepthAvg__'] = depth_num
                elif not (min_depth_num == 'NaN') and not (max_depth_num == 'NaN'):
                    chemistry_df.loc[index, 'DepthAvg__'] = (min_depth_num + max_depth_num) / 2  # average of min & max
                else:  # NONE of them are numeric values
                    if depth == 'above peat':
                        # if the original depth is "above peat," try to get numeric using midpoint between surface and
                        # WTD (if the WTD is above the surface)
                        # *** COPIED FROM Standardize_Coring_Sheets.py; these WTD columns don't likely exist for geochem sheets ***
                        try:
                            if 'Water Table Depth (WTD, cm, neg = below peat surface)' in row:
                                wtd_value = row['Water Table Depth (WTD, cm, neg = below peat surface)']
                                if float(wtd_value) > 0:
                                    chemistry_df.loc[index, 'DepthAvg__'] = -0.5 * float(wtd_value)
                                else:
                                    warning(
                                        'Could not determine DepthAvg__ for "above peat" depth based on WTD, as the WTD is below the surface.')

                            elif 'Water Table Depth (WTD, cm, neg = ABOVE peat surface)' in row:
                                wtd_value = row['Water Table Depth (WTD, cm, neg = ABOVE peat surface)']
                                if float(wtd_value) < 0:
                                    chemistry_df.loc[index, 'DepthAvg__'] = 0.5 * float(wtd_value)
                                else:
                                    warning(
                                        'Could not determine DepthAvg__ for "above peat" depth based on WTD, as the WTD is below the surface.')

                            elif 'Water Table Depth (WTD, cm)' in row:
                                wtd_value = row['Water Table Depth (WTD, cm)']
                                # If we don't know the WTD sign convention, then just assume the WTD is above the surface.
                                # Not great, but at least the result will be a negative (above peat) depth regardless.
                                chemistry_df.loc[index, 'DepthAvg__'] = -0.5 * abs(float(wtd_value))
                            else:
                                # No WTD column; this means something else is probably wrong, so this errors out.
                                error('Could not locate a WTD row to determine a number for "above peat" sample depth.')

                        except ValueError:  # non-numeric WTD?
                            warning(
                                'WTD is non-numeric; cannot use it to determine numeric value for "above peat" sample depth: \n{}'.format(
                                    row))

                    elif chemistry_df.loc[index, 'Depth__'] == "None":  # "None" depths will be deleted anyway
                        pass

                    else:  # not an "above peat" or a "None" depth; therefore something else is weird
                        warning('Could not determine DepthAvg__ from the available data; defaulting to NaN: \n{}'.format(row))

            # Depth subsamples ----
            # (samples from the same core & numerical depth that should be counted as separate Depth nodes)
            # This is not currently relevant for anything standardized using this script, but is retained just in case.
            subdepth = 'NaN'
            # if 'E.time' in row['Name_unique']:
            #     subdepth = row['Name_unique'][-5:]

            chemistry_df.loc[index, 'DepthSubsample__'] = subdepth

            # Sites ----

            if not skip_individual_sites:
                site = False
                # Site = official designation
                # Name_s/f = Site + depth?
                # Name_unique = Site + depth range
                if 'Site' in row:
                    site = row['Site']
                elif 'site' in row:
                    site = row['site']
                else:
                    error('Could not identify Site')

                # change site name to "official" name
                name_changed = potential_site = False
                for site_name, variants in site_syn.items():
                    if site in variants:
                        potential_site = site_name
                        name_changed = True
                        break

                if not name_changed:
                    error('Could not determine site from {}'.format(site))

                chemistry_df.loc[index, 'Site__'] = potential_site

            # Data subsamples ----
            # (sim. to depth subsamples, but if instead wanting separate Data nodes within same Depth node; e.g. some Aug. 2012 geochem)
            # Note: There is some code in the Cores section below that reassigns this for a specific special case, before it's
            #  actually added to chemistry_df.
            subdata = 'NaN'

            # Cores ----
            if not skip_individual_cores:

                core = False

                if 'Rep' in row:
                    if ((row['Site'] == 'PHS' or row['Site'] == 'Fen1') and (row['Year'] == 2012 or row['Year'] == '2012') and row['Month'] == 'August'):
                    # this long conditional takes care of a few weird porewater replicates in August 2012
                        core = 1
                        # Now we assign the subdata
                        if not (row['Name_unique'][-1].isdigit()):
                            subdata = row['Name_unique'][-1]  # this letter should match the letter of alphabet corresponding w/ Rep
                    else:
                        core = row['Rep']
                else:
                    error('Could not identify Core/Rep')

                # Apparently, if not splitting by '.', then ALL Core__ values for errors_poregas end up being floats.
                # So change back, except for those particular cases.

                if 'E.time' in row['Name_unique']:
                    chemistry_df.loc[index, 'Core__'] = 'TimeSeries' + row['Name_unique'][-5:].replace(':', '')
                elif isinstance(core, float):
                    if core % int(core) == 0:  # if core can be changed to an int without loss of information, do this
                        chemistry_df.loc[index, 'Core__'] = str(int(core))
                    else:
                        chemistry_df.loc[index, 'Core__'] = str(core)
                else:
                    chemistry_df.loc[index, 'Core__'] = str(core)

            # Data subsamples (cont'd; see note above) ----
            chemistry_df.loc[index, 'DataSubsample__'] = subdata

            # GPS ----
            gps = False
            if 'GPS' in row:
                gps = row['GPS']
                if gps == False:
                    gps = 'NaN'

            if gps:
                # Standardize GPS format

                # Old: Using LatLon (dosen't work in Python 3)

                # New: Using strings only
                standard_gps = standardize_GPS(row['GPS'])
                chemistry_df.loc[index, 'GPS__'] = standard_gps

            else:
                # error('Could not identify GPS')
                if index == 0:
                    warning("No GPS column(s) found. If this is unexpected, check the file for the correct column "
                            "name and add it to the this script's conditional that assigns 'gps'.")


        # End of row-by-row loop

        # Put "_err" at the end of column names if it's a table of errors
        # Doing this last, as it also renames the original metadata columns (e.g. Site, GPS, Date_pg, etc.)
        if chemistry_fn.startswith('errors_'):
            err_headers = {header: '{}_err'.format(header) for header in chemistry_df.columns if '__' not in header}
            chemistry_df.rename(columns=err_headers, inplace=True)

        # Output to new csv
        updated_csv = chemistry_csv.replace('.csv', '_standardized.csv')
        chemistry_df.to_csv(updated_csv, index=False, quotechar='"', sep=',')

        # Standardize the column info CSV
        if standardize_whole_data_columns:
            standardize_column_info(data_standardized_column_list=column_info_list,
                                    existing_column_csv=args.column_info_file,
                                    data_csv=updated_csv)
