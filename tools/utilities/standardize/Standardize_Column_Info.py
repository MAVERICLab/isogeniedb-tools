#!/usr/bin/env python
"""
OVERVIEW:
This script reads in a contributor-submitted & pre-formatted column definitions CSV, combines it with additional
 column info generated during standardization of the corresponding data file, and outputs a standardized column info
 CSV that's ready to import along with the standardized data CSV.
This is done via a function (rather than direct command-line inputs) to allow direct input of the additional column info
 in dict format, without having to save it as an intermediate file.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""



import os
import sys
import numpy as np
import pandas as pd
import argparse

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
from local_file_paths import *
from tools.data.common_vars import *

def standardize_column_info(data_standardized_column_list, existing_column_csv=None, data_csv=None):
    # data_standardized_column_list:    List of dicts from data standardization script. Should be in the form:
    #                                       [{"Column__": "d13C_CH4__",
    #                                         "OriginalColumnName__": "d13CH4",
    #                                         "OriginalColumnRenamed__": True}]
    # existing_column_csv:              Existing column defs CSV (if one exists).
    #                                       This should have the headers already renamed to their DB-standardized names.
    #                                       User-contributed data column names should be under OriginalColumnName__,
    #                                       with all of these also existing in data_standardized_column_list.
    # data_csv:                         Data file CSV, to be used for generating a filename for the standardized column
    #                                       defs CSV (if an existing_column_csv wasn't provided).
    #                                       Ideally, this should be the *standardized* data CSV.

    # For the purpose of this function, "column" refers to a column header from the data file (each of which will have
    #  a *row* in the final column defs CSV), while "header" refers to a header from the column defs CSV itself.

    # convert data_standardized_column_list to a dataframe (to be expanded into the final dataframe for export)
    standardized_column_df = pd.DataFrame.from_dict(data_standardized_column_list)

    # make sure the headers are as expected
    # TODO: May need to expand this later
    expected_headers_std_col_list = ['Column__', 'OriginalColumnName__', 'OriginalColumnRenamed__']
    expected_headers_std_col_list.sort()
    actual_headers_std_col_list = list(standardized_column_df.columns)
    actual_headers_std_col_list.sort()
    if expected_headers_std_col_list != actual_headers_std_col_list:
        warning("Unexpected headers in column info from data standardization script (see below):")
        print("Actual: {}".format(actual_headers_std_col_list))
        print("Expected: {}".format(expected_headers_std_col_list))

    # then process the existing column defs (if applicable)
    if existing_column_csv is not None:
        existing_column_df = pd.read_csv(existing_column_csv, sep=',', na_filter=False, dtype=object)

        # make sure there aren't any extra column defs that aren't accounted for in the dataset
        original_names_from_csv = list(existing_column_df['OriginalColumnName__'])
        original_names_from_data = list(standardized_column_df['OriginalColumnName__'])
        bad_names = [item for item in original_names_from_csv if item not in original_names_from_data]
        if len(bad_names) > 0:
            print(bad_names)
            error("The above data column names from the existing column defs CSV do not exist in the actual dataset.")

        # check for any potentially-conflicting headers
        actual_headers_defs_csv = list(existing_column_df.columns)
        actual_headers_defs_csv.sort()
        common_headers = [header for header in actual_headers_defs_csv if header in actual_headers_std_col_list and header != 'OriginalColumnName__']
        if len(common_headers) > 0:
            print(common_headers)
            error("The above headers exist in both the existing column defs CSV and in the column info from the data standardization script, and may create conflicts when merging.")

        # merge the dataframes
        # TODO: Make sure the result is as expected
        standardized_column_df = pd.merge(standardized_column_df, existing_column_df, how="outer", on=["OriginalColumnName__"])

        # if Column__ is empty or NaN, replace it with the value from OriginalColumnName__
        for index, row in standardized_column_df.iterrows():
            if pd.isnull(row['Column__']) or row['Column__'] in ['NaN', '']:
                standardized_column_df.loc[index, 'Column__'] = row['OriginalColumnName__']
                standardized_column_df.loc[index, 'OriginalColumnRenamed__'] = False
            else:
                pass

        # generate filepath for export
        standardized_column_csv = existing_column_csv.replace('.csv', '_standardized.csv')

    else:
        if data_csv is not None:
            # generate filepath for export
            # (just add "variables_" to the beginning of the data_csv filename)
            standardized_column_csv = os.path.join(os.path.split(data_csv)[0], "variables_" + os.path.basename(data_csv))

        else:
            error("Must provide either an existing column defs CSV to standardize, or the name of the corresponding "
                  "data file for naming a new column defs CSV.")

    # export to final standardized column info CSV
    print(f"Saving standardized column info to {standardized_column_csv}...")
    standardized_column_df.to_csv(standardized_column_csv, index=False, quotechar='"', sep=',')
