#!/usr/bin/env python
"""
OVERVIEW:
This script standardizes Stordalen Mire temperature logger data by cleaning up the filenames, verifying that the
  file contents match the filenames, adding the missing "hour" column, and outputting a summary table with
  standardization results (plus the standardized files, if specified).


This script was created by Dr. Suzanne Hodgkins, with support from the NSF-funded EMERGE Biology Integration Institute.
Copyright © 2024 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""



import os
import sys
import pandas as pd
import argparse

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
from tools.utilities.utils import error, warning

parser = argparse.ArgumentParser(description="Standardize Stordalen temperature logger data.")

inputs = parser.add_argument_group('Inputs')


inputs.add_argument('--source-files-dir', default='test_data/IsoGenie_datasets/private_data/T_loggers_installed2021/originals/',
                    help="Directory containing temperature logger CSVs to standardize. "
                         "This directory should not contain any other CSVs.")

inputs.add_argument('--write-standardized-files', action="store_true",
                    help="Standardize the data files and write the output into a 'standardized' directory "
                         "(in the same root, and with the same subdirectory structure, as source_files_dir). "
                         "To allow QC-ing the input files before they are standardized, this option is turned off by default. "
                         "In addition, to avoid accidentally overwriting old data, this option only works if the "
                         "'standardized' directory doesn't exist yet. ")

args = parser.parse_args()

if __name__ == "__main__":

    # List the input CSVs
    data_csvs = []
    for root, dirs, files in os.walk(args.source_files_dir):
        for fname in files:
            if fname.endswith(".csv"):
                data_csvs.append(os.path.join(root, fname))


    # initialize dataframe for QC-ing
    source_files_summary = pd.DataFrame(data = {"filepath_full": data_csvs,
                                                "foldername": [filepath.replace(args.source_files_dir, "").replace(os.path.basename(filepath), "") for filepath in data_csvs],
                                                "filename": [os.path.basename(filepath) for filepath in data_csvs],
                                                "logger_name_from_file": "NaN",
                                                "start_date": "NaN",
                                                "end_date": "NaN"})

    # Define the output directories
    source_root_dir = os.path.abspath(os.path.join(args.source_files_dir, ".."))

    standardized_output_dir = os.path.join(source_root_dir, "standardized")
    if args.write_standardized_files:
        write_standardized_files = True
        try:
            os.makedirs(standardized_output_dir)
            
            # standardized_output_subdirs = [os.path.join(standardized_output_dir, subdir) for subdir in source_files_summary['foldername'].unique().tolist()]
            # # TODO: Maybe omit these subfolders, and instead just create the main "standardized" subfolder and rename the standardized files with their actual date ranges?
            # for dir in standardized_output_subdirs:
            #     os.makedirs(dir)                
            
        except OSError as oserror:
            print("ERROR: Output directory(s) could not be created because they already exist (see error message below). "
                  "No standardized files will be written.")
            print(oserror)
            write_standardized_files = False
    else:
        write_standardized_files = False

    # go through and check each file
    for index, row in source_files_summary.iterrows():

        print("Processing " + row["foldername"] + row["filename"])

        data_df = pd.read_csv(row["filepath_full"], sep=',', quotechar='"', na_filter=False, dtype=object)

        # one folder has files with an extra space, so need to remove spaces first before obtaining the logger name
        # this new_filename will also be used to export the final standardized files (after appending the final date range)
        new_filename = row["filename"].replace(" ", "")

        # make sure the logger name matches
        logger_name_from_filename = new_filename.replace("_FinalHourlyAverages.csv", "")
        logger_names_from_file_contents = data_df["loc_name"].unique().tolist()
        logger_names_OK = False
        if len(logger_names_from_file_contents) == 1:
            if logger_names_from_file_contents[0] == logger_name_from_filename:
                logger_names_OK = True
                source_files_summary.loc[index, "logger_name_from_file"] = "OK"

        if not(logger_names_OK):
            source_files_summary.loc[index, "logger_name_from_file"] = "; ".join(logger_names_from_file_contents)

        # get the date ranges from the actual file
        # the timeseries restarts at each new depth, so need to sort before getting min & max
        dates_sorted = data_df['ym'].tolist()
        dates_sorted.sort()
        source_files_summary.loc[index, "start_date"] = dates_sorted[0]
        source_files_summary.loc[index, "end_date"] = dates_sorted[-1]

        # shortened start and end dates for output filenames
        startdate_short = dates_sorted[0].replace("-", "")[2:8]
        enddate_short = dates_sorted[-1].replace("-", "")[2:8]

        # Append the actual date range to new_filename
        new_filename = new_filename.replace(".csv", "_" + startdate_short + "-" + enddate_short + ".csv")

        # If standardizing the data files: 
        # Make a separate hour column
        if write_standardized_files:

            # Skip this step if it's in the "220825 through 221219" folder
            # TODO: Need to confirm
            if row["foldername"] == "220825 through 221219/":
                continue

            # Iterate through data_df rows:
            # Make sure the hr_id has valid hour, then copy to separate column
            for hr_index, hr_row in data_df.iterrows():
                hr_id_components = hr_row["hr_id"].split(" ")
                bad_hr = False
                if len(hr_id_components) == 5:
                    hour = hr_id_components[2]
                    if int(hour) == float(hour) and 0 <= int(hour) <= 23:  # makes sure it's parseable
                        data_df.loc[hr_index, "hour"] = int(hour)
                        # tried to ensure int format by doing this conversion, but it still saved with ".0"?
                        # TODO for next version: Try changing the above line to save the original "hour" variable, and
                        #  see if that preserves the int-like hour format without decimals.
                    else:
                        bad_hr = True
                else:
                    bad_hr = True
                if bad_hr:
                    error(f'Invalid hr_id: "{hr_row["hr_id"]}"')
                        
            # output into the new "standardized" folder
            standardized_output_fp = os.path.join(standardized_output_dir, new_filename)
            data_df.to_csv(standardized_output_fp, index=False, quotechar='"', sep=',')



    # Clean up output summary and export
    # Note: Unlike the standardized data files, this WILL overwrite any summary files of the same name
    source_files_summary_fp = os.path.join(source_root_dir, "T-logger_originals_summary.csv")
    source_files_summary.to_csv(source_files_summary_fp, index=False, quotechar='"', sep=',')






