#!/usr/bin/env python
"""
OVERVIEW:
Main script for standardizing and combining sequencing metadata files for import into the database.
Each metadata file type (from JGI spReport files, NCBI metadata from Kingfisher, etc.) is standardized in its own
  separate script; this script calls those other scripts, and then merges the metadata from different files into a
  combined standardized file (using methods appropriate to the specific input files).


This script was created by Dr. Suzanne Hodgkins and Dr. Ben Bolduc, with support from the DOE-funded IsoGenie Project,
 NASA-funded A2A Project, and NSF-funded EMERGE Project. Copyright © 2023 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""



import os
import sys
import re
import pandas as pd
import argparse

# import local_file_paths & common_vars, which are in the root isogenie-tools directory
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))

# import utility functions
from tools.utilities.utils import error, warning, lookup_id_from_df

# import file-specific standardization functions
# TODO: Maybe better not to keep these separate? Or maybe still do, since each type of sheet has its own complexities?
from tools.utilities.standardize.specific_filetypes.Standardize_JGI_spReport import standardize_jgi_spReport

# suppress SettingWithCopyWarning
pd.options.mode.chained_assignment = None

parser = argparse.ArgumentParser(description="Standardize sequencing metadata")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('--mode', dest='mode', choices=['JGI_spReport', 'NCBI_Kingfisher'],
                    default='default',
                    help="Type of input metadata to standardize.")
# TODO: (2024-02-13) Maybe move towards standardizing *all* of them and then combining, instead of forcing one mode?

inputs.add_argument('--seq-metadata', default='default',
                    help="File containing the main sequencing metadata to standardize (incl. sample IDs, accessions, "
                         "and sequencing methods).")

inputs.add_argument('--seq-filelist', default=None,
                    help="Optional additional file containing a list of specific fastq filenames (& related info) to "
                         "combine with info from the main --seq-metadata file. This differs depending "
                         "on the mode; e.g., for mode='JGI_spReport', --seq-metadata would be the spReport file, while "
                         "--seq-filelist be a list of fastq files outputted by the idm-team scripts.")

# (the arguments below may not be necessary, as they're probably constant based on the filetype and can be specified
#   within each filetype's own script; but keeping them below just in case)

# inputs.add_argument('--delim-metadata', default='default', choices=['c', 't', 's'],
#                     help="Field delimiter used by the --seq-metadata file (c=comma, t=tab, or s=semicolon). \n"
#                          "Default: Determine from filename extension.")
#
# inputs.add_argument('--delim-filelist', default='default', choices=['c', 't', 's'],
#                     help="Field delimiter used by the --seq-filelist file (c=comma, t=tab, or s=semicolon). \n"
#                          "Default: Determine from filename extension, or ignore if no --seq-filelist.")

args = parser.parse_args()

def only_empty_or_na(column):
    # utility for identifying columns that contain only "" or "not applicable" values
    unique_values = column.unique()
    return set(unique_values) <= {"", "not applicable"}

if __name__ == "__main__":

    # standardize the main sheet
    if args.mode == 'JGI_spReport':
        # Not usually used on its own from this script.
        # TODO: Add other arguments if deciding to use this mode
        jgi_spReport = standardize_jgi_spReport(args.seq_metadata)

    elif args.mode == 'default':  # Usual usage - TO BE MADE INTO A COMPREHENSIVE SET OF STANDARDIZATONS

        # Define filepaths of input metadata ----

        # Sequencing metadata from external repositories (these will also each receive official Metadata nodes):
        # TODO: Update each file below to the latest version.
        #  Updated versions will go in: test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists
        #
        # TODO: Also add JGI Analysis Projects -- Specifically we need the ITS and GOLD Analysis Project IDs, and the
        #  IMG "taxon_oid" accessions, matched to source Sequencing Project accession(s). This info should be
        #  straightforwardly obtainable from IMG search results, or possibly GOLD search results (which might be easier,
        #  but need to check them for completeness of this metadata).
        #  Analysis projects don't match 1:1 with sequencing runs, but this info will still be needed in order to look
        #  up the source sample info from IMG accessions (and similar tasks).
        # ----------
        # JGI:
        # The Sample Report (spReport) below can be downloaded from the JGI Genome Portal, and contains the most comprehensive run-specific information for raw sequencing reads prior to public deposition in NCBI.
        jgi_spReport_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/spReport_AllMyProjects_20250128.csv"

        # The file below was generated using the script at (private to EMERGE project members) https://github.com/emerge-bii/idm-team/blob/main/jgi_interfacing/bin/list_jgi_data.py,
        #  using the default run mode, which retrieves a listing of raw sequence files that can be downloaded from the JGI Genome Portal.
        #  That script uses an API to download an XML containing info on the downloadable files, then formats this info
        #  into a TSV containing one row per sequencing run.
        #  This info includes file and folder names, file md5sums, rta versions, and library and sequencing project IDs,
        #  which are useful for deciphering the identity of files downloaded directly from JGI.
        jgi_api_filelist_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/raw_sequence_files_20250127.tsv"  # TODO

        # The GOLD sequencing project list, below, only contains information at the Sequencing Project level (i.e.,
        #  different runs are not separated, even if they were done on different machines). However, this info is still
        #  useful for retrieving the GOLD sequencing project IDs (Gp*******), which JGI uses for the NCBI BioSample names.
        #  This file is created using GOLD's search result export system, exporting all columns (even if empty).
        #  As of 2025-01-27, the resulting XLSX file required a post-correction to fix some mismatched column headers near the end of the file, before exporting to TSV.
        jgi_gold_seqproj_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/GOLD-download_All-Seq-Projects_all-columns_2025-01-27_fix-cols_formatted.tsv"

        # IMG microbiome lists:
        # These are downloaded separately for each JGI GOLD study (Gs*******), so the path is a directory
        jgi_img_metaome_dir = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/IMG_searches/"  # current as of 20250128

        # ----------
        # NCBI (also includes JGI data deposited there):
        # The file below was generated using the Kingfisher tool, which is available at https://github.com/wwood/kingfisher-download.
        ncbi_kingfisher_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/Kingfisher_PRJNA888099_20250124.tsv"

        # Additional NCBI list: Mostly NOT in the Umbrella BioProject (even though they should be)!
        #  This needs to be corrected, but for now just standardize this list separately, & then re-name the output file
        #  to match the input & comment out the line below when finished (this was done on 2024-09-17).
        #ncbi_kingfisher_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/Kingfisher_JGI-runs-from-Tijana-20240820_20240917.tsv"

        # ----------
        # Sample metadata in EMERGE-DB:
        # Using the 'Soil-Depth' instead of the 'Depth-Info' label in order to retrieve more samples that may not have
        #  detailed coring sheet data.
        db_soildepths_fp = "querying_output/official_query_output/cached-query_IsoGenie_server-download_20240609_Soil-Depth.csv"

        # Misc. naming conversion lists (for additional_metadata_fps):
        # edit these as needed
        # TODO: Make sure these include any newly-finalized SampleIDs (e.g. 201407_Virome., IncubationMaterial.201407_)
        sip_incubation_fp = "test_data/IsoGenie_datasets/private_data/incubation_metadata/Hough_SIP/parsing_SIP_metaGs_v2.csv"
        chanton_incubation_fp = "test_data/IsoGenie_datasets/private_data/incubation_metadata/Chanton_substrates/20230213_updated_Rachel_incubation_DNA_extraction_SH-notes_formatted.csv"
        additional_ids_fp = "test_data/IsoGenie_datasets/private_data/sequencing/fromSamAroney_datasetsforibis/sample_naming_corrections_formatted-for-script-parsing.csv"

        additional_metadata_fps = [sip_incubation_fp, chanton_incubation_fp, additional_ids_fp]

        # ----------

        # misc file imports from above (more files are read within specific files' standardization scripts)
        jgi_api_filelist = pd.read_csv(jgi_api_filelist_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)
        jgi_gold_seqproj = pd.read_csv(jgi_gold_seqproj_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)
        ncbi_kingfisher = pd.read_csv(ncbi_kingfisher_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)
        db_soildepths = pd.read_csv(db_soildepths_fp, sep=',', quotechar='"', na_filter=False, dtype=object)

        # iteratively import IMG files
        img_dfs = []
        for root, dirs, files in os.walk(jgi_img_metaome_dir):
            for fname in files:
                if fname.startswith('exported_img_data_'):
                    img_dfs.append(pd.read_csv(os.path.join(root, fname), sep='\t', quotechar='"', dtype=object))
        img_all_projs = pd.concat(img_dfs, ignore_index=True)
        # remove empty columns
        img_all_projs.dropna(axis='columns', how='all', inplace=True)

        # Edit the headers of each externally-sourced (non-DB) file so that each source uses a constant prefix, & remove spaces and dots (if any)
        jgi_api_filelist.columns = ["JGI_API_filelist." + col.replace(" ", "_") for col in jgi_api_filelist.columns]
        jgi_gold_seqproj.columns = ["JGI_GOLD_SeqProj." + col.replace("Project.", "").replace(".", "_").replace(" ", "_") for col in jgi_gold_seqproj.columns]  # the GOLD table begins each header with "Project.", so need to replace
        img_all_projs.columns = ["JGI_IMG." + col.replace(" ", "_").replace("/", "_").replace("*", "_").replace("-", "_").replace("%", "pct").replace("__", "_") for col in img_all_projs.columns]  # the last replacement hopefully ensures no more than 2 consecutive underscores in the final column names, and then only if there were originally 3+ (___)
        ncbi_kingfisher.columns = ["NCBI_Kingfisher." + col.replace(" ", "_") for col in ncbi_kingfisher.columns]

        # already edited in standardize_jgi_spReport:
        #spReport.columns = ["JGI_spReport." + col.replace(" ", "_") for col in spReport.columns]

        # Standardize spReport
        # Maybe change this to use keep_only_usable=True ?  But only if not wanting output for the virus reads?
        jgi_spReport = standardize_jgi_spReport(jgi_spReport_fp, db_soildepths_fp, additional_metadata_fps, save_output=True)

        # Generate & print a list of all Sequencing Project IDs that appear in GOLD but not spReport (thankfully not many),
        #  as well as any other "missing" lists of interest
        # Also should check for other missing IDs before using any given source to create a list of data for JGI to release.
        # TODO: Also import some type of file from IMG, and do the same!
        its_sp_IDs_GOLD = set(jgi_gold_seqproj['JGI_GOLD_SeqProj.ITS_SP_ID'])
        its_sp_IDs_spReport = set(jgi_spReport['JGI_spReport.Sequencing_Project_ID'])
        its_sp_IDs_API = set(jgi_api_filelist['JGI_API_filelist.project'])
        its_sp_IDs_IMG = set(img_all_projs['JGI_IMG.ITS_SP_ID'])

        missing_sp_IDs = [id for id in its_sp_IDs_GOLD if id not in its_sp_IDs_spReport]  # as of 7/8/2024, this includes 3 IDs
        missing_api_IDs = [id for id in its_sp_IDs_GOLD if id not in its_sp_IDs_API]      # as of 7/8/2024, this includes 15 IDs (including all the ones in missing_sp_IDs)
        missing_sp_IDs_from_API = [id for id in its_sp_IDs_API if id not in its_sp_IDs_spReport]  # as of 7/8/2024, there are none (good) - but checking in case some files may have been downloaded that don't have metadata in spReport
        missing_sp_IDs_from_IMG = [id for id in its_sp_IDs_IMG if id not in its_sp_IDs_spReport]  # as of 7/10/2024, this includes 5 IDs  (including the 3 missing_sp_IDs, plus 2 metaTs from "717 S3 20-24" and "716 P2 20-24" -- all "Abandoned")

        missing_sp_IDs_df = jgi_gold_seqproj[jgi_gold_seqproj['JGI_GOLD_SeqProj.ITS_SP_ID'].isin(missing_sp_IDs)][['JGI_GOLD_SeqProj.ITS_SP_ID', 'JGI_GOLD_SeqProj.ITS_Sequencing_Project_Name']]
        missing_api_IDs_df = jgi_gold_seqproj[jgi_gold_seqproj['JGI_GOLD_SeqProj.ITS_SP_ID'].isin(missing_api_IDs)][['JGI_GOLD_SeqProj.ITS_SP_ID', 'JGI_GOLD_SeqProj.ITS_Sequencing_Project_Name']]
        missing_sp_IDs_from_IMG_df = img_all_projs[img_all_projs['JGI_IMG.ITS_SP_ID'].isin(missing_sp_IDs_from_IMG)][['JGI_IMG.taxon_oid', 'JGI_IMG.ITS_SP_ID', 'JGI_IMG.Genome_Name__Sample_Name']]

        if len(missing_sp_IDs_df)>0:
            print("The following Sequencing Project IDs exist in GOLD but not in the Sample Report:")
            print(missing_sp_IDs_df.to_string(index=False))
            print()

        if len(missing_api_IDs_df)>0:
            print("The following Sequencing Project IDs exist in GOLD but not in the API output:")
            print(missing_api_IDs_df.to_string(index=False))
            print()

        if len(missing_sp_IDs_from_IMG_df)>0:
            print("The following Sequencing Project IDs exist in IMG but not in the Sample Report:")
            print(missing_sp_IDs_from_IMG_df.to_string(index=False))
            print()

        # Import, merge, and standardize the separate spReport files (manually downloaded) with the missing IDs
        if len(missing_sp_IDs) > 0 and len(missing_sp_IDs_from_IMG) > 0:

            # TODO: If any future standardizations reach this point, then the files in the directory below will need to be updated (if possible).
            jgi_spReport_missingprojs_dir = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/missing_sample_spReports/"
            print(f"Importing spReports for 'abandoned' projects from {jgi_spReport_missingprojs_dir}")
            missing_spReport_dfs = []
            for root, dirs, files in os.walk(jgi_spReport_missingprojs_dir):
                for fname in files:
                    if fname.startswith('spReport_'):
                        missing_spReport_dfs.append(standardize_jgi_spReport(os.path.join(root, fname), db_soildepths_fp, additional_metadata_fps, keep_only_usable=True, save_output=False))

            jgi_spReport_missingprojs = pd.concat(missing_spReport_dfs, ignore_index=True)
            jgi_spReport_missingprojs_fp = "test_data/IsoGenie_datasets/private_data/sequencing/output/spReport_abandoned-but-usable-runs.csv"
            print(f"Saving 'abandoned' project output to {jgi_spReport_missingprojs_fp}")
            jgi_spReport_missingprojs.to_csv(jgi_spReport_missingprojs_fp, index=False)

            # Add the missing JGI projects to the main jgi_spReport to help with the remaining standardizations
            # TODO: If any future standardizations reach this point, then we may need to check for & remove duplicates in the concatenated dataframe.
            jgi_spReport = pd.concat([jgi_spReport, jgi_spReport_missingprojs], ignore_index=True)

        # ADD SAMPLEID TO IMG LIST ----
        for index, row in img_all_projs.iterrows():

            # match by ITS SP ID - result of the below is a dataframe, regardless of number of matches
            its_sp_matches = jgi_spReport.loc[jgi_spReport["JGI_spReport.Sequencing_Project_ID"] == row['JGI_IMG.ITS_SP_ID']]

            if len(its_sp_matches) == 0:  # no match
                # TODO: This means it's one of the "abandoned" projects identified earlier; need to combine those extra ones with the main spReport so that the metadata can be collected.
                pass

            else:  # one or more matches
                sampleIDs = list(set(its_sp_matches['SampleID__']))
                if len(sampleIDs) == 1:
                    img_all_projs.loc[index, "SampleID__"] = sampleIDs[0]
                else:
                    error(f"Multiple unique SampleIDs identified for {row['JGI_IMG.ITS_SP_ID']}: {sampleIDs}")

        img_all_projs.to_csv(os.path.join(os.path.dirname(jgi_spReport_fp), "..", "output", "img_all_projs_standardized.csv"), sep=',', index=False)


        # NCBI sample summary (expanded with info from the DB and JGI) ----

        # extract necessary columns from spReport; remove duplicate rows
        # some of these are used only for matching, while others are copied to the final output
        # anything from jgi_spReport that should be copied to the final output, currently must be assigned manually (below)
        jgi_filtrd = jgi_spReport[["JGI_spReport.Proposal", "JGI_spReport.Sequencing_Project_Name", "JGI_spReport.Sequencing_Project_ID",
                                   "JGI_spReport.Library_Name", "JGI_spReport.Actual_Sequencer_Model", "JGI_spReport.RQC_Seq_Unit_Name",
                                   "JGI_spReport.NCBI_SRA_Run_Accession_ID", "SampleID__"]]

        # Other columns to maybe include if it later proves useful (but omitted for now, for simplicity):
        # "JGI_spReport.Final_Deliverable_Project_ID", "JGI_spReport.Sequencing_Product", "JGI_spReport.Library_Type", "JGI_spReport.Library_and_Seq_Methods", "JGI_spReport.Library_Number_PCR_Cycles", "JGI_spReport.Sample_Contact", "JGI_spReport.Project_Manager"
        # TODO: Should actually parse/simplify Library_Type & Library_and_Seq_Methods into library kit names; see info from Simon Roux on how to do so.

        # Some samples have multiple runs yet same library name; this is now "fixed" below by further filtering library matches between spReport and Kingfisher by SRR.
        #   (e.g., "713 P-1-X1 metaG" has SRR11568089 and SRR11568090 both published to NCBI with library name GCYHS)

        jgi_filtrd.drop_duplicates(keep='first', inplace=True)

        # Do the same thing with Kingfisher output, keeping relevant columns.
        # Keep everything by default, excluding only ones that contain potentially misleading and/or useless info
        excluded_kingfisher_cols = ["sample_name",                  # definition is inconsistent; it's defined within Kingfisher (not NCBI), and usually just defaults to library_name
                                    "study_alias",                  # definition is inconsistent; usually it's the BioProject, but a few times (though not often enough to be useful) it's the JGI SP ID.
                                    "organisation_department",      # these specific details aren't useful; already have "organisation" for the name, and "organisation_contact_name" for the specific person
                                    "organisation_institution",
                                    "organisation_street",
                                    "organisation_city",
                                    "organisation_country",
                                    "organisation_contact_email",
                                    "study_links",                  # may add this one back later; although many of the links don't work, the JGI ones do contain the ITS SP ID
                                    "number_of_runs_for_sample"]    # this field is inaccurate; it's basically always 1 even if multiple runs were submitted for the same BioSample at the same time

        ncbi_filtrd = ncbi_kingfisher[[col for col in ncbi_kingfisher.columns if col not in ["NCBI_Kingfisher." + c for c in excluded_kingfisher_cols]]]

        # Also remove columns where all the values are either blank or "not applicable".
        empty_kingfisher_cols = [col for col in ncbi_filtrd.columns if only_empty_or_na(ncbi_filtrd[col])]
        ncbi_filtrd.drop(columns=empty_kingfisher_cols, inplace=True)

        # Also remove any fully-duplicated rows - this only does anything if >1 input files were concatenated, which would be unusual
        ncbi_filtrd.drop_duplicates(keep='first', inplace=True)

        # add SampleID__ to ncbi_kingfisher_filtrd (depending on data source and existing name)
        for index, row in ncbi_filtrd.iterrows():
            quality_flag = ""
            sampleID = ""

            # assign JGI properties for reads from JGI
            jgi_proposal = ""   # JGI_spReport.Proposal
            its_sp_id = ""      # JGI_spReport.Sequencing_Project_ID
            jgi_reads_fn = ""   # JGI_spReport.RQC_Seq_Unit_Name


            if row["NCBI_Kingfisher.organisation"] == "DOE Joint Genome Institute":

                library_match = jgi_filtrd.loc[jgi_filtrd["JGI_spReport.Library_Name"] == row["NCBI_Kingfisher.library_name"]].squeeze()

                # If it's a dataframe, then try again, this time further filtering by run:
                if type(library_match) == pd.DataFrame:
                    # print(f"Further filtering library {row['NCBI_Kingfisher.library_name']} by run ID, as there were duplicates.")
                    library_match = library_match.loc[library_match["JGI_spReport.NCBI_SRA_Run_Accession_ID"] == row["NCBI_Kingfisher.run"]].squeeze()

                # Make sure it's now a series:
                if type(library_match) == pd.Series:

                    # assign the JGI info
                    jgi_proposal = library_match["JGI_spReport.Proposal"]
                    its_sp_id = library_match["JGI_spReport.Sequencing_Project_ID"]
                    jgi_reads_fn = library_match["JGI_spReport.RQC_Seq_Unit_Name"]

                    # make sure the name also matches (for JGI data, the original name is under experiment_title)
                    # if it does, assign SampleID__
                    if library_match["JGI_spReport.Sequencing_Project_Name"] == row["NCBI_Kingfisher.experiment_title"]:
                        # assign SampleID__
                        sampleID = library_match["SampleID__"]

                    # if not, something is wrong
                    else:
                        # first see if it's one of the ones that JGI accidentally mislabeled as "714" instead of "717"
                        # get corrected title from sample_title, then check it to be sure
                        new_title = row["NCBI_Kingfisher.sample_title"].replace("Peat soil microbial communities from Stordalen Mire, Sweden - ", "") + " metaG"
                        if library_match["JGI_spReport.Sequencing_Project_Name"] == new_title:
                            sampleID = library_match["SampleID__"]
                            quality_flag = 'The sample name was mislabeled by JGI, with the wrong name appearing ' \
                                           'under "experiment_title". The SampleID__ reflects the corrected name, ' \
                                           'which appears as part of the "sample_title" in NCBI.'

                        # if not one of these cases, something else is wrong; give a warning and don't assign SampleID__
                        # and also revert the JGI info back to empty strings (just to be safe)
                        else:
                            msg = f'Error: Sample name mismatch in JGI metadata ({library_match["JGI_spReport.Sequencing_Project_Name"]}) for this library name ({library_match["JGI_spReport.Library_Name"]}).'
                            warning(msg)
                            quality_flag = msg
                            sampleID = ""
                            jgi_proposal = ""
                            its_sp_id = ""
                            jgi_reads_fn = ""

                else:
                    # anything other than 1 row gives a dataframe
                    # in this case, something is wrong

                    if len(library_match) == 0:
                        # if no match found, just enter an error message into the output
                        # TODO: Find out if this happens due to using an outdated spReport, or if it's an internal JGI error?
                        sampleID = ""
                        quality_flag = 'Error: No match found in JGI metadata (spReport) for this library ID.'
                    else:
                        # If multiple matches, then there's no straightforward solution, so throw an error.
                        error(f'Error: {len(library_match)} matches found in spReport for {row["NCBI_Kingfisher.library_name"]}; it should ideally be 1.')

            else:  # non-JGI data isn't yet parsed, so need to do that here

                # first see if it's already a valid SampleID__
                sampleID = lookup_id_from_df(value_lookup=row["NCBI_Kingfisher.sample_alias"],
                                             colname_lookup="SampleID__",
                                             colname_result="SampleID__",
                                             df=db_soildepths)


                # next try Sequencing_SampleID-1__
                if sampleID in ["", "NaN"]:
                    # If it's a specific sample (20110800_E1D) that fails when looked up in Sequencing_SampleID-1__
                    #  due to two matches (one of which has no known sequencing data), assign it manually.
                    # TODO: Figure out if that extra match is really supposed to exist, and if not, remove it!
                    if row["NCBI_Kingfisher.sample_alias"] == "20110800_E1D":
                        sampleID = "MainAutochamber.201108_E_1_24to27"

                    else:
                        sampleID = lookup_id_from_df(value_lookup=row["NCBI_Kingfisher.sample_alias"],
                                                     colname_lookup="Sequencing_SampleID-1__",
                                                     colname_result="SampleID__",
                                                     df=db_soildepths)

                # if it's still not found, see if it's from a BioProject with known IDs that aren't in Soil-Depth nodes
                if sampleID in ["", "NaN"] and row["NCBI_Kingfisher.bioproject"]=="PRJNA825577":
                    sampleID = row["NCBI_Kingfisher.sample_alias"]

            # there shouldn't be any other ways to look it up (yet), so can now just assign SampleID__ if we have it
            if sampleID == "NaN":
                sampleID = ""

            # final assignments
            ncbi_filtrd.loc[index, "JGI.Proposal_ID"] = jgi_proposal            # equals "JGI_spReport.Proposal"
            ncbi_filtrd.loc[index, "JGI.ITS_SP_ID"] = its_sp_id                 # equals "JGI_spReport.Sequencing_Project_ID"
            ncbi_filtrd.loc[index, "JGI.raw_reads_filename"] = jgi_reads_fn     # equals "JGI_spReport.RQC_Seq_Unit_Name"
            ncbi_filtrd.loc[index, "SampleID__"] = sampleID
            ncbi_filtrd.loc[index, "QualityFlag__"] = quality_flag


        # write final list to CSV
        ncbi_filtrd.to_csv(os.path.join(os.path.dirname(jgi_spReport_fp), "..", "output", "EMERGE_SRA_runs_standardized.csv"), sep=',', index=False)

