#!/usr/bin/env python
"""
OVERVIEW:
From excel-formatted (*.XLS, *.XLSX), manually-curated and triple-verified files were converted into CSV using Excel's
export functionality. These CSVs are then read in, and then data headers are "re-mapped" to standardized nomenclature.
These headers are then saved to a new CSV so that future work will no longer need to go through this process. This new
"standardized" CSV is then used for importing the data into the database.

Because this standardization is done once at this stage, the actual import process is faster, and the same import
scripts can also be reused for other standardized data sheets.

This particular script is designed for the following GPMP data sheets:
GPMP_AncillaryData_MasterSheet_June11_2020_A2AOnly.csv
GPMP_SiteInfo_forA2A_corrected_SH.csv


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
import sys
import re
import numpy as np
import pandas as pd
import argparse
# import LatLon  # doesn't work in Python 3
from dateutil.parser import parse as date_parse
from datetime import datetime
import warnings

# suppress FutureWarning
warnings.filterwarnings("ignore", category=FutureWarning)

# from pprint import pprint

# import local_file_paths, common_vars, and common_vars_A2A
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
from local_file_paths import *
from tools.data.common_vars import standardize_GPS, warning, error
from tools.data.common_vars_A2A import *

parser = argparse.ArgumentParser(description="Standardize GPMP sheets. Written especially for the 'Ancillary Data' sheet.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-i', '--input', dest='input_file', help="File name of CSV to standardize")

args = parser.parse_args()

# Suzanne's new function for getting ranges of close-together dates from a dataframe of dates
def get_date_ranges(df, gap=7):
    # Edited on 8/24/20 to work with table cells containing multiple comma-separated dates. This ended up not being
    #  actually useful for this dataset, but keeping the changes anyway in case it's useful later.

    # Arguments:
    # df = dataframe with date columns only
    # gap = minimum # of days needed to separate date ranges (default 1 week)

    # Put all the values in df into one long list
    alldates_rough = []
    alldates = []
    for column in df:
        alldates_rough += list(df[column])

    # Convert alldates to integers (if not already)
    for date_item in alldates_rough:
        small_date_list = date_item.split(', ')  # convert list strings to lists
        alldates += [int(d) for d in small_date_list]

    # Remove duplicates from alldates and sort ascending
    alldates = set(alldates)
    alldates = list(alldates)
    alldates.sort()

    # Convert dates to date format for getting differences in # of days
    alldates_int = alldates.copy()
    for index, date in enumerate(alldates):
        alldates[index] = datetime.strptime(str(date), "%Y%m%d")

    # Find the breakpoints between the date ranges
    isStartDate = []
    for index, date in enumerate(alldates):
        if index == 0:
            # the earliest date is always a start date
            isStartDate.append(True)
        # elif index == len(alldates) - 1:
        #     # the latest date is never a start date
        #     # EDIT: Unless it's only date during that field sampling! So get rid of this condition.
        #     isStartDate.append(False)
        else:
            # all other dates
            if (alldates[index] - alldates[index - 1]).days >= gap:
                isStartDate.append(True)
            else:
                isStartDate.append(False)

    # Slice alldates_int (unique sorted dates in integer format) into discrete ranges
    alldates_sep = []
    indices_StartDates = [i for i, x in enumerate(isStartDate) if x]
    indices_StartDates.append(len(alldates_int))
    for j, index_date in enumerate(indices_StartDates):
        if index_date == len(alldates_int):
            # if at the end of indices_StartDates, break out of the loop
            break
        else:
            # get the sublist of alldates_int
            daterange = alldates_int[indices_StartDates[j]:indices_StartDates[j + 1]]
            # keep the first and last dates only
            daterange = [daterange[0], daterange[len(daterange) - 1]]
            alldates_sep.append(daterange)

    return alldates_sep


def parse_dates_Ymd(date):
    # convert arbitrary date format to YYYYMMDD
    if not (pd.isnull(date)):
        date = date_parse(date).strftime('%Y%m%d')
    return (date)


def formatDate(intdate):
    # convert YYYYMMDD to YYYY-MM-DD, or YYYYMM to YYYY-MM
    if not (pd.isnull(intdate)):
        intdate = str(intdate)

        if len(intdate) == 8:
            formatted = datetime.strptime(intdate, '%Y%m%d').strftime('%Y-%m-%d')
        elif len(intdate) == 6:
            formatted = datetime.strptime(intdate, '%Y%m').strftime('%Y-%m')
        else:
            error("Could not format date {}".format(intdate))
    else:
        formatted = 'NaN'

    return formatted


if __name__ == "__main__":

    # site_info_df = pd.DataFrame.from_dict(site_info, orient='index')

    source_dir = source_GPMP_dir  # from local_file_paths.py

    # Gather all the CSVs
    core_file = args.input_file

    data_csvs = []
    # can add more code later to input multiple sheets
    data_csvs.append(os.path.join(source_dir, core_file))

    # Read each CSV into a dataframe

    for data_csv in data_csvs:
        print(data_csv)
        csv_df = pd.read_csv(data_csv, sep=',', quotechar='"')

        # csv_df.replace(
        #     {'---': np.nan, '`': np.nan, 'n/a': np.nan, 'n/s': np.nan, 'na': np.nan, 'NA': np.nan, '-': np.nan},
        #     inplace=True)

        # Drop empty rows and columns (byproduct of Excel's "save as csv") for some sheets
        csv_df.dropna(axis='index', how='all', inplace=True)
        csv_df.dropna(axis='columns', how='all', inplace=True)

        # Create standardized column headers
        # For now, standardize ONLY GPMP.ID.
        # For the rest of the columns, just get the column name for each variable so that it can be easily retrieved.
        # TODO: For the misspellings fixed below, also fix the spellings in the columns_ file.
        col_key = {k: np.nan for k in GPMP_column_syn.keys()}
        new_columns = []
        for column in csv_df.columns:
            for official_column_name, variants in GPMP_column_syn.items():
                if column in variants:
                    col_key[official_column_name] = column
                    break
            if column in GPMP_column_syn['GPMP.ID']:
                new_columns.append('GPMP.ID')
            # correct any misspellings
            elif 'Teperature' in column:
                new_columns.append(column.replace('Teperature', 'Temperature'))
            elif 'H20.Table' in column:
                new_columns.append(column.replace('H20.Table', 'H2O.Table'))
            elif 'of.H20.used' in column:
                # TODO: Re-standardize with this change included
                new_columns.append(column.replace('of.H20.used', 'of.H2O.used'))
            else:
                new_columns.append(column)
        try:
            csv_df.columns = new_columns
        except ValueError:
            print('Old columns: {}'.format(csv_df.columns))
            print('New columns: {}'.format(new_columns))
            exit()

        for index, row in csv_df.iterrows():  # iterate over rows (index is the index of each row)

            # GPS
            try:
                adjusted_coords = standardize_GPS(lat=float(row[col_key['Latitude']]), lon=float(row[col_key['Longitude']]))
                # New code without using latlon:
                csv_df.loc[index, 'GPS__'] = adjusted_coords

            except ValueError:
                print(row[col_key['Latitude']])
                print(row[col_key['Longitude']])
                error('Unable to identify/determine GPS information. ALL cores must have a value, even if NaN.')

            except KeyError:
                print(row)
                error('Unable to locate any GPS information.')

            except TypeError:
                error('Unable to parse GPS "{}" of type {}'.format(row['GPS'], type(row['GPS'])))

            # Areas and Sites

            area = False
            site = False

            # Check columns from broadest --> narrowest, assigning Area first, then Site
            for column in [col_key['Wetland.Cluster'], col_key['Wetland'], col_key['Site'], col_key['Site.Notes'],
                           col_key['Plot']]:
                if pd.isnull(column):
                    continue
                else:
                    if not area:
                        for official_area_name, variants in area_syn.items():
                            if row[column] in variants:
                                area = official_area_name
                                break
                            if official_area_name == 'APEX-Beta' and 'APEX Beta' in row[column]:
                                # "APEX Beta" is sometimes part of a longer string
                                area = official_area_name
                                break
                    elif not site:
                        for official_site_name, variants in a2a_site_syn[area].items():
                            if row[column] in variants:
                                site = official_site_name
                                break

            if not area:
                error('Could not identify the Area from row {}'.format(index))

            # If the site is still unidentified, first try getting it from the sample name
            if not site:
                if not pd.isnull(col_key['Sample.Name']):
                    for official_site_name, variants in a2a_site_syn[area].items():
                        for variant in variants:
                            if str(variant) in row[col_key['Sample.Name']]:
                                site = official_site_name
                else:
                    try:
                        for official_site_name, variants in a2a_site_syn[area].items():
                            for variant in variants:
                                if str(variant) in row['Sample.Code.Names']:
                                    site = official_site_name
                    except KeyError:
                        pass

            # If the site is *still* unidentified, then proceed with Area name, with a warning
            if not site:
                warning('Could not identify the Site from row {}.\n'
                        'Proceeding using the Area name.'.format(index))
                site = area

            if area in area_groups.keys():
                csv_df.loc[index, 'AreaGroup__'] = area_groups[area]

            csv_df.loc[index, 'Area__'] = area
            csv_df.loc[index, 'Site__'] = site

            # In the SiteInfo sheet, use the DataSubsample__ column to separate the APEX-Beta timeseries "sites" from
            #  the non-timeseries "sites" (though they're actually the same site):
            if 'GPMP_SiteInfo' in data_csv:
                if 'time series' in row[col_key['Site']]:
                    csv_df.loc[index, 'DataSubsample__'] = 'Time Series'
                else:
                    csv_df.loc[index, 'DataSubsample__'] = 'NaN'

            # Habitats: Get from column

            if pd.isnull(col_key['Habitat']):
                warning('No Habitat column identified. No habitats are being assigned.')

            else:
                habitat = False

                for official_habitat_name, variants in habitat_syn.items():
                    if row[col_key['Habitat']] in variants:
                        habitat = official_habitat_name
                        break

                if not habitat:
                    error('Could not identify the habitat from "{}" in row {}'.format(row[col_key['Habitat']], index))

            csv_df.loc[index, 'Habitat__'] = habitat

            if not pd.isnull(col_key['Core.Name']):  # Continue only if the sheet is separated by cores.
                # This means that Date, Core, and Depth-related properties won't be set if it's a broad site info sheet.

                # Date

                # standardize date formats
                try:
                    coring_date = parse_dates_Ymd(row[col_key['Date.Sampled']])

                    # Save these dates to the dataframe before filling in all the dates to get date ranges
                    # coring date is the ONLY date for GPMP samples, so only need to assign DateC__ (Date__ will be assigned during import)
                    csv_df.loc[index, 'DateC__'] = coring_date

                except ValueError:  # If there are multiple dates, then don't put them in a permanent column.
                    if ', ' in row[col_key['Date.Sampled']]:
                        coring_dates_list = row[col_key['Date.Sampled']].split(', ')
                        coring_date = ', '.join([parse_dates_Ymd(date) for date in coring_dates_list])
                        # coring_date will now be another list-like string, but reformatted

                # Now save this to a temporary column
                csv_df.loc[index, 'DateC_tmp'] = coring_date

                # Cores:
                # Should be easy, as the identifiers should be unique.
                csv_df.loc[index, 'Core__'] = row[col_key['Core.Name']]


                # Depths
                core_depth = row['Depth.Increment.cm']

                min_depth = 'NaN'
                max_depth = 'NaN'  # will assign these later

                try:
                    if type(core_depth) == str and '-' in str(core_depth):
                        components = re.findall(r"[.\w]+",
                                                core_depth)  # Make sure decimals (.) are counted within the numbers.

                        if len(components) == 2:  # start-end
                            #                            if components[0].isdigit() and components[1].isdigit():
                            #                                core_depth = '{}-{}'.format(components[0], components[1])
                            #                            else:
                            #                                print('not passed', components)
                            try:
                                test = float(components[0]) + float(
                                    components[1])  # See if they can be converted to floats
                                min_depth = components[0]
                                max_depth = components[1]
                                core_depth = '{}-{}'.format(min_depth, max_depth)
                            except ValueError:
                                error('not passed: {}, {}'.format(components[0], components[1]))
                        else:
                            error('Core depth "{}" in row {} is not a range.'.format(core_depth, index))
                    else:
                        error('Core depth "{}" in row {} is not a range.'.format(core_depth, index))

                except ValueError:
                    error('Value error for {}'.format(core_depth))

                # if core_depth == 'surface' or core_depth == 'overlying water' or core_depth == 'standing water':
                #     core_depth = 'above peat'

                csv_df.loc[index, 'Depth__'] = core_depth
                csv_df.loc[index, 'DepthMin__'] = min_depth
                csv_df.loc[index, 'DepthMax__'] = max_depth

                # Make a DepthAvg__ column, which is always a single number
                avg_depth = 'NaN'  # set it to NaN first, just in case it's never assigned later

                # First try to convert all the depth values to floats, and change to NaN if this isn't possible.
                # This avoids having to check for both NaN *and* non-numeric depths separately when determining DepthAvg__.
                try:
                    core_depth_num = float(core_depth)
                except ValueError:
                    core_depth_num = 'NaN'

                try:
                    min_depth_num = float(min_depth)
                    max_depth_num = float(max_depth)
                except ValueError:
                    min_depth_num = 'NaN'
                    max_depth_num = 'NaN'

                # Now calculate a numeric DepthAvg__ if possible
                if not (pd.isnull(core_depth_num) or core_depth_num == 'NaN'):
                    avg_depth = core_depth_num
                elif not (pd.isnull(
                        min_depth_num) or min_depth_num == 'NaN'):  # check if min_depth_num is NaN; assume max_depth_num is the same
                    avg_depth = (min_depth_num + max_depth_num) / 2  # average of min & max

                else:  # not an "above peat" or a "None" depth; therefore something else is weird
                    warning('Could not determine DepthAvg__ from the available data; defaulting to NaN: \n{}'.format(row))

                csv_df.loc[index, 'DepthAvg__'] = avg_depth

                # As an error-checking measure, make sure the upper limit and midpoint match the values entered in separate
                #  columns.
                if min_depth_num != float(row['Depth.Upper.Limit.cm']):
                    error("Depth upper limit mismatch in row {}: min_depth={}, row['Depth.Upper.Limit.cm']={}".format(index, min_depth, row['Depth.Upper.Limit.cm']))
                if avg_depth != float(row['Depth.Midpoint.cm']):
                    error("Depth midpoint mismatch in row {}: avg_depth={}, row['Depth.Midpoint.cm']={}".format(index, avg_depth, row['Depth.Midpoint.cm']))

                # SampleIDs: Make this a standardized property for matching other datasets.
                sampleID = row[col_key['Sample.Name']]

                # treatment identifiers: treat as data subsamples, separate from sampleID
                if sampleID.endswith('.F.D.'):
                    subdata = 'FreezeDried'
                    sampleID = sampleID[0:len(sampleID)-5]

                elif sampleID.endswith('.F.D'):  # another variant
                    subdata = 'FreezeDried'
                    sampleID = sampleID[0:len(sampleID) - 4]

                elif sampleID.endswith('.Bench'):
                    subdata = 'Bench'
                    sampleID = sampleID[0:len(sampleID) - 6]

                elif sampleID.endswith('.IcePackCooler'):
                    subdata = 'IcePackCooler'
                    sampleID = sampleID[0:len(sampleID) - 14]

                else:
                    subdata = 'NaN'

                csv_df.loc[index, 'GPMP_SampleID__'] = sampleID
                csv_df.loc[index, 'DataSubsample__'] = subdata

        if not pd.isnull(col_key['Core.Name']):
            # At this point we're done with the first looping over rows, so test again for whether cores are
            #  differentiated before looping again to consolidate dates.

            # Suzanne's new method for dates: Consolidate dates that "go together" to make summarized DateStart__ and DateEnd__ columns
            # For A2A GPMP sheet, do this separately for each Area.

            for area, area_df in csv_df.groupby(by='Area__'):
                date_ranges = get_date_ranges(area_df[['DateC_tmp']])
                for index, row in area_df.iterrows():
                    if row['Area__'] == area:
                        date_int = int(row['DateC_tmp'])
                        date_start = np.nan
                        for date_range in date_ranges:
                            if date_int >= date_range[0] and date_int <= date_range[1]:
                                date_start = date_range[0]
                                date_end = date_range[1]
                                break

                        if pd.isnull(date_start):
                            error('Date in row {} out of range: {} \n{}'.format(index, row['DateC_tmp'], date_ranges))
                        else:
                            date_start = str(date_start)
                            date_end = str(date_end)

                            # although looping through area_df, the index for csv_df should be the same
                            csv_df.loc[index, 'DateStart__'] = date_start
                            csv_df.loc[index, 'DateEnd__'] = date_end
                            csv_df.loc[index, 'FieldSampling__'] = date_start[0:len(date_start) - 2]  # month and year only

                        # Convert dates from format YYYYMMDD to YYYY-MM-DD (to match with other datasets)
                        # To get the date ranges, they had to be ints; now they can be converted into formatted strings
                        csv_df.loc[index, 'DateC__'] = formatDate(csv_df.loc[index, 'DateC__'])
                        csv_df.loc[index, 'DateStart__'] = formatDate(csv_df.loc[index, 'DateStart__'])
                        csv_df.loc[index, 'DateEnd__'] = formatDate(csv_df.loc[index, 'DateEnd__'])
                        csv_df.loc[index, 'FieldSampling__'] = formatDate(csv_df.loc[index, 'FieldSampling__'])

            # Drop 'None' depths
            csv_df = csv_df[csv_df.Depth__ != 'None']

            # Drop temporary date columns
            csv_df.drop(columns=['DateC_tmp'], inplace=True)

        # Remnant from when aggregating all Cores columns to best consolidate "duplicates"
        if len(csv_df.columns) > len(set(csv_df.columns)):
            error('Duplicate columns identified in {}. Full list of columns: {}'.format(data_csv, csv_df.columns))

        # Set the csv export filename by putting "_standardized" at the end of the original filename.
        updated_csv = data_csv.replace('.csv', '_standardized.csv')

        # Export to csv.
        csv_df.to_csv(updated_csv, index=False, quotechar='"', sep=',')


