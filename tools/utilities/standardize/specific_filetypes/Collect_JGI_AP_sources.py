"""
OVERVIEW:
For a list of IMG Taxon IDs and associated Analysis Projects (AP) (these should correspond 1:1), determine which
 readsets/runs were used as the input data.
Ideally, these should be obtained from the AP README files (see output from idm-team/jgi_interfacing/bin/parse_metadata.py).
 If there is no README available, use the GOLD AP metadata as an alternate source, making note of this with a
 QualityFlag__ (depending on whether the GOLD metadata matches the READMEs for APs that have both). (Don't use IMG's
 metadata for this, as it's already been found to list inaccurate source runs for some projects.)
Output all of the following:
    - raw data filenames (if any are missing from READMEs, try locating them in another row based on the matching
       Filtered Data filename)
    - SRRs (including a note, in the same output string, about any couldn't be found)
    - libraries (ideally from the READMEs; otherwise from the XML-parsed "library" column of the parse_metadata output),
       to be used if more specific source run info can't be found

This script was created by Dr. Suzanne Hodgkins, with support from the NSF-funded EMERGE Biology Integration Institute.
Copyright © 2024 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
import sys
import pandas as pd
import numpy as np
import re
import argparse
import logging
from datetime import datetime


# import other variables by filepath (from the root isogenie-tools directory)
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../..')))
# In an effort to keep this script portable, the dependencies below should only be imported if absolutely necessary:
#from tools.data.common_vars import *
#from tools.utilities.utils import error, warning, lookup_id_from_df

# Setup logging
loglevel = logging.INFO
logging.basicConfig(level=loglevel, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

parser = argparse.ArgumentParser(description="Collect JGI Analysis Project sources")
inputs = parser.add_argument_group('Inputs')

inputs.add_argument("--img-list-fp", default=None,
                    help="Filepath listing specific IMG Taxon IDs or bin IDs (which will be reduced to Taxon IDs) to parse. "
                         "If None (default), all IDs in the GOLD Analysis Project list will be processed. Limiting the "
                         "IMG IDs to a specific subset is helpful if there are some problematic projects that don't "
                         "need immediate parsing. "
                         "Currently, this assumes that the IDs are in the first (or only) column of an input "
                         "tab-delimited text file with 1 header row. Future updates may allow other input formats.")

inputs.add_argument("--gold-ap-fp",
                    default="test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/GOLD-download_All-APs_all-cols-except-taxonomy_2024-09-26.tsv",
                    help="Filepath of GOLD Analysis Project search results. Must include columns for (at minimum): "
                         "ITS Analysis Project ID, IMG Taxon ID, Library Names")

inputs.add_argument("--parsed-metadata-fp",
                    default="test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/idm-team.jgi_interfacing/metadata_20230614_reparsed.tsv",
                    help="Filepath of metadata list parsed using idm-team/jgi_interfacing/bin/parse_metadata.py "
                         "(commit a9cec67514adfcb8386d6f4646b852b57f36bf66 or later).")

inputs.add_argument("--spReport-fp",
                    default="test_data/IsoGenie_datasets/private_data/sequencing/output/spReport_AllMyProjects_20240927_standardized.csv",
                    help="Filepath of the *standardized* JGI Sample Report, used for matching raw data filenames with other raw reads metadata.")
# old: test_data/IsoGenie_datasets/private_data/sequencing/output/spReport_AllMyProjects_20240708_standardized.csv

inputs.add_argument("--kingfisher-fps", nargs="+",
                    default=["test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/Kingfisher_PRJNA888099_20240916.tsv",
                             "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/Kingfisher_JGI-runs-from-Tijana-20240820_20240917.tsv"],
                    help="Filepaths of NCBI metadata (Kingfisher output) for getting final run accessions.")

inputs.add_argument("--img-assembly-search-fp",
                    default="test_data/IsoGenie_datasets/private_data/sequencing/output/img_all_projs_standardized.csv",
                    help="Filepath of the *standardized* IMG meta-ome list, for obtaining source sequencing projects after all other methods have failed.")

# TODO: Add arg for custom output filename?

args = parser.parse_args()

if __name__ == "__main__":

    # import the metadata parsed from AP files via idm-team/jgi_interfacing/bin/parse_metadata.py
    parsed_metadata_df = pd.read_csv(args.parsed_metadata_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)

    # import the GOLD AP metadata
    gold_ap_df = pd.read_csv(args.gold_ap_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)
    gold_ap_df.columns = [col.replace("Analysis Project.", "").replace(".", "_").replace(" ", "_") for col in gold_ap_df.columns]

    # because it was imported without an na_filter, blank columns must be found & dropped manually
    #gold_ap_df.dropna(axis='columns', how='all', inplace=True)
    gold_ap_df.drop(columns=[col for col in gold_ap_df.columns if all(gold_ap_df[col]=="")], inplace=True)

    # import the IMG metadata
    # The source run info in IMG is not reliable, so this list should ONLY be used for obtaining SampleID__,
    #  and for obtaining info for Analysis Projects with no other available metadata for matching the IMG IDs to
    #  Sequencing Projects in the spReport.
    img_assembly_df = pd.read_csv(args.img_assembly_search_fp, sep=',', quotechar='"', na_filter=False, dtype=object)

    # for renaming columns extracted from img_assembly_df for merging with gold_ap_df
    # (also including other potentially-useful information)
    img2gold_rename = {
        "JGI_IMG.taxon_oid": "IMG_Taxon_ID",
        "JGI_IMG.IMG_Submission_ID": "IMG_Submission_ID",
        "JGI_IMG.GOLD_Analysis_Project_ID": "GOLD_Analysis_Project_ID",
        "JGI_IMG.ITS_AP_ID": "ITS_Analysis_Project_ID",
        "JGI_IMG.Genome_Name__Sample_Name": "Analysis_Project_Name",
        "JGI_IMG.Assembly_Method": "Assembly_Method",
        "JGI_IMG.IMG_Release_Pipeline_Version": "Annotation_Pipeline"
    }

    # the following columns aren't actually in the GOLD dataframe, but may be needed to find source runs if other methods fail
    img2placeholder_rename = {
        "JGI_IMG.ITS_SP_ID": "IMG.ITS_SP_ID",
        "JGI_IMG.JGI_Project_ID__ITS_SP_ID": "IMG.JGI_Project_ID__ITS_SP_ID"
    }

    # import the list of IMG IDs of interest (if one was given) and compare with the GOLD list
    if args.img_list_fp is not None:
        logging.info(f"Parsing input file {args.img_list_fp}")
        img_list_df = pd.read_csv(args.img_list_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)
        img_IDs_to_parse = list(set([re.sub("_[0-9]+", "", img_id) for img_id in img_list_df.iloc[:,0].tolist()]))

        # note any that were missing from GOLD
        img_IDs_missing_from_GOLD = [id for id in img_IDs_to_parse if id not in gold_ap_df["IMG_Taxon_ID"].tolist()]
        if len(img_IDs_missing_from_GOLD) > 0:
            logging.warning("The following IMG IDs don't exist in GOLD:")
            logging.warning(img_IDs_missing_from_GOLD)

            # Get placeholder values from IMG search results
            missing_ids_to_add_df = img_assembly_df[img_assembly_df["JGI_IMG.taxon_oid"].isin(img_IDs_missing_from_GOLD)][
                list(img2gold_rename.keys()) + list(img2placeholder_rename.keys())]
            missing_ids_to_add_df.rename(columns=img2gold_rename, inplace=True)
            missing_ids_to_add_df.rename(columns=img2placeholder_rename, inplace=True)

            # Check for any that are still missing; throw an error if there are any
            img_IDs_still_missing = [id for id in img_IDs_missing_from_GOLD if id not in missing_ids_to_add_df["IMG_Taxon_ID"].tolist()]
            if len(img_IDs_still_missing) > 0:
                logging.error(f"Still could not find these IDs in IMG: {str(img_IDs_still_missing)}. Exiting...")
                exit()

            # Also see if any are coassemblies OR have non-matched SP IDs; throw an error if so, as then the SP IDs can't be trusted
            if any(["coassembly" in name for name in missing_ids_to_add_df["Analysis_Project_Name"].tolist()]):
                logging.error("POTENTIALLY INCORRECT OUTPUT: Some IMG ID(s) not in GOLD are coassemblies; cannot trust their ITS SP IDs. Don't trust any of their outputted runs.")

            if any(missing_ids_to_add_df["IMG.ITS_SP_ID"] != missing_ids_to_add_df["IMG.JGI_Project_ID__ITS_SP_ID"]):
                logging.error("POTENTIALLY INCORRECT OUTPUT: Some IMG ID(s) not in GOLD have multiple ITS SP IDs; cannot trust their ITS SP IDs. Don't trust any of their outputted runs.")
            else:
                missing_ids_to_add_df.drop(columns=["IMG.JGI_Project_ID__ITS_SP_ID"], inplace=True)

            # Merge into gold_ap_df - this also adds a few extra columns, which are only populated for the missing IDs above
            gold_ap_df = pd.concat([gold_ap_df, missing_ids_to_add_df], ignore_index=True)
            gold_ap_df.fillna("", inplace=True)
            gold_ap_df.reset_index(drop=True, inplace=True)

    else:
        img_IDs_to_parse = list(set(gold_ap_df["IMG_Taxon_ID"]))
        img_IDs_to_parse.remove("")
        img_IDs_missing_from_GOLD = []

    # import the pre-standardized Sample Report (spReport) for matching filenames to other JGI metadata
    spReport_df = pd.read_csv(args.spReport_fp, sep=',', quotechar='"', na_filter=False, dtype=object)

    # import NCBI (Kingfisher) files for getting final SRRs

    # Dict for fixing some inconsistently-named columns appearing in some Kingfisher outputs.
    # There are also others containing spaces (such as "GOLD Ecosystem Classification"), but these don't seem to duplicate any pre-defined columns (as far as we know)
    kingfisher_col_rename = {
        "Broad-scale Environmental Context": "env_broad_scale",
        "Local-scale Environmental Context": "env_local_scale",
        "Environmental Medium": "env_medium"
    }

    kingfisher_dfs = []
    for kingfisher_fp in args.kingfisher_fps:
        kingfisher_df = pd.read_csv(kingfisher_fp, sep='\t', quotechar='"', na_filter=False, dtype=object)

        # For some reason, some columns in some output aren't renamed to their "simpler" versions. Rename them now.
        for old_col, corr_col in kingfisher_col_rename.items():
            if old_col in kingfisher_df.columns:
                if corr_col not in kingfisher_df.columns:
                    kingfisher_df.rename(columns={old_col: corr_col}, inplace=True)
                else:
                    logging.warning(f"Could not rename column '{old_col}' to '{corr_col}' (from filepath '{kingfisher_fp}'), as the new column name already exists.")

        kingfisher_dfs.append(kingfisher_df)

    ncbi_df = pd.concat(kingfisher_dfs, ignore_index=True)
    ncbi_df.fillna("", inplace=True)
    ncbi_df.drop_duplicates(inplace=True, ignore_index=True)
    ncbi_df.reset_index(drop=True, inplace=True)

    # make sure there aren't any duplicate runs still present
    if any(ncbi_df["run"].duplicated()):
        logging.error("Duplicate runs found in NCBI metadata, even after attempting to remove them.")
        exit()

    # Loop through the GOLD dataframe, focusing on rows with img_IDs_to_parse
    for index, row in gold_ap_df.iterrows():

        img_id = row["IMG_Taxon_ID"]
        its_id = row["ITS_Analysis_Project_ID"]
        libraries_gold = row["Library_Names"]
        n_raw = 0
        n_filtered = 0
        raw_files_list = None
        filtered_files_list = None
        quality_flags = []
        sampleID_img = ""
        sampleID_spReport = ""
        source_runs_final = ""
        source_models_final = ""
        source_biosamples_final = ""
        assembly_method = ""
        assembly_method_version = ""
        bad_result = False  # for flagging results that can't be reconciled; previously this triggered an exit(), but now just flagging them instead

        if img_id not in img_IDs_to_parse:
            # Even if no specific list of IMG IDs was provided, some rows will still be skipped due to blank IMG ID in GOLD.
            # TODO: Maybe still try to get sources based on AP IDs? But if there's no IMG ID, then it's lower priority
            continue

        else:

            # Pre-processing: Assign initial SampleID__ from img_assembly_df
            sampleID_img = img_assembly_df[img_assembly_df["JGI_IMG.taxon_oid"] == img_id]["SampleID__"].tolist()
            if len(sampleID_img) == 1:
                sampleID_img = sampleID_img[0]
                gold_ap_df.loc[index, "SampleID__IMG"] = sampleID_img
            else:
                logging.error(f"No unique record exists in IMG for '{img_id}'.")
                sampleID_img = ""

            # First add a quality flag to anything not in the original GOLD list
            if img_id in img_IDs_missing_from_GOLD:
                quality_flags += ["Not in GOLD; substituting AP metadata from IMG (which may point to incorrect run sources)."]

            # Search for this project in the parsed README metadata
            parsed_metadata = parsed_metadata_df.loc[parsed_metadata_df["project"]==its_id]

            if len(parsed_metadata) > 1:
                # this shouldn't ever happen, so give an error instead of a warning
                error_str = "There are multiple records of this Analysis Project in the API/XML-derived metadata."
                quality_flags += [f"NOT PARSED: {error_str}"]
                logging.error(f"NOT PARSED: {error_str} (AP: {its_id}; IMG: {img_id})")

            elif len(parsed_metadata) == 0:
                error_str = "There is no record of this Analysis Project in the API/XML metadata."
                quality_flags += [error_str]
                logging.warning(f"{error_str} (AP: {its_id}; IMG: {img_id})")
                # TODO: The IMG IDs previously not found in GOLD (see above) got this error.
                #  As of 2024-10-02, there was still nothing matching these AP IDs found via the API/XML.
                #  So, just use any info we *do* have, e.g. from spReport (with a quality flag).

            else:  # exactly one record; ideal scenario
                parsed_metadata.reset_index(drop=True, inplace=True)
                parsed_metadata = parsed_metadata.iloc[0, :]  # convert to series

                # See how many listed raw datafiles there are, and compare with the number of "filtered data".
                # If there are mismatches, also flag this (but still try to resolve them later).
                if parsed_metadata["Raw_Data"] == "":
                    raw_files_list = []
                    quality_flags += ["Could not parse any raw data filenames."]
                else:
                    raw_files_list = parsed_metadata["Raw_Data"].split(", ")
                    n_raw = len(raw_files_list)

                if parsed_metadata["Filtered_Data"] == "":
                    filtered_files_list = []
                    quality_flags += ["Could not parse any filtered data filenames."]
                else:
                    filtered_files_list = parsed_metadata["Filtered_Data"].split(", ")
                    n_filtered = len(filtered_files_list)

                if n_raw == n_filtered:
                    pass
                else:
                    quality_flags += ["Mismatch between number of raw and filtered source files."]
                    # TODO: Some READMEs for co-assemblies only list filtered input files; look these up by name in
                    #  the rest of parsed_metadata_df to obtain their raw data filenames & libraries, which should
                    #  resolve at least some of the mismatches.


                # Miscellaneous quality checks:
                # Raw reads metadata, in order of increasing specificity:
                #   sequencing project < sequencing tech < library < filename/run
                # So, try to match by filename if possible, but can use libraries if that doesn't work.

                # Initial check (easiest, but not as rigorous): See if the libraries match between GOLD APs and READMEs
                # TODO: Be able to handle co-assemblies with multiple libraries, which may not be listed in the same order
                #  (these aren't used in MAGs v1 or v2, so are lower priority)
                if libraries_gold == "":
                    quality_flags += ["No libraries listed in GOLD AP; cannot test for match to README."]

                elif libraries_gold == parsed_metadata["Library_fromREADME"]:  # best case
                    pass  # no quality flag needed

                elif parsed_metadata["Library_fromREADME"] == "":
                    quality_flags += ["Could not get library from README."]

                    if libraries_gold == parsed_metadata["library"]:  # if it matches the XML, it's probably still OK
                        pass
                    elif parsed_metadata["library"] == "":
                        quality_flags += ["Could not get library from XML output."]
                    else:
                        quality_flags += ["Libraries do NOT match between GOLD AP and XML."]
                else:
                    quality_flags += ["Libraries do NOT match between GOLD AP and README."]

            # At this point, assuming we're confident that we have all the raw files, they can be matched to runs.
            # Perhaps the library checks above should also factor into this (beyond just outputting quality flags)?
            if n_raw == n_filtered and n_raw > 0:
                raw_reads_df = pd.DataFrame.from_dict({"raw_reads_file": [os.path.basename(file) for file in raw_files_list]})

            elif sampleID_img != "":  # cannot use filenames; so instead look for SampleID__ (if it exists) in the spReport
                quality_flags += ["Attempting to find sources based on other available metadata, as no valid input files could be identified."]

                spReport_info_for_sampleID = spReport_df[(spReport_df["SampleID__"] == sampleID_img) &
                                                            (spReport_df["JGI_spReport.RQC_Seq_Unit_State"] == "usable")][
                                                                ["JGI_spReport.RQC_Seq_Unit_Name",
                                                                 "JGI_spReport.Sequencing_Project_ID",
                                                                 "JGI_spReport.Library_Name",
                                                                 "JGI_spReport.Total_Bases",
                                                                 "JGI_spReport.NCBI_SRA_Run_Accession_ID",
                                                                 "SampleID__"]]

                # Before using this result, filter and/or check to ensure it matches existing info
                if libraries_gold != "":
                    quality_flags += ["Using Library Names from GOLD, as these are usually accurate."]
                    libraries_gold_list = libraries_gold.split(", ")

                    # Filter spReport_info_for_sampleID so that it only includes the relevant library(s)
                    spReport_info_for_sampleID = spReport_info_for_sampleID[
                        spReport_info_for_sampleID['JGI_spReport.Library_Name'].isin(libraries_gold_list)]

                elif row["IMG.ITS_SP_ID"] != "":
                    quality_flags += ["Attempting to identify potential source run(s) based on the presence of a "
                                      "SP ID in spReport that matches the IMG record (which may not be accurate, "
                                      "but is the only available option)."]

                    if row["IMG.ITS_SP_ID"] in list(spReport_info_for_sampleID["JGI_spReport.Sequencing_Project_ID"]):
                        # Only check if the IMG.ITS_SP_ID is present in the spReport, as other SP IDs could also be relevant
                        if len(spReport_info_for_sampleID)>1:
                            error_str = ("WARNING: Multiple potentially-matching runs were found in spReport. "
                                         "Although at least one of them has a Sequencing Project ID that matches "
                                         "the ITS_SP_ID listed in IMG, all runs are included in the result, as IMG "
                                         "does not always list all source SPs. Check all these runs manually to be "
                                         "sure they're relevant.")
                            logging.warning(f"{img_id}: {error_str}")
                            quality_flags += [error_str]
                        else:
                            quality_flags += ["Found exactly one usable run for this sample, which also has a matching "
                                              "SP ID; this run is therefore most likely accurate."]
                    else:
                        quality_flags += ["No runs were found with this ITS_SP_ID."]
                        # the filter below then essentially empties out spReport_info_for_sampleID
                        spReport_info_for_sampleID.drop(index=spReport_info_for_sampleID.index, inplace=True)

                else:
                    error_str = "Could not find ANY usable information (source files, libraries in GOLD, or ITS_SP_ID) for identifying specific source runs."
                    quality_flags += [error_str]
                    logging.error(f"{img_id}: {error_str}")
                    # empty out spReport_info_for_sampleID, as the info can't be verified
                    spReport_info_for_sampleID.drop(index=spReport_info_for_sampleID.index, inplace=True)

                # Put the final result into a simple dataframe with only the filenames, so that the same code below
                #  can be used for raw_reads_df as if the filenames had been obtained from READMEs (even though this
                #  will involve re-obtaining the same detailed spReport info as before).
                raw_reads_df = pd.DataFrame.from_dict({"raw_reads_file": list(spReport_info_for_sampleID["JGI_spReport.RQC_Seq_Unit_Name"])})

            else:  # sampleID_img == "":
                error_str = "Cannot use SampleID__ (as it is unknown), and finding AP sources based on input files has also failed."
                quality_flags += [error_str]
                logging.error(error_str)
                raw_reads_df = pd.DataFrame()

            # raw_reads_df should now either contain the final list of source filenames for the Analysis Project, or
            #  be empty if no set of filenames could be confidently identified.
            if len(raw_reads_df) > 0:
                for ix, rw in raw_reads_df.iterrows():
                    # reset previously-assigned variables (just in case)
                    sampleID_spReport = ""
                    library_spReport = ""
                    bases_spReport = ""
                    model_spReport = ""
                    model = ""
                    biosample = ""
                    srr_spReport = ""
                    run = ""

                    fname = rw["raw_reads_file"]

                    # Look up file in spReport
                    spReport_info = spReport_df.loc[spReport_df["JGI_spReport.RQC_Seq_Unit_Name"] == fname,
                                                    ["JGI_spReport.RQC_Seq_Unit_Name",
                                                     "JGI_spReport.Library_Name",
                                                     "JGI_spReport.Total_Bases",
                                                     "JGI_spReport.Actual_Sequencer_Model",
                                                     "JGI_spReport.NCBI_SRA_Run_Accession_ID",
                                                     "JGI_spReport.NCBI_SRA_Accession_ID",
                                                     "SampleID__"]]

                    if len(spReport_info)==1:
                        library_spReport = spReport_info["JGI_spReport.Library_Name"].squeeze()
                        bases_spReport = spReport_info["JGI_spReport.Total_Bases"].squeeze()
                        model_spReport = spReport_info["JGI_spReport.Actual_Sequencer_Model"].squeeze()
                        srp_spReport = spReport_info["JGI_spReport.NCBI_SRA_Accession_ID"].squeeze()
                        srr_spReport = spReport_info["JGI_spReport.NCBI_SRA_Run_Accession_ID"].squeeze()
                        sampleID_spReport = spReport_info["SampleID__"].squeeze()

                        if sampleID_spReport != sampleID_img:  # check for match to existing ID
                            error_str = f"Mismatched SampleID__ in IMG ({sampleID_img}) and spReport ({sampleID_spReport})."
                            quality_flags += [error_str]
                            logging.error(error_str)
                            bad_result = True

                        # Look up this result in NCBI - Even if the spReport contains an SRR, we still want to make sure the other info matches
                        if bases_spReport != "":
                            ncbi_info = ncbi_df.loc[(ncbi_df["library_name"] == library_spReport) & (ncbi_df["bases"] == bases_spReport)]
                        else:
                            ncbi_info = ncbi_df.loc[(ncbi_df["library_name"] == library_spReport)]

                        if len(ncbi_info) == 1:
                            run = ncbi_info["run"].squeeze()
                            model = ncbi_info["model"].squeeze()
                            biosample = ncbi_info["biosample"].squeeze()
                            if srr_spReport not in ["", np.nan]:
                                if srr_spReport != run:
                                    quality_flags += ["The SRRs don't match between the spReport and the matching run in NCBI (based on library, & # of bass if available); using the SRR in NCBI."]
                                    logging.error(f"Found an SRR in spReport ({srr_spReport}) that differs from the SRR in NCBI ({run}) with the same library & bases.")

                        elif len(ncbi_info)==0:
                            # No results in NCBI.
                            # Before giving up, try omitting the bases from search, and accept the result (with a quality flag) if there is an SRR in the spReport that matches
                            ncbi_info_redo = ncbi_df.loc[(ncbi_df["library_name"] == library_spReport) & (ncbi_df["run"] == srr_spReport)]

                            if len(ncbi_info_redo) == 1:
                                run = ncbi_info_redo["run"].squeeze()
                                model = ncbi_info_redo["model"].squeeze()
                                biosample = ncbi_info_redo["biosample"].squeeze()
                                error_str = "Found an SRR in spReport with a library match to NCBI, but the number of bases doesn't match between these sources; using it anyway."
                                quality_flags += [error_str]
                                logging.warning(error_str + " (" + run + "; spReport: " + bases_spReport + "; ncbi: " + ncbi_info_redo["bases"].squeeze() + ")")

                            else:
                                run = fname
                                model = model_spReport
                                biosample = "UNKNOWN:" + srp_spReport
                                error_str = "Run not found in NCBI; substituting filename in output instead."
                                quality_flags += [error_str]
                                logging.error(error_str + " (filename: " + fname + "; library: " + library_spReport + "; bases: " + bases_spReport + ")")
                        else:
                            run = fname
                            model = model_spReport
                            biosample = "UNKNOWN:" + srp_spReport
                            logging.error(f"Multiple runs located with library {library_spReport} and {bases_spReport} bases.")
                            quality_flags += ["ERROR: Multiple runs found in NCBI with this library and # of bases; substituting filename in output instead."]

                        # Compare sequencer models (though the string matches won't be perfect).
                        if model != model_spReport:
                            # check special case of "RSII" vs "RS II", which are actually the same
                            if ("RS II" in model.upper()) and ("RSII" in model_spReport.upper()):
                                pass
                            else:
                                # otherwise, check whether substrings match as-is
                                for model_type in ["HISEQ", "NOVASEQ", "NEXTSEQ", "MISEQ", "RS II", "SEQUEL"]:
                                    # For each model_type string, check whether its presence or absence is the same in both sources
                                    if (model_type in model.upper()) != (model_type in model_spReport.upper()):
                                        logging.error(f"Mismatched sequencer models for run {run}. NCBI: '{model}'; spReport: {model_spReport}")
                                        quality_flags += ["Mismatched sequencer models in NCBI and spReport."]
                                        bad_result = True  # added on 2025-01-24; but as of 2024-10-30 (when this script was run to produce output that was subsequently used), there weren't any of these
                                        break

                        # Finish out the "model" string to also include the machine brand (Illumina or PacBio)
                        # model_spReport doesn't include this; model (from NCBI) sometimes does.
                        if model.startswith("Illumina") or model.startswith("PacBio"):
                            pass
                        elif model.startswith("HiSeq") or model.startswith("NovaSeq") or model.startswith("NextSeq") or model.startswith("MiSeq"):
                            model = "Illumina " + model
                        elif model.startswith("RS II") or model.startswith("RSII") or model.upper().startswith("SEQUEL"):
                            model = "PacBio " + model
                        else:
                            logging.error(f"Undefined sequencer model: {model}")
                            # (the model string will still be used as-is)

                        # Just to be sure, it's probably a good idea to make sure the other info matches?
                        # Except there are no other 100% identical fields that can be compared.
                        # So, just save the spReport info & run to the raw_reads_df.

                        raw_reads_df.loc[ix, "SampleID__spReport"] = sampleID_spReport
                        raw_reads_df.loc[ix, "library_spReport"] = library_spReport
                        raw_reads_df.loc[ix, "bases_spReport"] = bases_spReport
                        raw_reads_df.loc[ix, "model_spReport"] = model_spReport
                        raw_reads_df.loc[ix, "model_final"] = model
                        raw_reads_df.loc[ix, "biosample"] = biosample
                        raw_reads_df.loc[ix, "srr_spReport"] = srr_spReport
                        raw_reads_df.loc[ix, "run_id_final"] = run

                    elif len(spReport_info)==0:
                        error_str = "File not found in spReport"
                        quality_flags += [f"{error_str}."]
                        logging.error(f"{error_str}: {fname}")
                    else:
                        error_str = "Multiple copies of file found in spReport"
                        quality_flags += [f"{error_str}."]
                        logging.error(f"{error_str}: {fname}")

                source_runs_final = ', '.join(list(raw_reads_df["run_id_final"]))
                source_models_final = ', '.join(list(raw_reads_df["model_final"]))
                #source_biosamples_final = ', '.join(list(set(list(raw_reads_df["biosample"]))))  # should only be 1
                # Note on biosamples: Coassemblies have multiple source_biosamples_final, and list(set(list())) changes the order in an unpredictable way (i.e. differently every time this script is run)!
                #  This inconsistency ended up in the 2024-10-30 output file (which was used elsewhere), though the affected samples were all flagged with bad_result anyway (due to mismatched SampleID), so it shouldn't have affected any downstream analyses.
                #  The code below (which only consolidates duplicate BioSamples if ALL values are the same) was substituted on 2025-01-24.
                if len(list(set(list(raw_reads_df["biosample"])))) == 1:
                    source_biosamples_final = list(raw_reads_df["biosample"])[0]
                else:
                    source_biosamples_final = ', '.join(list(raw_reads_df["biosample"]))

                # Can also output more of the raw_reads_df info, if needed for more debugging

                # Finally, verify that all runs have the same SampleID__ in spReport
                if len(set(raw_reads_df["SampleID__spReport"])) == 1:
                    pass
                else:
                    error_str = "Multiple SampleIDs in spReport for the same grouping of raw reads files."
                    quality_flags += [error_str]
                    logging.error(error_str)

            # Split Assembly_Method into method & version for NCBI-formatted MAG lists
            assembly_method_and_version = row["Assembly_Method"]
            assembly_method_components = assembly_method_and_version.split(" v. ")
            if len(assembly_method_components) != 2:
                logging.error(f"Invalid assembly method: '{assembly_method_and_version}'")
                quality_flags += ["Could not parse Assembly_Method."]
            else:
                assembly_method = assembly_method_components[0]
                assembly_method = assembly_method.replace("spades", "SPAdes").replace("metaflye", "metaFlye")  # TODO: Other replacements?
                assembly_method_version = assembly_method_components[1]

            # Prefix the quality_flags with important flag markers (if applicable)
            if source_runs_final == "":
                quality_flags = ["NOT PARSED:"] + quality_flags  # append to beginning of quality flags list

            if bad_result:
                quality_flags = ["ERROR: BAD RESULT; DO NOT USE."] + quality_flags  # append to beginning of quality flags list

            gold_ap_df.loc[index, "SampleID__spReport"] = sampleID_spReport
            gold_ap_df.loc[index, "source_biosample"] = source_biosamples_final
            gold_ap_df.loc[index, "source_run_IDs"] = source_runs_final
            gold_ap_df.loc[index, "sequencing_technology"] = source_models_final
            gold_ap_df.loc[index, "assembly_method"] = assembly_method
            gold_ap_df.loc[index, "assembly_method_version"] = assembly_method_version
            gold_ap_df.loc[index, "QualityFlag__"] = " ".join(quality_flags)
            gold_ap_df.loc[index, "n_raw_files"] = n_raw
            gold_ap_df.loc[index, "n_filtered_files"] = n_filtered


    # filter to only include the relevant results
    gold_ap_df = gold_ap_df.loc[gold_ap_df["IMG_Taxon_ID"].isin(img_IDs_to_parse)]

    # Final cleanups:
    # Consolidate redundant SampleID__ columns
    if any(gold_ap_df["SampleID__IMG"] != gold_ap_df["SampleID__spReport"]):
        logging.error("Mismatched SampleID__(s) found between IMG and spReport. Those IDs will not be saved under SampleID__, and their output should not be used.")
        # but still saving the different SampleIDs under their original separate columns, & assigning SampleID__ where they DO match
        for index, row in gold_ap_df.iterrows():
            if row["SampleID__IMG"] == row["SampleID__spReport"]:
                gold_ap_df.loc[index, "SampleID__"] = row["SampleID__IMG"]
            else:
                gold_ap_df.loc[index, "SampleID__"] = ""
    else:
        gold_ap_df.drop(columns=["SampleID__spReport"], inplace=True)
        gold_ap_df.rename(columns={"SampleID__IMG": "SampleID__"}, inplace=True)

    # write to file
    today = datetime.now().strftime('%Y%m%d')
    save_fp = f"test_data/IsoGenie_datasets/private_data/sequencing/output/JGI_AnalysisProject_source-runs_{today}.tsv"
    gold_ap_df.to_csv(save_fp, sep="\t", index=False)
