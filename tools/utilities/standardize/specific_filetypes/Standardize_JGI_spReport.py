#!/usr/bin/env python
"""
OVERVIEW:
Standardize a Sample Report ("spReport") file obtained from the JGI Genome Portal.


This script was created by Dr. Suzanne Hodgkins and Dr. Ben Bolduc, with support from the DOE-funded IsoGenie Project,
 NASA-funded A2A Project, and NSF-funded EMERGE Project. Copyright © 2023 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import os
import sys
import pandas as pd
import numpy as np
import re
from datetime import datetime

# import other variables by filepath (from the root isogenie-tools directory)
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../..')))
from tools.data.common_vars import *
from tools.utilities.utils import error, warning, lookup_id_from_df, parse_official_SampleID
from tools.utilities.standardize.Standardize_Coring_Sheets import generate_official_SampleID

def parse_datecode(datecode_str, verbose=False):
    # Parse a 3-number datecode in the format myy (e.g. 718 = July 2018), returning a valid FieldSampling__ string.
    try:
        datecode = int(datecode_str)
        yearcode = datecode % 100
        monthcode = int(datecode_str[0])
        if yearcode < 10 or yearcode > int(datetime.now().strftime('%y')):
            # should be no years before 2010, or after the current year
            if verbose:
                warning(f"Invalid 3-character date code: {datecode_str}")
            return np.nan
        if monthcode < 5 or monthcode > 8:
            # all dates that use these codes should be between May and August
            if verbose:
                warning(f"Invalid 3-character date code: {datecode_str}")
            return np.nan

        # at this point, the code should be valid, so can parse it
        fieldsampling = "20" + datecode_str[1:3] + "-0" + datecode_str[0]
        return fieldsampling

    except ValueError:
        if verbose:
            warning(f"Invalid 3-character date code: {datecode_str}")
        return np.nan

def parse_samplecode(samplecode):
    # Parse canonical IsoGenie sample codes (WITHOUT dates) in various formats; e.g.:
    #   P1S
    #   S-2-D
    #   C-3-X1
    #   C1 20-24
    # The site code component must be present in site_syn.
    # Depth can either be a code (assuming it's been recorded somewhere), or a numeric range preceded by a space.
    # This function is in beta and may not yet successfully parse *all* formats, but can be expanded provided that
    #  backward compatibility is maintained.
    # The reason for not including dates is so that the function can be re-used to parse codes where the dates are
    #  omitted because they would be all the same.

    samplecode = samplecode.strip()  # remove any beginning & end spaces

    sample_dict = {
        "Site__": np.nan,
        "Core__": np.nan,
        "DepthCode__": np.nan,
        "Depth__": np.nan,  # to be filled here only for numeric ranges e.g. "10-14"
        "DepthMin__": np.nan,
        "DepthMax__": np.nan
    }

    sitecode = None
    corecode = None
    depthcode = None
    depthrange = None
    depthmin = None
    depthmax = None

    # TODO: other formats?
    components = samplecode.split(" ")
    if len(components) == 1:
        if re.search(r'^[PSEC][1-3][SMD]$', samplecode) is not None:
            # something like "S2S"
            sitecode = samplecode[0]
            corecode = samplecode[1]
            depthcode = samplecode[2]

        elif re.search(r'_', samplecode) is None:
            # if no underscores, then can safely try splitting by "-" to get the codes
            components_h = samplecode.split("-")

            if len(components_h) == 3:
                # e.g. "CPON2-1-X0"
                sitecode = components_h[0]
                corecode = components_h[1]
                depthcode = components_h[2]

    elif len(components) == 2:
        # e.g.:
        # AvC-F1(2) 20-24
        # AvC-C3 30-34
        # PHB-Rim2 100-104
        # P1 20-24

        # Assume the second component is always the depth range (but check to be sure)
        if re.search(r'^\d{1,3}-\d{1,3}$', components[1]) is not None:
            # range of integers, each with 1-3 digits
            depthrange = components[1]
            depthmin = depthrange.split("-")[0]
            depthmax = depthrange.split("-")[1]

            # First component will only be parsed if the second one (depthrange) is valid.
            # This is harder to parse, as the site/core separation format can vary.
            # Easiest case is if it ends with a digit enclosed in parentheses; then the core component is the # in parentheses
            if re.search(r'.*\(\d\)$', components[0]) is not None:
                sitecode = re.sub(r'\(\d\)$', "", components[0])
                corecode = components[0][-2]

            # Next, see if it ends with a digit (preceded by a non-digit)
            elif re.search(r'.*\D\d$', components[0]) is not None:
                sitecode = components[0][0:-1]
                corecode = components[0][-1]

            # Otherwise, still don't know how to parse
            else:
                pass

        else:  # if second component isn't formatted like a depth range, then can't parse without more format cases
            pass

    else:
        # can't yet process >2 components
        pass

    # Final assignment

    # Site
    # TODO: Maybe convert site_syn into a 1-level dict in the form {synonym: official name} (created inside
    #  common_vars) so that it's simpler to search, and duplicated synonyms can be caught?
    #  Might not be able to use it in all cases immediately (e.g. the alternate synonym option in
    #  Standardize_Chemistry_Sheets.py), but could be good in many cases.
    if sitecode is not None:
        for site_name, variants in site_syn.items():
            if sitecode in variants:
                sample_dict["Site__"] = site_name
                break

    # Core (probably best to leave as a string for maximum compatibility)
    if corecode is not None:
        sample_dict["Core__"] = corecode

    # Depths - Parse from depthcode if one exists
    if depthcode is not None:
        # TODO: If expanding this function to parse IDs with X, Y, Z depth codes, uncomment the below (and indent after "else")
        #  (in the EMERGE-DB, those depth codes are denoted under Sequencing_DepthCode-1__)
        # if depthcode in ['X', 'Y', 'Z']:
        #     # alternate depth codes used instead of X0, X1, X2 by some ACE/QUT IDs
        #     # note that for 2011-2012 samples, 'X' is also the DepthCode__; maybe this should be handled separately?
        #     sample_dict.update({"Sequencing_DepthCode-1__": depthcode})
        # else:
        sample_dict["DepthCode__"] = depthcode

    if depthrange is not None:
        sample_dict["Depth__"] = depthrange

    # Note that depthmin and depthmax still might NOT yet have the correct format for direct input into the DB
    #  (e.g. could be "05" instead of just "5"). This will be cleaned up during creation of SampleID__, but just
    #  flagging that DepthMin__ and DepthMax__ generated here should not be used for any other purpose!

    if depthmin is not None:
        sample_dict["DepthMin__"] = depthmin

    if depthmax is not None:
        sample_dict["DepthMax__"] = depthmax

    return sample_dict

def standardize_jgi_spReport(spReport_fp, db_depthinfo_fp, other_metadata_fps=None, keep_only_usable=False, save_output=False):
    # spReport_fp is the filepath of the JGI Sample Report file.
    # db_depthinfo_fp should be a query result file from the EMERGE-DB, and can be for either the 'Depth-Info',
    #   'Soil-Depth', or any other nodes containing the properties "Sequencing_SampleID-2__" and "SampleID__".
    # other_metadata_fps should be a list of filepaths for CSVs containing additional metadata, e.g. from incubations,
    #   each of which must have the columns "Identifier" (which should match JGI's sample IDs minus "metaG" etc.) and "SampleID__"

    # import, rename columns, and sort the spReport
    spReport = pd.read_csv(spReport_fp, sep=',', quotechar='"', na_filter=False, dtype=object)
    spReport.columns = ["JGI_spReport." + col.replace(" ", "_") for col in spReport.columns]
    spReport = spReport.sort_values(by=['JGI_spReport.Sequencing_Project_Name',
                                        'JGI_spReport.Actual_Sequencer_Model',
                                        'JGI_spReport.NCBI_SRA_Accession_ID',
                                        'JGI_spReport.Total_Bases'])


    # Filter out any rows missing an "Actual Sequencer Model", as this likely means that no data exists.
    # As of now, nothing missing an Actual Sequencer Model has any RQC Unit State or SRA accessions, but some with
    #  non-"usable" RQC Unit State still have an Actual Sequencer Model (and some even have SRA accessions).
    # Therefore, filtering by "Actual Sequencer Model" leaves behind a larger subset of rows, so is probably a "safer"
    #  one to filter on.
    # TODO (NOTE): The final Metadata node for the spReport should still include the original unedited file, just in case.
    #  E.g., "in-progress" samples may just have yet to receive this piece of metadata.
    spReport = spReport[spReport["JGI_spReport.Actual_Sequencer_Model"] != ""]
    # OPTIONAL (via input argument): Delete any rows that don't have "usable" RQC Seq Unit State.
    #   This *could* mean that no good data exists, but not sure (e.g. some viral metaGs have this value as "bad" or
    #   missing, yet still have SRA accessions listed).
    if keep_only_usable:
        spReport = spReport[spReport["JGI_spReport.RQC_Seq_Unit_State"] == "usable"]

    # NEXT STEP: Just get the unique sample IDs and parse them (starting with the groups that were already handled
    #  in the Sample Metadata Sheet code, to get them out of the way), converting to full SampleID__ whenever possible.
    # Then only after that, start parsing sequencing methods and collapsing the duplicate rows, etc.

    # Make a column with *only* the sample name (eliminating the text "metaG", etc.)
    # As of Sept 26 2023, "Final Deliverable Project Name" should always equal "Sequencing Project Name"
    spReport["JGI_sample_name_only"] = spReport["JGI_spReport.Final_Deliverable_Project_Name"].str.replace(
        " metaG", "").str.replace(" MetaG", "").str.replace(" metaT", "")

    # First look up the IDs where it's possible to do so from the DB.
    #
    # Import the DB query output containing "Sequencing_SampleID-2__" and "SampleID__"
    db_depthinfo = pd.read_csv(db_depthinfo_fp, sep=',', quotechar='"', na_filter=False, dtype=object)

    # got this to work where exact matches exist, but it doesn't find everything...
    # TODO: not sure if it's better to make separate columns for each method of obtaining SampleID__ before combining them?
    # arguments for lookup_id_from_df() below are:
    #   value_lookup = spReport['JGI_sample_name_only'] (individual values from this column)
    #   colname_lookup = 'Sequencing_SampleID-2__'
    #   colname_result = 'SampleID__'
    #   df = db_depthinfo
    spReport["depthinfo_SampleID__"] = spReport["JGI_sample_name_only"].apply(lookup_id_from_df,
                                                                              args=('Sequencing_SampleID-2__',
                                                                                    'SampleID__',
                                                                                    db_depthinfo,))

    # Next try parsing the IDs of known incubation sets (e.g. SIP)
    if other_metadata_fps is not None:
        if type(other_metadata_fps) == str:
            other_metadata_fps = [other_metadata_fps]

        file_number = 0
        for other_metadata_fp in other_metadata_fps:
            file_number += 1
            colname_SampleID = "other" + str(file_number) + "_SampleID__"
            other_metadata_df = pd.read_csv(other_metadata_fp, sep=',', quotechar='"', na_filter=False, dtype=object)
            spReport[colname_SampleID] = spReport["JGI_sample_name_only"].apply(lookup_id_from_df, args=('Identifier', 'SampleID__', other_metadata_df,))

    # Merge the separate SampleID__ columns
    sampleID_cols = [col for col in spReport.columns if col.endswith('_SampleID__')]
    for index, row in spReport[sampleID_cols].iterrows():
        id_list = [id for id in list(row) if id not in ["NaN", "", np.nan]]
        id_list = list(set(id_list))  # eliminate any duplicates just in case

        if len(id_list) == 1:
            id_final = id_list[0]
        elif len(id_list) == 0:
            id_final = ""
        else:
            # This shouldn't ever happen, but just in case...
            id_final = ""
            print(id_list)
            error("Multiple conflicting SampleID__'s found (see above).")

        spReport.loc[index, "SampleID__"] = id_final
        if not(id_final == ""):
            spReport.loc[index, "SampleID_flag"] = "SampleID__ assigned based on simple lookup of JGI sample name."
            # TODO: Use the SampleID__ to get full sample metadata.
            #  For field samples, do this now. For incubations, can do this later but still add a flag that it's an incubation.

            # look up sample info in db_depthinfo from id_final
            sample_dict = parse_official_SampleID(sampleID=id_final, data_source=db_depthinfo, parse_str=True)
            for key, value in sample_dict.items():
                # assign all standardized properties in sample_dict
                spReport.loc[index, key] = value


    # remove the temporary *_SampleID__ columns
    spReport = spReport.drop(columns=sampleID_cols)

    # # create a TEMPORARY export dataframe with what we've done so far
    # temp_export_cols = ["JGI_spReport.Final_Deliverable_Project_Name", "JGI_sample_name_only", "SampleID__"]
    # # maybe also add "Sample Name"?
    # temp_export_df = spReport[temp_export_cols].drop_duplicates()

    # Next try parsing the info from remaining field samples to see if any more IDs can be obtained that way
    # get unique JGI IDs that haven't yet been assigned a SampleID__, to simplify iteration
    unassigned_jgi_IDs = list(spReport[(spReport["SampleID__"].isin(["", "NaN", np.nan]))]["JGI_sample_name_only"].unique())

    for jgi_ID in unassigned_jgi_IDs:

        # first define a sampleID_flag to be stored in the output, for various imperfect SampleID__ scenarios
        sampleID_flag = "not assigned"  # default in case it's never assigned

        sample_dict = None
        jgi_ID_orig = jgi_ID  # in case it's changed later

        # Previous note (likely won't do after all): Possibly move much of the code below inside parse_samplecode(), so that year-dependent sample codes can be dealt with there.

        # first remove "Permafrost " from a few IDs that look like "Permafrost 712P3D"
        if re.search(r'^Permafrost \d{3}\D\d\D$', jgi_ID) is not None:
            jgi_ID = jgi_ID.replace("Permafrost ", "")

        if re.search(r'^\d{3}\D+.*$', jgi_ID) is not None:
            # 3 digits followed by non-digit(s) (plus anything else after that) = most likely a date code

            # # DEBUG
            # if jgi_ID == "718 P3 50-54":
            #     print("attempting to get info...")

            fieldsampling = parse_datecode(jgi_ID[0:3], verbose=True)
            sample_dict = parse_samplecode(jgi_ID[3:len(jgi_ID)])  # this includes everything from immediately after the 3-digit code until the end (i.e., it may or may not start with a space)
            sample_dict.update({"FieldSampling__": fieldsampling})

        elif re.search(r'^20\d{2}\D+.*$', jgi_ID) is not None:
            # "20" plus 2 more digits followed by non-digit(s) (plus anything else after that) = most likely a year

            sample_dict = parse_samplecode(jgi_ID[4:len(jgi_ID)])

            # Since the date is year-only, assume it's July
            # TODO: Need to see if there's anything where that's not the case!
            fieldsampling = jgi_ID[0:4] + "-07"
            sample_dict.update({"FieldSampling__": fieldsampling})

        elif jgi_ID == "P3-2":
            # Special case: This sample has NO date info in the spReport, nor in most easily-accessible metadata lists.
            # The BioSample was located in NCBI and found to be from May 2012, and a close match was found in the DB
            #  with Sequencing_SampleID-1__ == "20120500_P32". SampleID__ was assigned based on this match.
            seq_proj_ids = spReport.loc[(spReport['JGI_sample_name_only'] == jgi_ID), "JGI_spReport.Sequencing_Project_ID"].unique()

            # make absolutely sure that it's this one known Sequencing Project before assigning SampleID__
            if len(seq_proj_ids) == 1 and seq_proj_ids[0] == "1132130":
                sample_dict = {
                    "FieldSampling__": "2012-05",
                    "SampleID__": "MainAutochamber.201205_P_3_10to15"
                }

            else:
                pass


        else:
            pass  # for now

        # Verify sample_dict is complete and accurate

        if sample_dict is None:
            sampleID_flag = "Could not parse any meaningful sample info from the JGI ID using existing logic."

        else:
            # First see if it was already pre-assigned; this should ONLY occur in a few special cases.
            if "SampleID__" in sample_dict.keys():

                if sample_dict["SampleID__"] not in [np.nan, 'NaN']:
                    sampleID_flag = "SampleID__ was pre-assigned based on manual lookup of sample info. If this flag is unexpected, check the sample and try again."

                    # look up remainng sample info from db_depthinfo
                    sample_dict.update(parse_official_SampleID(sampleID=sample_dict["SampleID__"], data_source=db_depthinfo, parse_str=True))

                else:
                    sampleID_flag = "SampleID__ was pre-assigned a nonexistent value. This should not have happened; check the sample and try again."


            # Most "normal" case:
            # First see if it's "complete", i.e. has FieldSampling, Site, Core, and at least one Depth property
            #  (no need to separately check DepthMin__ and DepthMax__ because here they're directly derived from Depth__,
            #  and the number format is also more likely to differ from the DB).
            elif sample_dict["FieldSampling__"] not in [np.nan, 'NaN'] and \
                    sample_dict["Site__"] not in [np.nan, 'NaN'] and \
                    sample_dict["Core__"] not in [np.nan, 'NaN'] and \
                    (sample_dict["Depth__"] not in [np.nan, 'NaN'] or sample_dict["DepthCode__"] not in [np.nan, 'NaN']):

                # First try to get more info from the official query output,
                # e.g., to get the ACTUAL sampling date, as these (not FieldSampling__) are what the SampleID__ actually uses

                relevant_samples = db_depthinfo[
                        (db_depthinfo["FieldSampling__"] == sample_dict["FieldSampling__"]) & (~db_depthinfo["FieldSampling__"].isna()) &
                        (db_depthinfo["Site__"] == sample_dict["Site__"]) & (~db_depthinfo["Site__"].isna()) &
                        (db_depthinfo["Core__"] == sample_dict["Core__"]) & (~db_depthinfo["Core__"].isna())]

                # filter depth separately since it's not always in the same lookup key
                if sample_dict["DepthCode__"] not in [np.nan, 'NaN']:
                    # try DepthCode__ first, since numeric Depth__ sometimes varies slightly
                    relevant_samples = relevant_samples[(relevant_samples["DepthCode__"] == sample_dict["DepthCode__"]) & (~relevant_samples["DepthCode__"].isna())]

                elif sample_dict["Depth__"] not in [np.nan, 'NaN']:
                    relevant_samples = relevant_samples[(relevant_samples["Depth__"] == sample_dict["Depth__"]) & (~relevant_samples["Depth__"].isna())]

                relevant_samples.reset_index(drop=True, inplace=True)

                # ideally relevant_samples should have 1 row; check and flag various scenarios
                if len(relevant_samples) == 0:
                    sampleID_flag = "This sample does not appear to exist yet in the DB; if this is unexpected, check the exact depth and other formats. Proceeding to create a SampleID__ based on new info from JGI ID."
                    # In this scenario, we *should* still have the minimum info for creating a SampleID__ (even if
                    #  DepthMin__, DepthMax__, and FieldSampling__ aren't perfect). Worst case scenario, we might
                    #  only have DepthCode__ and therefore get an error, but hopefully won't run into this.

                    # Generate SampleID__ and related info (this just adds more properties to the existing sample_dict)
                    sample_dict = generate_official_SampleID(sample_dict)

                else:  # If there's something there, then can use it to look up exact dates, etc.
                    if len(relevant_samples) == 1:
                        sampleID_flag = "Successfully found exactly 1 matching sample in the DB."
                    else:  # len(relevant_samples) > 1
                        sampleID_flag = "WARNING: Found >1 matching sample in the DB; CHECK WHAT'S CAUSING THE DUPLICATES (possibly DepthSubsample?) before linking this new info to any existing samples."

                    # Regardless of if there's >1, just use the first row to obtain the exact date and generate SampleID__
                    # TODO: For any rows flagged as having >1 matching sample, check and see if just using the first
                    #   one is sufficient (thankfully there don't seem to be any like this).

                    # look up exact date
                    sample_dict["Date__"] = relevant_samples.loc[0, "Date__"]

                    # look up exact depths (this also fixes any number formats in sample_dict that are inconsistent with the DB)
                    sample_dict["Depth__"] = relevant_samples.loc[0, "Depth__"]
                    sample_dict["DepthMin__"] = relevant_samples.loc[0, "DepthMin__"]
                    sample_dict["DepthMax__"] = relevant_samples.loc[0, "DepthMax__"]
                    sample_dict["DepthAvg__"] = relevant_samples.loc[0, "DepthAvg__"]
                    sample_dict["DepthSubsample__"] = relevant_samples.loc[0, "DepthSubsample__"]

                    # Generate SampleID__ and related info (this just adds more properties to the existing sample_dict)
                    sample_dict = generate_official_SampleID(sample_dict)

                    if relevant_samples.loc[0, "SampleID__"] not in [np.nan, 'NaN', '']:
                        existing_sampleID = relevant_samples.loc[0, "SampleID__"]
                        new_sampleID = sample_dict["SampleID__"]
                        if existing_sampleID == new_sampleID:
                            sampleID_flag += " Found a matching SampleID__; reusing."
                        else:
                            sampleID_flag += f" WARNING: New SampleID__ ({new_sampleID}) does NOT match existing SampleID__ from the DB (f{existing_sampleID})."

            # Although there's a sample_dict, the info is incomplete
            else:
                sampleID_flag = "Could not create SampleID__, but some incomplete sample info was identified."

            # Fill the separate sample metadata properties
            for key, value in sample_dict.items():
                # assign all standardized properties in sample_dict (this includes SampleID__ if one was created)
                if key.endswith("__"):
                    spReport.loc[(spReport['JGI_sample_name_only'] == jgi_ID_orig), key] = sample_dict[key]


        # Fill relevant dataframe rows with sampleID_flag based on current jgi_ID.
        spReport.loc[(spReport['JGI_sample_name_only'] == jgi_ID_orig), "SampleID_flag"] = sampleID_flag


    if save_output:
        output_fp = spReport_fp.replace("official_input_lists", "output").replace(".csv", "_standardized.csv")  # put a breakpoint here in order to be able to view stuff

        print(f"Saving output to {output_fp}")
        spReport.to_csv(output_fp, index=False)

    return spReport

# For quick standalone standardization of JGI spReport (to combine with other sources, use Standardize_Sequencing_Metadata instead)
# Standardize_Sequencing_Metadata now uses save_output=True, so the __main__ below is no longer needed; but keeping for now just in case.
if __name__ == "__main__":

    spReport_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/spReport_AllMyProjects_20250128.csv"
    #spReport_fp = "test_data/IsoGenie_datasets/private_data/sequencing/official_input_lists/missing_sample_spReports/spReport_G.F.S.T75.r2.metaG_20240708.csv"
    db_soildepths_fp = "querying_output/official_query_output/cached-query_IsoGenie_server-download_20240609_Soil-Depth.csv"

    sip_incubation_fp = "test_data/IsoGenie_datasets/private_data/incubation_metadata/Hough_SIP/parsing_SIP_metaGs_v2.csv"
    chanton_incubation_fp = "test_data/IsoGenie_datasets/private_data/incubation_metadata/Chanton_substrates/20230213_updated_Rachel_incubation_DNA_extraction_SH-notes_formatted.csv"
    additional_ids_fp = "test_data/IsoGenie_datasets/private_data/sequencing/fromSamAroney_datasetsforibis/sample_naming_corrections_formatted-for-script-parsing.csv"

    additional_metadata_fps = [sip_incubation_fp, chanton_incubation_fp, additional_ids_fp]
    additional_metadata_fps = [fp for fp in additional_metadata_fps if fp != ""]  # gets rid of blank ones that haven't yet been created

    result = standardize_jgi_spReport(spReport_fp, db_soildepths_fp, additional_metadata_fps, save_output=True)

    # OLD: Output is now saved in the function itself.
    # output_fp = spReport_fp.replace("official_input_lists", "output").replace(".csv", "_standardized.csv")
    #
    # print(f"Saving output to {output_fp}")
    # result.to_csv(output_fp, index=False)

