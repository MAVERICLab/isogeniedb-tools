
import argparse
import pandas as pd
import numpy as np
import os

parser = argparse.ArgumentParser(description="Check for the existence of filepaths in a file_list CSV.")

inputs = parser.add_argument_group('Inputs')

inputs.add_argument('-d', '--db', dest='database', choices=['db', 'local', 'test'], default='local',
                    help='Machine (db, local, or test) on which this script is being run.')

inputs.add_argument('--file-list', dest='file_list', help='Filename of CSV containing file paths to validate.')

inputs.add_argument('--custom-filepath-cols', dest='custom_filepath_cols', default=[], nargs='+',
                    help='Optionally specify additional columns containing file paths (any columns ending in "href__" '
                         'are already checked by default).')


def validate_filepaths(file_list, db, custom_filepath_cols=None):

    if custom_filepath_cols is None:
        custom_filepath_cols = []

    df = pd.read_csv(file_list, sep=',', quotechar='"', na_filter=False)

    filepath_cols = [col for col in df.columns if col.endswith('href__')]
    filepath_cols += [col for col in custom_filepath_cols if col not in filepath_cols]

    unconfirmed_fps = []
    bad_fps = []
    good_fps = []

    for index, row in df.iterrows():
        for col in filepath_cols:

            fps = row[col]

            if fps in ['', np.nan]:
                continue

            fps_list = fps.split(', ')  # split multi-entry cells with multiple filepaths (which are separated by commas)

            for fp in fps_list:

                if db in ['local', 'test']:  # If running locally, change filepaths to Suzanne's local directory
                    if fp.startswith('/srv/'):
                        unconfirmed_fps += [fp]
                        continue
                    else:
                        if fp.startswith('/var/'):
                            fp = fp.replace('/var/www/isogenie/', '/home/suzanne/IsoGenieDB/Bitbucket/isogenie-web/')
                            fp = fp.replace('/var/www/a2a/', '/home/suzanne/IsoGenieDB/Bitbucket/a2a-web/')
                        else:  # assume the filepaths refer to local paths in test_data
                            fp = 'test_data/' + fp

                if os.path.exists(fp):
                    good_fps += [fp]
                else:
                    bad_fps += [fp]

    if unconfirmed_fps:
        print('The following filepaths could not be checked:')
        for fp in unconfirmed_fps:
            print(fp)

        print('Results for the remaining filepaths are shown below.\n')

    if bad_fps:
        print('Bad filepaths found:')
        for fp in bad_fps:
            print(fp)
        exit()

    else:
        print('Success! All {} filepaths are valid.'.format(len(good_fps)))


if __name__ == "__main__":

    args = parser.parse_args()

    validate_filepaths(file_list=args.file_list, db=args.database, custom_filepath_cols=args.custom_filepath_cols)
