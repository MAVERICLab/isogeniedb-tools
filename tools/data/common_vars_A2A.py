"""
@author: suzanne

Purpose: Stores all re-used variables and functions in one place.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""

import sys
import os

# append two '..' to the current directory of this file, because current directory is two levels down
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

from tools.data.common_vars import site_syn as stordalen_site_syn


GPMP_column_syn = {
    # To be used in a similar manner to column_syn in common_vars.py.
    # The following 3 columns represent unique identifiers, so they should be standardized.
    'GPMP.ID': ['GPMP.ID', 'GPMP#'],  # Since this ends in "ID", it will be propagated to downstream nodes, which is
                                      #  fine since Jamie flagged it to use for sample matching anyway. So this column
                                      #  should be renamed to "GPMP.ID" if it's "GPMP#" or some other variant.
    'Sample.Name': ['Sample.Name', 'Sample'],  # GPMP_SampleID__
    'Core.Name': ['Core.Name', 'Core', 'Core.Code.Name'],  # Core__
    # The rest of the columns from here on (especially site-related columns) may be inconsistently defined, so they
    #  shouldn't be used to actually rename any columns, but just to assign standardized metadata columns.
    'Date.Sampled': ['Date.Sampled', 'Date.Collected'],
    'Latitude': ['Latitude', 'Latitude(decimal.degrees)'],
    'Longitude': ['Longitude', 'Longitude(decimal.degrees)'],
    # Wetland, site, subsite, etc. names are tricky, because they're not always partitioned the same way.
    # The following location designations are ordered from broadest to narrowest, and are based on the SiteInfo,
    #  AncillaryData, and Vegetation sheets. The same column name might be used for a different "level" in different
    #  sheets, but if >1 of the names below are used in the same sheet, they should at least be in the order below.
    'Wetland.Cluster': ['Wetland.Cluster', 'General.Location'],
    'Wetland': ['Wetland', 'Site.Name'],
    'Site': ['Site', 'Subsite.Or.Unique.Collection'],
    'Site.Notes': ['Site.Notes'],
    'Plot': ['Plot', 'Experiment.Plot.Name'],
    # Habitats: Need to decide which sheet to use to define these (probably AncillaryData, as this assigns different
    #  habitats by core in some cases).
    'Habitat': ['Habitat', 'Peatland.Habitat', 'Peatland.Habitat.Rough.Categories.For.Analysis']
}

# Unlike in Stordalen, the same "Site" may sometimes have different Habitats for each Core, with no
#  sub-sites separating the habitats. This is bad and should be avoided whenever possible, but for some
#  areas (e.g. Smith Creek, site "Permafrost-Bog Edge Gradient 1, Permafrost Peat Plateau"), there's no
#  good way around it.
# Therefore, don't assign habitats based on sites, but instead based on the Habitat column.
# Then when importing, check to make sure there's only one habitat per site before assigning Habitat at
#  the Site level, and if there's >1, wait to assign until the Core level.

# TODO: Might want to transition away from always associating a Site with just one Habitat in general, to
#  be able to account for habitat change within a site over time!

# For simplicity, just make each sub-Area its own Area. There's few enough that they can be easily separated or combined
#  if needed. Just make an AreaGroup__ property, based on the below, so this can be done easily.

area_groups = {
    'APEX-Alpha': 'APEX',
    'APEX-Beta': 'APEX',
    'APEX-Gamma': 'APEX',
    'Kaamanen': 'Finnish Lapland',
    'Kiposuo': 'Finnish Lapland'
}

area_syn = {
    'Stordalen': ['Stordalen'],
    'APEX-Alpha': ['APEX-Alpha', 'APEX Alpha', 'APEX alpha'],
    'APEX-Beta': ['APEX-Beta', 'APEX Beta', 'APEX beta'],
    'APEX-Gamma': ['APEX-Gamma', 'APEX Gamma', 'APEX gamma'],
    'Kaamanen': ['Kaamanen'],
    'Kiposuo': ['Kiposuo'],
    'Smith Creek': ['Smith Creek', 'Smith creek', 'Wrigley'],
    'Lutose': ['Lutose']
}

# Partition each Area into Sites.
a2a_site_syn = {
    'Stordalen': stordalen_site_syn,
    'APEX-Alpha': {
        'Control Water Table Plot': ['Control Plot', 'APEX Alpha Control plot', 'Control water table'],
        'Lowered Water Table Plot': ['Lowered Water Table Plot', 'APEX Alpha Lowered water table plot', 'Lowered water table'],
        'Raised Water Table Plot': ['Raised Water Table Plot', 'APEX Alpha Raised water table plot', 'Raised water table']
    },
    'APEX-Beta': {
        'Intermediate/Tower Bog': ['Intermediate/Tower Bog', 'Intermediate Bog', 'APEX Beta Intermediate Bog', 'APEX  Beta 2014 Intermediate Bog', 'Intermediate Bog Time Series', 'APEX  Beta Intermediate/tower Bog Time Series', 'APEX  Beta 2015 Intermediate Bog time series'],
        'Old Bog': ['Old Bog', 'Old Bog Time Series', 'APEX  Beta Old Bog Time Series', 'APEX  Beta 2015 Old Bog time series'],
        'North of Sunken Boardwalk': ['North of Sunken Boardwalk', 'APEX Beta North of Sunken Boardwalk', 'APEX Beta North of sunken boardwalk', 'APEX  Beta 2016 north of sunken boardwalk'],
    },
    'APEX-Gamma': {},
    'Kaamanen': {
        'Plot 2': ['Plot 2', '2', 2],
        'Plot 3': ['Plot 3', '3', 3],
        'Plot 4': ['Plot 4', '4', 4]
    },
    'Kiposuo': {
        'Plot 2': ['Plot 2', '2', 2],
        'Plot 3': ['Plot 3', '3', 3],
        'Plot 4': ['Plot 4', '4', 4]
    },
    'Smith Creek': {
        'Permafrost-Bog Edge Gradient 1, Fen':
            ['Permafrost-Bog Edge Gradient 1, Fen',
             'SMIT.OS.001'],
        'Permafrost-Bog Edge Gradient 1, Fen Transition with Permafrost Plateau':
            ['Permafrost-Bog Edge Gradient 1, Fen Transition with Permafrost Plateau',
             'SMIT.OS.002',
             'SMIT.OS.003'],
        'Permafrost-Bog Edge Gradient 1, Permafrost Peat Plateau':
            ['Permafrost-Bog Edge Gradient 1, Permafrost Peat Plateau',
             'SMIT.OS.004',
             'SMIT.OS.005',
             'SMIT.OS.006'],
        'Permafrost-Bog Edge Gradient 2, Bog/Poor Fen Transition with Permafrost Plateau':
            ['Permafrost-Bog Edge Gradient 2, Bog/Poor Fen Transition with Permafrost Plateau',
             'SMIT.OS.007'],
        'Permafrost-Bog Edge Gradient 2, Collapse Scar Bog':
            ['Permafrost-Bog Edge Gradient 2, Collapse Scar Bog',
             'SMIT.OS.008']
    },
    'Lutose': {}
}

# Habitat categories based on IsoGenie habitats
habitat_syn = {
    'Palsa': ['Palsa', 'Permafrost plateau', 'Permafrost plateau/palsa', 'Permafrost'],
    'Collapsed Palsa': ['Collapsed Palsa'],
    'Bog': ['Bog'],
    'Poor Fen': ['Poor Fen', 'Poor fen', 'Bog/Poor fen', 'Bog/poor fen'],
    'Fen': ['Fen', 'Rich Fen', 'Rich fen', 'Intermediate fen']
    # "Intermediate fen" is classified as just "Fen" for consistency with the Stordalen fen, which is just a "Fen" in
    #  the IsoGenieDB, but labeled as "Intermediate fen" in the GPMP data.
}