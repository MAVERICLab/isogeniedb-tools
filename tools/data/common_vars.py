#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 19:48:49 2018

@author: suzanne

Purpose: Stores all re-used variables and functions in one place.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import os
import sys
import pandas as pd
import re
import time
import json
from neo4j import GraphDatabase
from datetime import datetime
from tools.utilities.utils import printv

def check_names(graph, label, key, id):
    """Checks for multiple nodes in the database (graph) with label=label, property_key=key,
    and property_value=id. Returns nodes if they exist, or False if no node."""

    seconds = 0

    while True:

        try:
            nodes = graph.find(label, key, id)

            if not nodes:
                return False
            else:
                return nodes

        except SocketError:
            if seconds > 60:
                error('Too many socket errors.')
            else:
                print('Socket error. Trying again in 10 seconds.')
                seconds += 10
                time.sleep(10)

        except DatabaseError:
            if seconds > 60:
                error('Too many cypher errors.')
            else:
                print('Cypher error. Trying again in 10 seconds.')
                seconds += 10
                time.sleep(10)


def error(msg):
    sys.stderr.write("\nERROR: " + msg + os.linesep + '\n')
    sys.stderr.flush()
    sys.exit(1)


def warning(msg):
    sys.stderr.write("\nWARNING: " + msg + os.linesep + '\n')
    sys.stderr.flush()
    
    
def load_config(config_fn, location):
    # This function uses outdated py2neo code, so those imports are put in here to isolate them.
    from py2neo import Graph

    config_df = pd.read_csv(config_fn, delimiter=',', index_col=0)

    graph_loc = config_df.loc[location, 'graph']
    graph_pass = config_df.loc[location, 'password']

    graph_db = Graph(graph_loc, password=graph_pass)
    root_dir = config_df.loc[location, 'rootdir']

    return graph_db, root_dir


def load_config_driver(config_fn, location):
    config_df = pd.read_csv(config_fn, delimiter=',', index_col=0)
    
    user = 'neo4j'
    password = config_df.loc[location, 'password']
    uri = config_df.loc[location, 'graph'].replace('http', 'bolt').replace('7474', '7687')
    
    driver = GraphDatabase.driver(uri, auth=(user, password))
    root_dir = config_df.loc[location, 'rootdir']

    return driver, root_dir


def gather_df(cypher_query, neo4j_graph):
    # This function converts messy neo4j query output into a dataframe.
    
    results = neo4j_graph.run(cypher_query).data()
    # results = a list of dicts, each of which has one key:value pair with the value being a Node object
    # each Node object in turn is structured like a dict, with its key:value pairs corresponding to property keys

    to_dict = []

    for result in results:
        for key, value in result.items():  # err, n: list of dicts
            to_dict.append(dict(value))

    df = pd.DataFrame.from_dict(to_dict)

    return df


# ALTERNATE VERSION from isogenie-tools/build/web
def gather_df2(cypher_query, neo4j_graph):
    results = neo4j_graph.run(cypher_query).data()

    to_dict = []

    for n_node in results:

        for key, value in n_node.items():  # err, n: list of dicts

            node_dict = dict(value)
            node_dict.update({'Labels': ','.join([label for label in value.labels()])})

            to_dict.append(node_dict)

    df = pd.DataFrame.from_dict(to_dict)

    return df


# Modified from set_property() in run_add_missing_sample_metadata_from_soildepths() in io_misc.Modify_nodes.
# Modification allows searching by any IDkey, adds the condition "OR n.`%s`='NaN'", and changes print to printv.
# 7/15/2022: Added the ability to choose whether to update only if the values are missing, or to update regardless.
def set_property_by_id(tx, IDkey, IDvalue, key, value, missing_only):
    # Sets a key:value property for all nodes with a given UUID.
    # Set missing_only=True to only update properties on nodes that don't already have that property,
    #  or missing_only=False to replace existing properties.

    if missing_only is None:
        print("No nodes updated due to missing_only=None.")
        return

    updatedate = datetime.now().strftime('%Y%m%d')

    if missing_only:
        result = tx.run("MATCH (n {%s: $IDvalue}) WHERE (NOT(EXISTS(n.`%s`))) OR n.`%s`='NaN' "
                        "SET n.`%s` = $value, n.UpdateDate__ = $updatedate RETURN n" % (IDkey, key, key, key),
                        IDvalue=IDvalue, value=value, updatedate=updatedate)
    else:
        result = tx.run("MATCH (n {%s: $IDvalue}) "
                        "SET n.`%s` = $value, n.UpdateDate__ = $updatedate RETURN n" % (IDkey, key),
                        IDvalue=IDvalue, value=value, updatedate=updatedate)

    count = 0
    for record in result:
        count += 1

    if count > 0:
        printv('Updated {}="{}" on {} nodes with {}="{}"'.format(key, value, count, IDkey, IDvalue))


def standardize_GPS(coords='', lat=False, lon=False):
    # Assumes coords is degree-minute, and lat and lon are decimal degrees.
    # New as of 2/19/18: Also works for S and W coordinates.
    
    if lat and lon:  # if lat and lon are specified, ignore coords argument
        # round minutes to 10 decimal places (more than enough precision even for cm-resolution GPS) to get rid of extra
        #  digits caused by floating point arithmetic
        lat_min = abs(round((lat - int(lat)) * 60, 10))
        lon_min = abs(round((lon - int(lon)) * 60, 10))

        # format: N 68 21.1959, E 19 02.7974
        if lat >= 0:
            lat_h_str = 'N '
        else:
            lat_h_str = 'S '

        if lon >= 0:
            lon_h_str = ', E '
        else:
            lon_h_str = ', W '

        new_coords = lat_h_str + str(int(abs(lat))) + ' ' + str(lat_min) + lon_h_str + str(int(abs(lon))) + ' ' + str(lon_min)
        # add a zero before single-digit numbers, e.g. "2.8" becomes "02.8"
        # with a decimal point
        new_coords = re.sub(' (?=\d\.)', ' 0', new_coords)
        # without a decimal point (not likely to appear, but just in case)
        new_coords = re.sub(' (?=\d )', ' 0', new_coords)
    
    else:    
        if pd.isnull(coords) or coords in ['', 'NaN', 'Not Supplied', 'nan, nan']:
                new_coords = 'NaN'
        elif 'No core' in coords or 'sample does not exist' in coords:
                new_coords = 'No Sample'
        else:
            # Replace strings of adjacent spaces
            while "  " in coords:  # iteratively replace double spaces with single spaces
                coords = coords.replace("  ", " ")
    
            try:
                if len(coords.split(',')[0].split()) == 3 and coords.split(',')[0].split()[-1] in ['N', 'S']:
                    # Switch the order of the direction and numbers, if the direction is last (i.e. "68 21.0 N" becomes "N 68 21.0")
                    # Also check to make sure there's actually 3 components before parsing further
                    lat_deg, lat_min, lat_h = coords.split(',')[0].split()
                    lon_deg, lon_min, lon_h = coords.split(',')[1].split()
            
                    coords = '{} {} {}, {} {} {}'.format(lat_h, lat_deg, lat_min, lon_h, lon_deg, lon_min)

                # TODO: Other formats?
                else:
                    error('This GPS could not be resolved: {}'.format(str(coords)))

            except AttributeError:
                error('This GPS could not be resolved: {}'.format(str(coords)))
            
            # remove any trailing whitespace
            coords = coords.rstrip()
            
            # add a zero before single-digit numbers, e.g. "2.8" becomes "02.8"
            # with a decimal point
            coords = re.sub(' (?=\d\.)', ' 0', coords)
            # without a decimal point (not likely to appear, but just in case)
            coords = re.sub(' (?=\d )', ' 0', coords)
            
            new_coords = coords

            # make sure the final format is as expected
            if not(len(new_coords.split()) == 6) or new_coords.split()[0] not in ['N', 'S']:
                error('This GPS could not be resolved: {}'.format(str(coords)))

    return new_coords


# Field teams (copied verbatim from "Field & Analtyic Teams" Google doc on 3/19/2019)
# On 1/12/2019: Added 2019 (but didn't yet check others to see if they needed updating)
# Later updated with 2020 field team.
# Eventually want to switch to a CSV that includes contributions/roles for ALL data files
with open('tools/data/field_teams.json') as field_teams_file:
    field_teams = json.load(field_teams_file)

# Master site list. If it's not here, it doesn't exist in the DB.
# TODO: Changed some 'Short' names on 10/6/21. Will need to change the 'Core Code' column in Photos-All_retagged.csv
#  accordingly, then rerun redo_core_images_csv.py (this is so far the only other place, besides standardized sample
#  IDs added on 10/6/21, where the 'Short' names are used).
with open('tools/data/site_tag.json') as site_tag_file:
    site_tag = json.load(site_tag_file)
    site_abbrevs = {attrs['Short']: sitename for (sitename, attrs) in site_tag.items()}

# Site synonyms
with open('tools/data/site_syn.json') as site_syn_file:
    site_syn = json.load(site_syn_file)

# Column synonyms (used when standardizing coring sheets)
with open('tools/data/column_syn.json') as column_syn_file:
    column_syn = json.load(column_syn_file)

# Categories for Map Interface on website
with open('tools/data/map_categories.json') as map_categories_file:
    map_categories = json.load(map_categories_file)

# SampleGroup__ & CoreGroup__

# SampleGroup__ == CoreGroup__ for the field sample group names below.
# From core_group assignment in Standardize_Coring_Sheets; this was hard-coded so those group names are manually copied here.
# Also in "Site Categories & Abbreviations" Google sheet, which should be kept in sync with any other groups added here.
field_sample_groups = [
    "MainAutochamber",
    "AdditionalAutochamber",
    "IncubationMaterial",
    "Optimization",
    "PalsaHole",
    "MalhotraTransect",
    "MalhotraTransectPerryman",
    "Hodgkins",
    "Garnello"
]

# SampleGroup__ for incubation & bioreactor samples; these are NOT the same as the CoreGroup__ for the source field samples.
# From "Site Categories & Abbreviations" Google sheet, which should be kept in sync with any other groups added here.
incubation_sample_groups = [
    "201610_SIP_0to5",
    "CMR_bioreactor2",
    "201907_TempControlInc_9to19",
    "201607_SubstrateInc_9to19",
    "LowGalAInc_9to19",
    "201607_Wrighton_Bioreactor"
]

# doesn't seem to be used anywhere, but keeping (commented out) until sure
# lakes = {
#     'Inre Harrsjon': {
#         'style': 'lake',
#         'major_image': ''
#     },
#     'Mellan Harrsjon': {
#         'style': 'lake',
#         'major_image': ''
#     },
#     'Villasjon': {
#         'style': 'lake',
#         'major_image': ''
#     }
# }
