
# copied from Build-Pictures-HTML.py
# internal_tag = ['File Location', 'Notes/Qs', 'Photo Credit', 'Person Name (in photo)', 'Lowest Directory', 'Core Code',
#                 'Date Code', 'Day', '_tagging_group', '_file_exists', '_thumbnail_exists',  'Site Name']

# internal tag list (modified from internal_tag in Build-Pictures-HTML.py)-- these are all actual tag columns, but we don't currently want them embedded or used on the website
internal_tags = ['File Location', 'Notes/Qs', 'Date Code', 'Day', 'Site Name', 'Person Name (in photo)']

# specific tag types for tags embedded in images and used on website

keywords_presabs_tags = ['Core', 'Quadrat', 'Sampling Site', 'Landscape', 'Autochamber', 'People', 'Sampling',
                         'Plants', 'Lakes', 'Site Overview', 'Equipment']

keyword_groups_tags = ['Month', 'Year', 'Season', 'Habitat']

byline_tags = ['Photo Credit']

headline_tags = ['Core Code']

# Full categorized list of specific keywords embedded in photos -
#  This should be updated in tandem with photo_tags_list.txt, which simply lists them all without categories.
#  TODO: One good way to do this might be to create a small script that does this automatically.
keywords_tags_all = {
    '_extras': ['Data Sheet'],
    '_general': keywords_presabs_tags,
    'Month': ['May', 'June', 'July', 'August', 'August-September', 'October'],
    'Year': [str(year) for year in range(2010, 2023)],
    'Season': ['Winter', 'Spring', 'Summer', 'Summer-Fall', 'Fall'],
    'Habitat': ['Palsa', 'Collapsed Palsa', 'Bog', 'Poor Fen', 'Fen', 'Palsa-Fen transition']
}
