#!/usr/bin/env python
"""
Contains logic behind each node in the graph database network.

This version uses the slower py2neo package and is being phased out. Instead, use nodes_new whenever possible.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import uuid
import time
import sys
import pandas as pd
import select
from datetime import datetime
from py2neo import Relationship, Node
from py2neo.database import DatabaseError
from tools.utilities.utils import error, warning, printv
from py2neo.packages.httpstream.http import SocketError
from pprint import pprint
from dateutil.parser import parse
import cProfile
pr = cProfile.Profile()
pr.disable()

# Every "lower" level data element should be able to identify it's location in the hierarchical and aggregate major
# properties of the data above
# This is what get_upstreams() is for.


class Vertex(object):
    """
    Master class object, basically to be used for inheritance

    Attributes:
        name (str)
        kind (str): Type of vertex object, i.e. Site, Core, Depth, etc...
    """

    def __init__(self, name=None, kind=None):

        if name is None:
            self.name = 'Vertex'

        if kind is None:
            self.kind = 'Generic'

    def check_name(self, graph, label, key, id):
        """Checks if there's already a node in the database (graph) with label=label, property_key=key,
        and property_value=id. Returns the node if there is, or False if no node."""

        seconds = 0

        while True:

            try:
                node = graph.find_one(label, key, id)
                # node = graph.nodes.match(label, key=id) For version 4.0 of py2neo

                if not node:
                    return False
                else:
                    return node

            # Does not exist in py2neo v4
            except SocketError:
                if seconds > 60:
                    error('Too many socket errors.')
                else:
                    print('Socket error. Trying again in 10 seconds.')
                    seconds += 10
                    time.sleep(10)

            except DatabaseError:
                if seconds > 60:
                    error('Too many cypher errors.')
                else:
                    print('Cypher error. Trying again in 10 seconds.')
                    seconds += 10
                    time.sleep(10)

    # diff
    def check_names(self, graph, label, key, id):
        """Checks for multiple nodes in the database (graph) with label=label, property_key=key,
        and property_value=id. Returns nodes if they exist, or False if no node."""

        seconds = 0

        while True:

            try:
                nodes = graph.find(label, key, id)

                if not nodes:
                    return False
                else:
                    return nodes

            except SocketError:
                if seconds > 60:
                    error('Too many socket errors.')
                else:
                    print('Socket error. Trying again in 10 seconds.')
                    seconds += 10
                    time.sleep(10)

            except DatabaseError:
                if seconds > 60:
                    error('Too many cypher errors.')
                else:
                    print('Cypher error. Trying again in 10 seconds.')
                    seconds += 10
                    time.sleep(10)
    # Create
    def create_node(self, graphDB, label, metadata, sourceNodes=False, sourceIDKey=False, sourceID=False, 
                    rel_type=False, dataDict=False):
        """
        Generalized node creation, to replace the individual create_* methods.       
        Can be used for any "conventional" node, i.e. nodes that will have a relationship pointing from <=1 source node.
        
        Nodes that represent an aggregation of relationships from several source nodes (e.g. binned MAGs) will still 
        need their own dedicated create_* functions. However, these may still be able to get their own generalized 
        function that takes a list of source nodes, loops over them to make the Relationships, and doesn't use 
        get_upstreams().
        
        NEW: Can specify a list of multuiple sourceNodes. Assume rel_type is the same for both. sourceIDKey and sourceID can be from any of them.
        
        """
        pr.enable()
        
        # make sure all sourceNodes info is specified, if applicable (all labels except except Area & Metadata)
        if not (label in ['Area', 'Metadata'] or (sourceNodes and sourceIDKey and sourceID)):
            error('Must specify sourceNodes, sourceIDKey, and sourceID for label {}'.format(label))
        
        newID = str(uuid.uuid4())
            
        # Assign metadataType, the string appended to '__' or 'ID' to build the canonical ID and property keys.
        if '-' in label:
            # Use of too-specific metadataType, resulting in properties like 'Soil-Depth__', is strongly discouraged.
            # Therefore use only the last part of the label after the hyphen ('Depth__' instead of 'Soil-Depth__')
            metadataType = label.split('-')[-1]
        else:
            metadataType = label
            
        # Make the canonical ID and __ keys
        metadataID = metadataType + 'ID'
        metadataKey = metadataType + '__'
            
        if not rel_type:
            # If rel_type isn't specified, it defaults to a value based on label (incl. parts before a hyphen).
            # For example, label 'Soil-Depth' yields rel_type 'HAS_SOIL_DEPTH'.
            rel_type = 'HAS_' + label.replace('-','_').upper()
            
        newNode = Node(label, CreationDate__=datetime.now().strftime('%Y%m%d'))
        
        newNode[metadataID] = str(newID)
        newNode[metadataKey] = str(metadata)
        
        if dataDict:
            # Here is where additional new metadata (e.g. DepthMin__, DepthMax__ for Depth nodes) would be added  
            #  along with any other data. This means we'll need to manually add these new properties to the data dict
            #  within each class.
            for key, value in dataDict.items():
                newNode[key] = str(value)
        
        graphDB.merge(newNode)
        newNode.push()
        
        if sourceNodes:
            newNode[sourceIDKey] = str(sourceID)  # As an error-checking measure, add sourceID manually and check it (and everything else) with get_upstreams
            
            # NEW: Allow sourceNode to be a list of several nodes, and rename to sourceNodes
            if type(sourceNodes) != list:
                sourceNodes = [sourceNodes]
                
            for sourceNode in sourceNodes:
                
                upstream_dict = self.get_upstreams(sourceNode, dict(newNode))
                
                for key, value in upstream_dict.items():
                    newNode[key] = str(value)                  
                    # Check for issue where upstream data (usually GPS__) inexplicably fails to replace NaN in new data
                    if 'NaN' in str(newNode[key]) and 'NaN' not in str(sourceNode[key]):
                        error('Failed to update {} data from the sourceNode {}'.format(key, sourceNode))
                                
                newNode.push()

                source2new = Relationship(sourceNode, rel_type, newNode)
                graphDB.merge(source2new)
                source2new.push()

        pr.disable()

        return newNode, newID
    
    # Function for creating additional relationships (other than those created by create_node, e.g. Data to Metadata)
    def create_relationship(self, graphDB, sourceNode, rel_type, endNode):
        source2end = Relationship(sourceNode, rel_type, endNode)
        graphDB.merge(source2end)
        source2end.push()
    
    # Update
    def update_values(self, node, new_data={}, update_existing='toValues'):
        """
        Updates a node with additional information.
        Options for update_existing, in order of comprehensiveness:
            'none'          Does nothing (used here in place of checking every single time before calling this function).
            'missing'       Replaces ONLY missing data with new_data.
            'toValues'      Replaces all data with new_data, except when doing so would change a value to NaN.
            'everything'    Replaces ALL data with new_data, even if doing so would change it to NaN.
        """
        if update_existing not in ['none', 'missing', 'toValues', 'everything']:
            error('Invalid update_existing argument to update_values: {}'.update_existing)
            
        def ismissing(value):
            if pd.isnull(value) or str(value).lower() in ['nan', 'unknown', 'nan, nan', '']:
                return True
            else:
                return False
            
        changed = False
            
        if update_existing != 'none':
            
            for key, new_value in new_data.items():
                
                if ismissing(node[key]):
                    # if it's missing from the node but exists in new_data, add it if update_existing is anything besides 'none'
                    if not ismissing(new_value):
                        node[key] = str(new_value)
                        changed = True
                    
                else:  # if it already exists in the node
                    if update_existing in ['toValues', 'everything']:
                        # continue only if using an option that replaces existing data
                        if not ismissing(new_value) or update_existing == 'everything':
                            # update if it doesn't change a value to NaN, OR if update_existing == 'everything' (most comprehensive)
                            if str(node[key]) != str(new_value):  # but only need to bother if it's actually different
                                node[key] = str(new_value)
                                changed = True

#            if changed:
            if update_existing in ['toValues', 'everything']:
                # Update the 'UpdateDate__' property
                
                # For some reason, even when the old and new data should be identical, it still sometimes (but not always)
                #  sets changed=True.

                # So instead  of using 'changed', set UpdateDate__ if the node was set for updating any non-missing values.
                #  Then UpdateDate__ is just interpreted as the last time any non-missing values were *checked* for updating.
                #
                # This does have the side effect that datasets updated using 'missing' won't get an UpdateDate__,
                #  but at least it prevents setting of UpdateDate__ in cases where 'none' is automatically changed to 
                #  'missing' (lake temps and autochamber data) and no data is actually updated.
                
                node['UpdateDate__'] = datetime.now().strftime('%Y%m%d')
                
            if update_existing == 'everything':
                # new code for deleting keys not in new_data, so that the 'everything' option effectively re-writes the node
                # TODO: this seems risky and hasn't yet been tested as of 12/19/18
                for key in dict(node).keys():
                    # avoid deleting essential information by ignoring anything ending in __ or ID
                    if key not in new_data.keys() and not (key.endswith('__') or key.endswith('ID')):
                        del node[key]
                
            # Push the node if any update was attempted, even if UpdateDate__ was not set
            node.push()

    def update_labels(self, node, attrs):
        # Get habitat associated with site
        if not isinstance(attrs, list):
            attrs = [attrs]

        for attr in attrs:
            if not node.has_label(attr):
                node.update_labels(attrs)
                node.push()
                
    def get_upstreams(self, sourceNode, data_dict):
        # Instead of needing to 'request' this information multiple times, check to see if the node that the data
        # will be associated with already has it
            
        # Edited to include anything ending in '__' or 'ID'. There are a LOT of these, so used str.endswith() instead of
        #  listing them individually.
        
        # Edited again to make the logic more consistent
        
        upstreams = [key for key in dict(sourceNode).keys() if key.endswith('__') or key.endswith('ID')]
        upstream_data = {}

        for upstream in upstreams:
            if upstream in ['CreationDate__', 'UpdateDate__',
                            'DatasetID', 'Version__', 'VersionDate__', 'Type__', 'Availability__', 'ContactName__',
                            'ContactEmail__', 'Quality__', 'DOI__', 'ExternalRepositoryURL__', 'Published__',
                            'ManuscriptCitation__', 'ManuscriptURL__', 'readme_file_href__', 'original_file_href__',
                            'imported_file_href__', 'previous_version_href__']:
                # Unlike the other upstreams, CreationDate__ and UpdateDate__ should be independent of the node's position in the DB.
                # UPDATE: Also added properties specific to Metadata nodes, as a new node with the Metadata label may
                #  have its own set of properties which are different from an "upstream" Metadata node. "Normal"
                #  Metadata nodes won't have this issue, as the Metadata class doesn't call get_upstreams(), but this
                #  would be an issue for combined Data/Metadata nodes that were originally created using the Data class
                #  (e.g. in sequencing imports). Listing these properties individually, as such a node might still need
                #  to inherit other upstream properties (e.g. those corresponding to cores, depths, etc.).
                continue

            elif upstream not in data_dict.keys():
                # Only include if it's not already in data_dict. If it's already in data_dict, then sourceNode[upstream] *should* be the 
                # same, but if there's a conflict, then we don't want to overwrite the value in data_dict.
                upstream_data[upstream] = sourceNode[upstream]

            elif str(sourceNode[upstream]) == str(data_dict[upstream]):
                # if it exists in both and its value is the same, no need to do anything
                continue

            else:
                # If it exists in both, but it's NOT the same, then we should investigate why...
                if upstream == 'Depth__':
                    # Depth__ is inconsistently formatted, and there are already other checks to make sure they're matched
                    continue

                elif 'NaN' in [str(data_dict[upstream]), str(sourceNode[upstream])]:
                    # e.g. some GPS__ and DepthMin__/DepthMax__ values

                    if str(data_dict[upstream]) == 'NaN':
                        # If it's NaN in the new data, can still get these values from upstream
                        upstream_data[upstream] = sourceNode[upstream]
                        
                    if str(sourceNode[upstream]) == 'NaN':
                        # Sometimes it's the other way around: sourceNode has NaN but data_dict has a value.
                        # Don't like this, but can let it go with a warning.
                        # Could also update sourceNode, but this isn't going to fix any further NaNs even further upstream, so won't bother.
                        warning('New {} data appears after an upstream value of NaN.\n'
                                'Upstream node: {}\n'
                                'New data: {}'.format(upstream, sourceNode, str(data_dict[upstream])))
                    
                else:
                    # If the upstream exists in both but its value is different, and it's not one of the above cases, then something 
                    #  is likely wrong.
                    # But it also could just be a mismatch in # of decimal places recorded between labs. This depends on what it is.

                    message = ("New {} data does not match the upstream data in the DB! \n" + 
                      "Upstream node: {}\n" + 
                      "Upstream data: {}\n" + 
                      "New data: {}").format(upstream, sourceNode, str(sourceNode[upstream]), str(data_dict[upstream]))

                    if upstream == 'GPS__':
                        # GPS is inconsistent anyway
                        # common_vars.standardize_gps should have omitted any trivial format inconsistencies,
                        #   so it's the actual values that are different
                        warning(message)
                            # TODO Add an option to specify which GPS is the "correct" one

                    elif upstream in ['DepthMin__', 'DepthMax__', 'DepthAvg__']:
                        if abs(float(sourceNode[upstream]) - float(data_dict[upstream])) == 0:
                            # if converting to floats and subtracting results in 0, then it's probably just an int vs. decimal issue
                            continue

                        elif abs(float(sourceNode[upstream]) - float(data_dict[upstream])) <= 5:
                            # if they're different by <=1cm, assume that Depth.is_depth_match() took care of it, and send a warning
                            # UPDATE: Adjust to 5 for some big incubation depth ranges
                            warning(message)

                        else:
                            error(message)
                    else:
                        error(message)
        
        return upstream_data

class Metadata(Vertex):
    """
    Metadata vertex -- used to store data contacts, quality level, DOI, version # of original file(s),
    link(s) to original file(s), link(s) to READMEs, etc.
    
    The idea is that each of these would have multiple HAS_METADATA relationships pointing to it from 
    its associated Data nodes. This is essentially similar to a Area node connected to several Site nodes,
    except that all the relationships are pointing in the opposite direction. So easiest to re-use that same 
    code structure, i.e. create (or check for) the Metadata node at the very beginning of each import script
    (before any other nodes are created), and then associate each Data node with that Metadata node.
    
    ID/versioning scheme:
        
        Each dataset "entity" (corresponding to a table row on the Data Downloads page on the web) gets its own 
        unique DatasetID, which is assigned in the file list CSV. If a new version of a file is imported, the 
        version # is updated, but the DatasetID stays the same.
    
    Still need to do (SHOULD BE DONE NOW; but still keeping this text just in case):
        
      - If this file is different than the original (e.g. it's the output of a Standardize_ script), then need
        to add the file path of the original file to data_dict. Need a standardized naming scheme: maybe 
        original_file_href__ if applicable, and don't include intermediate "formatted" files (possibly don't
        even need to provide links to those on the web server)? or maybe change the "standardized" label to "imported"
        so that this label can also apply to simple files that didn't need to be standardized?
        
      - Add a "Proceed? y/n" option to the warning
        
    """
    
    def __init__(self, db, name, data_dict, update_existing_data, kind='Metadata'):
        # update_existing_data should receive the value of update_existing for the associated Data nodes.
        # It controls whether or not the Metadata is updated, depending on how/if the associated Data is being updated.
        #
        # NEW (as of July 1 2019): Added an update_existing_data=False option, to be used when updating ONLY the Metadata nodes  
        #   but no actual data (for example, if adding a new contact person or DOI).
        # So its purpose is slightly different than update_existing in the Data class, hence the slightly different name.
        
        # Dict of human-readable descriptions for the update_existing_data options.
        # These should be understandable to someone not familiar with the database. May need to further edit for clarity.
        update_descriptions = {
            'none' : "This file was not actually used to update any existing data; it was only used to check for data and import it if it didn't already exist.",
                # 'none' and 'missing' sound similar because they are; the difference is that 'none' can only create new nodes, whereas 'missing' can add data to existing nodes
            'missing' : "This file was used to add data missing from the last version of the same dataset, but no old data was actually changed.",
            'toValues' : "This file was used to update the data values from the last version of the same dataset, but some obsolete data (if any) from the old version may still exist.",
            'everything' : "This file was used to completely overwrite the last version of the same dataset, including deleting obsolete data (if any)."
        }
        
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.node = self.check_name(graph=db, label=self.kind, key='DatasetID', id=self.name)
        
        # If node doesn't exist, create it
        if not self.node:
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, dataDict=data_dict)
            # The Metadata node created here has the following properties:
            # DatasetID (in data_dict) and Metadata__ (generated by create_node) both contain the manually-created
            #   DatasetID from the file list csv.
            # MetadataID (generated by create_node) will be auto-generated by uuid.uuid4().
            # Other properties in data_dict will include file path, version, quality level, etc.
            
        else: # if the Metadata node already exists
            self.id = self.node['MetadataID']
            
            if update_existing_data in ['none', 'missing', False] and data_dict['Version__'] != self.node['Version__']:
                warning("Attempting to import from a new version of a file without updating any existing values from the old version. "
                        "This may not be an issue if you're only adding completley new data and are SURE that no existing values need to "
                        "be changed, and if speed is a concern. Otherwise, please either use a more comprehensive value for '--update' "
                        "(either 'toValues' or 'everything'), or import an old version of the file (if no update is intended).\n"
                        "File being imported: {}\n"
                        "Version of this file: {}\n"
                        "Version in database: {}".format(data_dict['imported_file_href__'], data_dict['Version__'], self.node['Version__']))
                proceed = ''
                while proceed not in ['y', 'n']:
                    
                    print('Proceed with current --update argument of "{}"?'.format(update_existing_data))
                    print('You have 10 minutes to answer (y or n), beginning at {}:'.format(datetime.now().strftime('%H:%M')))
                    i, o, e = select.select( [sys.stdin], [], [], 10*60 )
                    
                    if i:
                        proceed = sys.stdin.readline().strip().lower()
                        if proceed not in ['y', 'n']:
                            print('You said "{}". Please select a valid response (either "y" or "n").'.format(proceed))
                    else:
                        warning('Timed out with no response. Automatically proceeding with --update argument of "{}".'.format(update_existing_data))
                        proceed = 'y'
                    
                if proceed == 'n':
                    print('Exiting...')
                    sys.stderr.flush()
                    sys.exit(1)
            
            if update_existing_data:  # I.e. unless it's explicitly False
                data_dict['UpdateMethod__'] = update_descriptions[update_existing_data]
                
            self.update_values(node=self.node, new_data=data_dict, update_existing='everything')  # here update_existing refers to updating the Metadata node itself

    def __repr__(self):
        return "Metadata for a set of Data nodes"
    
    

class Area(Vertex):
    """
    Area vertex -- almost called it Peatland but may eventually want to include non-Peatland areas if this DB expands.
    For IsoGenie there is just one Area ("Stordalen"), with which all Sites are associated.
    For A2A there are five Areas.
    """

    def __init__(self, db, name, kind='Area', data_dict=False):
        
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.node = self.check_name(graph=db, label=self.kind, key='Area__', id=self.name)

        # If node doesn't exist, create it
        if not self.node:
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, dataDict=data_dict)
        else:
            self.id = self.node['AreaID']

    def __repr__(self):
        return "Area with attributes"


class FieldSampling(Vertex):
    """
    For storing info about sampling trips (ranges of dates).
    These are on the same "level" as Sites (between Areas and Cores), but parallel to Sites.
    """

    def __init__(self, db, name, area_node, data_dict, kind='FieldSampling'):
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name  # use DateShort__, renamed to FieldSampling__
        self.kind = kind
        self.area_node = area_node.node
        self.area_id = area_node.id
        
        putative_smp_nodes = [node for node in db.find(self.kind, 'AreaID', self.area_id)]
        
        if putative_smp_nodes:
            for p_smp_node in putative_smp_nodes:
                if self.name == p_smp_node['FieldSampling__']:
                    printv('Identified FieldSampling node: {} from Area: {}. Reusing.'.format(self.name, p_smp_node['Area__']))
                    self.node = p_smp_node
                    self.id = p_smp_node['FieldSamplingID']
                    break

        if not hasattr(self, 'id'):

            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, sourceNodes=self.area_node, 
                                                  sourceIDKey='AreaID', sourceID=self.area_id, dataDict=data_dict)

    def __repr__(self):
        return "Field sampling trip with attributes"


class Site(Vertex):
    """
    Site vertex

    """

    def __init__(self, db, name, area_node, kind='Site', data_dict=False):
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name        
        self.kind = kind
        self.area_node = area_node.node
        self.area_id = area_node.id
        
        putative_site_nodes = [node for node in db.find(self.kind, 'AreaID', self.area_id)]
        
        if putative_site_nodes:
            for p_site_node in putative_site_nodes:
                if self.name == p_site_node['Site__']:
                    printv('Identified Site node: {} from Area: {}. Reusing.'.format(self.name, p_site_node['Area__']))
                    self.node = p_site_node
                    self.id = p_site_node['SiteID']
                    break

        if not hasattr(self, 'id'):

            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, sourceNodes=self.area_node, 
                                                  sourceIDKey='AreaID', sourceID=self.area_id, dataDict=data_dict)

    def __repr__(self):
        return "Site with attributes"


class Core(Vertex):
    """
    Core vertex. Cores are *always* part of Sites, and inherit many of their attributes


    """

    def __init__(self, db, name, site_node, fieldsampling_node, gps, kind='Core', data_dict=False):
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.gps = gps
        self.site_node = site_node.node
        self.fieldsampling_node = fieldsampling_node.node
        self.site_id = site_node.id

        putative_core_nodes = [node for node in db.find(self.kind, 'SiteID', self.site_id)]

        if putative_core_nodes:
            for p_node in putative_core_nodes:  # If false, wouldn't make it to this point
                if name == p_node['CoreDated__']:
                    printv('Identified Core node: {} from Site: {}. Reusing.'.format(name, self.site_node['Site__']))
                    self.node = p_node
                    self.id = p_node['CoreID']
                    break

        if not hasattr(self, 'id'):
            
            data = {
                    'CoreDated__': self.name,
                    'GPS__': gps
            }
            
            if data_dict:  # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
                # Unlike Data nodes, this only adds values to new nodes, and doesn't change existing nodes.
                # This is so that "official" info from coring sheets isn't overwritten by geochem info, etc.
                data = {**data, **data_dict}
                
            corenum = str(self.name.split('|')[0])  # get core NUMBER to match with the auto-created Core__ property
            
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=corenum, 
                                                  sourceNodes=[self.site_node, self.fieldsampling_node], sourceIDKey='SiteID', 
                                                  sourceID=self.site_id, dataDict=data)

    def __repr__(self):
        return "Core with attributes"


class CoreDepth(Vertex):
    """
    For depths associated with a core.
    Unlike other nodes, "identical" depths are matched numerically instead of based on an exact name match.

    """

    def __init__(self, db, name, core_node, data_dict, kind='Core-Depth'):  # site/core_node should be class
        # Default kind = 'Core-Depth' so that it works as a generic for both soil and sediment depths, but in practice
        #  should be changed to 'Soil-Depth' or 'Sediment-Depth' depending on the sample type.
        # NEW: data_dict is now mandatory, and contains the depth components used for depth matching (it raises an error
        #  if any of the mandatory keys are missing). This allows data_dict updates, but uses update_existing='missing'
        #  to discourage overwriting metadata in this node type (as it doesn't typically receive the Data label). This
        #  was done to allow bug-free updating with the new DepthAvg__ property.
        # This class also now includes the ability to update labels internally.
        # TODO: Once DepthAvg__ is added to these nodes, also update any "downstream" nodes with this property that
        #  weren't already updated (this should just include sequencing data).

        Vertex.__init__(self, name=name, kind=kind)

        # TODO Better handling of neo4j nodes vs IsoGenieDB

        self.name = name  # This is the *only* class where name is not matched directly
        self.kind = kind  # Can be Soil-Depth or Sediment-Depth, unless using the default Core-Depth
        self.core_node = core_node.node
        self.core_id = core_node.id

        # Get specific depth components from data_dict
        min_depth = data_dict['DepthMin__']
        max_depth = data_dict['DepthMax__']
        avg_depth = data_dict['DepthAvg__']  # not used for matching, but should check to make sure it exists
        depth_only = data_dict['Depth__']
        subdepth = data_dict['DepthSubsample__']

        putative_depth_nodes = [node for node in db.find(self.kind, 'CoreID', self.core_id)]

        if putative_depth_nodes:
            for p_depth_node in putative_depth_nodes:
                if self.is_depth_match(min_depth=min_depth, max_depth=max_depth, depth_only=depth_only, 
                                       subdepth=subdepth, depth_node=p_depth_node):
                    printv('Identified Depth node: {} from Core: {}. Reusing.'.format(name, self.core_node['CoreDated__']))
                    self.node = p_depth_node
                    self.id = p_depth_node['DepthID']

                    # New: Add the ability to update values, even though this isn't a Data node type
                    self.update_values(node=self.node, new_data=data_dict, update_existing='missing')

                    break

        if not hasattr(self, 'id'):

            data = {
                    'Depth': self.name  # Analogous to DepthComplete__, this includes subdepth, but we're using the
                                        #  un-underscored Depth instead because it can be inconsistent with
                                        #  DepthComplete__ in the downstream Data nodes (i.e. ranges vs. single #s)
            }

            # merge data_dict into data. if there are duplicates, the values in the 2nd argument win.
            data = {**data, **data_dict}
                
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=depth_only, sourceNodes=self.core_node, 
                                                  sourceIDKey='CoreID', sourceID=self.core_id, dataDict=data)
    
    def is_depth_match(self, min_depth, max_depth, depth_only, subdepth, depth_node):
        # code for numerically comparing min and max depths
   
        db_depth = depth_node['Depth__']  # depth_only = new data, db_depth = DB data
        db_subdepth = depth_node['DepthSubsample__']

        if not db_subdepth and not isinstance(db_depth, (int, float)):
            # Not sure if this is really a good descriptor for this error, or even if it should be an error.
            # It was left over from earlier; changed the placeholders so that it will print.
            error('No depth identified in DB for Core {}, Site {}, Date {}. It is possible that the core'
                  ' did not have a depth associated with it, or a measurement "disconnect" between'
                  ' labs'.format(depth_node['Core__'], depth_node['Site__'], depth_node['FieldSampling__']))

        # Below is logic for determining ends and whether or not they fall within a range
        if db_depth == 'None':  # Should NOT happen, but just in case...
            error('Identified "None" depth for Core: {}, Site {}, Date {}'.format(depth_node['Core__'], 
                    depth_node['Site__'], depth_node['FieldSampling__']))
        
        # "Sub"-depths can only be an exact match, so weed out cases where these don't match
        if str(db_subdepth) != str(subdepth):
            return False
        
        # Now we can actually compare the depths.
        
        # simplest case: the values match *exactly*
        if str(db_depth) == str(depth_only):
            return True

        # special case: one of the values is 'above peat'
        # CONVENTION: Any non-numeric above-peat depths should use this name in the standardized sheets!
        elif db_depth == 'above peat':
            try:
                depth_only = float(depth_only)
                if depth_only < 0:
                    printv('Identified negative new depth: {} as "above peat"'.format(depth_only))
                    return True
                else:
                    return False
            except ValueError:  # depth_only can't be converted to float
                return False  # the 'simplest case' already took care of cases where both == 'above peat'
            
        elif depth_only == 'above peat':
            return False  # may later try to match new 'above peat' depth with negative db depth

        # numerical depth comparison
        else:
            # New streamlined method: Compare only the min and max depths. For single-value depths,
            # use min = max = depth value.

            # For some reason, even though we did fillna('NaN') earlier, it's still sometimes changing these 'NaN'
            #  strings back to np.nan. Force them back to being strings (even if 'nan' or '' instead of 'NaN'):

            # Should use pd.isnull for objects, should cover strings, ints and floats
            min_db = str(depth_node['DepthMin__'])
            max_db = str(depth_node['DepthMax__'])
            min_str = str(min_depth)
            max_str = str(max_depth)
            db_depth_str = str(db_depth)
            depth_only_str = str(depth_only)

            # Add _ to the NaN values to keep them as strings
            # The below doesn't work - because the loop only re-assigns a *copy* of each variable
#            for obj in [min_db, max_db, min_str, max_str, db_depth_str, depth_only_str]:
#                if obj.lower() == 'nan':
#                    obj += '_'
            # instead need to edit each individually        
            if min_db.lower() == 'nan':
                min_db += "_"
            if max_db.lower() == 'nan':
                max_db += "_"
            if min_str.lower() == 'nan':
                min_str += "_"
            if max_str.lower() == 'nan':
                max_str += "_"
            if db_depth_str.lower() == 'nan':
                db_depth_str += "_"
            if depth_only_str.lower() == 'nan':
                depth_only_str += "_"                                        

            # Now that everything is a string:
            
            # Try to get numbers for both min and max, for both the new data and the DB
            try:
                min_db = float(min_db)
                max_db = float(max_db)
            except ValueError:
                try:
                    min_db = float(db_depth_str)
                    max_db = min_db
                except ValueError:
                    error('db depth: {} is not recognizable'.format(db_depth))

            try:
                min_str = float(min_str)
                max_str = float(max_str)
            except ValueError:
                try:
                    min_str = float(depth_only_str)
                    max_str = min_str
                except ValueError:
                    error('new depth: {} is not recognizable'.format(depth_only))

            # Next compare the min and max between db and new data
            # One must be completely within the other
            if min_db <= min_str <= max_str <= max_db or min_str <= min_db <= max_db <= max_str:
                return True

            elif ((isinstance(min_str, float) and isinstance(max_str, float) and isinstance(
                    min_db, float) and isinstance(max_db, float)) and not
            (pd.isnull(float(min_str)) or pd.isnull(float(max_str)) or pd.isnull(
                float(min_db)) or pd.isnull(float(max_db)))):
                return False  # continue if the depth isn't a match but the depth values are non-NaN floats (as they should be)

            else:  # invalid depths, which aren't floats
                error('Unable to identify entire or partial depths from either new data or '
                      'database information.\nNew Min: {}\nNew Max: {}\nDB Min: {}\nDB Max: {}\n'
                      'New Subdepth: {}\n DB Subdepth: {}. \n'
                      'The database depth_node is: \n{}'.
                      format(min_str, max_str, min_db, max_db, subdepth, db_subdepth,
                             depth_node))
                
        return False  # only gets to this point if it hasn't already returned True


class IncubationRep(Vertex):
    """
    Incubation (or incubation-associated field sample) from a core depth.
    """

    def __init__(self, db, name, depth_node, kind='IncubationRep', data_dict=False):
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name        
        self.kind = kind
        self.depth_node = depth_node.node
        self.depth_id = depth_node.id
        
        putative_inc_nodes = [node for node in db.find(self.kind, 'DepthID', self.depth_id)]
        
        if putative_inc_nodes:
            for p_inc_node in putative_inc_nodes:
                if self.name == p_inc_node['IncubationRep__']:
                    printv('Identified IncubationRep node: {} from Depth: {}. Reusing.'.format(self.name, p_inc_node['Depth__']))
                    self.node = p_inc_node
                    self.id = p_inc_node['IncubationRepID']
                    break

        if not hasattr(self, 'id'):

            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, sourceNodes=self.depth_node, 
                                                  sourceIDKey='DepthID', sourceID=self.depth_id, dataDict=data_dict)

    def __repr__(self):
        return "Incubation replicate"
    

class Data(Vertex):
    def __init__(self, db, subdata, source_node, source_id_key, rel_type, data_dict, update_existing, metadata_node, kind='Data', no_create=False):
        Vertex.__init__(self, name=subdata, kind=kind)
        
        self.subdata = subdata
        self.kind = kind
        self.source_node = source_node.node
        self.source_id = source_node.id
        self.metadata_node = metadata_node.node
        
        # Check to see if the node has any data relating to it.
        # The line below is checking for a Data node with arbitrary id_key and id_value (usually DepthID).
        #  In other words, all Data nodes that have that ID. If there are none, putative_nodes is set to False.
        putative_nodes = self.check_names(graph=db, label=self.kind, key=source_id_key, id=self.source_id)
        
        if putative_nodes:
            for p_node in putative_nodes:  # If false, wouldn't make it to this point

                relationships = db.match(start_node=self.source_node, rel_type=rel_type, end_node=p_node)

                if relationships:
                    for rel in relationships:
                        # account for "sub"-datasets of the same type by only selecting the one where the 
                        #   DataSubsample__ matches
                        # if it does match, proceed as usual
                        # if it does NOT match, data_node won't be assigned.
                        # Note that if it's on an error_ dataframe, then all of the multiple Data nodes already exist,
                        #   and the correct data_node should still be selected for error matching.
                        # Note also that this *inner* for loop *should* only execute once; but still need to 
                        #   keep it as a loop to avoid data type errors. Whereas the *outer* for loop will be 
                        #   executed the same # of times as there are Data nodes.
                        if str(rel.end_node()['DataSubsample__']) == str(subdata):
                            
                            self.node = p_node
                            self.id = p_node['DataID']
                            
                            if not no_create:
                                # if no_create, this also means don't update values or associate with the passed metadata_node
                                
                                # First update the values as needed
                                self.update_values(node=self.node, new_data=data_dict, update_existing=update_existing)
                                
                                # Now check to see if there's already a relationship to the metadata node, and if not, create it
                                metadata_rels = db.match(start_node=self.node, rel_type='HAS_METADATA', end_node=self.metadata_node)

                                # metadata_rels still returns something of length 0 even if there are no relationships matching the criteria!
                                # So we need to use a roundaout method to check if it exists
                                found = False
                                if metadata_rels:
                                    for rel in metadata_rels:
                                        if str(rel.end_node()['DatasetID']) == str(self.metadata_node['DatasetID']):
                                            found = True
                                if not found:
                                    self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)
                            
        if not hasattr(self, 'id'):
            if no_create:
                warning('Unable to identify data node and given "No create" option. Not an issue if this was intended.')
                self.node = False
                self.id = False
            
            else:
               
                data = {
                    'DataSubsample__': str(self.subdata)
                }
                
                if data_dict:  # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
                    data = {**data, **data_dict}
                
                self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.subdata, 
                                                      sourceNodes=self.source_node, 
                                                      sourceIDKey=source_id_key, sourceID=self.source_id, 
                                                      rel_type=rel_type, dataDict=data)
                
                self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)
                
                printv('Created data node: {}'.format(self.id))


class Errors(Vertex):
    def __init__(self, db, name, data_node, error_dict, update_existing, metadata_node, rel_type=False, kind='Errors'):
        """
        Errors nodes store the uncertainty values (std errors, std devs, etc) associated with a Data node.
        
        Specifying a rel_type is highly recommended to distinguish different uncertainty types from each other.
        The recommended rel_types are as follows:
            standard errors:          HAS_STD_ERRORS
            standard deviations:      HAS_STD_DEVS
            95% confidence interval:  HAS_95CONF_INTERVALS
            mixture of above: leave rel_type blank, which will result in HAS_ERRORS, but the error types should then 
              be specified in the keys of error_dict
        """
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name
        self.kind = kind
        self.data_node = data_node.node
        self.data_id = data_node.id
        self.error_type = rel_type.replace('HAS_', '')  # set error type based on rel_type by deleting the HAS_
        self.metadata_node = metadata_node.node
        
        putative_error_nodes = [node for node in db.find(self.kind, 'DataID', self.data_id)]
        
        if putative_error_nodes:
            for p_error_node in putative_error_nodes:
                
                relationships = db.match(start_node=self.data_node, rel_type=rel_type, end_node=p_error_node)

                if relationships:
                    count = 0
                    for rel in relationships:
                        # should only execute once; use a counter because can't use len(relationships)
                        count += 1
                        if count > 1:
                            error('Found multiple Error nodes of the same type ({}) for the same Data node: \n'
                                  '{}'.format(self.error_type, self.data_node))
                            
                        if str(rel.end_node()['Errors__']) == self.error_type:
                            self.node = p_error_node
                            self.id = p_error_node['ErrorsID']
                            
                            # Update the values as needed
                            self.update_values(node=self.node, new_data=error_dict, update_existing=update_existing)
                                                            
                            # Now check to see if there's already a relationship to the metadata node, and if not, create it
                            metadata_rels = db.match(start_node=self.node, rel_type='HAS_METADATA', end_node=self.metadata_node)

                            # metadata_rels still returns something of length 0 even if there are no relationships matching the criteria!
                            # So we need to use a roundaout method to check if it exists
                            found = False
                            if metadata_rels:
                                for rel in metadata_rels:
                                    if str(rel.end_node()['DatasetID']) == str(self.metadata_node['DatasetID']):
                                        found = True
                            if not found:
                                self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)

        if not hasattr(self, 'id'):
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.error_type,
                                                  sourceNodes=self.data_node, sourceIDKey='DataID', sourceID=self.data_id, 
                                                  rel_type=rel_type, dataDict=error_dict)
            
            self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)
        

class AutochamberChamber(Vertex):
    """

    """
    def __init__(self, db, name, site_node, kind='Chamber', data_dict=False):
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.site_node = site_node.node
        self.site_id = site_node.id

        putative_chmbr_nodes = [node for node in db.find(self.kind, 'SiteID', self.site_id)]

        if putative_chmbr_nodes:

            for putative_chmbr_node in putative_chmbr_nodes:
                if self.name == putative_chmbr_node['Chamber__']:
                    self.node = putative_chmbr_node
                    self.id = putative_chmbr_node['ChamberID']
                    break

        if not hasattr(self, 'id'):
            
            data = {
                'Chamber': self.name
            }
            
            if data_dict:  # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
                data = {**data, **data_dict}
            
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, 
                                                  sourceNodes=self.site_node, sourceIDKey='SiteID', sourceID=self.site_id, 
                                                  dataDict=data)


class FractionalTime(Vertex):
    """
    Used instead of Hour for autochamber times because chamber times are fractions of a day.
    By convention, Hour__ and Minute__ use HH:MM format, while Time__ is a fraction of a day.
    
    For now we will assume these are all also Data nodes, and thus must have a metadata_node.
    """
    
    def __init__(self, db, name, date_node, data_dict, update_existing, metadata_node, kind='Time'):
        Vertex.__init__(self, name=name, kind=kind)

        self.date_node = date_node.node
        self.date_id = date_node.id
        self.name = name
        self.kind = kind
        self.metadata_node = metadata_node.node

        putative_time_nodes = [node for node in db.find(self.kind, 'DateID', self.date_id)]

        if putative_time_nodes:

            for putative_time_node in putative_time_nodes:
                if float(self.name) == float(putative_time_node['Time__']):  # convert to floats in case of inconsistent trailing zeros
                    self.node = putative_time_node
                    self.id = putative_time_node['TimeID']
                    self.update_values(node=self.node, new_data=data_dict, update_existing=update_existing)
                        
                    # Now check to see if there's already a relationship to the metadata node, and if not, create it
                    metadata_rels = db.match(start_node=self.node, rel_type='HAS_METADATA', end_node=self.metadata_node)

                    # metadata_rels still returns something of length 0 even if there are no relationships matching the criteria!
                    # So we need to use a roundaout method to check if it exists
                    found = False
                    if metadata_rels:
                        for rel in metadata_rels:
                            if str(rel.end_node()['DatasetID']) == str(self.metadata_node['DatasetID']):
                                found = True
                    if not found:
                        self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)
                    
                    break

        if not hasattr(self, 'id'):

            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, 
                                                  sourceNodes=self.date_node, sourceIDKey='DateID', sourceID=self.date_id, 
                                                  dataDict=data_dict)
            
            self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)


class Date(Vertex):
    """
    General Date class
    Some (not all!) of these are also Data nodes, and will thus have a metadata_node.
    update_existing must be supplied, but is only used if it has a data_dict.
    """
     
    def __init__(self, db, name, source_node, source_id_key, update_existing, kind='Date', metadata_node=False, data_dict=False):
        # May specify a more specific date type (e.g. 'ANS-Date') for kind, but this is discouraged as all dates are fundamentally the same
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name
        self.kind = kind
        self.source_node = source_node.node
        self.source_id = source_node.id
        
        if metadata_node:
            self.metadata_node = metadata_node.node
        
        # Check if date exists
        putative_date_nodes = [node for node in db.find(self.kind, source_id_key, self.source_id)]

        if putative_date_nodes:
            for p_date_node in putative_date_nodes:
                if str(self.name) == p_date_node['Date__']:
                    printv('Identified Date node: {} from source node with {}: {}. Reusing.'.format(self.name, source_id_key, self.source_id))
                    self.node = p_date_node
                    self.id = p_date_node['DateID']
                    self.name = p_date_node['Date__']
                    if data_dict:
                        # First update the values as necessary
                        self.update_values(node=self.node, new_data=data_dict, update_existing=update_existing)

                        if metadata_node:
                            # Now check to see if there's already a relationship to the metadata node, and if not, create it
                            metadata_rels = db.match(start_node=self.node, rel_type='HAS_METADATA', end_node=self.metadata_node)

                            # metadata_rels still returns something of length 0 even if there are no relationships matching the criteria!
                            # So we need to use a roundaout method to check if it exists
                            found = False
                            if metadata_rels:
                                for rel in metadata_rels:
                                    if str(rel.end_node()['DatasetID']) == str(self.metadata_node['DatasetID']):
                                        found = True
                            if not found:
                                self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)

                    break
            
        if not hasattr(self, 'id'):

                self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, 
                                                      sourceNodes=self.source_node, sourceIDKey=source_id_key, 
                                                      sourceID=self.source_id, dataDict=data_dict)
                
                if metadata_node:
                    self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)


class SiteDepth(Vertex):
    """
    Depths associated directly with a Site with no Core.
    Previously called LakeDepth but changed name to be more general.
    
    """

    def __init__(self, db, name, site_node, kind, gps='NaN', data_dict=False):
        # usually kind='Water-Depth' but keep it unspecified for now
        Vertex.__init__(self, name=name, kind=kind)
        
        # TODO Unlike CoreDepth, SiteDepth simply matches the depth name and doesn't do any complex 
        #  number-range matching. May add this later, but would also have to handle the ' m' unit
        #  in lake depths.
        self.name = name
        self.kind = kind
        self.gps = gps
        self.site_node = site_node.node
        self.site_id = site_node.id
        self.site_name = self.site_node['Site__']

        putative_depth_nodes = [node for node in db.find(self.kind, 'SiteID', self.site_id)]

        if putative_depth_nodes:
            for p_depth_node in putative_depth_nodes:  # If false, wouldn't make it to this point
                if self.name == p_depth_node['Depth__']:
                    printv('Identified Depth node: {} from Site: {}. Reusing.'.format(self.name, self.site_name))
                    self.node = p_depth_node
                    self.id = p_depth_node['DepthID']
                    break

        if not hasattr(self, 'id'):
            
            data = {
                'GPS__': gps,
                'Depth': self.name
            }
            
            if data_dict:  # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
                data = {**data, **data_dict}
            
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, 
                                                  sourceNodes=self.site_node, sourceIDKey='SiteID', 
                                                  sourceID=self.site_id, dataDict=data)


class Hour(Vertex):
    """
    To be used for hour data where the identifying hour is formatted HH:MM (not fractions of a day).
    For now we will assume these are all also Data nodes, and thus must have a metadata_node.
    """
    
    def __init__(self, db, name, date_node, update_existing, data_dict, metadata_node, kind='Hour'):
        # May change kind to be a more specific type of hour, but this is discouraged.

        Vertex.__init__(self, name=name, kind=kind)

        self.kind = kind
        self.name = name
        self.date_node = date_node.node
        self.date_id = date_node.id
        self.date = self.date_node['Date__']
        self.metadata_node = metadata_node.node

        putative_hour_nodes = [node for node in db.find(self.kind, 'DateID', self.date_id)]
        
        if putative_hour_nodes:
            for p_hour_node in putative_hour_nodes:
                if str(self.name) == p_hour_node['Hour__']:
                    printv('Identified Hour node: {} from Date: {}. Reusing.'.format(self.name, self.date))
                    self.node = p_hour_node
                    self.id = p_hour_node['HourID']
                    self.update_values(node=self.node, new_data=data_dict, update_existing=update_existing)
                    
                    # Now check to see if there's already a relationship to the metadata node, and if not, create it
                    metadata_rels = db.match(start_node=self.node, rel_type='HAS_METADATA', end_node=self.metadata_node)

                    # metadata_rels still returns something of length 0 even if there are no relationships matching the criteria!
                    # So we need to use a roundaout method to check if it exists
                    found = False
                    if metadata_rels:
                        for rel in metadata_rels:
                            if str(rel.end_node()['DatasetID']) == str(self.metadata_node['DatasetID']):
                                found = True
                    if not found:
                        self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)
                        
                    break

        if not hasattr(self, 'id'):
            
            self.node, self.id = self.create_node(graphDB=db, label=self.kind, metadata=self.name, sourceNodes=self.date_node, 
                                                  sourceIDKey='DateID', sourceID=self.date_id, dataDict=data_dict)
            
            self.create_relationship(graphDB=db, sourceNode=self.node, rel_type='HAS_METADATA', endNode=self.metadata_node)
