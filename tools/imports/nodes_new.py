#!/usr/bin/env python
"""
Contains logic behind each node in the graph database network.


This script was created by Dr. Ben Bolduc and Dr. Suzanne Hodgkins, with support from the DOE-funded IsoGenie Project
and the NASA-funded A2A Project. Copyright © 2020 The Ohio State University.

This program is distributed under the terms of the GNU General Public Licence v3, with ABSOLUTELY NO WARRANTY. This is
free software, and you are welcome to redistribute and/or modify it under the terms of the GNU General Public License v3
(or later).
"""


import uuid
import time
import sys
import pandas as pd
import select
from datetime import datetime
from distutils.version import StrictVersion
from tools.utilities.utils import error, warning, printv
from pprint import pprint
from dateutil.parser import parse
#import cProfile
#pr = cProfile.Profile()
#pr.disable()

# NEW neo4j Python driver
from neo4j import GraphDatabase

# Every "lower" level data element should be able to identify it's location in the hierarchical and aggregate major
# properties of the data above
# This is what get_upstreams() is for.

"""
Roadmap of class attributes in nodes_new.py:

All Vertex subclasses include the following attributes:

self.kind: Main node label corresponding to the class name.
self.name: Descriptive property used to name this specific node (e.g., for the Site class, this would be the site name).
self.id: uuid (e.g. SiteID), unique to each node of its kind, but which can be passed to "downstream" nodes.
self.node: Data structure (of type <class 'neo4j.types.graph.Node'>) outputted from transaction functions, and which
    contains the labels, properties, and other data in the node itself (see below).

Unlike the old py2neo Node class, the type <class 'neo4j.types.graph.Node'> is merely a snapshot of the node, and does 
not directly link back to the graph database. To refer to the same node again in another query, it needs to be matched 
based on attributes in this data structure. These attributes are:

._id: node id (not uuid, but the id created automatically by neo4j). This is the most foolproof way to refer back to the 
    same node, and so the transaction functions defined here do just that. To distinguish these from UUIDs (which are 
    stored in variables with names formatted *_id, e.g. "source_id"), the variables storing these neo4j-assigned IDs 
    are named using the format id_* (e.g., "id_source").
._properties: properties (type: dict)
._labels: labels (type: set) (this attribute isn't currently used here, but may be useful for querying)
"""


class Vertex(object):
    """
    Master class object, basically to be used for inheritance

    Attributes:
        name (str)
        kind (str): Type of vertex object, i.e. Site, Core, Depth, etc...
    """

    def __init__(self, name=None, kind=None):

        if name is None:
            self.name = 'Vertex'

        if kind is None:
            self.kind = 'Generic'

    def run_fixed_query(self, tx, query):
        """For running any query string as-is, without returning a result. This should be avoided in most cases, but is
         useful for simple one-time operations (e.g. deleting all nodes with a given label from the database)."""
        tx.run(query)

    def check_name(self, tx, label, key, value):  # To be called with session.read_transaction(check_name, label=label, key=key, value=value)
        """Checks if there's already a node in the database (graph) with label=label, property_key=key,
        and property_value=value. Returns the node if there is, or False if no node."""
        
        """No longer need to retry each time; transactions have auto-retry mechanism."""

        result = tx.run("MATCH (n:`%s` {`%s`: $value}) RETURN n LIMIT 1" % (label, key), value=value)
        nodes = [record['n'] for record in result]
        
        if nodes == []:
            return False
        else:
            return nodes[0]
        
    # diff
    def check_names(self, tx, label, key, value):  # To be called with session.read_transaction(check_names, label=label, key=key, value=value)
        """Checks for multiple nodes in the database (graph) with label=label, property_key=key,
        and property_value=value. Returns nodes if they exist, or False if no node."""
        
        """No longer need to retry each time; transactions have auto-retry mechanism."""

        result = tx.run("MATCH (n:`%s` {`%s`: $value}) RETURN n" % (label, key), value=value)
        nodes = [record['n'] for record in result]
        
        if nodes == []:
            return False
        else:
            return nodes

    # def check_unique_names(self, tx, label, key1, value1, key2, value2):
    #     # Can't use a dict with MATCH, so have to put each property into the query string individually
    #     result = tx.run("MATCH (n:`%s` {`%s`: $value1, `%s`: $value2}) RETURN n" % (label, key1, key2), value1=value1, value2=value2)
    #     nodes = [record['n'] for record in result]
    #
    #     if nodes == []:
    #         return False
    #     elif len(nodes) > 1:
    #         error('Found more than one ({}) unique nodes with label={} and properties={}.'.format(len(nodes), label,
    #                                                                                               {key1: value1, key2: value2}))
    #     else:
    #         return nodes[0]

    def check_unique_names_dict(self, tx, label, data_dict, return_list=False):
        # Find a unique node that matches label and data_dict with up to 4 properties.
        # If return_list is set to True, then it can return multiple nodes.
        # TODO: Once this has been tested, get rid of check_unique_names and use this method instead.
        # TODO: Consider adding an option for an arbitrary-length dict using string replacement? An example of this is
        #  in import_sequence_data.py (old version). The disadvantage is that it won't work for dict values containing
        #  the quote character used in the parameter string.

        # Can't use a dict with MATCH, so have to put each property into the query string individually, even though it's
        #  tedious to do this for up to 4 properties.

        data_s = pd.Series(data_dict)  # convert to Series to make it iterable

        if len(data_s) == 0:
            error('Empty data_dict')

        elif len(data_s) == 1:
            key0 = data_s.keys()[0]
            value0 = data_s[0]
            result = tx.run("MATCH (n:`%s` {`%s`: $value0}) RETURN n" % (label, key0), value0=value0)

        elif len(data_s) == 2:
            key0 = data_s.keys()[0]
            value0 = data_s[0]
            key1 = data_s.keys()[1]
            value1 = data_s[1]
            result = tx.run("MATCH (n:`%s` {`%s`: $value0, `%s`: $value1}) RETURN n" % (label, key0, key1),
                            value0=value0, value1=value1)

        elif len(data_s) == 3:
            key0 = data_s.keys()[0]
            value0 = data_s[0]
            key1 = data_s.keys()[1]
            value1 = data_s[1]
            key2 = data_s.keys()[2]
            value2 = data_s[2]
            result = tx.run("MATCH (n:`%s` {`%s`: $value0, `%s`: $value1, `%s`: $value2}) RETURN n" % (label, key0, key1, key2),
                            value0=value0, value1=value1, value2=value2)

        elif len(data_s) == 4:
            key0 = data_s.keys()[0]
            value0 = data_s[0]
            key1 = data_s.keys()[1]
            value1 = data_s[1]
            key2 = data_s.keys()[2]
            value2 = data_s[2]
            key3 = data_s.keys()[3]
            value3 = data_s[3]
            result = tx.run("MATCH (n:`%s` {`%s`: $value0, `%s`: $value1, `%s`: $value2, `%s`: $value3}) RETURN n" % (label, key0, key1, key2, key3),
                            value0=value0, value1=value1, value2=value2, value3=value3)

        else:  # if it's longer than 4 items
            error('data_dict too long: {} items'.format(len(data_dict)))

        nodes = [record['n'] for record in result]

        if nodes == []:
            return False

        elif return_list:
            return nodes  # this returns the whole list, even if its length is 1

        elif len(nodes) > 1:
            error('Found multiple ({}) nodes with label={} and properties={}.'.format(len(nodes), label, data_dict))

        else:
            return nodes[0]

    def check_relationships(self, tx, id_source, rel_type, id_end):
        result = tx.run("MATCH (a)-[:`%s`]->(b) WHERE id(a)=%s AND id(b)=%s RETURN a,b" % (rel_type, str(id_source), str(id_end)))
        relationships = [{'source_node': record['a'], 'end_node': record['b']} for record in result]

        if relationships == []:
            return False
        elif len(relationships) > 1:
            error('Found more than one identical relationship connecting the same two nodes: {}, {}'.format(id_source, id_end))
        else:
            return relationships

    def update_labels(self, tx, id_node, labels):  # To be called with session.write_transaction(update_labels, ...)
        # Add additional labels to an existing node, such as habitat associated with site
        if not isinstance(labels, list):
            labels = [labels]

        labels_str = '`' + '`:`'.join(labels) + '`'
        result = tx.run("MATCH (n) WHERE id(n)=%s SET n:%s RETURN n" % (str(id_node), labels_str))
        node = [record['n'] for record in result][0]
        return node

    def update_all_values(self, tx, id_node, props):
        # CAUTION: This resets ALL properties, deleting any that aren't in the 'props' dict! When using this, make sure
        # to include any old properties that you want to keep in 'props'. The method update_values, which calls this
        # method, already does this. The methods that call this method (update_values and create_node_with_properties)
        # will already do this, so are safe to use, but just be careful when calling this method directly.
        try:
            result = tx.run("MATCH (n) WHERE id(n)=%s SET n=$props RETURN n" % (str(id_node)), props=props)
        except TypeError as e:
            print('Information on "props" parameter: ')
            print(type(props))
            print(props)
            print(e)
        node = [record['n'] for record in result][0]
        return node
                    
    def update_single_value(self, tx, id_node, key, value):  # To be called with session.write_transaction(update_single_value, ...)
        result = tx.run("MATCH (n) WHERE id(n)=%s SET n.`%s` = $value RETURN n" % (str(id_node), key), value=value)
        node = [record['n'] for record in result][0]
        return node
        
    def remove_single_value(self, tx, id_node, key):  # To be called with session.write_transaction(remove_single_value, ...)
        result = tx.run("MATCH (n) WHERE id(n)=%s REMOVE n.`%s` RETURN n" % (str(id_node), key))
        node = [record['n'] for record in result][0]
        return node
        
    def create_node(self, tx, label, additional_labels=None):  # To be called with session.write_transaction(create_node, ...)
        if additional_labels is None:
            additional_labels = []
        labels = [label] + additional_labels
        labels_str = '`' + '`:`'.join(labels) + '`'
        result = tx.run("CREATE (n:%s) RETURN n" % (labels_str))
        node = [record['n'] for record in result][0]
        return node
        
    # Function for creating additional relationships (other than those created by create_node, e.g. Data to Metadata)
    def create_relationship(self, tx, id_source, rel_type, id_end):  # To be called with session.write_transaction(create_relationship, ...)
        tx.run("MATCH (a),(b) WHERE id(a)=%s AND id(b)=%s MERGE (a)-[:`%s`]->(b)" % (str(id_source), str(id_end), rel_type))

    def remove_relationship(self, tx, id_source, rel_type, id_end):  # for removing relationships by name from specific node IDs
        tx.run("MATCH (a)-[r:`%s`]->(b) WHERE id(a)=%s AND id(b)=%s DELETE r" % (rel_type, str(id_source), str(id_end)))
                    
        
    # The methods below are "higher-level" functions that call the "lower-level" transaction functions above.
        
    # Create
    def create_node_with_properties(self, session, label, metadata, sourceNodes=False, sourceIDKey=False, sourceID=False,
                                    rel_type=False, dataDict=None, additional_labels=None):
        """
        Generalized node creation, to replace the individual create_* methods.       
        Can be used for any "conventional" node, i.e. nodes that will have a relationship pointing from <=1 source node.
        
        Nodes that represent an aggregation of relationships from several source nodes (e.g. binned MAGs) will still 
        need their own dedicated create_* functions. However, these may still be able to get their own generalized 
        function that takes a list of source nodes, loops over them to make the Relationships, and doesn't use 
        get_upstreams().
        
        NEW: Can specify a list of multiple sourceNodes. Assume rel_type is the same for both. sourceIDKey and sourceID
        can be from any of them.
        
        """
        #pr.enable()

        if additional_labels is None:
            additional_labels = []
        if dataDict is None:
            dataDict = {}

        # make sure all sourceNodes info is specified, if applicable (all labels except except Area & Metadata)
        if not (label in ['Area', 'Metadata'] or (sourceNodes and sourceIDKey and sourceID)):
            error('Must specify sourceNodes, sourceIDKey, and sourceID for label {}'.format(label))
        
        newID = str(uuid.uuid4())
            
        # Assign metadataType, the string appended to '__' or 'ID' to build the canonical ID and property keys.
        if '-' in label:
            # Use of too-specific metadataType, resulting in properties like 'Soil-Depth__', is strongly discouraged.
            # Therefore use only the last part of the label after the hyphen ('Depth__' instead of 'Soil-Depth__')
            metadataType = label.split('-')[-1]
        else:
            metadataType = label
            
        # Make the canonical ID and __ keys
        metadataID = metadataType + 'ID'
        metadataKey = metadataType + '__'
            
        if not rel_type:
            # If rel_type isn't specified, it defaults to a value based on label (incl. parts before a hyphen).
            # For example, label 'Soil-Depth' yields rel_type 'HAS_SOIL_DEPTH'.
            rel_type = 'HAS_' + label.replace('-','_').upper()

        newNode = session.write_transaction(self.create_node, label, additional_labels)
        # this creates it with a label and ID, but it doesn't have any properties yet
        
        basic_properties = {
            'CreationDate__' : datetime.now().strftime('%Y%m%d'),
            metadataID : str(newID),
            metadataKey : str(metadata)
        }

        dataDict.update(basic_properties)
        
        if sourceNodes:
            # As an error-checking measure, add sourceID manually and check it (and everything else) with get_upstreams
            dataDict.update({sourceIDKey: str(sourceID)})
            
            # NEW: Allow sourceNode to be a list of several nodes, and rename to sourceNodes
            if type(sourceNodes) != list:
                sourceNodes = [sourceNodes]
                
            for sourceNode in sourceNodes:

                # skipMetadataProps = False
                # if 'Metadata' in newNode._labels:
                #     skipMetadataProps = True

                if 'Metadata' in newNode._labels:
                    keepMetadataProps = False
                else:
                    keepMetadataProps = True


                # NOTE: The above logic should work well for a newNode that was initially created with the Metadata label
                #  as its first label, but it won't catch nodes that will have the Metadata label added post-creation.
                # TODO: Instead of adding new labels by calling update_labels from outside these classes, add the option
                #  to set multiple labels during node creation.

                upstream_dict = self.get_upstreams(sourceNode._properties, dataDict, keepMetadataProps=keepMetadataProps)

                dataDict.update(upstream_dict)

                # Create relationship from sourceNode
                session.write_transaction(self.create_relationship, id_source=sourceNode._id, rel_type=rel_type, id_end=newNode._id)

        # Remove any NaN properties (except for keys ending in __ or ID)
        dataDict = {key: value for key, value in dataDict.items() if (value != 'NaN' and not(pd.isnull(value))) or key.endswith('__') or key.endswith('ID')}

        # Now, finally, add all the properties in one single transaction
        newNode = session.write_transaction(self.update_all_values, id_node=newNode._id, props=dataDict)

        #pr.disable()

        return newNode, newID

    # Update
    def update_values(self, session, node, new_data=None, update_existing='toValues'):
        """
        Updates a node with additional information.
        Options for update_existing, in order of comprehensiveness:
            'none'          Does nothing (used here in place of checking every single time before calling this function).
            'missing'       Replaces ONLY missing data with new_data.
            'toValues'      Replaces all data with new_data, except when doing so would change a value to NaN.
            'everything'    Replaces ALL data with new_data, even if doing so would change it to NaN.
        """
        if new_data is None:
            new_data = {}

        if update_existing not in ['none', 'missing', 'toValues', 'everything']:
            error('Invalid update_existing argument to update_values: {}'.update_existing)
            
        def ismissing(value):
            if pd.isnull(value) or str(value).lower() in ['nan', 'unknown', 'nan, nan', '']:
                return True
            else:
                return False
        
        # New code for extracting information from the node, for linking back to the graph DB.

        # NOTE: Edited on 2/14/2020 to use update_all_values to update the node in a single transaction. Because
        #  update_all_values overwrites ALL properties, essentially acting like update_existing=='everything' by
        #  default, this required an almost complete re-write of the code below.

        old_data = node._properties

        # First make sure there are no obvious issues.
        # For example, CreationDate__ should NEVER exist in new_data, as it should only ever be assigned on initial node creation.
        # (There may also be other things to check for; add them as they're discovered.)
        if "CreationDate__" in new_data.keys():
            print("\nExisting data:")
            print(old_data)
            print("\nNew data:")
            print(new_data)
            error("Attempting to change the CreationDate__ of an existing node. This should NEVER happen; aborting!!!")

        # Build the updated_dict, and update the node
        if update_existing != 'none':  # if updating anything at all

            updated_dict = dict(new_data)  # use a copy of new_data, so that adding things doesn't interfere with looping

            # Remove any non-essential-metadata properties with missing values from updated_dict
            for key in new_data.keys():
                if ismissing(new_data[key]) and not(key.endswith('__') or key.endswith('ID')):
                    del updated_dict[key]

            # The update_all_values method overwrites all properties. However, even if update_existing=='everything',
            #  we still want to make sure to keep essential metadata properties (i.e., keys ending in __ or ID) even if
            #  they're just NaN. So add them back (using old_data) if they are absent.
            for key in old_data.keys():
                # avoid deleting essential information by keeping anything ending in __ or ID, or source_DatasetIDs
                if key not in updated_dict.keys() and (key.endswith('__') or key.endswith('ID') or key == 'source_DatasetIDs'):
                    updated_dict.update({key: old_data[key]})

            # update_existing == 'everything' is the simplest case, requiring none of the complex comparisons below
            if update_existing != 'everything':
                # If update_existing is 'missing' or 'toValues', then we need to check each value to see if it would be
                #  replacing any old data, and if so, with what.
                for key, old_value in old_data.items():
                    try:
                        new_value = new_data[key]
                    except KeyError:
                        new_value = 'NaN'

                    # If the old node has missing data for this key, then don't need to do anything, as updated_dict
                    #   already contains the updated value. This should also cover "placeholder" properties with value
                    #   NaN that need to be kept (e.g., DataSubsample__ for most Data nodes), as updated_dict should
                    #   still contain this NaN value (and if it doesn't, then that's probably intentional anyway).
                    #   So we therefore only need to worry about old_value if it contains data.

                    if not(ismissing(old_value)):  # If the old node contains data, do the following:
                        if ismissing(new_value):  # If the old node contains data, but the new dict does not...
                            updated_dict.update({key: old_value})  # ...then add the old data to the new dict
                                                                   # (this applies to both 'missing' and 'toValues').

                        else:  # If BOTH the old node and the new dict contain data, then what to do next depends on
                               #  whether update_existing is 'toValues' or 'missing'.

                            # If it's 'toValues', then we want to replace the old value with the new value, which is
                            #  already in updated_dict, so we don't need to do anything.

                            # If, however, we're using 'missing' (i.e., only updating missing values)...
                            if update_existing == 'missing':
                                # ...then we need to revert updated_dict back to the old value.
                                updated_dict.update({key: old_value})

            # Now updated_dict contains all the old and new data that should be in the updated node.

            # Update the UpdateDate__ property (do this last, to avoid keeping old dates)
            updatedate = datetime.now().strftime('%Y%m%d')
            if update_existing != 'missing' or updatedate != old_data['CreationDate__']:
                # Set the UpdateDate__ property only if replacing actual values, or if adding previously-missing values
                #  to a node that isn't brand-new.
                updated_dict.update({'UpdateDate__': updatedate})

            # Finally, update the node in the DB.
            node = session.write_transaction(self.update_all_values, id_node=node._id, props=updated_dict)
            
        return node
        
                
    def get_upstreams(self, upstream_properties, downstream_properties, keepMetadataProps="default"):

        # change "default" keepMetadataProps to the actual default list (don't use the list as the default,
        #   because it's mutable and therefore might give hard-to-debug errors)
        if keepMetadataProps == "default":
            keepMetadataProps = ['Metadata__', 'MetadataID']

        # Instead of needing to 'request' this information multiple times, check to see if the node that the data
        # will be associated with already has it.
            
        # Edited to include anything ending in '__' or 'ID'. There are a LOT of these, so used str.endswith() instead of
        #  listing them individually.
        
        # Edited again to make the logic more consistent.
        
        upstream_keys = [key for key in dict(upstream_properties).keys() if key.endswith('__') or key.endswith('ID')]
        inherited_data = {}

        for key in upstream_keys:

            if key in ['Metadata__', 'MetadataID',
                       'DatasetID', 'Version__', 'VersionDate__', 'Type__', 'Availability__', 'ContactName__',
                       'ContactEmail__', 'Quality__', 'DOI__', 'ExternalRepositoryURL__', 'Published__',
                       'ManuscriptCitation__', 'ManuscriptURL__', 'readme_file_href__', 'original_file_href__',
                       'imported_file_href__', 'previous_version_href__', 'columns_file_href__']:
                #  Added properties specific to Metadata nodes, as a new node with the Metadata label may
                #  have its own set of properties which are different from an "upstream" Metadata node. "Normal"
                #  Metadata nodes won't have this issue, as the Metadata class doesn't call get_upstreams(), but this
                #  would be an issue for combined Data/Metadata nodes that were originally created using the Data class
                #  (e.g. in sequencing imports). Listing these properties individually, as such a node might still need
                #  to inherit other upstream properties (e.g. those corresponding to cores, depths, etc.).
                #
                # Old default behavior was to inherit only Metadata__ and MetadataID, but not the others-- re-added the
                # option to keep this functionality (which keeps the properties deriving from the 'Metadata' label
                # without cluttering the new node with all the other properties) as default. Can also specify other
                # properties to keep.
                if keepMetadataProps == False:
                    continue
                elif type(keepMetadataProps) == list:
                    if key not in keepMetadataProps:
                        continue

            if key in ['CreationDate__', 'UpdateDate__']:
                # Unlike the other upstreams, CreationDate__ and UpdateDate__ should be independent of the node's
                # position in the DB.
                continue

            if key == 'QualityFlag__':
                # Quality flags should also probably not be propagated, as they would usually only apply to actual data
                #  in Data nodes, and other Data nodes downstream (e.g., (Date)-->(Hour)) may have different (or no)
                #  quality flags.
                # TODO: May change this behavior later.
                continue

            elif key not in downstream_properties.keys():
                # Only include if it's not already in downstream_properties. If it's already in downstream_properties,
                # then upstream_properties[key] *should* be the same, but if there's a conflict, then we don't want to
                # overwrite the value in downstream_properties.
                inherited_data[key] = upstream_properties[key]

            elif str(upstream_properties[key]) == str(downstream_properties[key]):
                # if it exists in both and its value is the same, no need to do anything
                continue

            else:
                # If it exists in both, but it's NOT the same, then we should investigate why...
                if key == 'Depth__':
                    # Depth__ is inconsistently formatted, and there are already other checks to make sure they're matched
                    continue

                elif 'NaN' in [str(downstream_properties[key]), str(upstream_properties[key])]:
                    # e.g. some GPS__ and DepthMin__/DepthMax__ values

                    if str(downstream_properties[key]) == 'NaN':
                        # If it's NaN in the new data, can still get these values from key
                        inherited_data[key] = upstream_properties[key]
                        
                    if str(upstream_properties[key]) == 'NaN':
                        # Sometimes it's the other way around: upstream_properties has NaN but downstream_properties has a value.
                        # Don't like this, but can let it go with a warning.
                        # Could also update upstream_properties, but this isn't going to fix any further NaNs even further key, so won't bother.
                        warning('New {} data appears after an upstream value of NaN.\n'
                                'Upstream properties: {}\n'
                                'New value: {}'.format(key, upstream_properties, str(downstream_properties[key])))
                    
                else:
                    # If the key exists in both but its value is different, and it's not one of the above cases, then something 
                    #  is likely wrong.
                    # But it also could just be a mismatch in # of decimal places recorded between labs. This depends on what it is.
                    message = ("New {} data does not match the upstream data in the DB! \n" + 
                      "Upstream properties: {}\n" + 
                      "Upstream value: {}\n" + 
                      "New value: {}").format(key, upstream_properties, str(upstream_properties[key]), str(downstream_properties[key]))

                    if key == 'GPS__':
                        # GPS is inconsistent anyway
                        # common_vars.standardize_gps should have omitted any trivial format inconsistencies,
                        #   so it's the actual values that are different
                        warning(message)
                            # TODO Add an option to specify which GPS is the "correct" one

                    elif key in ['DepthMin__', 'DepthMax__', 'DepthAvg__']:

                        if abs(float(upstream_properties[key]) - float(downstream_properties[key])) == 0:
                            # if converting to floats and subtracting results in 0, then it's probably just an int vs. decimal issue
                            continue

                        elif abs(float(upstream_properties[key]) - float(downstream_properties[key])) <= 5:
                            # if they're different by <=1cm, assume that Depth.is_depth_match() took care of it, and send a warning
                            # UPDATE: Adjust to 5 for some big incubation depth ranges
                            warning(message)

                        else:
                            error(message)

                    elif key in ['DateStart__',  'DateEnd__']:
                        # Allow it if both the new start and end dates are between the upstream start and end dates.

                        upstream_datestart = datetime.strptime(str(upstream_properties['DateStart__']), "%Y-%m-%d")
                        upstream_dateend = datetime.strptime(str(upstream_properties['DateEnd__']), "%Y-%m-%d")
                        downstream_datestart = datetime.strptime(str(downstream_properties['DateStart__']), "%Y-%m-%d")
                        downstream_dateend = datetime.strptime(str(downstream_properties['DateEnd__']), "%Y-%m-%d")

                        if upstream_datestart < downstream_datestart and upstream_dateend > downstream_dateend:
                            continue
                        else:
                            error(message)

                    else:
                        error(message)
        
        return inherited_data


class UniqueNode(Vertex):
    """
    General-purpose Vertex for identifying a node of *any* type based on a unique combination of a label and up to 4
    properties, without the need to supply an upstream node.
    UniqueNode objects can then be supplied as inputs when initializing other Vertex objects.
    Since they are used *only* for this purpose, they are treated as "read-only," i.e. the class is used to identify
    nodes but does not create or edit them.
    """
    def __init__(self, session, kind, id_key, data_dict, result_required=True):
        # data_dict should be a dict of up to 4 unique properties for nodes with label=kind
        # id_key should be the key for a UUID unique for nodes with label=kind, and this UUID is stored once the node is
        #  found (e.g., if kind='Soil-Depth', then id_key should be 'DepthID')

        Vertex.__init__(self, kind=kind)  # name will be assigned to None

        self.kind = kind

        #self.node = session.read_transaction(self.check_names, label=self.kind, key='Area__', value=self.name)
        self.node = session.read_transaction(self.check_unique_names_dict, label=self.kind, data_dict=data_dict)

        if not self.node:
            if result_required:
                error("Could not find any {} nodes with {}".format(kind, data_dict))
            else:
                self.id=False  # self.node would already be False in this case
        else:
            self.id = self.node._properties[id_key]

    def __repr__(self):
        return "General class for any node found without use of an upstream node"


class Metadata(Vertex):
    """
    Metadata vertex -- used to store data contacts, quality level, DOI, version # of original file(s),
    link(s) to original file(s), link(s) to READMEs, etc.
    
    The idea is that each of these would have multiple HAS_METADATA relationships pointing to it from 
    its associated Data nodes. This is essentially similar to a Area node connected to several Site nodes,
    except that all the relationships are pointing in the opposite direction. So easiest to re-use that same 
    code structure, i.e. create (or check for) the Metadata node at the very beginning of each import script
    (before any other nodes are created), and then associate each Data node with that Metadata node.
    
    ID/versioning scheme:
        
        Each dataset "entity" (corresponding to a table row on the Data Downloads page on the web) gets its own 
        unique DatasetID, which is assigned in the file list CSV. If a new version of a file is imported, the 
        version # is updated, but the DatasetID stays the same.
    
    Still need to do (SHOULD BE DONE NOW; but still keeping this text just in case):
        
      - If this file is different than the original (e.g. it's the output of a Standardize_ script), then need
        to add the file path of the original file to data_dict. Need a standardized naming scheme: maybe 
        original_file_href__ if applicable, and don't include intermediate "formatted" files (possibly don't
        even need to provide links to those on the web server)? or maybe change the "standardized" label to "imported"
        so that this label can also apply to simple files that didn't need to be standardized?
        
      - Add a "Proceed? y/n" option to the warning
        
    """
    
    def __init__(self, session, name, data_dict, update_existing_data, kind='Metadata', additional_labels=None,
                 assume_metadata_only=False):
        # update_existing_data should receive the value of update_existing for the associated Data nodes.
        # It controls whether or not the Metadata is updated, depending on how/if the associated Data is being updated.
        #
        # NEW (as of July 1 2019): Added an update_existing_data=False option, to be used when updating ONLY the Metadata nodes  
        #   but no actual data (for example, if adding a new contact person or DOI).
        # So its purpose is slightly different than update_existing in the Data class, hence the slightly different name.
        
        # Dict of human-readable descriptions for the update_existing_data options.
        # These should be understandable to someone not familiar with the database. May need to further edit for clarity.
        update_descriptions = {
            'none' : "This file was not actually used to update any existing data; it was only used to check for data and import it if it didn't already exist.",
                # 'none' and 'missing' sound similar because they are; the difference is that 'none' can only create new nodes, whereas 'missing' can add data to existing nodes
            'missing' : "This file was used to add data missing from the last version of the same dataset, but no old data was actually changed.",
            'toValues' : "This file was used to update the data values from the last version of the same dataset, but some obsolete data (if any) from the old version may still exist.",
            'everything' : "This file was used to completely overwrite the last version of the same dataset, including deleting obsolete data (if any)."
        }
        
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                             data_dict={'DatasetID': self.name})
        
        # If node doesn't exist, create it
        if not self.node:
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  dataDict=data_dict, additional_labels=additional_labels)
            # The Metadata node created here has the following properties:
            # DatasetID (in data_dict) and Metadata__ (generated by create_node_with_properties) both contain the
            #   manually-created DatasetID from the file list csv.
            # MetadataID (generated by create_node_with_properties) will be auto-generated by uuid.uuid4().
            # Other properties in data_dict will include file path, version, quality level, etc.
            
        else:  # if the Metadata node already exists
            self.id = self.node._properties['MetadataID']
            
            if update_existing_data in ['none', 'missing', False] and data_dict['Version__'] != self.node._properties['Version__'] and not(assume_metadata_only):
                warning("Attempting to import from a new version of a file without updating any existing values from the old version. "
                        "This may not be an issue if you're only updating a Metadata node whose corresponding file is not parsed for "
                        "detailed Data imports, OR you're only adding completely new data and are SURE that no existing values need to "
                        "be changed (and if speed is a concern). Otherwise, please either use a more comprehensive value for '--update' "
                        "(either 'toValues' or 'everything'), or import an old version of the file (if no update is intended).\n"
                        "File being imported: {}\n"
                        "Version of this file: {}\n"
                        "Version in database: {}".format(data_dict['imported_file_href__'], data_dict['Version__'], self.node._properties['Version__']))
                proceed = ''
                while proceed not in ['y', 'n']:
                    
                    print('Proceed with current --update argument of "{}"?'.format(update_existing_data))
                    print('You have 10 minutes to answer (y or n), beginning at {} (after which the import will proceed with the current --update argument):'.format(datetime.now().strftime('%H:%M')))
                    i, o, e = select.select( [sys.stdin], [], [], 10*60 )
                    
                    if i:
                        proceed = sys.stdin.readline().strip().lower()
                        if proceed not in ['y', 'n']:
                            print('You said "{}". Please select a valid response (either "y" or "n").'.format(proceed))
                    else:
                        warning('Timed out with no response. Automatically proceeding with --update argument of "{}".'.format(update_existing_data))
                        proceed = 'y'
                    
                if proceed == 'n':
                    print('Exiting...')
                    sys.stderr.flush()
                    sys.exit(1)
            
            if update_existing_data:  # I.e. unless it's explicitly False
                data_dict['UpdateMethod__'] = update_descriptions[update_existing_data]

            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id, labels=additional_labels)

            self.node = self.update_values(session=session, node=self.node, new_data=data_dict, update_existing='everything')  # here update_existing refers to updating the Metadata node itself

    def __repr__(self):
        return "Metadata for a set of Data nodes"


class Column(Vertex):
    """
    Metadata describing the columns in a data file (definitions, measurement units, methods, etc.)

    """

    def __init__(self, session, name, metadata_node, kind='Column', data_dict=None, additional_labels=None):

        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.metadata_node = metadata_node.node
        self.metadata_id = metadata_node.id

        p_col_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                              data_dict={'MetadataID': self.metadata_id, 'Column__': self.name})

        if p_col_node:
            printv('Identified Column node: {} from Metadata: {}. Reusing.'.format(self.name, p_col_node._properties['Metadata__']))
            self.node = p_col_node

            # Update data_dict with standardized metadata node properties, which would be auto-populated using
            #  get_upstreams if this was the first time creating the node, but wouldn't be automatically updated
            #  if the node already exists (which it does if the code makes it to this point). Normally,
            #  standardized properties are not changed once created, so this should not be done for other node
            #  classes unless there's a specific reason.
            if data_dict is None:
                data_dict = {}
            metadata_node_props = {k: v for k, v in self.metadata_node._properties.items()
                                   if (k.endswith('__') or k.endswith('ID')) and k not in ['CreationDate__', 'UpdateDate__']
                                   and k not in data_dict.keys()}

            data_dict.update(metadata_node_props)

            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id, labels=additional_labels)

            self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                           update_existing='everything')  # used same update_existing as for Metadata node class

            self.id = p_col_node._properties['ColumnID']

        if not hasattr(self, 'id'):
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.metadata_node, sourceIDKey='MetadataID',
                                                                  sourceID=self.metadata_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)

    def __repr__(self):
        return "Column name (from a datafile) with attributes"
    

class Area(Vertex):
    """
    Area vertex -- almost called it Peatland but may eventually want to include non-Peatland areas if this DB expands.
    For IsoGenie there is just one Area ("Stordalen"), with which all Sites are associated.
    For A2A there are five Areas.
    """

    def __init__(self, session, name, kind='Area', data_dict=None, additional_labels=None, update_existing='missing'):
        
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                             data_dict={'Area__': self.name})

        # If node doesn't exist, create it
        if not self.node:
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  dataDict=data_dict, additional_labels=additional_labels)
        else:
            self.id = self.node._properties['AreaID']

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

    def __repr__(self):
        return "Area with attributes"


class Site(Vertex):
    """
    Site vertex

    """

    def __init__(self, session, name, area_node, kind='Site', data_dict=None, additional_labels=None, update_existing='missing'):
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name        
        self.kind = kind
        self.area_node = area_node.node
        self.area_id = area_node.id

        p_site_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                               data_dict={'AreaID': self.area_id, 'Site__': self.name})

        if p_site_node:
            printv('Identified Site node: {} from Area: {}. Reusing.'.format(self.name, p_site_node._properties['Area__']))
            self.node = p_site_node
            self.id = p_site_node._properties['SiteID']

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):

            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.area_node, sourceIDKey='AreaID',
                                                                  sourceID=self.area_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)

    def __repr__(self):
        return "Site with attributes"


class FieldSampling(Vertex):
    """
    For storing info about sampling trips (ranges of dates).
    These are on the same "level" as Sites (between Areas and Cores), but parallel to Sites.
    """

    def __init__(self, session, name, area_node, data_dict, kind='FieldSampling', additional_labels=None,
                 update_existing='missing'):

        Vertex.__init__(self, name=name, kind=kind)

        self.name = name  # use DateShort__, renamed to FieldSampling__
        self.kind = kind
        self.area_node = area_node.node
        self.area_id = area_node.id

        p_smp_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                              data_dict={'AreaID': self.area_id, 'FieldSampling__': self.name})

        if p_smp_node:
            printv('Identified FieldSampling node: {} from Area: {}. Reusing.'.format(self.name,
                                                                                      p_smp_node._properties[
                                                                                          'Area__']))
            self.node = p_smp_node
            self.id = p_smp_node._properties['FieldSamplingID']

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.area_node, sourceIDKey='AreaID',
                                                                  sourceID=self.area_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)

    def __repr__(self):
        return "Field sampling trip with attributes"


class Core(Vertex):
    """
    Core vertex. Cores are *always* part of Sites, and inherit many of their attributes


    """

    def __init__(self, session, name, site_node, fieldsampling_node, gps, kind='Core', data_dict=None,
                 additional_labels=None, update_existing='missing'):
        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.gps = gps
        self.site_node = site_node.node
        self.fieldsampling_node = fieldsampling_node.node
        self.site_id = site_node.id

        p_core_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                          data_dict={'SiteID': self.site_id, 'CoreDated__': self.name})

        if p_core_node:
            printv('Identified Core node: {} from Site: {}. Reusing.'.format(name, p_core_node._properties['Site__']))
            self.node = p_core_node
            self.id = p_core_node._properties['CoreID']

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):
            
            data = {
                    'CoreDated__': self.name,
                    'GPS__': gps
            }
            
            # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
            # Unlike Data nodes, this only adds values to new nodes, and doesn't change existing nodes.
            # This is so that "official" info from coring sheets isn't overwritten by geochem info, etc.
            if data_dict is None:
                data_dict = {}
            data = {**data, **data_dict}
                
            corenum = str(self.name.split('|')[0])  # get core NUMBER to match with the auto-created Core__ property
            
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=corenum,
                                                                  sourceNodes=[self.site_node, self.fieldsampling_node],
                                                                  sourceIDKey='SiteID', sourceID=self.site_id,
                                                                  dataDict=data, additional_labels=additional_labels)

    def __repr__(self):
        return "Core with attributes"


class CoreDepth(Vertex):
    """
    For depths associated with a core.
    Unlike other nodes, "identical" depths are matched numerically instead of based on an exact name match.

    """

    def __init__(self, session, name, core_node, data_dict, kind='Core-Depth', additional_labels=None, update_existing='missing'):
        # Default kind = 'Core-Depth' so that it works as a generic for both soil and sediment depths, but in practice
        #  should be changed to 'Soil-Depth' or 'Sediment-Depth' depending on the sample type.
        # NEW: data_dict is now mandatory, and contains the depth components used for depth matching (it raises an error
        #  if any of the mandatory keys are missing). This allows data_dict updates, but uses update_existing='missing'
        #  to discourage overwriting metadata in this node type (as it doesn't typically receive the Data label). This
        #  was done to allow bug-free updating with the new DepthAvg__ property.
        # This class also now includes the ability to update labels internally.
        # TODO: Once DepthAvg__ is added to these nodes, also update any "downstream" nodes with this property that
        #  weren't already updated (this should just include sequencing data).

        Vertex.__init__(self, name=name, kind=kind)

        # TODO Better handling of neo4j nodes vs IsoGenieDB

        self.name = name  # This is the *only* class where name is not matched directly
        self.kind = kind  # Can be Soil-Depth or Sediment-Depth, unless using the default Core-Depth
        self.core_node = core_node.node
        self.core_id = core_node.id

        # Get specific depth components from data_dict
        min_depth = data_dict['DepthMin__']
        max_depth = data_dict['DepthMax__']
        avg_depth = data_dict['DepthAvg__']  # not used for matching, but should check to make sure it exists
        depth_only = data_dict['Depth__']
        try:
            subdepth = data_dict['DepthSubsample__']
        except KeyError:
            subdepth = 'NaN'

        # For depths, find multiple nodes and loop through them in the old way to allow for non-standard depth matching.
        putative_depth_nodes = session.read_transaction(self.check_names, label=self.kind, key='CoreID', value=self.core_id)

        if putative_depth_nodes:
            for p_depth_node in putative_depth_nodes:
                if self.is_depth_match(min_depth=min_depth, max_depth=max_depth, depth_only=depth_only, 
                                       subdepth=subdepth, depth_node=p_depth_node):
                    printv('Identified Depth node: {} from Core: {}. Reusing.'.format(name, p_depth_node._properties['CoreDated__']))
                    self.node = p_depth_node
                    self.id = p_depth_node._properties['DepthID']

                    # New: Add the ability to update labels and values, even though this isn't a Data node type
                    if additional_labels:
                        self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                              labels=additional_labels)

                    if data_dict:
                        self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                                       update_existing=update_existing)
                    break

        if not hasattr(self, 'id'):

            data = {
                    'Depth': self.name  # Analogous to DepthComplete__, this includes subdepth, but we're using the
                                        #  un-underscored Depth instead because it can be inconsistent with
                                        #  DepthComplete__ in the downstream Data nodes (i.e. ranges vs. single #s)
            }
            
            # merge data_dict into data. if there are duplicates, the values in the 2nd argument win.
            data = {**data, **data_dict}
                
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=depth_only,
                                                                  sourceNodes=self.core_node, sourceIDKey='CoreID',
                                                                  sourceID=self.core_id, dataDict=data,
                                                                  additional_labels=additional_labels)
    
    def is_depth_match(self, min_depth, max_depth, depth_only, subdepth, depth_node):
        # code for numerically comparing min and max depths

        depth_n_props = depth_node._properties
   
        db_depth = depth_n_props['Depth__']  # depth_only = new data, db_depth = DB data
        try:
            db_subdepth = depth_n_props['DepthSubsample__']
        except KeyError:
            db_subdepth = 'NaN'

        if not db_subdepth and not isinstance(db_depth, (int, float)):
            # Not sure if this is really a good descriptor for this error, or even if it should be an error.
            # It was left over from earlier; changed the placeholders so that it will print.
            error('No depth identified in DB for Core {}, Site {}, Date {}. It is possible that the core'
                  ' did not have a depth associated with it, or a measurement "disconnect" between'
                  ' labs'.format(depth_n_props['Core__'], depth_n_props['Site__'], depth_n_props['FieldSampling__']))

        # Below is logic for determining ends and whether or not they fall within a range
        if db_depth == 'None':  # Should NOT happen, but just in case...
            error('Identified "None" depth for Core: {}, Site {}, Date {}'.format(depth_n_props['Core__'], 
                    depth_n_props['Site__'], depth_n_props['FieldSampling__']))
        
        # "Sub"-depths can only be an exact match, so weed out cases where these don't match
        if str(db_subdepth) != str(subdepth):
            return False
        
        # Now we can actually compare the depths.
        
        # simplest case: the values match *exactly*
        if str(db_depth) == str(depth_only):
            return True

        # special case: one of the values is 'above peat'
        # CONVENTION: Any non-numeric above-peat depths should use this name in the standardized sheets!
        elif db_depth == 'above peat':
            try:
                depth_only = float(depth_only)
                if depth_only < 0:
                    printv('Identified negative new depth: {} as "above peat"'.format(depth_only))
                    return True
                else:
                    return False
            except ValueError:  # depth_only can't be converted to float
                return False  # the 'simplest case' already took care of cases where both == 'above peat'
            
        elif depth_only == 'above peat':
            return False  # may later try to match new 'above peat' depth with negative db depth

        # numerical depth comparison
        else:
            # New streamlined method: Compare only the min and max depths. For single-value depths,
            # use min = max = depth value.

            # For some reason, even though we did fillna('NaN') earlier, it's still sometimes changing these 'NaN'
            #  strings back to np.nan. Force them back to being strings (even if 'nan' or '' instead of 'NaN'):

            # Should use pd.isnull for objects, should cover strings, ints and floats
            min_db = str(depth_n_props['DepthMin__'])
            max_db = str(depth_n_props['DepthMax__'])
            min_str = str(min_depth)
            max_str = str(max_depth)
            db_depth_str = str(db_depth)
            depth_only_str = str(depth_only)

            # Add _ to the NaN values to keep them as strings
            # The below doesn't work - because the loop only re-assigns a *copy* of each variable
#            for obj in [min_db, max_db, min_str, max_str, db_depth_str, depth_only_str]:
#                if obj.lower() == 'nan':
#                    obj += '_'
            # instead need to edit each individually        
            if min_db.lower() == 'nan':
                min_db += "_"
            if max_db.lower() == 'nan':
                max_db += "_"
            if min_str.lower() == 'nan':
                min_str += "_"
            if max_str.lower() == 'nan':
                max_str += "_"
            if db_depth_str.lower() == 'nan':
                db_depth_str += "_"
            if depth_only_str.lower() == 'nan':
                depth_only_str += "_"                                        

            # Now that everything is a string:
            
            # Try to get numbers for both min and max, for both the new data and the DB
            try:
                min_db = float(min_db)
                max_db = float(max_db)
            except ValueError:
                try:
                    min_db = float(db_depth_str)
                    max_db = min_db
                except ValueError:
                    error('db depth: {} is not recognizable'.format(db_depth))

            try:
                min_str = float(min_str)
                max_str = float(max_str)
            except ValueError:
                try:
                    min_str = float(depth_only_str)
                    max_str = min_str
                except ValueError:
                    error('new depth: {} is not recognizable'.format(depth_only))

            # Next compare the min and max between db and new data
            # One must be completely within the other
            if min_db <= min_str <= max_str <= max_db or min_str <= min_db <= max_db <= max_str:
                return True

            elif ((isinstance(min_str, float) and isinstance(max_str, float) and isinstance(
                    min_db, float) and isinstance(max_db, float)) and not
            (pd.isnull(float(min_str)) or pd.isnull(float(max_str)) or pd.isnull(
                float(min_db)) or pd.isnull(float(max_db)))):
                return False  # continue if the depth isn't a match but the depth values are non-NaN floats (as they should be)

            else:  # invalid depths, which aren't floats
                error('Unable to identify entire or partial depths from either new data or '
                      'database information.\nNew Min: {}\nNew Max: {}\nDB Min: {}\nDB Max: {}\n'
                      'New Subdepth: {}\n DB Subdepth: {}. \n'
                      'The database depth_n_props is: \n{}'.
                      format(min_str, max_str, min_db, max_db, subdepth, db_subdepth,
                             depth_n_props))
                
        return False  # only gets to this point if it hasn't already returned True


class IncubationRep(Vertex):
    """
    Incubation (or incubation-associated field sample) from a core depth.
    """

    def __init__(self, session, name, depth_node, kind='IncubationRep', data_dict=None, additional_labels=None, update_existing='missing'):
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name        
        self.kind = kind
        self.depth_node = depth_node.node
        self.depth_id = depth_node.id

        p_inc_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                              data_dict={'DepthID': self.depth_id, 'IncubationRep__': self.name})

        if p_inc_node:
            printv('Identified IncubationRep node: {} from Depth: {}. Reusing.'.format(self.name, p_inc_node._properties['Depth__']))
            self.node = p_inc_node
            self.id = p_inc_node._properties['IncubationRepID']

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):

            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.depth_node, sourceIDKey='DepthID',
                                                                  sourceID=self.depth_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)

    def __repr__(self):
        return "Incubation replicate"
    

class Sequence(Vertex):
    def __init__(self, session, name, origin_node, data_dict, origin_id_key="DataID", kind="Sequence",
                 additional_labels=None, update_existing='none'):
        # default update_existing='none' because Sequence data is typically updated by adding new nodes

        Vertex.__init__(self, name=name, kind=kind)

        # This is really a subtype of Data node. Therefore, need to add the Data label during node creation (perhaps
        # similarly to the Hour node class, except the label should be added within the class itself).
        # Also added code to treat origin_node as a Metadata node in addition to its usual "upstream node" treatment.

        self.name = name  # this should be the record id parsed from the sequence file
        self.kind = kind
        self.origin_node = origin_node.node
        self.origin_id = origin_node.id  # If origin_node was created using the Data class (this is usually the case), then this would be DataID
        self.metadata_node = self.origin_node

        metadata_DatasetID = str(self.metadata_node._properties['DatasetID'])
        data_dict.update({'source_DatasetIDs': metadata_DatasetID})  # Although Sequence nodes will already inherit DatasetID from upstream via HAS_SEQUENCE, source_DatasetIDs is also added for consistency with other node types.

        current_version = data_dict['Version__']  # data_dict MUST include the version #

        # Three possibilities:
        # (1) there is no existing Sequence node at all
        # (2) there is an existing Sequence node with the same version number
        # (3) there is an existing Sequence node, but its version is different
        # These possibilities are denoted with their number in comments below.

        # code for checking if an identically-versioned node exists (#2)
        # this check goes straight to checking for the unique Sequence node instead of looping through all of them
        self.node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                             data_dict={origin_id_key: self.origin_id, 'Sequence__': self.name,
                                                        'Version__': current_version})
        if self.node:  # (#2)
            self.id = self.node._properties['SequenceID']

            # New: Add the ability to update labels and values
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)
            # For updating values, the default update_existing='none' because the Sequence node data is typically
            # updated by adding new nodes. If this default is used, the code below doesn't do anything.
            # However, this option is retained just in case it's needed later (e.g. for adding metadata properties).
            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

            # Not separately updating source_DatasetIDs, because the assumption is that a Sequence node should only
            #  have *one* origin (Metadata) node already listed in source_DatasetIDs.

        else:  # (#1) or (#3)
            # first see if there is an older version of this Sequence node (#3)
            putative_old_nodes = session.read_transaction(self.check_unique_names_dict, label=self.kind, return_list=True,
                                                          data_dict={origin_id_key: self.origin_id, 'Sequence__': self.name})
            if putative_old_nodes:
                for p_old_node in putative_old_nodes:
                    relationships = session.read_transaction(self.check_relationships, id_source=self.origin_node._id,
                                                             rel_type="HAS_SEQUENCE", id_end=p_old_node._id)
                    if relationships:  # (#3)
                        # the length of relationships will be 1 due to how check_relationships is defined
                        old_node = p_old_node  # this should be the same as relationships[0]['end_node']
                        old_node_version = old_node._properties['Version__']

                        if StrictVersion(old_node_version) < StrictVersion(current_version):  # expected

                            # delete old relationships
                            session.write_transaction(self.remove_relationship, id_source=self.origin_node._id,
                                                      rel_type='HAS_SEQUENCE', id_end=old_node._id)
                            session.write_transaction(self.remove_relationship, id_source=old_node._id,
                                                      rel_type='HAS_METADATA', id_end=self.origin_node._id)

                            # create new renamed relationship to old node
                            old_node_rel_type = 'HAS_SEQUENCE_VERSION_{}'.format(old_node_version)
                            session.write_transaction(self.create_relationship, id_source=self.origin_node._id,
                                                      rel_type=old_node_rel_type, id_end=old_node._id)

                        elif StrictVersion(old_node_version) > StrictVersion(current_version):  # bad
                            warning('Attempting to import a version of the sequence ({}) that is older than the '
                                    'most recent version in the database ({}) for {}, {}. '
                                    'This sequence was not imported.'.format(current_version, old_node_version,
                                                                             self.origin_node._properties['DatasetID'],
                                                                             self.name))
                        else:  # they're equal, but then the code shouldn't have gotten to this point
                            error('Found a Sequence node with the same version as the one being imported, but this'
                                  'was not caught as it should have been. Check the code and data for errors.')

            # create the new node (this will execute regardless of whether an old version was found and handled above)
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.origin_node, sourceIDKey=origin_id_key,
                                                                  sourceID=self.origin_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)
            # create the metadata relationship
            session.write_transaction(self.create_relationship, id_source=self.node._id,
                                      rel_type='HAS_METADATA', id_end=self.origin_node._id)



class Data(Vertex):

    """
    TODO (now done for existing nodes, but still need to test implementation in node classes):
     Add DatasetIDs for parent Metadata nodes directly to Data node properties, so that they're explicitly linked
     to individual Data nodes in query results without having to separately query for Metadata nodes.
     Did this using a new property name (source_DatasetIDs) to allow for storing DatasetIDs for multiple connected
     Metadata nodes.
    TODO: Related to the above (but separate, not part of this node class), also need to return full Metadata info
     (with source file links, contact info, etc.) in live queries, as a separate results file (sim. to cached queries).
    """

    def __init__(self, session, subdata, source_node, source_id_key, rel_type, data_dict, update_existing, metadata_node,
                 kind='Data', additional_labels=None, no_create=False):

        Vertex.__init__(self, name=subdata, kind=kind)

        self.subdata = subdata  # this is analogous to self.name in other classes, except this one is often NaN
        self.kind = kind
        self.source_node = source_node.node
        self.source_id = source_node.id
        self.metadata_node = metadata_node.node

        # Currently this Data class requires *one* metadata_node as an argument, so an instance of this class will have
        # only one associated DatasetID corresponding to that metadata_node (even if the Data node itself is linked to
        # several Metadata nodes)
        metadata_DatasetID = str(self.metadata_node._properties['DatasetID'])
        
        # Check to see if the node has any data relating to it.
        # The line below is checking for a Data node with arbitrary id_key and id_value (usually DepthID).
        #  In other words, all Data nodes that have that ID. If there are none, putative_nodes is set to False.
        putative_nodes = session.read_transaction(self.check_names, label=self.kind, key=source_id_key, value=self.source_id)
        
        if putative_nodes:
            for p_node in putative_nodes:  # If false, wouldn't make it to this point

                relationships = session.read_transaction(self.check_relationships, id_source=self.source_node._id,
                                                         rel_type=rel_type, id_end=p_node._id)

                if relationships:
                    for rel in relationships:
                        # account for "sub"-datasets of the same type by only selecting the one where the 
                        #   DataSubsample__ matches
                        # if it does match, proceed as usual
                        # if it does NOT match, data_node won't be assigned.
                        # Note that if it's on an error_ dataframe, then all of the multiple Data nodes already exist,
                        #   and the correct data_node should still be selected for error matching.
                        # Note also that this *inner* for loop *should* only execute once; but still need to 
                        #   keep it as a loop to avoid data type errors. Whereas the *outer* for loop will be 
                        #   executed the same # of times as there are Data nodes.
                        if 'DataSubsample__' in rel['end_node']._properties.keys():
                            subdata_prop = 'DataSubsample__'
                        elif 'Data__' in rel['end_node']._properties.keys():
                            subdata_prop = 'Data__'
                            # warning('DataSubsample__ is missing from the putative Data node; checking match for Data__ instead.')
                        else:
                            error('The putative Data node contains neither DataSubsample__ nor Data__ to check for uniqueness.')

                        if str(rel['end_node']._properties[subdata_prop]) == str(subdata):
                            
                            self.node = p_node
                            self.id = p_node._properties['DataID']
                            
                            if not no_create:
                                # if no_create, this also means don't update values or associate with the passed metadata_node

                                # Update the labels
                                if additional_labels:
                                    self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                                          labels=additional_labels)

                                # Update source_DatasetIDs to include the current Metadata node (if it's not already there)
                                # This is done separately from the main updating of values, to ensure that this is updated even
                                #  if update_existing=='missing'.
                                # This assumes that the property source_DatasetIDs already exists. If it doesn't exist,
                                #  it should throw a KeyError.
                                # NOT catching the KeyError for this Data class, as Data nodes should *always* have this property.
                                stored_DatasetIDs = str(self.node._properties['source_DatasetIDs'])

                                if not(metadata_DatasetID in stored_DatasetIDs):
                                    new_DatasetIDs = stored_DatasetIDs + ', ' + metadata_DatasetID  # this assumes there's something else already in there; i.e. it's not blank
                                    self.node = self.update_values(session=session, node=self.node,
                                                                   new_data={'source_DatasetIDs': new_DatasetIDs},
                                                                   update_existing='toValues')

                                # Update the other values as needed
                                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                                               update_existing=update_existing)

                                # Create relationship to Metadata (note: this uses MERGE, which *should* only create it if it doesn't already exist)
                                session.write_transaction(self.create_relationship, id_source=self.node._id,
                                                          rel_type='HAS_METADATA', id_end=self.metadata_node._id)
                            
        if not hasattr(self, 'id'):
            if no_create:
                warning('Unable to identify data node and given "No create" option. Not an issue if this was intended.')
                self.node = False
                self.id = False
            
            else:
               # additional custom parameters to add to data_dict
                data = {
                    'DataSubsample__': str(self.subdata),
                    'source_DatasetIDs': metadata_DatasetID
                }
                
                # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
                data = {**data, **data_dict}
                
                self.node, self.id = self.create_node_with_properties(session=session, label=self.kind,
                                                                      metadata=self.subdata, sourceNodes=self.source_node,
                                                                      sourceIDKey=source_id_key, sourceID=self.source_id,
                                                                      rel_type=rel_type, dataDict=data,
                                                                      additional_labels=additional_labels)
                
                session.write_transaction(self.create_relationship, id_source=self.node._id,
                                          rel_type='HAS_METADATA', id_end=self.metadata_node._id)
                
                printv('Created data node: {}'.format(self.id))


class Errors(Vertex):
    def __init__(self, session, name, data_node, error_dict, update_existing, metadata_node, rel_type=False,
                 kind='Errors', additional_labels=None):
        """
        Errors nodes store the uncertainty values (std errors, std devs, etc) associated with a Data node.
        
        Specifying a rel_type is highly recommended to distinguish different uncertainty types from each other.
        The recommended rel_types are as follows:
            standard errors:          HAS_STD_ERRORS
            standard deviations:      HAS_STD_DEVS
            95% confidence interval:  HAS_95CONF_INTERVALS
            mixture of above: leave rel_type blank, which will result in HAS_ERRORS, but the error types should then 
              be specified in the keys of error_dict
        """
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name
        self.kind = kind
        self.data_node = data_node.node
        self.data_id = data_node.id
        self.error_type = rel_type.replace('HAS_', '')  # set error type based on rel_type by deleting the HAS_
        self.metadata_node = metadata_node.node

        metadata_DatasetID = str(self.metadata_node._properties['DatasetID'])
        
        putative_error_nodes = session.read_transaction(self.check_names, label=self.kind, key='DataID', value=self.data_id)
        
        if putative_error_nodes:
            for p_error_node in putative_error_nodes:
                
                relationships = session.read_transaction(self.check_relationships, id_source=self.data_node._id,
                                                         rel_type=rel_type, id_end=p_error_node._id)

                if relationships:
                    count = 0
                    for rel in relationships:
                        # should only execute once; use a counter because can't use len(relationships)
                        count += 1
                        if count > 1:
                            error('Found multiple Error nodes of the same type ({}) for the same Data node: \n'
                                  '{}'.format(self.error_type, self.data_node))
                            
                        if str(rel['end_node']._properties['Errors__']) == self.error_type:
                            self.node = p_error_node
                            self.id = p_error_node._properties['ErrorsID']

                            # Update the labels
                            if additional_labels:
                                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                                      labels=additional_labels)

                            # Update source_DatasetIDs to include the current Metadata node (if it's not already there)
                            # This is done separately from the main updating of values, to ensure that this is updated even
                            #  if update_existing=='missing'.
                            # This assumes that the property source_DatasetIDs already exists. If it doesn't exist,
                            #  it should throw a KeyError.
                            # NOT catching the KeyError for this Errors class, as Errors nodes should *always* have this property.
                            stored_DatasetIDs = str(self.node._properties['source_DatasetIDs'])

                            if not (metadata_DatasetID in stored_DatasetIDs):
                                new_DatasetIDs = stored_DatasetIDs + ', ' + metadata_DatasetID  # this assumes there's something else already in there; i.e. it's not blank
                                self.node = self.update_values(session=session, node=self.node,
                                                               new_data={'source_DatasetIDs': new_DatasetIDs},
                                                               update_existing='toValues')

                            # Update the other values as needed
                            self.node = self.update_values(session=session, node=self.node, new_data=error_dict,
                                                           update_existing=update_existing)

                            # Create relationship to Metadata (note: this uses MERGE, which *should* only create it if it doesn't already exist)
                            session.write_transaction(self.create_relationship, id_source=self.node._id,
                                                      rel_type='HAS_METADATA', id_end=self.metadata_node._id)

        if not hasattr(self, 'id'):

            error_dict.update({'source_DatasetIDs': metadata_DatasetID})

            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.error_type,
                                                                  sourceNodes=self.data_node, sourceIDKey='DataID',
                                                                  sourceID=self.data_id, rel_type=rel_type,
                                                                  dataDict=error_dict, additional_labels=additional_labels)
            
            session.write_transaction(self.create_relationship, id_source=self.node._id,
                                      rel_type='HAS_METADATA', id_end=self.metadata_node._id)
        

class Device(Vertex):
    """
    Old name: AutochamberChamber, with default kind='Chamber'
    """
    def __init__(self, session, name, site_node, kind='Device', data_dict=None, additional_labels=None, update_existing='missing'):

        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.site_node = site_node.node
        self.site_id = site_node.id

        kind__ = self.kind + '__'
        kindID = self.kind + 'ID'

        p_device_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                                 data_dict={'SiteID': self.site_id, kind__: self.name})

        if p_device_node:
            self.node = p_device_node
            self.id = p_device_node._properties[kindID]

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):
            
            data = {
                self.kind: self.name
            }
            
            # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
            if data_dict is None:
                data_dict = {}
            data = {**data, **data_dict}
            
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.site_node, sourceIDKey='SiteID',
                                                                  sourceID=self.site_id, dataDict=data,
                                                                  additional_labels=additional_labels)


class FractionalTime(Vertex):
    """
    Used instead of Hour for autochamber times because chamber times are fractions of a day.
    By convention, Hour__ and Minute__ use HH:MM format, while Time__ is a fraction of a day.
    
    For now we will assume these are all also Data nodes, and thus must have a metadata_node.
    """
    
    def __init__(self, session, name, date_node, data_dict, update_existing, metadata_node, kind='Time', additional_labels=None):

        Vertex.__init__(self, name=name, kind=kind)

        self.date_node = date_node.node
        self.date_id = date_node.id
        self.name = name
        self.kind = kind
        self.metadata_node = metadata_node.node

        metadata_DatasetID = str(self.metadata_node._properties['DatasetID'])

        # For fractional times, find multiple nodes and loop through them in the old way to allow for inconsistent number formats.
        putative_time_nodes = session.read_transaction(self.check_names, label=self.kind, key='DateID', value=self.date_id)

        if putative_time_nodes:

            for p_time_node in putative_time_nodes:
                if float(self.name) == float(p_time_node._properties['Time__']):  # convert to floats in case of inconsistent trailing zeros
                    self.node = p_time_node
                    self.id = p_time_node._properties['TimeID']

                    # Update the labels
                    if additional_labels:
                        self.node = session.write_transaction(self.update_labels, id_node=self.node._id, labels=additional_labels)

                    # Update source_DatasetIDs to include the current Metadata node (if it's not already there)
                    # This is done separately from the main updating of values, to ensure that this is updated even
                    #  if update_existing=='missing'.
                    # This assumes that the property source_DatasetIDs already exists. If it doesn't exist,
                    #  it should throw a KeyError.

                    try:
                        stored_DatasetIDs = str(self.node._properties['source_DatasetIDs'])

                        if not (metadata_DatasetID in stored_DatasetIDs):
                            new_DatasetIDs = stored_DatasetIDs + ', ' + metadata_DatasetID  # this assumes there's something else already in there; i.e. it's not blank

                            self.node = self.update_values(session=session, node=self.node,
                                                           new_data={'source_DatasetIDs': new_DatasetIDs},
                                                           update_existing='toValues')

                    except KeyError:
                        # this is expected for cases where the node exists as an organizational entity but contains no data
                        # in this case, assign it the metadata_DatasetID
                        self.node = self.update_values(session=session, node=self.node,
                                                       new_data={'source_DatasetIDs': metadata_DatasetID},
                                                       update_existing='toValues')

                    # Update the other values as needed
                    self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                                   update_existing=update_existing)

                    # Create relationship to Metadata (note: this uses MERGE, which *should* only create it if it doesn't already exist)
                    session.write_transaction(self.create_relationship, id_source=self.node._id,
                                              rel_type='HAS_METADATA', id_end=self.metadata_node._id)

                    break

        if not hasattr(self, 'id'):

            data_dict.update({'source_DatasetIDs': metadata_DatasetID})

            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.date_node, sourceIDKey='DateID',
                                                                  sourceID=self.date_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)
            
            session.write_transaction(self.create_relationship, id_source=self.node._id,
                                      rel_type='HAS_METADATA', id_end=self.metadata_node._id)


class Date(Vertex):
    """
    General Date class
    Some (not all!) of these are also Data nodes, and will thus have a metadata_node.
    update_existing must be supplied, but is only used if it has a data_dict.
    """
     
    def __init__(self, session, name, source_node, source_id_key, update_existing, kind='Date', metadata_node=None,
                 data_dict=None, additional_labels=None):
        # May specify a more specific date type (e.g. 'ANS-Date') for kind, but this is discouraged as all dates are fundamentally the same
        
        Vertex.__init__(self, name=name, kind=kind)
        
        self.name = name
        self.kind = kind
        self.source_node = source_node.node
        self.source_id = source_node.id
        
        if metadata_node:
            self.metadata_node = metadata_node.node
            metadata_DatasetID = str(self.metadata_node._properties['DatasetID'])
        
        # p_date_node = session.read_transaction(self.check_unique_names, label=self.kind, key1=source_id_key,
        #                                        value1=self.source_id, key2='Date__', value2=self.name)
        p_date_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                               data_dict={source_id_key: self.source_id, 'Date__': self.name})

        if p_date_node:

            self.node = p_date_node
            self.id = p_date_node._properties['DateID']
            self.name = p_date_node._properties['Date__']

            printv('Identified Date node: {} from source node with {}: {},\n'
                   '{}: {}. Reusing.'.format(self.name, source_id_key, self.source_id, "DateID", self.id))

            if data_dict:  # empty dict resolves to False, as does None

                # First update the labels & values as necessary
                if additional_labels:
                    self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                          labels=additional_labels)
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

                if metadata_node:
                    # Now check to see if there's already a relationship to the metadata node, and if not, create it
                    # UPDATE: Unlike for the other node types, this doesn't include a complicated mechanism to check if
                    #  the relationship exists; I think this was done to test whether MERGE (in create_relationship)
                    #  already does this adequately.
                    session.write_transaction(self.create_relationship, id_source=self.node._id,
                                              rel_type='HAS_METADATA', id_end=self.metadata_node._id)

                    # Update source_DatasetIDs to include the current Metadata node (if it's not already there)
                    # This is done separately from the main updating of values, to ensure that this is updated even
                    #  if update_existing=='missing'.
                    # This assumes that the property source_DatasetIDs already exists. If it doesn't exist,
                    #  it should throw a KeyError.

                    try:
                        stored_DatasetIDs = str(self.node._properties['source_DatasetIDs'])

                        if not (metadata_DatasetID in stored_DatasetIDs):
                            new_DatasetIDs = stored_DatasetIDs + ', ' + metadata_DatasetID  # this assumes there's something else already in there; i.e. it's not blank

                            self.node = self.update_values(session=session, node=self.node,
                                                           new_data={'source_DatasetIDs': new_DatasetIDs},
                                                           update_existing='toValues')

                    except KeyError:
                        # this is expected for cases where the node exists as an organizational entity but contains no data
                        # in this case, assign it the metadata_DatasetID
                        self.node = self.update_values(session=session, node=self.node,
                                                       new_data={'source_DatasetIDs': metadata_DatasetID},
                                                       update_existing='toValues')

        if not hasattr(self, 'id'):

            if metadata_node:
                data_dict.update({'source_DatasetIDs': metadata_DatasetID})

            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.source_node, sourceIDKey=source_id_key,
                                                                  sourceID=self.source_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)

            if metadata_node:
                session.write_transaction(self.create_relationship, id_source=self.node._id,
                                          rel_type='HAS_METADATA', id_end=self.metadata_node._id)


class SiteDepth(Vertex):
    """
    Depths associated directly with a Site with no Core.
    Previously called LakeDepth but changed name to be more general.
    
    """

    def __init__(self, session, name, site_node, kind, gps='NaN', data_dict=None, additional_labels=None, update_existing='missing'):
        # usually kind='Water-Depth' but keep it unspecified for now
        Vertex.__init__(self, name=name, kind=kind)
        
        # TODO Unlike CoreDepth, SiteDepth simply matches the depth name and doesn't do any complex 
        #  number-range matching. May add this later, but would also have to handle the ' m' unit
        #  in lake depths.
        self.name = name
        self.kind = kind
        self.gps = gps
        self.site_node = site_node.node
        self.site_id = site_node.id
        #self.site_name = self.site_node._properties['Site__']  # seems dangerous to use this in the node being created, as it could overwrite a potentially non-matching value from data_dict

        p_depth_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                                data_dict={'SiteID': self.site_id, 'Depth__': self.name})

        if p_depth_node:
            printv('Identified Depth node: {} from Site: {}. Reusing.'.format(self.name, p_depth_node._properties['Site__']))
            self.node = p_depth_node
            self.id = p_depth_node._properties['DepthID']

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):
            
            data = {
                'GPS__': gps,
                'Depth': self.name
            }
            
            # merge data_dict into data if data_dict exists. if there are duplicates, the values in the 2nd argument win.
            if data_dict is None:
                data_dict = {}
            data = {**data, **data_dict}
            
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.site_node, sourceIDKey='SiteID',
                                                                  sourceID=self.site_id, dataDict=data,
                                                                  additional_labels=additional_labels)


class Hour(Vertex):
    """
    To be used for hour data where the identifying hour is formatted HH:MM (not fractions of a day).
    For now we will assume these are all also Data nodes, and thus must have a metadata_node.
    """

    # TODO: Standardize the time format so that all data types are consistent with leading zeros (i.e., 04:30 vs. 4:30).
    #  Maybe it's better WITH the leading zero-- because then they can be sorted without converting from strings.
    #  Currently the only datatype where this needs to be fixed is Autochamber.
    
    def __init__(self, session, name, date_node, update_existing, data_dict, metadata_node, kind='Hour', additional_labels=None):
        # May change kind to be a more specific type of hour, but this is discouraged.

        Vertex.__init__(self, name=name, kind=kind)

        self.kind = kind
        self.name = name
        self.date_node = date_node.node
        self.date_id = date_node.id
        # self.date = self.date_node._properties['Date__']  # seems dangerous to use this in the node being created, as it could overwrite a potentially non-matching value from data_dict
        self.metadata_node = metadata_node.node

        metadata_DatasetID = str(self.metadata_node._properties['DatasetID'])

        # OK so the changes below make it *slightly* faster, but not by much...

        # p_hour_node = session.read_transaction(self.check_unique_names, label=self.kind,
        #                                        key1='DateID', value1=self.date_id, key2='Hour__', value2=self.name)
        p_hour_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                               data_dict={'DateID': self.date_id, 'Hour__': self.name})

        if p_hour_node:
            printv('Identified Hour node: {} from Date: {}. Reusing.'.format(self.name, p_hour_node._properties['Date__']))
            self.node = p_hour_node
            self.id = p_hour_node._properties['HourID']

            # Update the labels, if specified
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            # Update source_DatasetIDs to include the current Metadata node (if it's not already there)
            # This is done separately from the main updating of values, to ensure that this is updated even
            #  if update_existing=='missing'.
            # This assumes that the property source_DatasetIDs already exists. If it doesn't exist,
            #  it should throw a KeyError.

            try:
                stored_DatasetIDs = str(self.node._properties['source_DatasetIDs'])

                if not (metadata_DatasetID in stored_DatasetIDs):
                    new_DatasetIDs = stored_DatasetIDs + ', ' + metadata_DatasetID  # this assumes there's something else already in there; i.e. it's not blank

                    self.node = self.update_values(session=session, node=self.node,
                                                   new_data={'source_DatasetIDs': new_DatasetIDs},
                                                   update_existing='toValues')

            except KeyError:
                # this is expected for cases where the node exists as an organizational entity but contains no data
                # in this case, assign it the metadata_DatasetID
                self.node = self.update_values(session=session, node=self.node,
                                               new_data={'source_DatasetIDs': metadata_DatasetID},
                                               update_existing='toValues')

            # Update the other values as needed
            self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                           update_existing=update_existing)

            # Create relationship to Metadata (note: this uses MERGE, which *should* only create it if it doesn't already exist)
            session.write_transaction(self.create_relationship, id_source=self.node._id,
                                      rel_type='HAS_METADATA', id_end=self.metadata_node._id)

        if not hasattr(self, 'id'):

            data_dict.update({'source_DatasetIDs': metadata_DatasetID})
            
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.date_node, sourceIDKey='DateID',
                                                                  sourceID=self.date_id, dataDict=data_dict,
                                                                  additional_labels=additional_labels)
            
            session.write_transaction(self.create_relationship, id_source=self.node._id,
                                      rel_type='HAS_METADATA', id_end=self.metadata_node._id)


class Collection(Vertex):
    """
    Sequence collection node, for aggregating all sequence data associated with a sample.
    """

    def __init__(self, session, name, source_node, source_id_key, rel_type, kind='Collection', data_dict=None,
                 additional_labels=None, update_existing='missing'):

        Vertex.__init__(self, name=name, kind=kind)

        self.name = name
        self.kind = kind
        self.source_node = source_node.node
        self.source_id = source_node.id

        p_collection_node = session.read_transaction(self.check_unique_names_dict, label=self.kind,
                                                     data_dict={source_id_key: self.source_id})
        #TODO: Expand Collection node properties to include sequencing type (metaG, metaT, etc.), technology, and other
        # info, and then add unique identifier(s) to data_dict above when checking for existing Collection nodes.

        if p_collection_node:
            self.node = p_collection_node
            self.id = p_collection_node._properties['CollectionID']
            printv('Identified Collection node: {}, with CollectionID: {}. Reusing.'.format(self.name, self.id))

            # New: Add the ability to update labels and values, even though this isn't a Data node type
            if additional_labels:
                self.node = session.write_transaction(self.update_labels, id_node=self.node._id,
                                                      labels=additional_labels)

            if data_dict:
                self.node = self.update_values(session=session, node=self.node, new_data=data_dict,
                                               update_existing=update_existing)

        if not hasattr(self, 'id'):
            self.node, self.id = self.create_node_with_properties(session=session, label=self.kind, metadata=self.name,
                                                                  sourceNodes=self.source_node,
                                                                  sourceIDKey=source_id_key, sourceID=self.source_id,
                                                                  rel_type=rel_type, dataDict=data_dict,
                                                                  additional_labels=additional_labels)

    def __repr__(self):
        return "Sequence collection"


# -*- coding: utf-8 -*-

